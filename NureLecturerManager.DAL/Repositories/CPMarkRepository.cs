﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Repositories
{
    class CPMarkRepository : Interfaces.IComplexKeyRepository<ControlPointMark>
    {
        DataContext db;

        public CPMarkRepository(DataContext context)
        {
            db = context;
        }

        public void Create(ControlPointMark item)
        {
            db.ControlPointMarks.Add(item);
        }

        public void Delete<K, T>(K CPId, K userId, T passDate)
        {
            ControlPointMark item = db.ControlPointMarks.Find(CPId, userId, passDate);
            db.ControlPointMarks.Remove(item);
        }

        public ControlPointMark Get<K, T>(K CPId, K userId, T passDate)
        {
            return db.ControlPointMarks.Find(CPId, userId, passDate);
        }

        public ICollection<ControlPointMark> Find(Func<ControlPointMark, bool> predicate)
        {
            return db.ControlPointMarks.Where(predicate).ToList();
        }

        public ICollection<ControlPointMark> GetAll()
        {
            return db.ControlPointMarks.ToList();
        }

        public void Update(ControlPointMark item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
