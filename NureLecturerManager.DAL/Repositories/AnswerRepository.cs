﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Repositories
{
    class AnswerRepository:ISingleKeyRepository<Answer>
    {
        private DataContext db;

        public AnswerRepository(DataContext context)
        {
            db = context;
        }

        public ICollection<Answer> GetAll()
        {
            return db.Answers.ToList();
        }

        public ICollection<Answer> Find(Func<Answer, bool> predicate)
        {
            return db.Answers.Where(predicate).ToList();
        }

        public void Create(Answer item)
        {
            db.Answers.Add(item);
        }

        public void Update(Answer item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        public Answer Get<K>(K id1)
        {
            return db.Answers.Find(id1);
        }

        public void Delete<K>(K id1)
        {
            db.Answers.Remove(db.Answers.Find(id1));
        }
    }
}
