﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Repositories
{
    class LecturerForYearRepository : Interfaces.ISingleKeyRepository<LecturerForYear>
    {
        DataContext db;

        public LecturerForYearRepository(DataContext context)
        {
            db = context;
        }

        public void Create(LecturerForYear item)
        {
            db.LecturersForYears.Add(item);
        }

        public void Delete<L>(L id)
        {
            LecturerForYear item = db.LecturersForYears.Find(id);
            db.LecturersForYears.Remove(item);
        }

        public LecturerForYear Get<L>(L id)
        {
            return db.LecturersForYears.Find(id);
        }

        public ICollection<LecturerForYear> Find(Func<LecturerForYear, bool> predicate)
        {
            return db.LecturersForYears.Where(predicate).ToList();
        }

        public ICollection<LecturerForYear> GetAll()
        {
            return db.LecturersForYears.ToList();
        }

        public void Update(LecturerForYear item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
