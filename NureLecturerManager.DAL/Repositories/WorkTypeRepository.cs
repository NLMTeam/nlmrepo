﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Repositories
{
    class WorkTypeRepository : Interfaces.ISingleKeyRepository<WorkType>
    {
        DataContext db;
        //MaterialRepository materials;
        //ReferenceRepository references;
        //GroupControlRepository controls;

        public WorkTypeRepository(DataContext context)
        {
            db = context;
            //materials = new MaterialRepository(context);
            //references = new ReferenceRepository(context);
            //controls = new GroupControlRepository(context);
        }

        public void Create(WorkType item)
        {
            db.WorkTypes.Add(item);
        }

        public void Delete<L>(L id)
        {
            WorkType item = db.WorkTypes.Find(id);
            //foreach (var material in item.Materials)
            //{
            //    materials.Delete(material.MaterialId);
            //}
            //foreach(var reference in item.References)
            //{
            //    references.Delete(reference.WorkTypeId, reference.LiteratureId, reference.PageFrom);
            //}
            //foreach (var groupControl in item.GroupControls)
            //    controls.Delete(groupControl.GroupControlId);
            db.WorkTypes.Remove(item);
        }

        public WorkType Get<L>(L id)
        {
            WorkType s = db.WorkTypes.Find(id);
            if (s != null)
            {
                int id1 = db.WorkTypes.Find(id).WorkTypeId;
                return db.WorkTypes.Where(t => t.WorkTypeId == id1).FirstOrDefault();
            }
            else
                return null;
        }

        public ICollection<WorkType> Find(Func<WorkType, bool> predicate)
        {
            return db.WorkTypes.Where(predicate).ToList();
        }

        public ICollection<WorkType> GetAll()
        {
            return db.WorkTypes.ToList();
        }

        public void Update(WorkType item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
