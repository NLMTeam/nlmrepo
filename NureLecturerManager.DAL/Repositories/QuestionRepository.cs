﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Repositories
{
    class QuestionRepository:ISingleKeyRepository<Question>
    {
        private DataContext db;

        public QuestionRepository(DataContext context)
        {
            db = context;
        }

        public ICollection<Question> GetAll()
        {
            return db.Questions.ToList();
        }

        public ICollection<Question> Find(Func<Question, bool> predicate)
        {
            return db.Questions.Where(predicate).ToList();
        }

        public void Create(Question item)
        {
            db.Questions.Add(item);
        }

        public void Update(Question item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        public Question Get<K>(K id1)
        {
            return db.Questions.Find(id1);
        }

        public void Delete<K>(K id1)
        {
            db.Questions.Remove(db.Questions.Find(id1));
        }
    }
}
