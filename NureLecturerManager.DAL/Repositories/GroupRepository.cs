﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Repositories
{
    class GroupRepository : Interfaces.ISingleKeyRepository<Group>
    {
        DataContext db;
        //GroupControlRepository controls;

        public GroupRepository(DataContext context)
        {
            db = context;
            //controls = new GroupControlRepository(context);
        }

        public void Create(Group item)
        {
            db.Groups.Add(item);
        }

        public void Delete<L>(L id)
        {
            Group item = db.Groups.Find(id);
            //foreach (var control in item.GroupControls)
            //{
            //    controls.Delete(control.GroupControlId);
            //}
            db.Groups.Remove(item);
        }

        public Group Get<L>(L id)
        {
            return db.Groups.Find(id);
        }

        public ICollection<Group> Find(Func<Group, bool> predicate)
        {
            return db.Groups.Where(predicate).ToList();
        }

        public ICollection<Group> GetAll()
        {
            return db.Groups.ToList();
        }

        public void Update(Group item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
