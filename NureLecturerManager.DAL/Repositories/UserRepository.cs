﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Entity;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;
using System.Data.Entity.Core.Objects;

namespace NLM.DAL.Repositories
{
    class UserRepository : Interfaces.ISingleKeyRepository<User>
    {
        DataContext db;

        public UserRepository(DataContext context)
        {
            db = context;
        }

        public void Create(User item)
        {
            db.Users.Add(item);
        }

        public void Delete<L>(L id)
        {
            User item = db.Users.Find(id);
            db.Users.Remove(item);
        }

        public User Get<L>(L id)
        {
            return db.Users.Find(id);
        }

        public ICollection<User> Find(Func<User, bool> predicate)
        {
            return db.Users.Where(predicate).ToList();
        }

        public ICollection<User> GetAll()
        { 
            return db.Users.ToList();
        }

        public void Update(User item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
