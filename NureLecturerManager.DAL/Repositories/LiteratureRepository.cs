﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;


namespace NLM.DAL.Repositories
{
    class LiteratureRepository : Interfaces.ISingleKeyRepository<Literature>
    {
        DataContext db;
        //ReferenceRepository references;

        public LiteratureRepository(DataContext context)
        {
            db = context;
            //references = new ReferenceRepository(context);
        }

        public void Create(Literature item)
        {
            db.Literature.Add(item);
        }

        public void Delete<L>(L id)
        {
            Literature item = db.Literature.Find(id);
            //foreach (var refer in item.References)
            //    references.Delete(refer.WorkTypeId, refer.LiteratureId, refer.PageFrom);
            db.Literature.Remove(item);
        }

        public Literature Get<L>(L id)
        {
            return db.Literature.Find(id);
        }

        public ICollection<Literature> Find(Func<Literature, bool> predicate)
        {
            return db.Literature.Where(predicate).ToList();
        }

        public ICollection<Literature> GetAll()
        {
            return db.Literature.ToList();
        }

        public void Update(Literature item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
