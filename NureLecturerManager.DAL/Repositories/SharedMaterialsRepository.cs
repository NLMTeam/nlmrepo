﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Repositories
{
    class SharedMaterialRepository:ISingleKeyRepository<SharedMaterials>
    {
        private DataContext db;

        public SharedMaterialRepository(DataContext context)
        {
            db = context;
        }

        public ICollection<SharedMaterials> GetAll()
        {
            return db.SharedMaterials.ToList();
        }

        public ICollection<SharedMaterials> Find(Func<SharedMaterials, bool> predicate)
        {
            return db.SharedMaterials.Where(predicate).ToList();
        }

        public void Create(SharedMaterials item)
        {
            db.SharedMaterials.Add(item);
        }

        public void Update(SharedMaterials item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        public SharedMaterials Get<K>(K id1)
        {
            return db.SharedMaterials.Find(id1);
        }

        public void Delete<K>(K id1)
        {
            db.SharedMaterials.Remove(db.SharedMaterials.Find(id1));
        }
    }
}
