﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Repositories
{
    class ConsolidationRepository : Interfaces.ISingleKeyRepository<Consolidation>
    {
        DataContext db;

        public ConsolidationRepository(DataContext context)
        {
            db = context;
        }

        public void Create(Consolidation item)
        {
            db.Consolidations.Add(item);
        }

        public void Delete<L>(L id)
        {
            Consolidation item = db.Consolidations.Find(id);
            db.Consolidations.Remove(item);
        }

        public Consolidation Get<L>(L id)
        {
            return db.Consolidations.Find(id);
        }

        public ICollection<Consolidation> Find(Func<Consolidation, bool> predicate)
        {
            return db.Consolidations.Where(predicate).ToList();
        }

        public ICollection<Consolidation> GetAll()
        {
            return db.Consolidations.ToList();
        }

        public void Update(Consolidation item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
