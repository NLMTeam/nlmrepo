﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Repositories
{
    class ControlPointRepository : Interfaces.ISingleKeyRepository<ControlPoint>
    {
        DataContext db;

        public ControlPointRepository(DataContext context)
        {
            db = context;
        }

        public void Create(ControlPoint item)
        {
            db.ControlPoints.Add(item);
        }

        public void Delete<L>(L id)
        {
            ControlPoint item = db.ControlPoints.Find(id);
            //foreach (var mark in item.ControlPointMarks)
            //{
            //    marks.Delete(mark.ControlPointId, mark.UserId, mark.PassDate);
            //}
            //foreach (var work in item.WorkTypes)
            //{
            //    works.Delete(work.WorkTypeId);
            //}
            db.ControlPoints.Remove(item);
        }

        public ControlPoint Get<L>(L id)
        {
            return db.ControlPoints.Find(id);
        }


        public ICollection<ControlPoint> Find(Func<ControlPoint, bool> predicate)
        {
            return db.ControlPoints.Where(predicate).ToList();
        }

        public ICollection<ControlPoint> GetAll()
        {
            return db.ControlPoints.ToList();
        }

        public void Update(ControlPoint item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
