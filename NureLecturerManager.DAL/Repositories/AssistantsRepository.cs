﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Repositories
{
    class AssistantsRepository : Interfaces.IComplexTwoKeyRepository<Assistant>
    {
        DataContext db;
        //CourseProjectRepository projects;
        //ControlPointRepository points;

        public AssistantsRepository(DataContext context)
        {
            db = context;
            //projects = new CourseProjectRepository(context);
            //points = new ControlPointRepository(context);
        }

        public void Create(Assistant item)
        {
            db.Assistants.Add(item);
        }

        public void Delete<L>(L id1, L id2)
        {
            Assistant item = db.Assistants.Find(id1, id2);

            db.Assistants.Remove(item);
        }

        public Assistant Get<L>(L id1, L id2)
        {
            return db.Assistants.Find(id1, id2);
            //if (s != null)
            //{
            //    int id1 = db.PartsOfDiscipline.Find(id).PartId;
            //    return db.PartsOfDiscipline.Where(t => t.PartId == id1).FirstOrDefault();
            //}
            //else
            //    return null;
        }

        public ICollection<Assistant> Find(Func<Assistant, bool> predicate)
        {
            return db.Assistants.Where(predicate).ToList();
        }

        public ICollection<Assistant> GetAll()
        {
            return db.Assistants.ToList();
        }

        public void Update(Assistant item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
