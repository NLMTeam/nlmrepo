﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Repositories
{
    class NewsRepository:ISingleKeyRepository<News>
    {
        private DataContext db;

        public NewsRepository(DataContext context)
        {
            db = context;
        }

        public ICollection<News> GetAll()
        {
            return db.News.ToList();
        }

        public ICollection<News> Find(Func<News, bool> predicate)
        {
            return db.News.Where(predicate).ToList();
        }

        public void Create(News item)
        {
            db.News.Add(item);
        }

        public void Update(News item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        public News Get<K>(K id1)
        {
            return db.News.Find(id1);
        }

        public void Delete<K>(K id1)
        {
            db.News.Remove(db.News.Find(id1));
        }
    }
}
