﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Repositories
{
    class CourseProjectRepository : Interfaces.ISingleKeyRepository<CourseProject>
    {
        DataContext db;
        //ConsolidationRepository consolidations;
        public CourseProjectRepository(DataContext context)
        {
            db = context;
            //consolidations = new ConsolidationRepository(context);
        }

        public void Create(CourseProject item)
        {
            db.CourseProjects.Add(item);
        }

        public void Delete<L>(L id)
        {
            CourseProject item = db.CourseProjects.Find(id);
            //foreach (var cons in item.Consolidations)
            //{
            //    consolidations.Delete(cons.ConsolidationId);
            //}
            db.CourseProjects.Remove(item);
        }

        public CourseProject Get<L>(L id)
        {
            return db.CourseProjects.Find(id);
        }

        public ICollection<CourseProject> Find(Func<CourseProject, bool> predicate)
        {
            return db.CourseProjects.Where(predicate).ToList();
        }

        public ICollection<CourseProject> GetAll()
        {
            return db.CourseProjects.ToList();
        }

        public void Update(CourseProject item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
