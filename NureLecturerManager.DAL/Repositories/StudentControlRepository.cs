﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Repositories
{
    class StudentControlRepository : Interfaces.IComplexTwoKeyRepository<StudentControl>
    {
        DataContext db;

        public StudentControlRepository(DataContext context)
        {
            db = context;
        }

        public void Create(StudentControl item)
        {
            db.StudentControls.Add(item);
        }

        public void Delete<L>(L id1, L id2)
        {
            StudentControl item = db.StudentControls.Find(id1, id2);
            db.StudentControls.Remove(item);
        }

        public StudentControl Get<L>(L id1, L id2)
        {
            return db.StudentControls.Find(id1, id2);
        }

        public ICollection<StudentControl> Find(Func<StudentControl, bool> predicate)
        {
            return db.StudentControls.Where(predicate).ToList();
        }

        public ICollection<StudentControl> GetAll()
        {
            return db.StudentControls.ToList();
        }

        public void Update(StudentControl item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
