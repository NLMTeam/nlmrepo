﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.DAL.Models.Context;

namespace NLM.DAL.Models
{
    public class DisciplineForYear {

        public DisciplineForYear()
        {
            this.Assistants = new HashSet<Assistant>();  
            this.ControlPoints = new HashSet<ControlPoint>();
            this.Lecturers = new HashSet<LecturerForYear>();
            this.Consolidations = new HashSet<Consolidation>();
            this.WorkTypes = new HashSet<WorkType>();
            this.Groups = new HashSet<DisciplineGroup>();
        }

        public int DisciplineForYearId { get; set; }
        public string Year { get; set; }
        public int Semester { get; set; }
        public int DisciplineId { get; set; }

        public virtual ICollection<ControlPoint> ControlPoints { get; set; }
        public virtual ICollection<Consolidation> Consolidations { get; set;}
        public virtual ICollection<LecturerForYear> Lecturers { get; set; }
        public virtual ICollection<Assistant> Assistants { get; set; }
        public virtual Discipline Discipline { get; set; }
        public virtual ICollection<WorkType> WorkTypes { get; set; }
        public virtual ICollection<DisciplineGroup> Groups { get; set; }
    }
}

        