﻿using System;
using System.Collections.Generic;

namespace NLM.DAL.Models
{
    public class Literature
    {
        public Literature()
        {
        }

        public int LiteratureId { get; set; }
        public string Name { get; set; }
        public int DisciplineId { get; set; }

        public virtual Discipline Discipline { get; set; }
    }
}
