﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.DAL.Models
{
    public class SharedMaterials
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string MaterialDoc { get; set; }

        public int DisciplinesId { get; set; }
        public virtual Discipline Discipline { get; set; }  
    }
}
