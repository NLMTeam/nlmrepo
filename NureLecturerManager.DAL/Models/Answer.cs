﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.DAL.Models
{
    public class Answer
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public DateTime PublicationDateTime { get; set; }

        public int QuestionId { get; set; }
        public int AuthorId { get; set; }
        public virtual Lecturer Author { get; set; }
        public virtual Question Question { get; set; }

    }
}
