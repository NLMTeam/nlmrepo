﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.DAL.Models
{
    public class Assistant
    {
        public Int32 DisciplineForYearId { get; set; }
        public Int32 LecturerId { get; set; }

        public virtual DisciplineForYear DisciplineForYear { get; set; }
        public virtual Lecturer Lecturer { get; set; }
    }
}
