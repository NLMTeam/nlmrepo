﻿using System.Data.Entity;
using NLM.DAL.App_Start;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NLM.DAL.Models.Context
{
    internal class DataContext : DbContext
    {
        static DataContext()
        {
            Database.SetInitializer<DataContext>(new MyContextInitializer());
        }

        public DataContext()
            : base("DataContext")
        {
        }

        public DataContext(string connectionString) : base(connectionString)
        {
        }

        #region DisciplineModels(8)

        public DbSet<Assistant> Assistants { get; set; }
        public DbSet<AssistantForGroupControl> AssistantForGroupControl { get; set; }
        public DbSet<ControlPoint> ControlPoints { get; set; }
        public DbSet<Discipline> Disciplines { get; set; }
        public DbSet<DisciplineForYear> DisciplinesForYear { get; set; }
        public DbSet<GroupControl> GroupControls { get; set; }
        public DbSet<LecturerForYear> LecturersForYears { get; set; }
        public DbSet<WorkType> WorkTypes { get; set; }
        public DbSet<DisciplineGroup> DisciplineGroups { get; set; }

        #endregion

        #region LiteratureModels(2)

        public DbSet<Literature> Literature { get; set; }
        public DbSet<SharedMaterials> SharedMaterials { get; set; }

        #endregion

        #region GroupModels(2)

        public DbSet<Group> Groups { get; set; }
        public DbSet<Student> Students { get; set; }

        #endregion

        #region LecturerModels(1)

        public DbSet<Lecturer> Lecturers { get; set; }

        #endregion

        #region AuthorizeModels(2)

        public DbSet<Token> Tokens { get; set; }
        public DbSet<User> Users { get; set; }

        #endregion

        #region MarksModels(2)

        public DbSet<ControlPointMark> ControlPointMarks { get; set; }
        public DbSet<StudentControl> StudentControls { get; set; }

        #endregion

        #region CourseProjectsModels(2)

        public DbSet<CourseProject> CourseProjects { get; set; }
        public DbSet<Consolidation> Consolidations { get; set; }

        #endregion

        #region FarFutureModels(4)

        public DbSet<Answer> Answers { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<NewsSubscription> NewsSubscriptions { get; set; }
        public DbSet<Question> Questions { get; set; }

        #endregion


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            #region PrimaryKeys

            modelBuilder.Entity<Assistant>().HasKey(a => new { a.DisciplineForYearId, a.LecturerId });
            modelBuilder.Entity<ControlPointMark>()
                .HasKey(l => new { l.ControlPointId, l.UserId, l.PassDate });

            modelBuilder.Entity<AssistantForGroupControl>()
                .HasKey(a => new { a.LecturerId, a.GroupControlId });

            modelBuilder.Entity<Consolidation>().HasKey(co => co.ConsolidationId);
            modelBuilder.Entity<StudentControl>().HasKey(sc => new { sc.GroupControlId, sc.UserId });
            modelBuilder.Entity<DisciplineForYear>().HasKey(m => m.DisciplineForYearId);
            modelBuilder.Entity<Question>().HasKey(q => q.Id);
            modelBuilder.Entity<Answer>().HasKey(a => a.Id);
            modelBuilder.Entity<News>().HasKey(n => n.Id);
            modelBuilder.Entity<Token>().HasKey(t => t.TokenValue);
            modelBuilder.Entity<Literature>().HasKey(li => li.LiteratureId);
            modelBuilder.Entity<DisciplineGroup>().HasKey(dg => new { dg.DisciplineForYearId, dg.GroupName });
            #endregion


            #region ForeignKeys

            modelBuilder.Entity<Student>()
               .HasMany(s => s.Questions)
               .WithOptional(q => q.Author)
               .HasForeignKey(f => f.AuthorId)
               .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Lecturer>()
            //    .HasMany(l => l.Answers)
            //    .WithRequired(a => a.Author)
            //    .HasForeignKey(h => h.AuthorId)
            //    .WillCascadeOnDelete(true);

            //modelBuilder.Entity<Question>()
            //    .HasMany(q => q.Answers)
            //    .WithRequired(a => a.Question)
            //    .HasForeignKey(f => f.QuestionId)
            //    .WillCascadeOnDelete(true);

            // Here is a trouble!!!
            //modelBuilder.Entity<Question>()
            //            .HasRequired(q => q.Author)
            //            .WithMany(s => s.Questions)
            //            .HasForeignKey(q => q.AuthorId)
            //            .WillCascadeOnDelete(true);

            modelBuilder.Entity<Answer>()
                        .HasRequired(a => a.Author)
                        .WithMany(l => l.Answers)
                        .HasForeignKey(a => a.AuthorId)
                        .WillCascadeOnDelete(true);

            modelBuilder.Entity<Answer>()
                        .HasRequired(a => a.Question)
                        .WithMany(q => q.Answers)
                        .HasForeignKey(a => a.QuestionId)
                        .WillCascadeOnDelete(true);

            modelBuilder.Entity<Lecturer>()
                .HasMany(l => l.News)
                .WithRequired(n => n.Author)
                .HasForeignKey(a => a.AuthorId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<News>()
            .HasMany(l => l.Subscriptions)
            .WithRequired(n => n.News)
            .HasForeignKey(a => a.NewsId)
            .WillCascadeOnDelete(true);

            modelBuilder.Entity<Group>()
                .HasMany(l => l.News)
                .WithRequired(n => n.Group)
                .HasForeignKey(a => a.GroupId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Discipline>()
                .HasMany(d => d.SharedMaterials)
                .WithRequired(s => s.Discipline)
                .HasForeignKey(a => a.DisciplinesId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<User>()
                        .HasMany(u => u.Tokens)
                        .WithRequired(t => t.User)
                        .HasForeignKey(t => t.UserId)
                        .WillCascadeOnDelete(true);

            modelBuilder.Entity<ControlPoint>().HasKey(cp => cp.ControlPointId)
               .HasMany(cp => cp.ControlPointMarks)
               .WithRequired(cpm => cpm.ControlPoint)
               .HasForeignKey(cpm => cpm.ControlPointId)
               .WillCascadeOnDelete(true);

            modelBuilder.Entity<GroupControl>().HasKey(gc => gc.GroupControlId)
                .HasMany(gc => gc.StudentControls)
                .WithRequired(sc => sc.GroupControl)
                .HasForeignKey(sc => sc.GroupControlId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Group>().HasMany(g => g.GroupControls)
                .WithRequired(gc => gc.Group)
                .HasForeignKey(gc => gc.GroupNumber)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<DisciplineForYear>().HasMany(dfy => dfy.WorkTypes)
                .WithRequired(w => w.DisciplineForYear)
                .HasForeignKey(w => w.DisciplineForYearId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<WorkType>().HasKey(w => w.WorkTypeId)
                .HasMany(w => w.GroupControls)
                .WithRequired(gc => gc.WorkType)
                .HasForeignKey(gc => gc.WorkTypeId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Discipline>().HasMany(d => d.Literature)
                .WithRequired(li => li.Discipline)
                .HasForeignKey(li => li.DisciplineId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Discipline>().HasMany(d => d.DisciplinesForYears)
                                             .WithRequired(dy => dy.Discipline)
                                             .HasForeignKey(dy => dy.DisciplineId)
                                             .WillCascadeOnDelete(true);

            modelBuilder.Entity<DisciplineForYear>().HasMany(dy => dy.ControlPoints)
                                                    .WithRequired(cp => cp.DisciplineForYear)
                                                    .HasForeignKey(cp => cp.DisciplineForYearId)
                                                    .WillCascadeOnDelete(true);

            modelBuilder.Entity<GroupControl>().HasMany(gc => gc.AssistantsForGroupControls)
                                                    .WithRequired(assis => assis.GroupControl)
                                                    .HasForeignKey(assis => assis.GroupControlId)
                                                    .WillCascadeOnDelete(true);

            modelBuilder.Entity<Lecturer>().HasMany(lect => lect.AssistantsForGroupControl)
                                        .WithRequired(assis => assis.Lecturer)
                                        .HasForeignKey(assis => assis.LecturerId)
                                        .WillCascadeOnDelete(true);

            modelBuilder.Entity<Discipline>().HasMany(d => d.CourseProjects)
                                             .WithOptional(cp => cp.Discipline)
                                             .HasForeignKey(cp => cp.DisciplineId)
                                             .WillCascadeOnDelete(true);

            modelBuilder.Entity<DisciplineForYear>().HasMany(dy => dy.Consolidations)
                                                    .WithRequired(cons => cons.DisciplineForYear)
                                                    .HasForeignKey(cons => cons.DisciplineForYearId)
                                                    .WillCascadeOnDelete(true);

            modelBuilder.Entity<Lecturer>().HasMany(l => l.Disciplines)
                                           .WithRequired(dy => dy.Lecturer)
                                           .HasForeignKey(dy => dy.LecturerId)
                                           .WillCascadeOnDelete(true);

            modelBuilder.Entity<DisciplineForYear>().HasMany(l => l.Lecturers)
                               .WithRequired(dy => dy.DisciplineForYear)
                               .HasForeignKey(dy => dy.DisciplineForYearId)
                               .WillCascadeOnDelete(true);

            modelBuilder.Entity<Student>().HasKey(u => u.UserId)
               .HasRequired(u => u.User)
               .WithOptional(u => u.Student)
               .WillCascadeOnDelete(true);

            modelBuilder.Entity<Lecturer>().HasKey(u => u.UserId)
                .HasRequired(u => u.User)
                .WithOptional(u => u.Lecturer)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Student>().HasKey(s => s.UserId)
                .HasMany(s => s.Consolidations)
                .WithRequired(co => co.Student)
                .HasForeignKey(co => co.UserId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<CourseProject>().HasKey(c => c.CourseProjectId)
                .HasMany(co => co.Consolidations)
                .WithRequired(co => co.CourseProject)
                .HasForeignKey(co => co.CourseProjectId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>().HasKey(u => u.UserId)
                .HasMany(u => u.CourseProjects)
                .WithOptional(cp => cp.User)
                .HasForeignKey(cp => cp.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Student>().HasMany(s => s.ControlPointMarks)
                .WithRequired(cpm => cpm.Student)
                .HasForeignKey(cpm => cpm.UserId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Student>().HasMany(s => s.StudentControls)
                .WithRequired(sc => sc.Student)
                .HasForeignKey(sc => sc.UserId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Group>().HasKey(g => g.GroupId)
                .HasMany(g => g.Students)
                .WithOptional(s => s.Group)
                .HasForeignKey(s => s.GroupNumber)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lecturer>().HasMany(l => l.Assistants)
                .WithRequired(p => p.Lecturer)
                .HasForeignKey(p => p.LecturerId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<DisciplineForYear>().HasMany(l => l.Assistants)
                .WithRequired(dfy => dfy.DisciplineForYear)
                .HasForeignKey(p => p.DisciplineForYearId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<DisciplineForYear>().HasMany(dfy => dfy.Groups)
                                                    .WithRequired(dg => dg.DisciplineForYear)
                                                    .HasForeignKey(dg => dg.DisciplineForYearId)
                                                    .WillCascadeOnDelete(true);

            modelBuilder.Entity<Group>().HasMany(g => g.Disciplines)
                                        .WithRequired(dg => dg.Group)
                                        .HasForeignKey(dg => dg.GroupName)
                                        .WillCascadeOnDelete(true);


            #endregion


            #region Triggers

            modelBuilder.Entity<CourseProject>()
                .MapToStoredProcedures(b => b.Delete(sp => sp.HasName("DeleteCourseProject")
                .Parameter(cp => cp.CourseProjectId, "id")));

            #endregion

            base.OnModelCreating(modelBuilder);
        }
    }
}
