﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.DAL.Models
{
    public class DisciplineGroup
    {
        public String GroupName { get; set; }
        public Int32 DisciplineForYearId { get; set; }

        public virtual DisciplineForYear DisciplineForYear { get; set; }
        public virtual Group Group { get; set; }
    }
}
