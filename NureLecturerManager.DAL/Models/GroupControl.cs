﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace NLM.DAL.Models
{
    public class GroupControl
    {
        public GroupControl()
        {
            StudentControls = new HashSet<StudentControl>();
        }

        public int WorkTypeId { get; set; }
        public string GroupNumber { get; set; }
        public System.DateTime? PassDate { get; set; }
        public string Type { get; set; }
        public string PassPlace { get; set; }
        public int GroupControlId { get; set; }

        public virtual ICollection<StudentControl> StudentControls { get; set; }
        public virtual Group Group { get; set; }
        public virtual WorkType WorkType { get; set; }
        public virtual ICollection<AssistantForGroupControl> AssistantsForGroupControls { get; set; }
    }
}
