﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.DAL.Models.Enums
{
    public enum UserType: byte
    {
        STUDENT,
        LECTURER,
        ADMIN
    }
}
