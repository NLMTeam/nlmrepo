﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.DAL.Models
{
    public class Consolidation
    {
        public int ConsolidationId { get; set; }
        public System.DateTime ConsolidationDate { get; set; }
        public System.DateTime? PassDate { get; set; }
        public int? Mark { get; set; }
        public int UserId { get; set; }
        public int CourseProjectId { get; set; }
        public int DisciplineForYearId { get; set; }

        public virtual CourseProject CourseProject { get; set; }
        public virtual DisciplineForYear DisciplineForYear { get; set; }
        public virtual Student Student { get; set; }
    }
}

