﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NLM.DAL.Models
{
    public partial class Group
    {
        public Group()
        {
            this.Students = new HashSet<Student>();
            this.GroupControls = new HashSet<GroupControl>();
            this.News = new HashSet<NewsSubscription>();
            this.Disciplines = new HashSet<DisciplineGroup>();
        }

        public string GroupId { get; set; }
        public int? CaptainId { get; set; }
        public virtual ICollection<Student> Students { get; set; }
        public virtual ICollection<GroupControl> GroupControls { get; set; }
        public virtual ICollection<NewsSubscription> News { get; set; }
        public virtual ICollection<DisciplineGroup> Disciplines { get; set; }
    }
}
