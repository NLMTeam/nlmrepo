﻿using System;
using NLM.DAL.Interfaces;
using NLM.DAL.Repositories;
using NLM.DAL.Models;
using NLM.DAL.Models.Context;

namespace NLM.DAL.EF
{
    public class EFUnitOfWork : Interfaces.IUnitOfWork
    {
        DataContext db;

        UserRepository userR;
        ConsolidationRepository consolidationR;
        ControlPointRepository cpR;
        CourseProjectRepository courseProjectR;
        CPMarkRepository cpMarkR;
        DisciplineRepository disciplineR;
        GroupControlRepository groupControlR;
        GroupRepository groupR;
        LecturerRepository lecturerR;
        LiteratureRepository literatureR;
        AssistantsRepository assistR;
        AssistantForGCRepository assistForGCR;
        StudentControlRepository studentControlR;
        StudentRepository studentR;
        WorkTypeRepository workTypeR;
        QuestionRepository questionR;
        AnswerRepository answerR;
        NewsRepository newsR;
        SharedMaterialRepository sharedMaterialsR;
        DisciplineForYearRepository disciplinesForYear;
        LecturerForYearRepository lecturersY;
        NewsSubscriptionRepository newsSub;
        TokenRepository tokensR;

        public EFUnitOfWork(string connectionString)
        {
            db = new DataContext(connectionString);
        }

        public IComplexKeyRepository<ControlPointMark> ControlPointMarks
        {
            get
            {
                if (cpMarkR == null)
                    cpMarkR = new CPMarkRepository(db);
                return cpMarkR;
            }
        }

        public ISingleKeyRepository<ControlPoint> ControlPoints
        {
            get
            {
                if (cpR == null)
                    cpR = new ControlPointRepository(db);
                return cpR;
            }
        }

        public ISingleKeyRepository<Consolidation> Consolidations
        {
            get
            {
                if (consolidationR == null)
                    consolidationR = new ConsolidationRepository(db);
                return consolidationR;
            }
        }

        public ISingleKeyRepository<CourseProject> CourseProjects
        {
            get
            {
                if (courseProjectR == null)
                    courseProjectR = new CourseProjectRepository(db);
                return courseProjectR;
            }
        }

        public ISingleKeyRepository<Discipline> Disciplines
        {
            get
            {
                if (disciplineR == null)
                    disciplineR = new DisciplineRepository(db);
                return disciplineR;
            }
        }

        public ISingleKeyRepository<GroupControl> GroupControls
        {
            get
            {
                if (groupControlR == null)
                    groupControlR = new GroupControlRepository(db);
                return groupControlR;
            }
        }

        public ISingleKeyRepository<Group> Groups
        {
            get
            {
                if (groupR == null)
                    groupR = new GroupRepository(db);
                return groupR;
            }
        }

        public ISingleKeyRepository<Lecturer> Lecturers
        {
            get
            {
                if (lecturerR == null)
                    lecturerR = new LecturerRepository(db);
                return lecturerR;
            }
        }

        public ISingleKeyRepository<Literature> Literature
        {
            get
            {
                if (literatureR == null)
                    literatureR = new LiteratureRepository(db);
                return literatureR;
            }
        }

        public IComplexTwoKeyRepository<Assistant> Assistants
        {
            get
            {
                if (assistR == null)
                    assistR = new AssistantsRepository(db);
                return assistR;
            }
        }

        public IComplexTwoKeyRepository<AssistantForGroupControl> AssistantsForGC
        {
            get
            {
                if (assistForGCR == null)
                    assistForGCR = new AssistantForGCRepository(db);
                return assistForGCR;
            }
        }

        public IComplexTwoKeyRepository<StudentControl> StudentControls
        {
            get
            {
                if (studentControlR == null)
                    studentControlR = new StudentControlRepository(db);
                return studentControlR;
            }
        }

        public ISingleKeyRepository<Student> Students
        {
            get
            {
                if (studentR == null)
                    studentR = new StudentRepository(db);
                return studentR;
            }
        }

        public ISingleKeyRepository<User> Users
        {
            get
            {
                if (userR == null)
                    userR = new UserRepository(db);
                return userR;
            }
        }

        public ISingleKeyRepository<WorkType> WorkTypes
        {
            get
            {
                if (workTypeR == null)
                    workTypeR = new WorkTypeRepository(db);
                return workTypeR;
            }
        }

        public ISingleKeyRepository<Question> Questions
        {
            get
            {
                if (questionR == null)
                    questionR = new QuestionRepository(db);
                return questionR;
            }
        }

        public ISingleKeyRepository<Answer> Answers
        {
            get
            {
                if (answerR == null)
                    answerR = new AnswerRepository(db);
                return answerR;
            }
        }

        public ISingleKeyRepository<News> News
        {
            get
            {
                if (newsR == null)
                    newsR = new NewsRepository(db);
                return newsR;
            }
        }

        public ISingleKeyRepository<SharedMaterials> SharedMaterials
        {
            get
            {
                if(sharedMaterialsR==null)
                    sharedMaterialsR = new SharedMaterialRepository(db);
                return sharedMaterialsR;
            }
        }

        public ISingleKeyRepository<DisciplineForYear> DisciplinesForYear
        {
            get
            {
                if (disciplinesForYear == null)
                    disciplinesForYear = new DisciplineForYearRepository(db);
                return disciplinesForYear;
            }
        }

        public ISingleKeyRepository<LecturerForYear> LecturerForYears
        {
            get
            {
                if (lecturersY == null)
                    lecturersY = new LecturerForYearRepository(db);
                return lecturersY;
            }
        }

        public ISingleKeyRepository<NewsSubscription> NewsSubscriptions
        {
            get
            {
                if (newsSub == null)
                    newsSub = new NewsSubscriptionRepository(db);
                return newsSub;
            }
        }

        public ISingleKeyRepository<Token> Tokens
        {
            get
            {
                if (tokensR == null)
                    tokensR = new TokenRepository(db);
                return tokensR;
            }
        }

        bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            db.SaveChanges();
        }
    }
}
