﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using NLM.DAL.Models.Context;
using System.Data;

namespace NLM.DAL.App_Start
{
    class MyContextInitializer : CreateDatabaseIfNotExists<DataContext>
    {
        protected override void Seed(DataContext db)
        {
            db.Database
                .ExecuteSqlCommand("ALTER TABLE dbo.Students DROP CONSTRAINT [FK_dbo.Students_dbo.Groups_GroupNumber]");

            db.Database
                .ExecuteSqlCommand("ALTER TABLE dbo.Students ADD CONSTRAINT [FK_dbo.Students_dbo.Groups_GroupNumber] FOREIGN KEY ([GroupNumber]) REFERENCES [dbo].[Groups]([GroupId]) ON UPDATE NO ACTION ON DELETE SET NULL");
            //CONSTRAINT[FK_dbo.PartOfDisciplines_dbo.Groups_GroupId] FOREIGN KEY ([GroupId]) REFERENCES[dbo].[Groups]([GroupId]),


            //db.Database
            //    .ExecuteSqlCommand("ALTER TABLE dbo.PartOfDisciplines DROP CONSTRAINT [FK_dbo.PartOfDisciplines_dbo.Lecturers_LecturerId]");

            //db.Database
            //    .ExecuteSqlCommand("ALTER TABLE dbo.PartOfDisciplines ADD CONSTRAINT [FK_dbo.PartOfDisciplines_dbo.Lecturers_LecturerId] FOREIGN KEY ([LecturerId]) REFERENCES [dbo].[Lecturers]([UserId]) ON UPDATE NO ACTION ON DELETE SET NULL");
            //CONSTRAINT[FK_dbo.PartOfDisciplines_dbo.Lecturers_LecturerId] FOREIGN KEY ([LecturerId]) REFERENCES[dbo].[Lecturers]([UserId])



            db.Database
                .ExecuteSqlCommand("ALTER TABLE dbo.CourseProjects DROP CONSTRAINT [FK_dbo.CourseProjects_dbo.Users_UserId]");

            db.Database
                .ExecuteSqlCommand("ALTER TABLE dbo.CourseProjects ADD CONSTRAINT [FK_dbo.CourseProjects_dbo.Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users]([UserId]) ON UPDATE NO ACTION ON DELETE SET NULL");
            //CONSTRAINT[FK_dbo.CourseProjects_dbo.Users_UserId] FOREIGN KEY ([UserId]) REFERENCES[dbo].[Users]([UserId])


            //db.Database
            //    .ExecuteSqlCommand("ALTER TABLE dbo.Literatures DROP CONSTRAINT [FK_dbo.Literatures_dbo.Disciplines_DisciplineId]");

            //db.Database
            //    .ExecuteSqlCommand("ALTER TABLE dbo.Literatures ADD CONSTRAINT [FK_dbo.Literatures_dbo.Disciplines_DisciplineId] FOREIGN KEY ([DisciplineId]) REFERENCES [dbo].[Disciplines]([DisciplineId]) ON UPDATE NO ACTION ON DELETE SET NULL");
            //CONSTRAINT[FK_dbo.Literatures_dbo.Disciplines_DisciplineId] FOREIGN KEY ([DisciplineId]) REFERENCES[dbo].[Disciplines]([DisciplineId])


            //db.Database
            //  .ExecuteSqlCommand("ALTER TABLE dbo.PartOfDisciplines DROP CONSTRAINT [FK_dbo.PartOfDisciplines_dbo.Groups_GroupNumber]");

            //db.Database
            //    .ExecuteSqlCommand("ALTER TABLE dbo.PartOfDisciplines ADD CONSTRAINT [FK_dbo.PartOfDisciplines_dbo.Groups_GroupNumber] FOREIGN KEY ([GroupNumber]) REFERENCES [dbo].[Groups] ([GroupId]) ON UPDATE NO ACTION ON DELETE SET NULL");
            //CONSTRAINT[FK_dbo.PartOfDisciplines_dbo.Groups_GroupId] FOREIGN KEY ([GroupId]) REFERENCES[dbo].[Groups]([GroupId]),

            db.Database
                .ExecuteSqlCommand("ALTER TABLE dbo.Questions DROP CONSTRAINT [FK_dbo.Questions_dbo.Students_AuthorId]");

            db.Database
                .ExecuteSqlCommand("ALTER TABLE dbo.Questions ADD CONSTRAINT [FK_dbo.Questions_dbo.Students_AuthorId] FOREIGN KEY ([AuthorId]) REFERENCES [dbo].[Students]([UserId]) ON UPDATE NO ACTION ON DELETE SET NULL");


            // Timeless

            //db.Database
            //    .ExecuteSqlCommand("ALTER TABLE dbo.GroupControls DROP CONSTRAINT [FK_dbo.GroupControls_dbo.PartOfDisciplines_PartId]");

            //db.Database
            //    .ExecuteSqlCommand("ALTER TABLE dbo.GroupControls ADD CONSTRAINT [FK_dbo.GroupControls_dbo.PartOfDisciplines_PartId] FOREIGN KEY ([PartId]) REFERENCES [dbo].[PartOfDisciplines]([PartId]) ON UPDATE NO ACTION ON DELETE SET NULL");

            //db.Database
            //  .ExecuteSqlCommand("ALTER TABLE dbo.CourseProjects DROP CONSTRAINT [FK_dbo.CourseProjects_dbo.Disciplines_DisciplineId]");

            //db.Database
            //    .ExecuteSqlCommand("ALTER TABLE dbo.CourseProjects ADD CONSTRAINT [FK_dbo.CourseProjects_dbo.Disciplines_DisciplineId] FOREIGN KEY ([DisciplineId]) REFERENCES [dbo].[Disciplines]([DisciplineId]) ON UPDATE NO ACTION ON DELETE SET NULL");



            db.Database.ExecuteSqlCommand(String.Join(" ", "ALTER PROCEDURE DeleteCourseProject",
                                                            "@id int",
                                                            "AS",
                                                            "DELETE FROM Consolidations WHERE CourseProjectId = @id;",
                                                            "DELETE FROM CourseProjects WHERE CourseProjectId = @id;"));


            //db.Database.ExecuteSqlCommand(String.Join(" ", "ALTER PROCEDURE DeletePartOfDiscipline",
            //                                                "@id int",
            //                                                "AS",
            //                                                "DELETE FROM GroupControls WHERE PartId = @id;",
            //                                                "DELETE FROM PartOfDisciplines WHERE PartId = @id;"));

            //db.Database.ExecuteSqlCommand(String.Join(" ", "CREATE TRIGGER PartGroupControlsTriggerDel",
            //                                                 "ON PartOfDisciplines INSTEAD OF DELETE",
            //                                                 "AS",
            //                                                 "BEGIN",
            //                                                 "DELETE FROM GroupControls",
            //\                                                "WHERE PartId IN (SELECT PartId FROM deleted);",
            //                                                 "DELETE FROM PartOfDisciplines",
            //                                                 "INNER JOIN PartOfDisciplines ON deleted.PartId = PartOfDisciplines.PartId;;",
            //                                                 "END"));


            // old
            //db.Database
            //   .ExecuteSqlCommand("ALTER TABLE dbo.DisciplineForYears DROP CONSTRAINT [FK_dbo.DisciplineForYears_dbo.Lecturers_LecturerId]");

            //db.Database
            //    .ExecuteSqlCommand("ALTER TABLE dbo.DisciplineForYears ADD CONSTRAINT [FK_dbo.DisciplineForYears_dbo.Lecturers_LecturerId] FOREIGN KEY ([LecturerId]) REFERENCES [dbo].[Lecturers]([UserId]) ON UPDATE NO ACTION ON DELETE SET NULL");
        }
    }
}
