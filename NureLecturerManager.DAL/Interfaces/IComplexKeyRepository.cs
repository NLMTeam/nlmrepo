﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.DAL.Interfaces
{
    public interface IComplexKeyRepository<L> : IRepository<L> where L : class
    {
        L Get<K, T>(K id1, K id2, T id3);
        void Delete<K, T>(K id1, K id2, T id3);
    }
}
