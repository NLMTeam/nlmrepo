﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using NLM.DAL.Repositories;
using NLM.DAL.Models;

namespace NLM.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ISingleKeyRepository<User> Users { get; }
        ISingleKeyRepository<Student> Students { get; }
        ISingleKeyRepository<Lecturer> Lecturers { get; }

        ISingleKeyRepository<CourseProject> CourseProjects { get; }

        ISingleKeyRepository<Consolidation> Consolidations { get; }

        IComplexKeyRepository<ControlPointMark> ControlPointMarks { get; }

        IComplexTwoKeyRepository<StudentControl> StudentControls { get; }

        IComplexTwoKeyRepository<Assistant> Assistants { get; }
        ISingleKeyRepository<Discipline> Disciplines { get; }

        ISingleKeyRepository<ControlPoint> ControlPoints { get; }

        ISingleKeyRepository<Group> Groups { get; }

        ISingleKeyRepository<GroupControl> GroupControls { get; }

        ISingleKeyRepository<Literature> Literature { get; }

        ISingleKeyRepository<WorkType> WorkTypes { get; }

        ISingleKeyRepository<Question> Questions { get; } 

        ISingleKeyRepository<Answer> Answers { get; } 

        ISingleKeyRepository<News> News { get; } 

        ISingleKeyRepository<SharedMaterials> SharedMaterials { get; } 
        
        ISingleKeyRepository<DisciplineForYear> DisciplinesForYear { get; }
        IComplexTwoKeyRepository<AssistantForGroupControl> AssistantsForGC { get; }
        ISingleKeyRepository<LecturerForYear> LecturerForYears { get; }
        ISingleKeyRepository<NewsSubscription> NewsSubscriptions { get; }
        ISingleKeyRepository<Token> Tokens { get; }
        void Save();
    }
}
