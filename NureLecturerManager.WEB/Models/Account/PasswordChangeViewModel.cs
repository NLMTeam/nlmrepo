﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NLM.WEB.Models.Account
{
    public class PasswordChangeViewModel
    {
        [Required(ErrorMessage = "Введите текущий e-mail")]
        [EmailAddress(ErrorMessage = "Введите корректный e-mail")]
        [MaxLength(20, ErrorMessage = "Введите корректный e-mail")]
        public string Mail { get; set; }

        [Required(ErrorMessage = "Введите текущий пароль")]
        [MaxLength(20, ErrorMessage = "Введите текущий пароль")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Введите новый пароль")]
        [MinLength(8, ErrorMessage = "Длина пароля должна быть > 8 символов")]
        [MaxLength(20, ErrorMessage = "Длина пароля должна быть < 20 символов")]
        public string NewPassword1 { get; set; }

        [Required(ErrorMessage = "Повторите новый пароль")]
        [MinLength(8, ErrorMessage = "Длина пароля должна быть > 8 символов")]
        [MaxLength(20, ErrorMessage = "Длина пароля должна быть < 20 символов")]
        public string NewPassword2 { get; set; }
    }
}