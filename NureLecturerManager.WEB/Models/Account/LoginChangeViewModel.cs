﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NLM.WEB.Models.Account
{
    public class LoginChangeViewModel
    {
        [Required(ErrorMessage = "Введите новый логин")]
        [MaxLength(20, ErrorMessage = "Длина логина не должна быть > 20 символов")]
        public string Login { get; set; }
        [Required(ErrorMessage = "Введите текущий e-mail")]
        [EmailAddress(ErrorMessage = "Введите корректный текущий e-mail")]
        [MaxLength(20, ErrorMessage = "Длина e-mail не должна превышать 20 символов")]
        public string Mail { get; set; }
    }
}