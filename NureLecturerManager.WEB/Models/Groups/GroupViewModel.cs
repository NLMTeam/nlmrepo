﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.WEB.Models.Groups
{
    public class GroupViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Group id is required.")]
        public string GroupId { get; set; }

        public Nullable<int> CaptainId { get; set; }

        public string CaptainName { get; set; }
    }
}
