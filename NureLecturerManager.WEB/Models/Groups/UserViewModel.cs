﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.WEB.Models.Groups
{
    public class UserViewModel
    {
        public int UserId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "FIO is required" )]
        public string FIO { get; set; }

        //[Required(AllowEmptyStrings = false, ErrorMessage = "User type is required.")]
        //public string Type { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Login is required.")]
        public string Login { get; set; }

        //[Required(AllowEmptyStrings = false, ErrorMessage = "Password is required.")]
        //public string Parol { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Mail is required.")]
        public string Mail { get; set; }

    }
}
