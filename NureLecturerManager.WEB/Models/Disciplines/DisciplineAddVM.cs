﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.WEB.Models.Disciplines
{
    public class DisciplineAddVM
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name of discipline is required.")]
        public String Name { get; set; }
    }
}
