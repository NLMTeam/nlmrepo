﻿using NLM.BLL.DTO;
using NLM.BLL.DTO.DisciplinePart;
using NLM.BLL.DTObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.WEB.Models.Disciplines
{
    public class DFYVM
    {
        public Int32 DisciplineId { get; set; }
        public String DisciplineName { get; set; }
        public Int32 DisciplineForYearId { get; set; }
        public List<LecturerForDisciplineDTO> Lecturers { get; set; }
        public List<LecturerForDisciplineDTO> Assistants { get; set; }
        public bool CanActivate { get; set; }
    }
}
