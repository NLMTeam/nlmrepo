﻿using NLM.BLL.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.WEB.Models.Disciplines
{
    public class DFYAddVM
    {
        [Required]
        public Int32 DisciplineId { get; set; }
        public String Year { get; set; }
        public Int32 Semester { get; set; }
    }
}
