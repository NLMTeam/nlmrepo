﻿using NLM.BLL.DTObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLM.WEB.Models.Lecturer
{
    public class DisciplineForYearsStudentViewModel
    {
        public int DFYId { get; set; }
        public string Year { get; set; }
        public int Semester { get; set; }
        public ICollection<LecturerDiscDTO> LecturerNames { get; set; }
        public ICollection<LecturerDiscDTO> AssistantsNames { get; set; }
    }
}