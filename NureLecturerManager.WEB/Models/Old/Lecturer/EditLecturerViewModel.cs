﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLM.BLL.DTO;

namespace NLM.WEB.Models.Lecturer
{
    public class EditLecturerViewModel
    {
        public UserDTO User { get; set; }
        public LecturerDTO Lecturer { get; set; }
    }
}