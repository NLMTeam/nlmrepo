﻿using NLM.BLL.DTO;
using NLM.BLL.DTObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLM.WEB.Models.Lecturer
{
    public class ProlongedDFYViewModel
    {
        public ProlongedDFYViewModel()
        {
            ControlPoints = new List<ControlPointDTO>();
            WorkTypes = new Dictionary<string, ICollection<WorkTypeDTO>>();
        }

        public DisciplineDFYLecturerViewModel DFY { get; set; }

        // Key is disciplineForYearId
        public List<ControlPointDTO> ControlPoints { get; set; }

        // Key is controlPointId
        public Dictionary<string, ICollection<WorkTypeDTO>> WorkTypes { get; set; }
    }
}