﻿using System.Collections.Generic;
using NLM.BLL.DTO;

namespace NLM.WEB.Models.Lecturer
{
    public class WorktTypeMaterialInfoViewModel
    {
        public WorktTypeMaterialInfoViewModel()
        {

        }

        public ICollection<LiteratureDTO> Literature { get; set; }
        public ICollection<WorkTypeMaterialViewModel> WorkTypes { get; set; }
    }
}