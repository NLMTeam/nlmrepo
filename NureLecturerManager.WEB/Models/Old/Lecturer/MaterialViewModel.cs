﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLM.WEB.Models.Lecturer
{
    public class MaterialViewModel
    {
        public int? WorkTypeId { get; set; }
        public string Type { get; set; }
        public HttpPostedFileBase MaterialFile { get; set; }
    }
}