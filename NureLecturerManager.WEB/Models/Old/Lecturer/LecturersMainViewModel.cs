﻿using System.Collections.Generic;
using NLM.BLL.DTO;

namespace NLM.WEB.Models.Lecturer
{
    public class LecturersMainViewModel
    {
        public UserDTO User { get; set; }

        public string Department { get; set; }
        public string Post { get; set; }
        public string Specialization { get; set; }
        public IEnumerable<string> Disciplines { get; set; }
    }
}