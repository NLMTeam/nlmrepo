﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLM.BLL.DTO;

namespace NLM.WEB.Models.Lecturer
{
    public class WorkTypeMaterialViewModel
    {
        public string WorkTypeName { get; set; }
        public int WorkTypeId { get; set; }
        
        public List<MaterialViewModel> Materials { get; set; } 
    }
}