﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLM.WEB.Models.Lecturer
{
    public class ControlPointViewModel
    {
        public int ControlPointId { get; set; }
        public int DFYId { get; set; }
        public int Number { get; set; }
        public string Name { get; set; }
        public string Date { get; set; }
        public Nullable<int> MaxMark { get; set; }
        public Nullable<int> MinMark { get; set; }
    }
}