﻿using System;
using System.Collections.Generic;
using NLM.BLL.DTO;
using System.Linq;
using System.Web;

namespace NLM.WEB.Models.Lecturer
{
    public class GroupMainViewModel
    {
        public ICollection<string> Years { get; set; }
        public ICollection<GroupDTO> Groups { get; set; }

        public ICollection<StudentViewModel> GroupStudents { get; set; }

        public ICollection<GroupControlViewModel> GroupControls { get; set; }
    }

    public class GroupControlViewModel
    {
        public int GroupControlId { get; set; }
        public string WorkType { get; set; }
        public string Type { get; set; }
        public DateTime? PassDate { get; set; }
        public string FormatDate { get; set; }
        public string Place { get; set; }
        public int PartId { get; set; }
        public bool AllowEdit { get; set; }
    }
}