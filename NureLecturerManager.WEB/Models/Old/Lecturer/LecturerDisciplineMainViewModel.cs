﻿using NLM.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLM.WEB.Models.Lecturer
{
    public class LecturerDisciplineMainViewModel
    {
        public LecturerDisciplineMainViewModel()
        {
            Disciplines = new List<DisciplineDFYLecturerViewModel>();
            ControlPoints = new Dictionary<string, ICollection<ControlPointDTO>>();
            WorkTypes = new Dictionary<string, ICollection<WorkTypeDTO>>();
        }

        public ICollection<DisciplineDFYLecturerViewModel> Disciplines { get; set; }

        // Key is disciplineForYearId
        public Dictionary<string, ICollection<ControlPointDTO>> ControlPoints { get; set; }

        // Key is controlPointId
        public Dictionary<string, ICollection<WorkTypeDTO>> WorkTypes { get; set; }
    }
}