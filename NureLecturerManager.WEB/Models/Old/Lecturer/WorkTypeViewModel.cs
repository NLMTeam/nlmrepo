﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLM.BLL.DTO;

namespace NLM.WEB.Models.Lecturer
{
    public class WorkTypeViewModel
    {
        public int WorkTypeId { get; set; }
        public string Name { get; set; }
        public int ControlPointId { get; set; }
        public Nullable<int> MaxMark { get; set; }
        public Nullable<int> MinMark { get; set; }
    }
}