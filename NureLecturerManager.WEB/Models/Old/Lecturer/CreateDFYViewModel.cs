﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLM.WEB.Models.Lecturer
{
    public class CreateDFYViewModel
    {
        public int DisciplineId { get; set; }
        public ICollection<int> Lecturers { get; set; }
        public ICollection<int> Assistants { get; set; }
        public String Year { get; set; }
        public Int32 Semester { get; set; }
    }
}