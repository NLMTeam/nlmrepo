﻿using System.Collections.Generic;
using NLM.BLL.DTO;
using System;
using System.Linq;

namespace NLM.WEB.Models.Lecturer
{
    [Serializable]
    public class StudentMarkViewModel
    {
        public int UserId { get; set; }
        public string FIO { get; set; }

        [NonSerialized]
        private Dictionary<Category, string> marks;
        public Dictionary<Category, string> Marks
        {
            get
            {
                return marks;
            }
            set
            {
                marks = value;
            }
        }

        public KeyValuePair<Category, string>[] StudentMarks
        {
            get
            {
                if (marks == null) return new KeyValuePair<Category, string>[0];
                return marks.ToArray();
            }
        }


    }

    [Serializable]
    public class Category
    {
        public Category()
        {
        }

        public Category(WorkTypeDTO workType)
        {
            WorkType = workType;
        }

        public Category(ControlPointDTO controlPoint)
        {
            ControlPoint = controlPoint;
        }

        public string Name
        {
            get
            {
                if (WorkType != null)
                    return WorkType.Name;

                if (ControlPoint != null)
                    return ControlPoint.Name;

                return "";
            }

        }

        public WorkTypeDTO WorkType { get; set; }
        public ControlPointDTO ControlPoint { get; set; }

        public static bool operator ==(Category c1, Category c2)
        {
            if (c1.WorkType != null)
            {
                if (c2.WorkType != null)
                {
                    return c1.WorkType.WorkTypeId == c2.WorkType.WorkTypeId;
                }
                else
                {
                    return false;
                }
            }
            else if (c1.ControlPoint != null)
            {
                if (c2.ControlPoint != null)
                {
                    return c1.ControlPoint.ControlPointId == c2.ControlPoint.ControlPointId;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static bool operator !=(Category c1, Category c2)
        {
            return !(c1 == c2);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            var cat2 = (Category)obj;


            return this == cat2;
        }
    }
}