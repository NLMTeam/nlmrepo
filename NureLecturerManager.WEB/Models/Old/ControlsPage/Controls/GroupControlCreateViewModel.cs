﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.WEB.Models.ControlsPage.Controls
{
    public class GroupControlCreateViewModel
    {
        public int WorkTypeId { get; set; }
        public string GroupNumber { get; set; }
        public Nullable<System.DateTime> PassDate { get; set; }
        public string Type { get; set; }
        public string PassPlace { get; set; }
        public int DisciplineId { get; set; }

        public String Year { get; set; }
        public Int32 Semester { get; set; }
    }
}
