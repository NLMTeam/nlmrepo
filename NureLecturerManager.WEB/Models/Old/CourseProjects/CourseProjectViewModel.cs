﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLM.WEB.Models.Lecturer
{
    public class CourseProjectViewModel
    {
        public string Theme { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Discipline { get; set; }
        public string Group { get; set; }
    }
}