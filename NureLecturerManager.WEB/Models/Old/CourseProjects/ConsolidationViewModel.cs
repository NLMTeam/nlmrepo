﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLM.WEB.Models
{
    public class ConsolidationViewModel
    {
        public int ConsolidationId { get; set; }
        public string FIO { get; set; }
        public string GroupNum { get; set; }
        public string ConsolidationDate { get; set; }
        public string PassDate { get; set; }
        public int? Mark { get; set; }
        public int CourseProjectId { get; set; }
    }
}