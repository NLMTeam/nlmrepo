﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLM.WEB.Models.Lecturer
{
    public class EditConsolidationViewModel
    {
        public int DisciplineId { get; set; }
        public int UserId { get; set; }
        public int CourseProjectId { get; set; }
        public string Year { get; set; }
        public int? Mark { get; set; }
        public DateTime? PassDate { get; set; }
    }
}