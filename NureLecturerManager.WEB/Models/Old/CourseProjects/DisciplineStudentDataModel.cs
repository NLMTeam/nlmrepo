﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLM.WEB.Models.CourseProjects
{
    public class DisciplineStudentDataModel : DisciplineDataModel
    {
        public DisciplineStudentDataModel()
        {
        }
        public DisciplineStudentDataModel(DisciplineDataModel baseModel, bool isConsolidated, ConsolidationViewModel cons)
        {
            this.IsConsolidated = isConsolidated;
            this.CourseProjects = baseModel.CourseProjects;
            this.Consolidation = cons;
        }
        public bool IsConsolidated { get; set; }
        public ConsolidationViewModel Consolidation { get; set; }
    }
}