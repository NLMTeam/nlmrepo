﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLM.WEB.Models.Lecturer;

namespace NLM.WEB.Models.CourseProjects
{
    public class CourseProjectMainStudentModel : CourseProjectMainViewModel
    {
        public bool IsConsolidated { get; set; }
        public ConsolidationViewModel Consolidation { get; set; }
    }
}