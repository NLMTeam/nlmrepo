﻿using System.Collections.Generic;
using NLM.BLL.DTO;

namespace NLM.WEB.Models.Lecturer
{
    public class CreateCourseProjectViewModel
    {
        public string Theme { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Year { get; set; }
        public string Discipline { get; set; }
    }

    public class InitialCourseProjectFill
    {
        public ICollection<DisciplineDTO> Disciplines { get; set; }
    }
}