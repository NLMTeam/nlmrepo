﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.WEB.Models.GroupPage
{
    public class StudentEditViewModel
    {
        public Int32 StudentId { get; set; }
        public String NewFIO { get; set; }
        public String NewGroupNum { get; set; }
    }
}
