﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.WEB.Models.GroupPage
{
    public class StudentCreateViewModel
    {
        public String FIO { get; set; }
        public String Email { get; set; }
        public String GroupNumber { get; set; }
    }
}
