using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLM.BLL.DTO;
using NLM.WEB.Models.Lecturer;
using NLM.BLL.Exceptions;
using NLM.WEB.Models;
using NLM.BLL.Interfaces;
using NLM.BLL.DTObjects;
using NLM.WEB.Util;
using NLM.WEB.Util.Helpers.CourseProject;
using NLM.WEB.Util.Helpers.Permissions;
using NLM.WEB.Models.CourseProjects;
using NLM.WEB.Filters;
using NLM.WEB.Util.Exceptions;
using System.Net;
using NLM.WEB.Filters.MVC;

namespace NLM.WEB.Controllers
{
    [AuthenticationFilter]
    [OnExceptionFilter]
    public class CourseProjectsController : Controller
    {
        private IAccountService accountService = DependencyResolver.Current.GetService<IAccountService>();
        private IDisciplineService discService = DependencyResolver.Current.GetService<IDisciplineService>();
        private ICourseProjectService _cpService = DependencyResolver.Current.GetService<ICourseProjectService>();

        private ICourseProjectHelper cpHelper = DependencyResolver.Current.GetService<ICourseProjectHelper>();
        private IPermissionHelper permisHelper = DependencyResolver.Current.GetService<IPermissionHelper>();

        private const string ADMIN_TYPE = "ADMIN";
        private const string LECTURER_TYPE = "LECTURER";
        private const string STUDENT_TYPE = "STUDENT";

        //GET: CourseProjects
        public ActionResult Index()
        {
            var user = (UserDTO)Session["User"];
            Session["Placement"] = "CourseProjects";

            if (user == null)
            {
                return RedirectToAction("Login", "Account");
            }

            if (user.Type == STUDENT_TYPE)
            {
                return RedirectToAction("StudentIndex");
            }

            if (user.Type == LECTURER_TYPE)
            {
                return RedirectToAction("LecturerIndex");
            }

            if (user.Type == ADMIN_TYPE)
            {
                return RedirectToAction("AdminIndex");
            }

            throw new NotExpectedUserTypeException();
        }

        public ActionResult StudentIndex()
        {
            var user = (UserDTO)Session["User"];
            if (user == null || user.Type != STUDENT_TYPE)
            {
                return HttpNotFound();
            }
            return View();
        }

        public ActionResult LecturerIndex()
        {
            var user = (UserDTO)Session["User"];
            if (user == null || user.Type != LECTURER_TYPE)
            {
                return HttpNotFound();
            }
            return View();
        }

        public ActionResult AdminIndex()
        {
            var user = (UserDTO)Session["User"];
            if (user == null || user.Type != ADMIN_TYPE)
            {
                return HttpNotFound();
            }
            return View();
        }

        public ActionResult GetStudentModel()
        {
            UserDTO user = (UserDTO)Session["User"];

            if (user.Type != STUDENT_TYPE)
            {
                return HttpNotFound();
            }

            var student = accountService.SearchStudent(user.UserId).Single();

            CourseProjectMainStudentModel model = new CourseProjectMainStudentModel();


            model = cpHelper.GetStudentModel(student);

            if (model == null)
            { 
                model.Consolidations = model.Consolidations != null ? model.Consolidations : new List<ConsolidationViewModel>();
                model.CourseProjects = model.CourseProjects != null ? model.CourseProjects : new List<CourseProjectDTO>();
                model.Disciplines = model.Disciplines != null ? model.Disciplines : new List<DisciplineDTO>();
                model.Groups = model.Groups != null ? model.Groups : new List<string>();
                model.Years = model.Years != null ? model.Years : new List<string>();
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetModel()
        {
            var user = (UserDTO)Session["User"];
            CourseProjectMainViewModel model = new CourseProjectMainViewModel();


            model = cpHelper.GetModel(user);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLecturerArchievedModel()
        {
            var user = (UserDTO)Session["User"];

            if (user == null || user.Type != LECTURER_TYPE)
            {
                return HttpNotFound();
            } 
            CourseProjectMainViewModel model = new CourseProjectMainViewModel();

            model = cpHelper.GetArchievedModel(user);

            return Json(model, JsonRequestBehavior.AllowGet);
         }

        public ActionResult GetDisciplineInfo(int disciplineId)
        {
            UserDTO user = (UserDTO)Session["User"];

            DisciplineDataModel model = new DisciplineDataModel();
            bool IsStudentConsolidated = false;
            ConsolidationViewModel consModel = null;


            model = cpHelper.GetDisciplineInfo(disciplineId);

            if (user.Type == STUDENT_TYPE)
            {
                try
                {
                    ConsolidationDTO consolidation = cpHelper.GetStudentConsolidation(disciplineId, user);
                    consModel = new ConsolidationViewModel()
                    {
                        CourseProjectId = consolidation.CourseProjectId,
                        GroupNum = accountService.SearchStudent(consolidation.UserId).First().GroupId, 
                        ConsolidationId = consolidation.ConsolidationId
                    };
                    IsStudentConsolidated = consolidation != null;
                }
                catch (NotAllowedConsolidationEditException)
                {
                    IsStudentConsolidated = false;
                }
            }
            if (user.Type == STUDENT_TYPE)
            {
                return Json(new DisciplineStudentDataModel(model, IsStudentConsolidated, consModel), JsonRequestBehavior.AllowGet);
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetYearGroups(string year, int disciplineId)
        {
            UserDTO user = (UserDTO)Session["User"];

            if (user == null)
            {
                return HttpNotFound();
            }

            if (user.Type == STUDENT_TYPE)
            {
                // if it's student we can't receive year from client, so we have to make suggestion
                year = cpHelper.GetStudentYear(disciplineId, user);
            }

            IEnumerable<string> groups = null;

            groups = cpHelper.GetGroupsOfYear(year, disciplineId);

            if (groups.ToList().Count == 0)
            {

                return Json(groups, JsonRequestBehavior.AllowGet);
            }

            return Json(groups, JsonRequestBehavior.AllowGet);
        }
  
        public ActionResult GetMarksForDisciplineAndGroup(string year, string disciplineName, string groupNum)
        {
            var user = (UserDTO)Session["User"];
            if (user == null)
            {
                return HttpNotFound();
            }

            if (user.Type == STUDENT_TYPE)
            {
                DisciplineDTO discipline = discService.SearchDiscipline(Name: disciplineName).First();
                // if it's student we can't receive year from client, so we have to make suggestion
                year = cpHelper.GetStudentYear(discipline.DisciplineId, user);
            }

            IEnumerable<ConsolidationViewModel> models = null;

            models = cpHelper.GetMarksForDisciplineAndGroup(year, disciplineName, groupNum);

            return Json(models, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMarksOfCourseProject(string year, int cpId)
        {
            var user = (UserDTO)Session["User"];

            if (user == null)
            {
                return HttpNotFound();
            }

            if (user.Type == STUDENT_TYPE)
            {
                CourseProjectDTO cp = _cpService.SearchCourseProjects(courseProjectId: cpId).First();
                // if it's student we can't receive year from client, so we have to make suggestion
                year = cpHelper.GetStudentYear(cp.DisciplineId, user);
            }

            IEnumerable<ConsolidationViewModel> models = null;

            models = cpHelper.GetMarksOfCourseProject(cpId, year);

            return Json(models, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProjectInfo(int cpId)
        {
            CourseProjectViewModel model = new CourseProjectViewModel();


            model = cpHelper.GetProjectViewModel(cpId);

            if (model == null)
            {
                return Json("Не найден проект.", JsonRequestBehavior.AllowGet);
            }


            return Json(model, JsonRequestBehavior.AllowGet);
        }

        #region consolidation create update delete
        public ActionResult CreateConsolidation(string groupId)
        {
            var user = (UserDTO)Session["User"];

            if (user.Type == STUDENT_TYPE)
            {
                return HttpNotFound();
            }

            InitialFillConsolidationViewModel model = null;

            model = cpHelper.GetConsolidationFillModel(groupId);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        #region filtering in model 
        public ActionResult GetStudentsOfGroup(string groupNum)
        {
            ICollection<UserDTO> groupParticipates = cpHelper.GetStudentsOfGroup(groupNum);

            if (groupParticipates.Count == 0)
            {
                return Json("В группе нет студентов.", JsonRequestBehavior.AllowGet);
            }

            return Json(groupParticipates, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetYearInfo(string year)
        {
            InitialFillConsolidationViewModel model = new InitialFillConsolidationViewModel();

            model = cpHelper.GetYearInfo(year);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        #endregion

        [HttpPost]
        public ActionResult CreateConsolidation(CreateConsolidationViewModel model)
        {
            var user = (UserDTO)Session["User"];

            if (user.Type == STUDENT_TYPE)
            {
                return HttpNotFound();
            }
            try
            {
                string disciplineName = discService.SearchDiscipline(model.DisciplineId).First().Name;

                if (!permisHelper.AllowConsolidationEdit(model.Year, disciplineName, user))
                {
                    return HttpNotFound();
                }
            }
            catch (NullReferenceException)
            {
                return HttpNotFound();
            } 
            var config = new FileConfiger(Server.MapPath("/Files/Config.txt"));
            string year = config.Year;
            DisciplineForYearDTO yearDisc = discService.SearchDisciplineForYear(discId: model.DisciplineId, year: year).First();

            if (yearDisc == null)
            {
                return Json(new { Title = "Ошибка", Body = "Дисциплина не продлена на текущий год." }, JsonRequestBehavior.AllowGet);
            }

            try
            {

                _cpService.ConsolidateCourseProject(model.UserId, model.CourseProjectId, yearDisc.DisciplineForYearId, when: DateTime.Now);
            }
            catch (AlreadyHaveAConsolidationException)
            {
                return Json(new { Title = "Ошибка", Body = "Данный студент уже закреплен за проектом" }, JsonRequestBehavior.AllowGet);
            }
            catch (ConsolidationIsBisyException)
            {
                return Json(new { Title = "Ошибка", Body = "В группе у этого студента уже есть студент, который закреплен за даной темой." }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Title = "Действие выполнено успешно", Body = "Курсовой проект успешно закреплен" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteConsolidation(int consolidationId)
        {
            var user = (UserDTO)Session["User"];

            if (user.Type == STUDENT_TYPE)
            {
                // delete student consolidation
                return HttpNotFound();
            }


            if (user.Type == LECTURER_TYPE && !permisHelper.AllowConsolidationDelete(consolidationId, user))
            {
                return HttpNotFound();
            }

            bool result = _cpService.DeleteConsolidation(consolidationId);

            if (!result)
            {
                return Json(new { Title = "Ошибка", Body = "Закрепление не найдено." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Title = "Действие выполнео успешно", Body = "Данное закрепление успешно удалено." }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditConsolidation(int consolidationId, string groupId)
        {
            Session["ConsolidationId"] = consolidationId;

            CreateConsolidationViewModel dataModel = new CreateConsolidationViewModel();
            InitialFillConsolidationViewModel model = new InitialFillConsolidationViewModel();
            dataModel = cpHelper.GetConsolidationDataModel(consolidationId);
            model = cpHelper.GetConsolidationFillModel(groupId);

            if (dataModel == null || model == null)
            { 
                return Json(new KeyValuePair<CreateConsolidationViewModel, InitialFillConsolidationViewModel>(dataModel, model), JsonRequestBehavior.AllowGet);
            }

            return Json(new KeyValuePair<CreateConsolidationViewModel, InitialFillConsolidationViewModel>(dataModel, model), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EditConsolidation(EditConsolidationViewModel model)
        {
            var user = (UserDTO)Session["User"];

            if (user.Type == STUDENT_TYPE)
            {
                return HttpNotFound();
            }

            string disciplineName = discService.SearchDiscipline(model.DisciplineId).First().Name;

            if (!permisHelper.AllowConsolidationEdit(model.Year, disciplineName, user))
            {
                return HttpNotFound();
            }


            int consolidationId = (int)Session["ConsolidationId"];
            Session.Remove("ConsolidationId");

            var consolidation = _cpService.SearchConsolidation(consolidationId).SingleOrDefault();
            model.CourseProjectId = consolidation.CourseProjectId;
            model.UserId = consolidation.UserId;

            bool editedCons = false;
            var config = new FileConfiger(Server.MapPath("/Files/Config.txt"));
            string year = config.Year;

            bool setMarkRes = false;
            var disciplineForYear = discService.SearchDisciplineForYear(discId: model.DisciplineId, year: year).First();
            if ((model.PassDate != null && model.PassDate.HasValue && (!consolidation.PassDate.HasValue || consolidation.PassDate.Value != model.PassDate)) || (model.Mark.HasValue && (!consolidation.Mark.HasValue || consolidation.Mark.Value != model.Mark.Value)) || consolidation.DisciplineForYearId != disciplineForYear.DisciplineForYearId)
            {
                if (model.PassDate != null && model.PassDate.HasValue)
                {
                    if (model.Mark != null && model.Mark.HasValue)
                    {
                        setMarkRes = _cpService.SetCourseProjectMark(model.CourseProjectId, model.UserId, model.Mark.Value, model.PassDate.Value, disciplineForYear.DisciplineForYearId);
                        editedCons = true;
                    }
                    else
                    {
                        setMarkRes = _cpService.SetCourseProjectMark(model.CourseProjectId, model.UserId, consolidation.Mark.HasValue ? consolidation.Mark.Value : 0, model.PassDate.Value, disciplineForYear.DisciplineForYearId);
                        editedCons = true;
                    }
                }
                else
                {
                    if (model.Mark != null && model.Mark.HasValue)
                    {
                        setMarkRes = _cpService.SetCourseProjectMark(model.CourseProjectId, model.UserId, model.Mark.Value, disciplineForYearId: disciplineForYear.DisciplineForYearId);
                        editedCons = true;
                    }
                    else
                    {
                        setMarkRes = _cpService.SetCourseProjectMark(model.CourseProjectId, model.UserId, consolidation.Mark.HasValue ? consolidation.Mark.Value : 0, disciplineForYearId: disciplineForYear.DisciplineForYearId);
                        editedCons = true;
                    }
                }

                if (consolidation.DisciplineForYearId != disciplineForYear.DisciplineForYearId && !editedCons)
                {
                    setMarkRes = _cpService.SetCourseProjectMark(model.CourseProjectId, model.UserId, model.Mark != null || model.Mark.HasValue ? model.Mark.Value : 0, disciplineForYearId: disciplineForYear.DisciplineForYearId);
                }
            }

            if (!setMarkRes)
            {
                return Json(new { Title = "Ошибка", Body = "Не найдено закрепление курсового проекта." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Title = "Действие выполнено успешно", Body = "Оценка выставлена" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ConsolidateStudent(int cpId)
        {
            var user = (UserDTO)Session["User"];

            if (user == null)
            {
                return HttpNotFound();
            }

            if (user.Type != STUDENT_TYPE)
            {
                return HttpNotFound();
            }

            CourseProjectDTO courseProject = null;

            courseProject = _cpService.SearchCourseProjects(courseProjectId: cpId).FirstOrDefault();

            if (courseProject == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NoContent, "No course projects were found");
            }

            try
            {
                string year = cpHelper.GetStudentYear(courseProject.DisciplineId, user);
                DisciplineForYearDTO dfy = discService.SearchDisciplineForYear(discId: courseProject.DisciplineId, year: year).First();
                _cpService.ConsolidateCourseProject(user.UserId, cpId, dfy.DisciplineForYearId, DateTime.Now);
            }
            catch (AlreadyHaveAConsolidationException)
            {
                return Json(new { Title = "Ошибка", Body = "Вы уже закрепились за другим проектом." }, JsonRequestBehavior.AllowGet);
            }
            catch (ConsolidationIsBisyException)
            {
                return Json(new { Title = "Ошибка", Body = "Даный проект уже закреплен за студентом из Вашей группы." }, JsonRequestBehavior.AllowGet);
            }
            catch (InvalidOperationException)
            {
                // error log because we have found more than  1 element searching by Id
                return HttpNotFound();
            }
            ConsolidationDTO cons = _cpService.SearchConsolidation(userId: user.UserId, courseProjectId: cpId).First();
            ConsolidationViewModel consModel = new ConsolidationViewModel()
            {
                CourseProjectId = cpId,
                GroupNum = accountService.SearchStudent(user.UserId).First().GroupId,
                ConsolidationId = cons.ConsolidationId
            };
            return Json(new { Title = "Действие выполнено успешно.", Body = "Закрепление успешно создано.", Consolidation = consModel }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteStudentConsolidation(int disciplineId)
        {
            UserDTO user = (UserDTO)Session["User"];

            // check user
            if (user == null || user.Type != STUDENT_TYPE)
            {
                return HttpNotFound();
            }

            ConsolidationDTO consolidation = null;

            // get student consolidation according to the chosed discipline
            try
            {
                consolidation = cpHelper.GetStudentConsolidation(disciplineId, user);
            }
            catch (NotAllowedConsolidationEditException)
            {
                return Json(new { Title = "Ошибка", Body = "Вы не можете редактировать закрепление по этой дисциплине." }, JsonRequestBehavior.AllowGet);
            }

            // we found noting
            if (consolidation == null)
            {
                return HttpNotFound();
            }

            try
            {
                _cpService.DeleleStudentConsolidation(consolidation.ConsolidationId);
            }
            catch (OneHundredHoursLeftException)
            {
                return Json(new { Title = "Ошибка", Body = "С момента закрепления прошло 100 часов. Поэтому Вы больше не может удалить свое закрепление по этой дисциплине. Обратитесь к преподавателю. " }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Title = "Действие выполнено успешно", Body = "Закрепление успешно удалено." }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region course projects create update delete
        public ActionResult CourseProjectsForDiscipline(int disciplineId)
        {
            UserDTO user = (UserDTO)Session["User"];

            if (user == null)
            {
                return HttpNotFound();
            }

            IEnumerable<CourseProjectDTO> model = new List<CourseProjectDTO>();

            model = cpHelper.GetCourseProjectsForDiscipline(disciplineId);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateCourseProject()
        {
            var user = (UserDTO)Session["User"];

            if (user.Type == STUDENT_TYPE)
            {
                return HttpNotFound();
            }

            InitialCourseProjectFill data = new InitialCourseProjectFill();

            data = cpHelper.GetCourseProjectFillModel();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CreateCourseProject(CreateCourseProjectViewModel model)
        {
            UserDTO user = (UserDTO)Session["User"];

            if (user.Type == STUDENT_TYPE)
            {
                return HttpNotFound();
            }

            if (!permisHelper.AllowProjectEdit(model.Year, model.Discipline, user))
            {
                return HttpNotFound();
            }

            DisciplineDTO disc = null;

            disc = discService.SearchDiscipline(Name: model.Discipline).FirstOrDefault();

            if (disc == null)
            {
                return Json(new { Title = "Ошибка", Body = String.Format("Не найдена дисциплины с именем {0}.", model.Discipline) }, JsonRequestBehavior.AllowGet);
            }

            bool crRes =_cpService.CreateCourseProject(user.UserId, disc.DisciplineId, model.Description, model.Theme, model.Type);
            if (!crRes) 
            {
                return Json(new { Title = "Ошибка", Body = "Уже существует такой курсовой проект в даной группе." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Title = "Действие выполнено успешно", Body = "Тема проекта успешно создана." }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditCourseProject(int courseProjectId)
        {
            var user = (UserDTO)Session["User"];

            if (user.Type == STUDENT_TYPE)
            {
                return HttpNotFound();
            }

            Session["CourseProjectId"] = courseProjectId;
            CreateCourseProjectViewModel model = new CreateCourseProjectViewModel();

            model = cpHelper.GetCourseProjectDataModel(courseProjectId);
            if (model == null)
            {
                return Json("Не найден курсовой проект.", JsonRequestBehavior.AllowGet);
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EditCourseProject(CreateCourseProjectViewModel model)
        {
            var user = (UserDTO)Session["User"];
            if (user.Type == STUDENT_TYPE)
            {
                return HttpNotFound();
            }

            if (!permisHelper.AllowProjectEdit(model.Year, model.Discipline, user))
            {
                return HttpNotFound();
            }

            int courseProjectId = (int)Session["CourseProjectId"];
            Session.Remove("CourseProjectId");

            bool updateRes = _cpService.UpdateCourseProject(courseProjectId, description: model.Description,
                                                theme: model.Theme, type: model.Type);
            if (!updateRes)
            {
                return Json(new { Title = "Ошибка", Body = "Не найден такой курсовой проект." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Title = "Действие выполнено успешно", Body = "Тема проекта успешно отредактирована." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteCourseProject(int cpId)
        {
            var user = (UserDTO)Session["User"];

            if (user.Type == STUDENT_TYPE)
            {
                return HttpNotFound();
            }

            if (user.Type == LECTURER_TYPE && !permisHelper.AllowProjectDelete(cpId, user))
            {
                return HttpNotFound();
            }

            bool delRes = _cpService.DeleteCourseProject(cpId);
            if (!delRes)
            {
                return Json(new { Title = "Ошибка", Body = "Курсовой проект не найден." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Title = "Действие выполнено успешно.", Body = "Тема проекта успешно удалена." }, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}