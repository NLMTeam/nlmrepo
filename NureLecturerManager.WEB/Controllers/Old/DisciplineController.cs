﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLM.BLL.DTO;
using NLM.WEB.Models.Lecturer;
using NLM.BLL.Interfaces;
using NLM.BLL.Exceptions;
using System.IO;
using NLM.WEB.Filters;
using NLM.BLL.DTObjects;
using NLM.WEB.Filters.MVC;

namespace NLM.WEB.Controllers
{
    [AuthenticationFilter]
  
    [OnExceptionFilter]
    public class DisciplineController : Controller
    {
        private IDisciplineService discService = DependencyResolver.Current.GetService<IDisciplineService>();
        private IControlPointService controlPointService = DependencyResolver.Current.GetService<IControlPointService>();
        private IControlService controlService = DependencyResolver.Current.GetService<IControlService>();
        private IHelpService helpService = DependencyResolver.Current.GetService<IHelpService>();
        private IAccountService accountService = DependencyResolver.Current.GetService<IAccountService>();

        private const string ADMIN_TYPE = "ADMIN";
        private const string LECTURER_TYPE = "LECTURER";
        private const string STUDENT_TYPE = "STUDENT";

        ////GET: Discipline
        //public ActionResult Index()
        //{
        //    //if (Session["User"] == null)
        //    //    return RedirectToAction("Login", "Account", null);

        //    Session["Placement"] = "Disciplines";
        //    string type = (string)Session["Type"];
        //}

        [AuthorizeFilter("ADMIN")]
        public ActionResult AdminIndex()
        {
                return View();
        }

        [AuthorizeFilter("STUDENT")]
        public ActionResult StudentIndex()
        {
                return View();
        }

        [AuthorizeFilter("LECTURER")]
        public ActionResult LecturerIndex()
        {
            if ((string)Session["Type"] != LECTURER_TYPE)
                return RedirectToAction("Index");
            else
                return View();
        }

        public JsonResult GetAdminModel()
        {
            if ((string)Session["Type"] != ADMIN_TYPE)
                return Json("Ошибка типа пользователя.", JsonRequestBehavior.AllowGet);

            DisciplineAdminViewModel model = new DisciplineAdminViewModel();
            UserDTO user = (UserDTO)Session["User"];
            model.Disciplines = new List<DisciplineDTO>();
            model.DFYs = new Dictionary<string, ICollection<DisciplineForYearsStudentViewModel>>();
            model.ControlPoints = new Dictionary<string, ICollection<ControlPointDTO>>();
            model.WorkTypes = new Dictionary<string, ICollection<WorkTypeDTO>>();

            //   1. Gather Disciplines.
            
            model.Disciplines = discService.GetAllDisciplines();

            if (model.Disciplines.Count == 0)
            {
                return Json(model, JsonRequestBehavior.AllowGet);
            }

            // /2. Gather DFYs

            var discIds = model.Disciplines.Select(d => d.DisciplineId);

            foreach (var disc in discIds)
            {
                // 3.От них собираем DisciplinesForYearStudentsViewModel.

                ICollection<DisciplineForYearDTO> discsForYear = discService.SearchDisciplineForYear(discId: disc);

                if (discsForYear.Count == 0)
                {
                    continue;
                }

                string sId = disc.ToString();
                model.DFYs.Add(sId, discsForYear.Select(dy => new DisciplineForYearsStudentViewModel()
                {
                    Semester = dy.Semester,
                    Year = dy.Year,
                    DFYId = dy.DisciplineForYearId,
                    LecturerNames = accountService.GetLecturersNames(dy.LecturerIds),
                    AssistantsNames = accountService.GetLecturersNames(dy.AssistantsIds)
                }).ToList());

                // 3. 

                // 4.От DisciplinesForYear собираем CPs

                foreach (var dfy in model.DFYs[disc.ToString()])
                {
                    var controlPoints = controlPointService.SearchControlPoints(dfy.DFYId);
                    if (controlPoints.Count != 0)
                    {
                        model.ControlPoints[dfy.DFYId.ToString()] = controlPoints; // Control Points
                    }
                    else
                    {
                        continue;
                    }

                    // /4.


                    //    5.От них - WorkTypes.
                    //    6.От них - Materials.

                    foreach (var point in model.ControlPoints[dfy.DFYId.ToString()])
                    {

                        var workTypes = controlService.SearchWorkType(DFYId: point.ControlPointId);

                        if (workTypes.Count == 0)
                        {
                            continue;
                        }
                        //model.WorkTypes[point.ControlPointId.ToString()] = workTypes; // Control Points
    

                        //foreach (var defaultWorkType in model.WorkTypes[point.ControlPointId.ToString()])
                        //{

                        //    var materials = helpService.SearchMaterials(WorkTypeId: defaultWorkType.WorkTypeId);

                        //    if (materials.Count == 0)
                        //    {
                        //        continue;
                        //    }
                        //    model.Materials[defaultWorkType.WorkTypeId.ToString()] = materials; // Control Points

                        //}
                    }
                }

            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLecturersModel()
        {
            if ((string)Session["Type"] != LECTURER_TYPE)
                return Json("Ошибка типа пользователя.", JsonRequestBehavior.AllowGet);

            LecturerDisciplineMainViewModel model = new LecturerDisciplineMainViewModel();
            UserDTO user = (UserDTO)Session["User"];
            int lecturerId = user.UserId;
            String year = (string)Session["Year"];
            Int32 semester = Convert.ToInt32(Session["Semester"]);

            ICollection<DisciplineForYearDeepDTO> discs = discService.GetDisciplinesForLecturerDeep(lecturerId);  // Disciplines
            if (discs.Count == 0)
            {
                return Json(model, JsonRequestBehavior.AllowGet);
            }

            // Forming model.DFYs

            foreach (var disc in discs.Where(dfy => dfy.Year == year && dfy.Semester == semester))
            {
                model.Disciplines.Add(new DisciplineDFYLecturerViewModel()
                {
                    DFYId = disc.DisciplineForYearId,
                    DisciplineName = disc.DisciplineName,
                    IsCourseProject = disc.IsCourseProject,
                    LecturerNames = accountService.GetLecturersNames(disc.LecturerIds),
                    AssistantsNames = accountService.GetLecturersNames(disc.AssistantsIds),
                    Semester = disc.Semester,
                    Year = disc.Year,
                    IsAdmin = discService.IsAdmin(disc.DisciplineForYearId, lecturerId)
                });

                var controlPoints = controlPointService.SearchControlPoints(disc.DisciplineForYearId);
                if (controlPoints.Count == 0)
                {
                    continue;
                }
                model.ControlPoints[disc.DisciplineForYearId.ToString()] = controlPoints; // Control Points

                //foreach (var point in model.ControlPoints[disc.DisciplineForYearId.ToString()])
                //{
                //    var workTypes = controlService.SearchWorkType(CpId: point.ControlPointId);
                //    if (workTypes.Count == 0)
                //    {
                //        continue;
                //    }
                //    model.WorkTypes[point.ControlPointId.ToString()] = workTypes; // Control Points

                //    foreach (var defaultWorkType in model.WorkTypes[point.ControlPointId.ToString()])
                //    {
                //        var materials = helpService.SearchMaterials(WorkTypeId: defaultWorkType.WorkTypeId);
                //        if (materials.Count == 0)
                //        {
                //            continue;
                //        }
                //        model.Materials[defaultWorkType.WorkTypeId.ToString()] = materials; // Control Points
                //    }
                //}
            }
            //JsonResult m = Json(model, JsonRequestBehavior.AllowGet);
            //return m;
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        //public JsonResult GetStudentsModel()
        //{
        //    if ((string)Session["Type"] != STUDENT_TYPE)
        //        return Json("Ошибка типа пользователя.", JsonRequestBehavior.AllowGet);

        //    DisciplineViewModel model = new DisciplineViewModel();
        //    UserDTO user = (UserDTO)Session["User"];
        //    model.DFYs = new Dictionary<string, ICollection<DisciplineForYearsStudentViewModel>>();
        //    model.ControlPoints = new Dictionary<string, ICollection<ControlPointDTO>>();
        //    model.WorkTypes = new Dictionary<string, ICollection<WorkTypeDTO>>();


        //    string groupId = accountService.SearchStudent(user.UserId).SingleOrDefault().GroupId;
        //    string year = (string)Session["Year"];
        //    string term = (string)Session["Semester"];

        //    //   1. Собираем Disciplines за этот год.
        //    model.Disciplines = discService.SearchDisciplinesForGroup(groupId, year, Convert.ToInt32(term));

        //    // /1.


        //    // 1
        //    foreach (var disc in model.Disciplines)
        //    {
        //        // 3.От них собираем DisciplinesForYearStudentsViewModel.

        //        ICollection<DisciplineForYearDTO> discsForYear;
        //        discsForYear = discService.SearchDisciplineForYear(discId: disc.DisciplineId, year: year);

        //        if (discsForYear.Count == 0)
        //        {
        //            continue;
        //        }

        //        string sId = disc.DisciplineId.ToString();
        //        model.DFYs.Add(sId, discsForYear.Select(dy => new DisciplineForYearsStudentViewModel()
        //        {
        //            Semester = dy.Semester,
        //            Year = dy.Year,
        //            DFYId = dy.DisciplineForYearId,
        //            LecturerNames = accountService.GetLecturersNames(dy.LecturerIds),
        //            AssistantsNames = accountService.GetLecturersNames(dy.AssistantsIds)
        //        }).ToList());

        //        // 3. 

        //        // 4.От DisciplinesForYear собираем CPs

        //        foreach (var dfy in model.DFYs[disc.DisciplineId.ToString()])
        //        {
        //            var controlPoints = controlPointService.SearchControlPoints(dfy.DFYId);
        //            if (controlPoints.Count == 0)
        //            {
        //                continue;
        //            }
        //            model.ControlPoints[dfy.DFYId.ToString()] = controlPoints; // Control Points

        //            // /4.


        //            //    5.От них - WorkTypes.
        //            //    6.От них - Materials.

        //            //foreach (var point in model.ControlPoints[dfy.DFYId.ToString()])
        //            //{
        //            //    var workTypes = controlService.SearchWorkType(CpId: point.ControlPointId);
        //            //    if (workTypes.Count == 0)
        //            //    {
        //            //        continue;
        //            //    }
        //            //    model.WorkTypes[point.ControlPointId.ToString()] = workTypes; // Control Points


        //            //    foreach (var defaultWorkType in model.WorkTypes[point.ControlPointId.ToString()])
        //            //    {
        //            //        var materials = helpService.SearchMaterials(WorkTypeId: defaultWorkType.WorkTypeId);
        //            //        if (materials.Count == 0)
        //            //        {
        //            //            continue;
        //            //        }
        //            //        model.Materials[defaultWorkType.WorkTypeId.ToString()] = materials; // Control Points
        //            //    }
        //            }
        //        }

        //    }
        //    return Json(model, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult GetStudentsAchievedModel()
        {
            if ((string)Session["Type"] != STUDENT_TYPE)
                return Json("Ошибка типа пользователя.", JsonRequestBehavior.AllowGet);

            DisciplineViewModel model = new DisciplineViewModel();
            UserDTO user = (UserDTO)Session["User"];
            model.Disciplines = new List<DisciplineDTO>();
            model.DFYs = new Dictionary<string, ICollection<DisciplineForYearsStudentViewModel>>();
            model.ControlPoints = new Dictionary<string, ICollection<ControlPointDTO>>();
            model.WorkTypes = new Dictionary<string, ICollection<WorkTypeDTO>>();



            //            План получения studentsAchievedDisciplineModel:
            //            1.Определяем части за этот год.

            //    2.От частей собираем Disciplines.

            //    3.От них собираем DisciplinesForYearStudentsViewModel.

            //    4.От DisciplinesForYear собираем CPs.

            //    5.От них - WorkTypes.

            //    6.От них - Materials.

            //С архивом всё аналогично, только в частях берём всё, у чего года меньше текущего.Switch как-то через сервисы Ангулара

            //провернуть.


            string groupId = accountService.SearchStudent(user.UserId).SingleOrDefault().GroupId;
            string year = (string)Session["Year"];
            string term = (string)Session["Semester"];

            // 1. Получаем дисциплины
            model.Disciplines = discService.SearchArchievedDisciplinesForGroup(groupId, year, Convert.ToInt32(term));

            // /1.
            foreach (var disc in model.Disciplines)
            {
                // 2.От них собираем DisciplinesForYearStudentsViewModel.

                ICollection<DisciplineForYearDTO> discsForYear = discService.SearchDisciplineForYear(discId: disc.DisciplineId, year: year, semester: Convert.ToInt32(term));

                if (discsForYear.Count == 0)
                {
                    continue;
                }

                string sId = disc.DisciplineId.ToString();
                model.DFYs.Add(sId, discsForYear.Select(dy => new DisciplineForYearsStudentViewModel()
                {
                    Semester = dy.Semester,
                    Year = dy.Year,
                    LecturerNames = accountService.GetLecturersNames(dy.LecturerIds),
                    AssistantsNames = accountService.GetLecturersNames(dy.AssistantsIds),
                    DFYId = dy.DisciplineForYearId
                }).ToList());

                // 2. 

                // 3.От DisciplinesForYear собираем CPs

                foreach (var dfy in model.DFYs[disc.DisciplineId.ToString()])
                {
                    var controlPoints = controlPointService.SearchControlPoints(dfy.DFYId);
                    if (controlPoints.Count == 0)
                    {
                        continue;
                    }
                    model.ControlPoints[dfy.DFYId.ToString()] = controlPoints; // Control Points

                    // /3.


                    //    4.От них - WorkTypes.
                    //    5.От них - Materials.

                    //foreach (var point in model.ControlPoints[dfy.DFYId.ToString()])
                    //{
                    //    var workTypes = controlService.SearchWorkType(CpId: point.ControlPointId);
                    //    if (workTypes.Count == 0)
                    //    {
                    //        continue;
                    //    }
                    //    model.WorkTypes[point.ControlPointId.ToString()] = workTypes; // Control Points
 

                    //    foreach (var defaultWorkType in model.WorkTypes[point.ControlPointId.ToString()])
                    //    {
                    //        var materials = helpService.SearchMaterials(WorkTypeId: defaultWorkType.WorkTypeId);
                    //        if (materials.Count == 0)
                    //        {
                    //            continue;
                    //        }
                    //        model.Materials[defaultWorkType.WorkTypeId.ToString()] = materials; // Control Points
                    //    }
                    //}
                }

            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #region Filters

        public ActionResult GetControlPoints(int partId)
        {
            ICollection<ControlPointDTO> controlPoints = controlPointService.SearchControlPoints(partId);
            return Json(controlPoints, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Discipline Create, Get, Update, Delete

        [HttpPost]
        public ActionResult CreateDiscipline(string disciplineName, bool isCourseProject)
        {
            if ((string)Session["Type"] != ADMIN_TYPE)
            {
                return Json("Ошибка типа пользователя.", JsonRequestBehavior.AllowGet);
            }

            if (disciplineName.Trim() == "")
            {
                return Json("Некорректное значение дисциплины.", JsonRequestBehavior.AllowGet);
            }

            bool createRes = discService.CreateDiscipline(disciplineName, isCourseProject);
            if (!createRes)
            {
                return Json("Такая дисциплина уже существует.", JsonRequestBehavior.AllowGet);
            }

            DisciplineDTO disc = discService.SearchDiscipline(Name: disciplineName).LastOrDefault();
            if (disc == null)
            {
                return Json("Что-то пошло не так... Созданная дисциплина не найдена. Обратитесь в техподдержку.", JsonRequestBehavior.AllowGet);
            }

            return Json(disc, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RenameDiscipline(int discId, string discName)
        {
            if ((string)Session["Type"] != ADMIN_TYPE)
            {
                return Json("Ошибка типа пользователя.", JsonRequestBehavior.AllowGet);
            }

            if (discName.Trim() == "")
            {
                return Json("Некорректное значение дисциплины.", JsonRequestBehavior.AllowGet);
            }

            bool updateRes = discService.UpdateDiscipline(discId, discName);
            if (!updateRes)
            {
                return Json("Дисциплина не найдена.", JsonRequestBehavior.AllowGet);
            }

            return Json("Дисциплина успешно переименована.", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteDiscipline(int disciplineId)
        {
            if ((string)Session["Type"] != ADMIN_TYPE)
            {
                return Json("Ошибка типа пользователя.", JsonRequestBehavior.AllowGet);
            }

            bool delRes = discService.DeleteDiscipline(disciplineId);
            if (!delRes)
            {
                return Json("Дисциплина не найдена.", JsonRequestBehavior.AllowGet);
            }

            return Json("Дисциплина успешно удалена.", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region DFY Create, Get, Update, Delete, Prolong

        [HttpPost]
        public ActionResult CreateDFY(CreateDFYViewModel model)
        {
            if ((string)Session["Type"] != ADMIN_TYPE)
            {
                return Json("Ошибка типа пользователя.", JsonRequestBehavior.AllowGet);
            }

            if (model.Year.Trim() == "")
                return Json("Некорректный год.", JsonRequestBehavior.AllowGet);

            if (model.Semester < 1 || model.Semester > 3)
                return Json("Семестр должен быть > 0 и < 4.", JsonRequestBehavior.AllowGet);

            if (model.Lecturers == null)
                model.Lecturers = new List<int>();

            bool createRes = discService.CreateDisciplineForYear(model.DisciplineId, model.Lecturers, model.Assistants, model.Year, model.Semester);
            if (!createRes)
            {
                return Json("Привязка этой дисциплины к указанному году и семестру уже существует.", JsonRequestBehavior.AllowGet);
            }

            DisciplineForYearDTO disc = discService.SearchDisciplineForYear(discId: model.DisciplineId, lecturersIds: model.Lecturers, 
                                                                                assistants: model.Assistants, year: model.Year,
                                                                                semester: model.Semester).LastOrDefault();
            if (disc == null)
            {
                return Json("Что-то пошло не так... Созданная привязка не найдена. Обратитесь в техподдержку.", JsonRequestBehavior.AllowGet);
            }

            DisciplineForYearsStudentViewModel discVM = new DisciplineForYearsStudentViewModel()
            {
                Semester = disc.Semester,
                Year = disc.Year,
                DFYId = disc.DisciplineForYearId,
                LecturerNames = accountService.GetLecturersNames(disc.LecturerIds),
                AssistantsNames = accountService.GetLecturersNames(disc.AssistantsIds)
            };

            return Json(discVM, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EditDFY(DFYEditViewModel model)
        {
            if ((string)Session["Type"] != ADMIN_TYPE)
                return Json("Ошибка типа пользователя.", JsonRequestBehavior.AllowGet);

            if (model.Year.Trim() == "")
                return Json("Некорректное значение года.", JsonRequestBehavior.AllowGet);

            if (model.Semester < 1 || model.Semester > 3)
                return Json("Семестр должен быть > 0 и < 4.", JsonRequestBehavior.AllowGet);

            bool updateRes = discService.UpdateDisciplineForYear(model.DFYId, model.Lecturers, model.Assistants,- 1, model.Year, model.Semester);
            if (!updateRes)
            {
                return Json("Либо привязка за указанные год и семестр уже существует, либо привязка была удалена другим пользователем. Во втором случае перезагрузите страницу.", JsonRequestBehavior.AllowGet);
            }

            return Json("Привязка успешно отредактирована", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteDFY(int dfyId)
        {
            String type = (string)Session["Type"];
            if (type == STUDENT_TYPE)
            {
                return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }

            if (type == LECTURER_TYPE)
            {
                UserDTO user = (UserDTO)Session["User"];

                if (!discService.IsAdmin(dfyId, user.UserId))
                {
                    return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
                }
            }
            else
                if (type != ADMIN_TYPE)
                {
                    return Json("Ошибка типа пользователя!", JsonRequestBehavior.AllowGet);
                }

            bool delRes = discService.DeleteDisciplineForYear(dfyId);
            if (!delRes)
            {
                return Json("Привязка не найдена.", JsonRequestBehavior.AllowGet);
            }

            return Json("Привязка успешно удалена.", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ProlongDFY(int DFYId, string year, int semester)
        {
            String type = (string)Session["Type"];
            if (type == STUDENT_TYPE)
            {
                return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }

            if (semester < 1 || semester > 3)
                return Json("Семестр должен быть > 0 и < 4.", JsonRequestBehavior.AllowGet);

            if (type == LECTURER_TYPE)
            {
                UserDTO user = (UserDTO)Session["User"];

                if (!discService.IsAdmin(DFYId, user.UserId))
                    return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }
            else
                if (type != ADMIN_TYPE)
                return Json("Ошибка типа пользователя!", JsonRequestBehavior.AllowGet);

            if (year.Trim() == "")
                return Json("Некорректный год.", JsonRequestBehavior.AllowGet);

            Int32 dfyId = -1;

            dfyId = discService.ProlongDFY(DFYId, year, semester);
            if (dfyId == -1)
            {
                return Json("Привязка этой дисциплины к указанному году и семестру уже существует.", JsonRequestBehavior.AllowGet);
            }
            if (dfyId == -2)
            {
                return Json("Ошибка на уровне бизнес-логики. Продлённая дисциплина не была найдена. Обратитесь в техподдержку и перезагрузите страницу.", JsonRequestBehavior.AllowGet);
            }

            // Building result

            // DFYDTO
            ProlongedDFYViewModel vm = new ProlongedDFYViewModel();

            var tempDFY = discService.SearchDisciplinesForYearDeep(dfyId).SingleOrDefault();

            if (tempDFY == null)
            {
                return Json("Ошибка синхронизации. Перезагрузите страницу.", JsonRequestBehavior.AllowGet);
            }

            vm.DFY = new DisciplineDFYLecturerViewModel()
            {
                DisciplineName = tempDFY.DisciplineName,
                IsAdmin = true,
                IsCourseProject = tempDFY.IsCourseProject,
                Semester = tempDFY.Semester,
                Year = tempDFY.Year,
                DFYId = tempDFY.DisciplineForYearId,
                LecturerNames = accountService.GetLecturersNames(tempDFY.LecturerIds),
                AssistantsNames = accountService.GetLecturersNames(tempDFY.AssistantsIds)
            };


            // /4. ControlPoints
            vm.ControlPoints = controlPointService.SearchControlPoints(vm.DFY.DFYId).ToList();


            //    5.От них - WorkTypes.

            var workTypes = controlService.SearchWorkType(DFYId: vm.DFY.DFYId);

            return Json(vm, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Control Points Create, Update, Delete

        [HttpPost]
        public JsonResult CreateControlPoint(ControlPointViewModel model)
        {
            String type = (string)Session["Type"];
            if (type == STUDENT_TYPE)
            {
                return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }

            if (type == LECTURER_TYPE)
            {
                UserDTO user = (UserDTO)Session["User"];

                if (!discService.IsAdmin(model.DFYId, user.UserId))
                    return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }
            else
                if (type != ADMIN_TYPE)
                return Json("Ошибка типа пользователя!", JsonRequestBehavior.AllowGet);

            if (model.MaxMark == null || model.MinMark == null || model.Name.Trim() == "")
                return Json("Некорректный пользовательский ввод.", JsonRequestBehavior.AllowGet);

            if (model.MaxMark < 0 || model.MinMark < 0 || model.Number < 0)
                return Json("Номер и баллы не могут быть отрицательными.", JsonRequestBehavior.AllowGet);

            string[] c = model.Date.Split('-');
            string day = c[2].Split('T')[0];
            bool createRes = controlPointService.CreateControlPoint(model.DFYId, model.Name, model.Number,
                                                        new DateTime(Convert.ToInt32(c[0]), Convert.ToInt32(c[1]),
                                                        Convert.ToInt32(day)),
                                                        model.MinMark.Value, model.MaxMark.Value);
            if (!createRes)
            {
                return Json("Такая контрольная точка уже существует.", JsonRequestBehavior.AllowGet);
            }

            ControlPointDTO cp = controlPointService.SearchControlPoints(discYId: model.DFYId, name: model.Name,
                                                             near: new DateTime(Convert.ToInt32(c[0]), Convert.ToInt32(c[1]),
                                                        Convert.ToInt32(day)), minScore: model.MinMark.Value, maxScore: model.MaxMark.Value).LastOrDefault();
            if (cp == null)
            {
                return Json("Что-то пошло не так... Созданная контрольная точка не была найдена. Обратитесь в техподдержку.", JsonRequestBehavior.AllowGet);
            }

            return Json(cp, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EditControlPoint(ControlPointViewModel model)
        {
            String type = (string)Session["Type"];
            if (type == STUDENT_TYPE)
            {
                return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }

            if (type == LECTURER_TYPE)
            {
                UserDTO user = (UserDTO)Session["User"];

                if (!discService.IsAdmin(controlPointService.SearchControlPoints(cpId: model.ControlPointId).Single().DisciplineForYearId, user.UserId))
                    return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }
            else
                if (type != ADMIN_TYPE)
                return Json("Ошибка типа пользователя!", JsonRequestBehavior.AllowGet);

            if (model.MaxMark < 0 || model.MinMark < 0 || model.Number < 0)
                return Json("Номер и баллы не могут быть отрицательными.", JsonRequestBehavior.AllowGet);

            string[] c = model.Date.Split('-');
            string day = c[2].Split('T')[0];

            bool updateRes = controlPointService.UpdateControlPoint(model.ControlPointId, model.DFYId, model.Name, 
                                                       model.Number, new DateTime(Convert.ToInt32(c[0]),
                                                       Convert.ToInt32(c[1]), Convert.ToInt32(day)+1),
                                                       model.MinMark.Value, model.MaxMark.Value);
            if (!updateRes)
            {
                return Json("Не найдена контрольная точка. Возможно, она была удалена другим пользователем. Попробуйте обновить страницу.", JsonRequestBehavior.AllowGet);
            }

            return Json("Контрольная точка успешно отредактирована.", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteControlPoint(int cpId)
        {
            String type = (string)Session["Type"];
            if (type == STUDENT_TYPE)
            {
                return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }

            if (type == LECTURER_TYPE)
            {
                UserDTO user = (UserDTO)Session["User"];

                if (!discService.IsAdmin(controlPointService.SearchControlPoints(cpId: cpId).Single().DisciplineForYearId, user.UserId))
                    return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }
            else
                if (type != ADMIN_TYPE)
                return Json("Ошибка типа пользователя!", JsonRequestBehavior.AllowGet);

            bool delRes = controlPointService.DeleteControlPoint(cpId);
            if (!delRes)
            {
                return Json("Не найдена контрольная точка.", JsonRequestBehavior.AllowGet);
            }

            return Json("Контрольная точка успешно удалена.", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region WorkTypes Create, Update, Delete

        [HttpPost]
        public ActionResult CreateWorkType(WorkTypeViewModel model)
        {
            String type = (string)Session["Type"];
            if (type == STUDENT_TYPE)
            {
                return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }

            if (type == LECTURER_TYPE)
            {
                UserDTO user = (UserDTO)Session["User"];

                var cp = controlPointService.SearchControlPoints(cpId: model.ControlPointId).Single();
                if (!discService.IsAdmin(cp.DisciplineForYearId, user.UserId))
                    return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }
            else
                if (type != ADMIN_TYPE)
                {
                    return Json("Ошибка типа пользователя!", JsonRequestBehavior.AllowGet);
                }

            if (model.MaxMark == null
                || model.MinMark == null || model.MaxMark < 1 || model.MinMark < 1)
            {
                return Json("Некорректный пользовательский ввод.", JsonRequestBehavior.AllowGet);
            }

            if (model.MaxMark < 0 || model.MinMark < 0)
            {
                return Json("Баллы не могут быть отрицательными.", JsonRequestBehavior.AllowGet);
            }

            bool createRes = controlService.CreateWorkType(model.Name, model.ControlPointId, model.MinMark.Value, model.MaxMark.Value);
            if (!createRes)
            {
                return Json("Такой вид работы уже присутствует.", JsonRequestBehavior.AllowGet);
            }

            WorkTypeDTO wt = controlService.SearchWorkType(Name: model.Name, DFYId
                                                   : model.ControlPointId, minScore: model.MinMark.Value,
                                                   maxScore: model.MaxMark.Value).LastOrDefault();
            if (wt == null)
            {
                return Json("Что-то пошло не так... Созданный вид работы не был найден. Обратитесь в техподдержку.", JsonRequestBehavior.AllowGet);
            }

            return Json(wt, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult EditWorkType(WorkTypeViewModel model)
        {
            String type = (string)Session["Type"];
            if (type == STUDENT_TYPE)
            {
                return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }

            if (type == LECTURER_TYPE)
            {
                UserDTO user = (UserDTO)Session["User"];

                var wts = controlService.SearchWorkType(model.WorkTypeId).Single();
                if (!discService.IsAdmin(wts.DisciplineForYearId, user.UserId))
                    return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }
            else
                if (type != ADMIN_TYPE)
                return Json("Ошибка типа пользователя!", JsonRequestBehavior.AllowGet);

            if (model.MaxMark == null
                || model.MinMark == null || model.Name.Trim() == "")
                return Json("Некорректный пользовательский ввод.", JsonRequestBehavior.AllowGet);

            if (model.MaxMark < 0 || model.MinMark < 0)
                return Json("Баллы не могут быть отрицательными.", JsonRequestBehavior.AllowGet);

            bool updateRes = controlService.UpdateWorkType(model.WorkTypeId, model.Name, model.ControlPointId, model.MinMark.Value, model.MaxMark.Value);
            if (!updateRes)
            {
                return Json("Не найден вид работ", JsonRequestBehavior.AllowGet);
            }

            return Json("Вид работ успешно отредактирован.", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteWorkType(int workTypeId)
        {
            String type = (string)Session["Type"];
            if (type == STUDENT_TYPE)
            {
                return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }

            if (type == LECTURER_TYPE)
            {
                UserDTO user = (UserDTO)Session["User"];

                var wts = controlService.SearchWorkType(workTypeId).Single();
                if (!discService.IsAdmin(wts.DisciplineForYearId, user.UserId))
                    return Json("Недостаточно прав для выполнения операции.", JsonRequestBehavior.AllowGet);
            }
            else
                if (type != ADMIN_TYPE)
                return Json("Ошибка типа пользователя!", JsonRequestBehavior.AllowGet);

            bool delRes = controlService.DeleteWorkType(workTypeId);
            if (!delRes)
            {
                return Json("Вид работы не найден.", JsonRequestBehavior.AllowGet);
            }

            return Json("Вид работы успешно удален.", JsonRequestBehavior.AllowGet);

        }

        #endregion

        #region Materials Create, Update, Delete
        [HttpGet]
        public FileResult GetMaterial(string filePath, string fileName)
        {
            // Путь к файлу
            string file_path = filePath;
            // Тип файла - content-type
            string file_type = "application/octet-stream";
            // Имя файла - необязательно
            string file_name = fileName;
            return File(file_path, file_type, file_name);
        }

        #endregion

        #region Additional
        [HttpGet]
        public JsonResult GetGroups()
        {
            ICollection<GroupDTO> groups = accountService.SearchGroup();
            if (groups.Count == 0)
            {
                return Json(new string[0], JsonRequestBehavior.AllowGet);
            }

            return Json(groups.Select(t => new { Name = t.GroupId }).OrderBy(t => t.Name), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetLecturers()
        {
            ICollection<LecturerDTO> lect;
            try
            {
                lect = accountService.GetAllLecturers();
            }
            catch
            {
                return Json(new string[0], JsonRequestBehavior.AllowGet);
            }

            return Json(lect.Select(t => new {
                FIO = t.FIO,
                Id = t.UserId
            }).OrderBy(t => t.FIO), JsonRequestBehavior.AllowGet);
        }

        public String GetYear()
        {
            return (String)Session["Year"];
        }
        #endregion

        #region differentModelsForLecturer
        public JsonResult GetLecturersAchievedModel()
        {
            if ((string)Session["Type"] != LECTURER_TYPE)
                return Json("Ошибка типа пользователя.", JsonRequestBehavior.AllowGet);

            LecturerDisciplineMainViewModel model = new LecturerDisciplineMainViewModel();
            UserDTO user = (UserDTO)Session["User"];
            int lecturerId = user.UserId;
            String year = (string)Session["Year"];
            Int32 semester = Convert.ToInt32(Session["Semester"]);

            ICollection<DisciplineForYearDeepDTO> discs = discService.GetDisciplinesForLecturerDeep(lecturerId);  // Disciplines
            if (discs.Count == 0)
            {
                return Json(model, JsonRequestBehavior.AllowGet);
            }

            // Forming model.DFYs

            foreach (var disc in discs.Where(dfy => Convert.ToInt32(dfy.Year.Split('-')[0]) < 
                                                    Convert.ToInt32(year.Split('-')[0]) || dfy.Year == year && dfy.Semester < semester))
            {
                model.Disciplines.Add(new DisciplineDFYLecturerViewModel()
                {
                    DFYId = disc.DisciplineForYearId,
                    DisciplineName = disc.DisciplineName,
                    IsCourseProject = disc.IsCourseProject,
                    LecturerNames = accountService.GetLecturersNames(disc.LecturerIds),
                    AssistantsNames = accountService.GetLecturersNames(disc.AssistantsIds),
                    Semester = disc.Semester,
                    Year = disc.Year,
                    IsAdmin = discService.IsAdmin(disc.DisciplineForYearId, lecturerId)
                });

                var controlPoints = controlPointService.SearchControlPoints(disc.DisciplineForYearId);
                if (controlPoints.Count == 0)
                {
                    continue;
                }
                model.ControlPoints[disc.DisciplineForYearId.ToString()] = controlPoints; // Control Points
               
                //foreach (var point in model.ControlPoints[disc.DisciplineForYearId.ToString()])
                //{

                //    var workTypes = controlService.SearchWorkType(CpId: point.ControlPointId);
                //    if (workTypes.Count == 0)
                //    {
                //        continue;
                //    }
                //    model.WorkTypes[point.ControlPointId.ToString()] = workTypes; // Control Points

                //    foreach (var defaultWorkType in model.WorkTypes[point.ControlPointId.ToString()])
                //    {
                //        var materials = helpService.SearchMaterials(WorkTypeId: defaultWorkType.WorkTypeId);
                //        if (materials.Count == 0)
                //        {
                //            continue;
                //        }
                //        model.Materials[defaultWorkType.WorkTypeId.ToString()] = materials; // Control Points
                //    }
                //}
            }
            //JsonResult m = Json(model, JsonRequestBehavior.AllowGet);
            //return m;
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLecturersFutureModel() {
            if ((string)Session["Type"] != LECTURER_TYPE)
                return Json("Ошибка типа пользователя.", JsonRequestBehavior.AllowGet);

            LecturerDisciplineMainViewModel model = new LecturerDisciplineMainViewModel();
            UserDTO user = (UserDTO)Session["User"];
            int lecturerId = user.UserId;
            String year = (string)Session["Year"];
            Int32 semester = Convert.ToInt32(Session["Semester"]);

            ICollection<DisciplineForYearDeepDTO> discs = discService.GetDisciplinesForLecturerDeep(lecturerId);  // Disciplines
            if (discs.Count == 0)
            {
                return Json(model, JsonRequestBehavior.AllowGet);
            }

            // Forming model.DFYs

            foreach (var disc in discs.Where(dfy => Convert.ToInt32(dfy.Year.Split('-')[0]) >
                                                    Convert.ToInt32(year.Split('-')[0]) || dfy.Year == year && dfy.Semester > semester))
            {
                model.Disciplines.Add(new DisciplineDFYLecturerViewModel()
                {
                    DFYId = disc.DisciplineForYearId,
                    DisciplineName = disc.DisciplineName,
                    IsCourseProject = disc.IsCourseProject,
                    LecturerNames = accountService.GetLecturersNames(disc.LecturerIds),
                    AssistantsNames = accountService.GetLecturersNames(disc.AssistantsIds),
                    Semester = disc.Semester,
                    Year = disc.Year,
                    IsAdmin = discService.IsAdmin(disc.DisciplineForYearId, lecturerId)
                });

                var controlPoints = controlPointService.SearchControlPoints(disc.DisciplineForYearId);
                if (controlPoints.Count == 0)
                {
                    continue;
                }
                model.ControlPoints[disc.DisciplineForYearId.ToString()] = controlPoints; // Control Points

                foreach (var point in model.ControlPoints[disc.DisciplineForYearId.ToString()])
                {

                    var workTypes = controlService.SearchWorkType(DFYId: point.ControlPointId);
                    if (workTypes.Count == 0)
                    {
                        continue;
                    }
                    model.WorkTypes[point.ControlPointId.ToString()] = workTypes; // Control Points
                }
            }
            //JsonResult m = Json(model, JsonRequestBehavior.AllowGet);
            //return m;
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}