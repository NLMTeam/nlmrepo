﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLM.BLL.DTO;
using NLM.WEB.Filters;
//using NLM.WEB.Models.Lecturer;
using NLM.BLL.Interfaces;
using NLM.WEB.Models.GroupPage;
using NLM.BLL.Exceptions;
using NLM.WEB.Filters.MVC;
//using System.IO;
//using NLM.WEB.Util;
//using NLM.WEB.Util.Exceptions;
//using NLM.WEB.Filters;
//using NLM.WEB.Util.Helpers.Groups;
//using NLM.WEB.Util.Helpers.Permissions;

namespace NLM.WEB.Controllers
{
    [OnExceptionFilter]
    [AuthenticationFilter]
    public class GroupsController : Controller
    {
        private IDisciplineService discService = DependencyResolver.Current.GetService<IDisciplineService>();
        private IAccountService accountService = DependencyResolver.Current.GetService<IAccountService>();

        private const string ADMIN_TYPE = "ADMIN";
        private const string LECTURER_TYPE = "LECTURER";
        private const string STUDENT_TYPE = "STUDENT";

        #region RouteActions

        public ActionResult Index()
        {
            var user = (UserDTO)Session["User"];
            Session["Placement"] = "Groups";

            if (user == null)
            {
                return RedirectToAction("Login", "Account");
            }

            if (user.Type == ADMIN_TYPE)
            {
                return RedirectToAction("AdminIndex");
            }

            if (user.Type == LECTURER_TYPE || user.Type == STUDENT_TYPE)
            {
                return RedirectToAction("LecturerStudentIndex");
            }

            throw new NotExpectedUserTypeException();
        }

        public ActionResult AdminIndex()
        {
            return View();
        }

        public ActionResult LecturerStudentIndex()
        {
            return View();
        }

        #endregion

        #region Groups CUD
        public ActionResult CreateGroup(String name)
        {
            // 1. Check permissions
            HttpStatusCodeResult permissionRes = CheckIfIsAdmin();

            if (permissionRes != null)
            {
                return permissionRes;
            }

            // 2. Create group
            bool createRes = accountService.CreateGroup(name);
            if (!createRes)
            {
                // angular:: Группа с таким именем уже существует в базе
                return Json(createRes);
            }

            // 3. Get group id and return it
            GroupDTO group = accountService.SearchGroup(name).SingleOrDefault();
            if (group == null)
            {
                throw new SomethingWithDBException("Созданная группа не отобразилась в БД");
            }

            // angular:: Группа успешно создана || добавление группы в модель
            return Json(true);
        }

        public ActionResult DeleteGroup(String name)
        {
            // 1. Check permissions
            HttpStatusCodeResult permissionRes = CheckIfIsAdmin();

            if (permissionRes != null)
            {
                return permissionRes;
            }

            // 2. Delete group and return result
            bool delRes = accountService.DeleteGroup(name);

            // angular:: Группа успешно удалена
            // angular:: Группа уже была кем-то удалена
            return Json(delRes);
        }

        public ActionResult SetCaptain(String groupName, Int32 captainId)
        {
            // 1. Check permissions
            HttpStatusCodeResult permissionRes = CheckIfIsAdmin();

            if (permissionRes != null)
            {
                return permissionRes;
            }

            // 2. Set captain of group and return result
            bool setRes = accountService.ChangeCaptain(groupName, captainId);

            // angular:: Студент назначен старостой группой
            // angular:: Группа или студент не были найдены в базе. Возможно, кто-то удалил их. Попробуйте перезагрузить страницу
            return Json(setRes);
        }
        #endregion

        #region Students CUD
        public ActionResult CreateStudent(StudentCreateViewModel model)
        {
            // 1. Check permissions
            HttpStatusCodeResult permissionRes = CheckIfIsAdmin();

            if (permissionRes != null)
            {
                return permissionRes;
            }

            // 2. Register user and create student
            bool regRes = accountService.Register(model.FIO, Guid.NewGuid().ToString(), STUDENT_TYPE, model.Email, null);

            if (!regRes)
            {
                // angular:: Пользователь с таким почтовым адресом уже зарегистрирован
                return Json(-1);
            }

            UserDTO newStud = accountService.SearchUser(Email: model.Email).SingleOrDefault();
            if (newStud == null)
            {
                throw new SomethingWithDBException("Ноль или более одного студента с одинаковым почтовым ящиком!");
            }

            bool studCreationRes = accountService.CreateStudent(newStud.UserId, model.GroupNumber);
            // angular: Студент успешно создан
            // angular: Группа была удалена другим пользователем, студент создан, но помещён в группу "Не указано". || Удаление группы на клиенте
            return Json(studCreationRes);
        }

        public ActionResult DeleteStudent(Int32 userId)
        {
            // 1. Check permissions
            HttpStatusCodeResult permissionRes = CheckIfIsAdmin();

            if (permissionRes != null)
            {
                return permissionRes;
            }

            // 2. Delete student
            bool delRes = accountService.DeleteUser(userId);

            // angular:: Студент удалён из базы
            // angular:: Видимо, студента уже удалил кто-то другой
            return Json(delRes);
        }

        public ActionResult EditStudent(StudentEditViewModel model)
        {
            // 1. Check permissions
            HttpStatusCodeResult permissionRes = CheckIfIsAdmin();

            if (permissionRes != null)
            {
                return permissionRes;
            }

            // 2. Edit student
            bool editRes = accountService.EditUser(model.StudentId, FIO: model.NewFIO);
            bool moveToGroupRes = accountService.AddStudentToGroup(model.StudentId, model.NewGroupNum);

            // angular:: Студент отредактирован
            // angular:: Студент или группа были удалены кем-то из базы. Попробуйте перезагрузить страницу
            return Json(moveToGroupRes);
        }
        #endregion

        #region Getting main view models
        public ActionResult GetAdminMainViewModel()
        {
            throw new NotImplementedException();
        }

        public ActionResult GetLecturerMainViewModel()
        {
            throw new NotImplementedException();
        }

        public ActionResult GetStudentMainViewModel()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Additional methods for models
        [NonAction]
        private List<String> GetYearsForGroups()
        {
            throw new NotImplementedException();
        }

        [HttpGet]
	    public List<GroupDTO> GetGroupsForYear(Int32 year)
        {
            throw new NotImplementedException();
        }

	    [HttpGet]
        public List<StudentDTO> GetStudentsForGroup(String groupName)
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        public List<GroupControlDTO> GetGroupControls(String groupName, String year, Int32 semester)
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        public void GetYearsAndSemesters()
        {
            throw new NotImplementedException();
        }
        #endregion
        #region Permissions methods
        [NonAction]
        private HttpStatusCodeResult CheckIfIsAdmin()
        {
            if (Session["User"] == null)
            {
                return new HttpStatusCodeResult(401);
            }

            if ((String)Session["Type"] != ADMIN_TYPE)
            {
                return new HttpStatusCodeResult(403);
            }

            return null;
        }
        #endregion




        //        public ActionResult GetStudentModel()
        //        {
        //            GroupMainViewModel model = new GroupMainViewModel();

        //            UserDTO user = (UserDTO)Session["User"];

        //            if (user == null)
        //            {
        //                return HttpNotFound();
        //            }
        //            StudentDTO student = null;
        //            if (user.Type == STUDENT_TYPE)
        //            {
        //                student = accountService.SearchStudent(user.UserId).Single();
        //            }
        //            else
        //            {
        //                student = accountService.SearchStudent().First();
        //            }

        //            try
        //            {
        //                model = groupHelper.GetStudentModel(student);
        //            }
        //            catch (NotFoundException)
        //            {
        //                //model.GroupControls = model.GroupControls != null && model.GroupControls.Count() > 0 ? model.GroupControls : new List<GroupControlViewModel>();
        //                //model.Groups = model.Groups != null && model.Groups.Count() > 0 ? model.Groups : new List<GroupDTO>();
        //                //model.GroupStudents = model.GroupStudents != null && model.GroupStudents.Count() > 0 ? model.GroupStudents : new List<StudentViewModel>();
        //                //model.Years = model.Years != null && model.Years.Count() > 0 ? model.Years : new List<string>();
        //            }

        //            return Json(model, JsonRequestBehavior.AllowGet);
        //        }

        //        public ActionResult GetYears()
        //        {
        //            IEnumerable<string> years = null;
        //            try
        //            {
        //                years = groupHelper.GetYears();
        //            }
        //            catch (NotFoundException)
        //            {
        //                // some log
        //            }

        //            return Json(years, JsonRequestBehavior.AllowGet);
        //        }

        //        public ActionResult GetGroupStudents(GroupDTO group)
        //        { 
        //            IEnumerable<StudentViewModel> result = null;
        //            try
        //            {
        //                result = groupHelper.GetGroupStudents(group);
        //            }
        //            catch (NotFoundException)
        //            {
        //                return Json(result, JsonRequestBehavior.AllowGet);
        //            }
        //            return Json(result, JsonRequestBehavior.AllowGet);
        //        }

        //        public ActionResult GetGroupControls(GroupDTO group)
        //        {
        //            UserDTO user = (UserDTO)Session["User"];

        //            if (user == null)
        //            {
        //                return HttpNotFound();
        //            }

        //            IEnumerable<GroupControlViewModel> groupControls = null;
        //            try
        //            {
        //                groupControls = groupHelper.GetGroupControls(group);

        //                groupControls = groupControls.Select(gc => {
        //                    gc.AllowEdit = permisHelper.AllowGroupControlEdit(gc.GroupControlId, user);
        //                    return gc;
        //                });
        //            }
        //            catch (NotFoundException)
        //            {
        //                return Json(new List<GroupControlViewModel>(), JsonRequestBehavior.AllowGet);
        //            }

        //            return Json(groupControls, JsonRequestBehavior.AllowGet);
        //        }

        //        public ActionResult GetAllControls(int year)
        //        {
        //            IEnumerable<GroupControlViewModel> groupControls = null;
        //            try
        //            {
        //                groupControls = groupHelper.GetAllControls(year);
        //            }
        //            catch (NotFoundException)
        //            {
        //                return Json(new List<GroupControlViewModel>(), JsonRequestBehavior.AllowGet);
        //            }

        //            return Json(groupControls, JsonRequestBehavior.AllowGet);
        //        }

        //        public ActionResult GetYearGroups(int year)
        //        {
        //            IEnumerable<GroupDTO> groups = null;
        //            try
        //            {
        //                groups = groupHelper.GetYearGroups(year);
        //            }
        //            catch (NotFoundException)
        //            {
        //                return Json(new List<GroupDTO>(), JsonRequestBehavior.AllowGet);
        //            }
        //            return Json(groups, JsonRequestBehavior.AllowGet);
        //        }

        //        public ActionResult GetGroupAndYears(string year)
        //        {
        //            IEnumerable<GroupDTO> groups = new List<GroupDTO>();
        //            IEnumerable<string> years = new List<string>();
        //            try
        //            {
        //                groups = accountService.SearchGroup();
        //            }
        //            catch (NotFoundException)
        //            {
        //                years = new List<string>();

        //                return Json(new KeyValuePair<IEnumerable<string>, IEnumerable<GroupDTO>>(years, groups));
        //            }

        //            years = groups.Select(gr =>
        //            {
        //                var arr = gr.GroupId.Split('-');
        //                if (arr.Length != 3)
        //                {
        //                    return null;
        //                }
        //                else
        //                {
        //                    return "20" + arr[1];
        //                }
        //            }).Where(s => s != null).Distinct();

        //            groups = groups.Where(group =>
        //            {
        //                var groupSplit = group.GroupId.Split('-');
        //                if (groupSplit.Length != 3)
        //                {
        //                    return false;
        //                }
        //                else
        //                {
        //                    return year == "20" + groupSplit[1];
        //                }
        //            });

        //            return Json(new KeyValuePair<IEnumerable<string>, IEnumerable<GroupDTO>>(years, groups), JsonRequestBehavior.AllowGet);
        //        }

        //        public ActionResult CreateGroup()
        //        {
        //            IEnumerable<UserDTO> users = groupHelper.GetGroupCreateFillModel();
        //            return Json(users, JsonRequestBehavior.AllowGet);
        //        }

        //        [HttpPost]
        //        public ActionResult CreateGroup(string GroupNum, int? CaptainId)
        //        {
        //            try
        //            {
        //                accountService.CreateGroup(GroupNum, CaptainId.HasValue ? CaptainId.Value : -1);
        //                if (CaptainId.HasValue)
        //                {
        //                    accountService.AddStudentToGroup(CaptainId.Value, GroupNum);
        //                }
        //            }
        //            catch (AlreadyRegisteredException)
        //            {
        //                return Json(new { Title = "Ошибка", Text = "Такая группа уже существует." }, JsonRequestBehavior.AllowGet);
        //            }

        //            return Json(new { Title = "Успешное действие", Text = "Группа успешно создана" }, JsonRequestBehavior.AllowGet);
        //        }

        //        [HttpPost]
        //        public ActionResult DeleteGroup(string groupNum)
        //        {
        //            try
        //            {
        //                accountService.DeleteGroup(groupNum);
        //            }
        //            catch (NotFoundException)
        //            {
        //                return Json(new { Title = "Ошибка", Text = "Группа не найдена." }, JsonRequestBehavior.AllowGet);
        //            }

        //            return Json(new { Title = "Действие успешно выполнено", Text = "Группа успешно удалена" }, JsonRequestBehavior.AllowGet);
        //        }

        //        public ActionResult ChangeCaptain(string GroupNum)
        //        {
        //            Session["GroupNum"] = GroupNum;

        //            GroupDTO group = accountService.SearchGroup(GroupNum).FirstOrDefault();
        //            GroupEditViewModel model = new GroupEditViewModel();

        //            try
        //            {
        //                model = groupHelper.GetGroupEditFillModel(GroupNum);
        //            }
        //            catch (NotFoundException)
        //            {

        //            }

        //            return Json(model, JsonRequestBehavior.AllowGet);
        //        }

        //        [HttpPost]
        //        public ActionResult ChangeCaptain(int captainId)
        //        {
        //            string GroupNum = (string)Session["GroupNum"];
        //            Session.Remove("GroupNum");

        //            try
        //            {
        //                accountService.ChangeCaptain(GroupNum, captainId);
        //            }
        //            catch (GroupNotFoundException)
        //            {
        //                return Json(new { Title = "Ошибка", Text = "Не найдена группа" }, JsonRequestBehavior.AllowGet);
        //            }
        //            catch (NotFoundException)
        //            {
        //                return Json(new { Title = "Ошибка", Text = "Не найден пользователь" }, JsonRequestBehavior.AllowGet);
        //            }

        //            return Json(new { Title = "Действие успешно выполнено.", Text = "Староста группы успешно сменен." }, JsonRequestBehavior.AllowGet);
        //        }

        //        public ActionResult CreateStudent()
        //        {
        //            FillStudentViewModel model = new FillStudentViewModel();

        //            IEnumerable<UserDTO> candidates = accountService.SearchStudent().Where(s => s.GroupId == null).Select(s => accountService.SearchUser(s.UserId).FirstOrDefault());

        //            IEnumerable<GroupDTO> groups = accountService.SearchGroup();

        //            model.Groups = groups;

        //            model.Users = candidates;

        //            return PartialView(candidates);
        //        }

        //        [HttpPost]
        //        public ActionResult CreateStudent(string GroupNum, string FIO, string Mail)
        //        {
        //            string password;
        //            string login;
        //            try
        //            {
        //                password = Guid.NewGuid().ToString().Substring(0, 8);
        //                login = Guid.NewGuid().ToString().Substring(0, 8);
        //                accountService.Register(FIO, login, password, "Студент", Mail, "");
        //            }
        //            catch (AlreadyRegisteredException)
        //            {
        //                return Json(new { Title = "Ошибка", Text = "Такой пользователь уже существует" }, JsonRequestBehavior.AllowGet);
        //            }
        //            var user = accountService.SearchUser(FIO: FIO, Login: login, Password: password, Email: Mail).FirstOrDefault();
        //            accountService.CreateStudent(user.UserId, GroupNum);

        //            return Json(new { Title = "Действие выполнено успешно", Text = "Студент группы " + GroupNum + " успешно создан." }, JsonRequestBehavior.AllowGet);
        //        }

        //        [HttpPost]
        //        public ActionResult EditStudent(int userId, string GroupNum, string FIO, string Mail)
        //        {
        //            try
        //            {
        //                var student = accountService.SearchStudent(userId);
        //            }
        //            catch (NotFoundException)
        //            {
        //                return Json(new { Title = "Ошибка", Text = "Не найден пользователь." }, JsonRequestBehavior.AllowGet);
        //            }

        //            if (GroupNum != null)
        //            {
        //                accountService.DeleteStudentFromGroup(userId);
        //                accountService.AddStudentToGroup(userId, GroupNum);
        //            }

        //            accountService.EditUser(userId, FIO, email: Mail);

        //            return Json(new { Title = "Действие выполнено успешно", Text = "Студент успешно отредактирован." }, JsonRequestBehavior.AllowGet);

        //        }

        //        [HttpPost]
        //        public ActionResult DeleteStudent(string GroupNum, int userId)
        //        {
        //            try
        //            {
        //                accountService.DeleteStudentFromGroup(userId);
        //            }
        //            catch (NotFoundException)
        //            {
        //                return Json(new { Title = "Ошибка", Text = "Не найден студент" }, JsonRequestBehavior.AllowGet);
        //            }

        //            return Json(new { Title = "Действие выполнено успешно", Text = "Студент успешно удален" }, JsonRequestBehavior.AllowGet);
        //        }

        //        public ActionResult CreateGroupControl()
        //        {
        //            IEnumerable<WorkTypeDTO> workTypes = null;
        //            try
        //            {
        //                workTypes = controlService.SearchWorkType();
        //            }
        //            catch (NotFoundException)
        //            {
        //                return Json(new List<WorkTypeDTO>(), JsonRequestBehavior.AllowGet);
        //            }
        //            return Json(workTypes, JsonRequestBehavior.AllowGet);
        //        }

        //        [HttpPost]
        //        public ActionResult CreateGroupControl(string WorkType, string GroupNum, string Type, DateTime Date, string Place, int partId)
        //        {
        //            WorkTypeDTO workType = controlService.SearchWorkType(Name: WorkType).FirstOrDefault();

        //            try
        //            {
        //                controlService.CreateGroupControl(workType.WorkTypeId, GroupNum, Date, Type, Place, partId );
        //            }
        //            catch (AlreadyRegisteredException)
        //            {
        //                return Json(new { Title = "Ошибка", Text = "Такой групповый контроль уже существует." }, JsonRequestBehavior.AllowGet);
        //            }

        //            return Json(new { Title = "Успешное действие", Text = "Групповой контроль успешно создан." }, JsonRequestBehavior.AllowGet);
        //        }

        //        public ActionResult DeleteGroupControl(int id)
        //        {
        //            try
        //            {
        //                controlService.DeleteGroupControl(id);
        //            }
        //            catch (NotFoundException)
        //            {
        //                return Json(new { Title = "Ошибка", Text = "Групповой контроль уже удален" }, JsonRequestBehavior.AllowGet);
        //            }

        //            return Json(new { Title = "Действие выполнено упешно", Text = "Групповой контроль успешно удален" }, JsonRequestBehavior.AllowGet);
        //        }

        //        public ActionResult EditGroupControl(int groupControlId)
        //        {
        //            Session["GroupControlId"] = groupControlId;

        //            var workTypes = controlService.SearchWorkType();

        //            return Json(workTypes, JsonRequestBehavior.AllowGet);
        //        }

        //        [HttpPost]
        //        public ActionResult EditGroupControl(GroupControlViewModel model)
        //        {
        //            var workType = controlService.SearchWorkType(Name: model.WorkType).First();
        //            var groupControlId = (int)Session["GroupControlId"];
        //            Session.Remove("GroupControlId");

        //            try
        //            {
        //                controlService.UpdateGroupControl(groupControlId, worktypeId: workType.WorkTypeId, passDate: model.PassDate.Value, type: model.Type, place: model.Place, partId: model.PartId);
        //            }
        //            catch (NotFoundException)
        //            {
        //                return Json(new { Title = "Ошибка", Text = "Не найден контроль." }, JsonRequestBehavior.AllowGet);
        //            }

        //            return Json(new { Title = "Действие выполнено успешно", Text = "Групповой контроль успешно отредактирован." }, JsonRequestBehavior.AllowGet);
        //        }

        //        public ActionResult GenerateGroupExcelFile(string groupId)
        //        {
        //            var fileName = "";
        //            DirectoryInfo dir = new DirectoryInfo(Server.MapPath("~/Files/Excel"));

        //            fileName = Guid.NewGuid().ToString().Substring(0, 8) + ".xls";

        //            while (dir.GetFiles().Count(file => file.Name == fileName) > 0)
        //            {
        //                fileName = Guid.NewGuid().ToString().Substring(0, 8) + ".xls";
        //            }

        //            GroupDTO group = accountService.SearchGroup(groupId).LastOrDefault();

        //            IEnumerable<UserDTO> users = accountService.SearchStudent(GroupNum: groupId).Select(s => accountService.SearchUser(userId: s.UserId).SingleOrDefault());

        //            ExcelWorker.GenerateGroupStudentFile(Path.Combine(dir.FullName, fileName), new List<KeyValuePair<GroupDTO, IEnumerable<UserDTO>>>() { new KeyValuePair<GroupDTO, IEnumerable<UserDTO>>(group, users) });

        //            return Json("/Files/Excel/" + fileName, JsonRequestBehavior.AllowGet);
        //        }

        //        public ActionResult UploadGroupExcelFile(string groupId)
        //        {
        //            var fileContent = Request.Files["file"];
        //            if (fileContent != null && fileContent.ContentLength > 0)
        //            {
        //                // get a stream
        //                var stream = fileContent.InputStream;
        //                // and optionally write the file to disk
        //                var fileName = Guid.NewGuid().ToString().Substring(0, 9) + ".xls";
        //                while (Directory.EnumerateFiles(Server.MapPath("~/Files/Excel")).Contains(fileName))
        //                {
        //                    fileName = Guid.NewGuid().ToString().Substring(0, 9) + ".xls";
        //                }
        //                var path = Path.Combine(Server.MapPath("~/Files/Excel"), fileName);
        //                using (var fileStream = System.IO.File.Create(path))
        //                {
        //                    stream.CopyTo(fileStream);
        //                }

        //                IEnumerable<KeyValuePair<GroupDTO, IEnumerable<UserDTO>>> data = null;
        //                try
        //                {
        //                    data = ExcelWorker.ParseStudentFileContent(path);
        //                    groupHelper.ExecuteGroupChanges(data);
        //                }
        //                catch (ExcelFormatException e)
        //                {
        //                    return Json(new { Title = "Ошибка", Text = e.Message }, JsonRequestBehavior.AllowGet);
        //                }
        //                catch (ExcelDataException e)
        //                {
        //                    return Json(new { Title = "Ошибка", Text = e.Message }, JsonRequestBehavior.AllowGet);
        //                }

        //                return Json(new { Title = "Действие выполнено успешно", Text = "База синхронизирована с файлом" }, JsonRequestBehavior.AllowGet);
        //            }
        //            return Json(new { Title = "Ошибка", Text = "Файл отсутствует в запросе." }, JsonRequestBehavior.AllowGet);
        //        }

        //        public ActionResult GenerateControlExcelFile(string groupId)
        //        {
        //            var fileName = "";
        //            DirectoryInfo dir = new DirectoryInfo(Server.MapPath("~/Files/Excel"));

        //            fileName = Guid.NewGuid().ToString().Substring(0, 8) + ".xls";

        //            while (dir.GetFiles().Count(file => file.Name == fileName) > 0)
        //            {
        //                fileName = Guid.NewGuid().ToString().Substring(0, 8) + ".xls";
        //            }
        //            GroupDTO group = accountService.SearchGroup(groupId).SingleOrDefault();
        //            IEnumerable<GroupControlDTO> controls = controlService.SearchGroupControls(GroupNum: groupId);

        //            ExcelWorker.GenerateGroupControlFile(Path.Combine(Server.MapPath("~/Files/Excel"), fileName), group, controls);

        //            return Json("/Files/Excel/" + fileName, JsonRequestBehavior.AllowGet);
        //        }

        //        public ActionResult UploadControlExcelFile(string groupId)
        //        {
        //            var fileContent = Request.Files["file"];
        //            if (fileContent != null && fileContent.ContentLength > 0)
        //            {
        //                // get a stream
        //                var stream = fileContent.InputStream;
        //                // and optionally write the file to disk
        //                var fileName = Guid.NewGuid().ToString().Substring(0, 9) + ".xls";
        //                while (Directory.EnumerateFiles(Server.MapPath("~/Files/Excel")).Contains(fileName))
        //                {
        //                    fileName = Guid.NewGuid().ToString().Substring(0, 9) + ".xls";
        //                }
        //                var path = Path.Combine(Server.MapPath("~/Files/Excel"), fileName);
        //                using (var fileStream = System.IO.File.Create(path))
        //                {
        //                    stream.CopyTo(fileStream);
        //                }

        //                try
        //                {
        //                    var data = ExcelWorker.ParseGroupControlFile(path);
        //                    groupHelper.ExecuteControlChanges(data);
        //                }
        //                catch (ExcelFormatException e)
        //                {
        //                    return Json(new { Title = "Ошибка", Text = e.Message }, JsonRequestBehavior.AllowGet);
        //                }
        //                catch (ExcelDataException e)
        //                {
        //                    return Json(new { Title = "Ошибка", Text = e.Message }, JsonRequestBehavior.AllowGet);
        //                }


        //                return Json(new { Title = "Действие выполнено успешно", Text = "База синхронизирована с файлом" }, JsonRequestBehavior.AllowGet);
        //            }

        //            return Json(new { Title = "Ошибка", Text = "Файл не обнаружен." }, JsonRequestBehavior.AllowGet);
        //        }

    }
}