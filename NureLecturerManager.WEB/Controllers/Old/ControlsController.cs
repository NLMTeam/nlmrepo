﻿using NLM.BLL.DTO;
using NLM.BLL.Exceptions;
using NLM.BLL.Interfaces;
using NLM.WEB.Filters;
using NLM.WEB.Filters.MVC;
using NLM.WEB.Models.ControlsPage.Controls;
using NLM.WEB.Models.ControlsPage.Marks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NLM.WEB.Controllers
{
    [OnExceptionFilter]
    [AuthenticationFilter]
    public class ControlsController : Controller
    {
        private IDisciplineService discService = DependencyResolver.Current.GetService<IDisciplineService>();
        private IAccountService accountService = DependencyResolver.Current.GetService<IAccountService>();
        private IControlService controlService = DependencyResolver.Current.GetService<IControlService>();
        private IControlPointService controlPointService = DependencyResolver.Current.GetService<IControlPointService>();

        private const string ADMIN_TYPE = "ADMIN";
        private const string LECTURER_TYPE = "LECTURER";
        private const string STUDENT_TYPE = "STUDENT";

        #region RouteActions

        public ActionResult Index()
        {
            var user = (UserDTO)Session["User"];
            Session["Placement"] = "Controls";

            if (user == null)
            {
                return RedirectToAction("Login", "Account");
            }

            if (user.Type == STUDENT_TYPE)
            {
                return RedirectToAction("StudentIndex");
            }

            if (user.Type == LECTURER_TYPE || user.Type == ADMIN_TYPE)
            {
                return RedirectToAction("LecturerAdminIndex");
            }

            throw new NotExpectedUserTypeException();
        }

        public ActionResult StudentIndex()
        {
            return View();
        }

        public ActionResult LecturerAdminIndex()
        {
            return View();
        }

        #endregion

        #region Group Controls CUD
        // Returns Id
        public ActionResult CreateGroupControl(GroupControlCreateViewModel model)
        {
            // 1. Check permissions
            HttpStatusCodeResult permissionRes = CheckPermissionForGroupControl(model.DisciplineId, model.Year, model.Semester);

            if (permissionRes != null)
            {
                return permissionRes;
            }

            // 2. Create group control
            bool createRes = controlService.CreateGroupControl(model.WorkTypeId, model.GroupNumber,
                                                                   model.Type, model.PassPlace, model.PassDate);
            if (!createRes)
            {
                // angular: Такой контроль уже существует
                return Json(-1);
            }

            // 3. Get created group control and return it's id
            GroupControlDTO newGC = controlService.SearchGroupControls(workTypeId: model.WorkTypeId, GroupNum: model.GroupNumber,
                                                                            Type: model.Type, Place: model.PassPlace, pass: model.PassDate).LastOrDefault();

            if (newGC == null)
            {
                throw new SomethingWithDBException("Контроль не отобразился в БД.");
            }

            return Json(newGC.GroupControlId);
        }

        public ActionResult EditGroupControl(GroupControlEditViewModel model)
        {
            // 1. CheckPermissions
            HttpStatusCodeResult permissionRes = CheckPermissionForGroupControl(model.DisciplineId, model.Year, model.Semester);

            if (permissionRes != null)
            {
                return permissionRes;
            }

            // 2. Edit group control
            bool editRes = controlService.UpdateGroupControl(model.GroupControlId, model.WorkTypeId, null, model.PassDate,
                                                                model.Type, model.PassPlace);

            // angular: Контроль был успешно отредактирован:: перезапись значений на клиенте
            // angular: Кто-то удалил редактируемый Вами контроль
            return Json(editRes);
        }

        public ActionResult DeleteGroupControl(Int32 id, Int32 disciplineId, String year, Int32 semester)
        {
            // 1. Check permissions
            HttpStatusCodeResult permissionRes = CheckPermissionForGroupControl(disciplineId, year, semester);

            if (permissionRes != null)
            {
                return permissionRes;
            }

            bool delRes = controlService.DeleteGroupControl(id);

            // angular: Контроль успешно удалён
            // angular: Контроль уже был удалён кем-то
            return Json(delRes);
        }

        #endregion

        #region Marks CUD
        public ActionResult SaveMarks(ChangedMarksViewModel model)
        {
            // Permissions are on BLL
            Int32 userId = ((UserDTO)Session["User"]).UserId;
            Int32 deniedControls = controlService.UpdateMarksList(model.StudentControlsMarks, model.DFYId, userId);
            Int32 deniedCpms = controlPointService.UpdateMarksList(model.ControlPointMarks, userId);

            return Json(new { deniedControls, deniedCpms });
        }

        #endregion
        #region Permissions methods
        [NonAction]
        private HttpStatusCodeResult CheckPermissionForGroupControl(Int32 disciplineId, string year, Int32 semester)
        {
            // 1. Check permissions
            UserDTO user = (UserDTO)Session["User"];
            if (user == null)
            {
                return new HttpStatusCodeResult(401);
            }

            if (user.Type == STUDENT_TYPE)
            {
                return new HttpStatusCodeResult(403);
            }

            // Добавлять групповые контроли к дисциплине и группе может только лектор за указанный год
            if (!discService.IsAdminForDiscipline(disciplineId, user.UserId, year, semester))
            {
                return new HttpStatusCodeResult(403);
            }

            return null;
        }
        #endregion
    }
}