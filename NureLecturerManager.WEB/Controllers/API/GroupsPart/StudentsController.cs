﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using NLM.BLL.DTO;
using NLM.BLL.Infrastructure;
using NLM.BLL.Interfaces;
using NLM.WEB.Models.Groups;
using AutoMapper;

namespace NLM.WEB.Controllers.API
{
    public class StudentsController : ApiController
    {
        private IStudentService _studentService = DependencyResolver.Current.GetService<IStudentService>();
        private IMapper _mapper;

        public StudentsController()
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserViewModel, UserDTO>();
            });

            _mapper = config.CreateMapper();
        }

        [System.Web.Http.HttpGet]
        public IEnumerable<StudentDTO> GetStudents()
        {
            return _studentService.GetStudents();
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult CreateStudent(UserViewModel user, string groupName)
        {
            if (String.IsNullOrWhiteSpace(groupName))
            {
                ModelState.AddModelError("GroupName", "Groupname mustn't be empty or whitespace");
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            CUDStatus result = _studentService.CreateStudent(_mapper.Map<UserDTO>(user), groupName);

            if (result.Status == ActionStatus.Ok)
            {
                StudentDTO student = _studentService.FindStudents(u => u.Login == user.Login && u.Mail == user.Mail).LastOrDefault();
                return Created("api/students/" + student.UserId, student);
            }
            else
            {
                return BadRequest(result.Message);
            }
        }

        [System.Web.Http.HttpPatch]
        public IHttpActionResult UpdateStudent(UserViewModel user, string groupName)
        {
            if (String.IsNullOrWhiteSpace(groupName))
            {
                ModelState.AddModelError("GroupName", "Groupname mustn't be empty or whitespace");
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            CUDStatus result = _studentService.UpdateStudent(_mapper.Map<UserDTO>(user), groupName);

            if (result.Status != ActionStatus.Ok)
            {
                return StatusCode(HttpStatusCode.OK);
            }
            else
            {
                return BadRequest(result.Message);
            }
        }

        [System.Web.Http.HttpDelete]
        public IHttpActionResult DeleteStudent(int studentId)
        {
            if (studentId <=0 )
            {
                ModelState.AddModelError("StudentId", "Invalid student id");
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            CUDStatus result = _studentService.DeleteStudent(studentId);

            if (result.Status == ActionStatus.Ok)
            {
                return StatusCode(HttpStatusCode.NoContent);
            }
            else
            {
                return BadRequest(result.Message);
            }
        }
    }
}
