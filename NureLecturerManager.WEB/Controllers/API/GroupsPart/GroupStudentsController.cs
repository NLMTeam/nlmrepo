﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using NLM.BLL.Interfaces;
using NLM.BLL.DTO;
using NLM.WEB.Models.Groups;
using AutoMapper;

namespace NLM.WEB.Controllers.API.GroupsPart
{
    public class GroupStudentsController : ApiController
    {
        private IGroupService _groupService = DependencyResolver.Current.GetService<IGroupService>();
        private IMapper _mapper;

        public GroupStudentsController()
        {
            MapperConfiguration config = new MapperConfiguration(cfg => 
            {
                cfg.CreateMap<UserDTO, StudentViewModel>();
            });

            _mapper = config.CreateMapper();
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/groups/studentsOfGroup")]
        public IEnumerable<StudentViewModel> GetStudentsOfGroup(string name)
        {
            if (String.IsNullOrWhiteSpace(name))
            {
                return null;
            }

            return _groupService.GetGroupStudents(name).Select(_mapper.Map<StudentViewModel>);
        }
    }
}
