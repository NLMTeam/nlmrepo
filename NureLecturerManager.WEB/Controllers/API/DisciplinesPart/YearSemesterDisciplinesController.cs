﻿using AutoMapper;
using NLM.BLL.DTO.DisciplinePart;
using NLM.BLL.DTObjects;
using NLM.BLL.Interfaces;
using NLM.WEB.Models.Disciplines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Mvc;

namespace NLM.WEB.Controllers.API.DisciplinesPart
{
    // [AuthorizedFilter]
    public class YearSemesterDisciplinesController : ApiController
    {
        private const String ADMIN_ROLE = "ADMIN";
        private const String LECTURER_ROLE = "LECTURER";
        private const String STUDENT_ROLE = "STUDENT";

        private IDisciplineService _discService = DependencyResolver.Current.GetService<IDisciplineService>();
        private IAccountService _accountService = DependencyResolver.Current.GetService<IAccountService>();
        private IMapper _mapper;

        public YearSemesterDisciplinesController()
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<DisciplineForYearLightDTO, DFYVM>();

                cfg.CreateMap<DisciplineForYearDTO, DFYVM>();

            });

            _mapper = config.CreateMapper();
        }

        [System.Web.Http.HttpGet]
        public List<DFYVM> GetDiscipline(String year, Int32 semester)
        {
            //CookieHeaderValue cookie = Request.Headers.GetCookies("pass3").FirstOrDefault();
            //byte[] token = _accountService.DecodeToken(cookie["pass3"].Value);
            //String role = this.GetUserRole(token);

            String role = LECTURER_ROLE;
            var res = new List<DFYVM>();
            switch (role)
            {
                case STUDENT_ROLE:
                    //String groupName = _accountService.GetUserGroupNameByToken(token);
                    String groupName = "БИКС-14-3";
                    res = _mapper.Map<List<DFYVM>>(_discService
                                                                .SearchDFYsForYearAndGroup(year, semester, groupName));

                    res.ForEach(t => t.CanActivate = false);
                    return res;

                case LECTURER_ROLE:
                    //Int32 id = _accountService.GetUserIdByToken(token);
                    Int32 id = 2;
                    res = _mapper.Map<List<DFYVM>>(_discService
                                                            .SearchDFYsForYearAndLecturer(year, semester, id));

                    res.ForEach(t =>
                    {
                        if (t.Lecturers.Any(l => l.Id == id))
                        {
                            t.CanActivate = true;
                        }
                        else
                        {
                            t.CanActivate = false;
                        }
                    }
                    );

                    return res;

                case ADMIN_ROLE:
                    res = _mapper.Map<List<DFYVM>>(_discService
                                                                .GetDFYs()).ToList();

                    res.ForEach(t => t.CanActivate = true);
                    return res;
                    

                default:
                    throw new UnauthorizedAccessException();
            }
        }

        [System.Web.Http.NonAction]
        private String GetUserRole(byte[] token)
        {
            if (token != null)
            {
                String role = _accountService.GetRoleByToken(token);
                return role;
            }
            else
            {
                return null;
            }
        }
    }
}
