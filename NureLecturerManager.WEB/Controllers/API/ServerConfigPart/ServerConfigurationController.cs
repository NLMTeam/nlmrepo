﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NLM.WEB.Controllers.API.ServerConfigPart
{
    public class ServerConfigurationController : ApiController
    {
        internal static object threadLock = new object();

        [HttpGet]
        public Tuple<String, String> GetConfig()
        {
            // Getting current year and semester

            String year, semester;
            lock (ServerConfigurationController.threadLock)
            {
                using (StreamReader stR = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("/Files/Config.txt")))
                {
                    year = stR.ReadLine();
                    semester = stR.ReadLine();
                }
            }

            return new Tuple<String, String>(year, semester);
        }
    }
}
