﻿function bitEncode(name, key) {
    var toEncode = key + name;
    if (typeof (key) == typeof ("")) {
        var length = key.length;

        var encoded = "";
        for (var i in name) {
            encoded += String.fromCharCode(name.charCodeAt(i) ^ length);
        }

        return encoded;
    }
    else {
        throw new Error('Invalid data type of "key"');
    }
}