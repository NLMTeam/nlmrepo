﻿lecturersApp.factory('selectedLecturer', function ($rootScope) {
    var lecturer = {
        lecturerBody: null
    };

    return {
        getSelectedLecturer: function(){
            return lecturer;
        },

        setSelectedLecturer: function (value) {
            lecturer.lecturerBody = value;
        }
    };
});