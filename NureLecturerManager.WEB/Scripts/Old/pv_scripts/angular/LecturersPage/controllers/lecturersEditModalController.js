﻿lecturerApp.controller('lecturersEditModalController', function ($scope, $http, lecturersModel, selectedLecturer, responseService, editLecturerService) {

    

    $scope.editLecturer = editLecturerService.getEditLecturer();
    
    $scope.save = function (e) {
        e.preventDefault();
        $('#edit-modal').modal('hide');
        $http.post('/Lecturers/EditLecturer', {
            User:
                { UserId: $scope.editLecturer.User.UserId, FIO: $scope.editLecturer.User.FIO },
            Lecturer: {
                UserId: $scope.editLecturer.User.UserId,
                Post: $scope.editLecturer.Post,
                Department: $scope.editLecturer.Department,
                Specialization: $scope.editLecturer.Spec
            }
        }).success(function (data) {
            responseService.setResponse(data);
            $('#response-modal').modal('show');

            if (data == 'Информация о преподавателе успешно отредактирована.') {
                var lectCol = lecturersModel.getLecturers();

                for (var i = 0; i < lectCol.length; i++) {
                    if (lectCol[i].User.UserId == $scope.editLecturer.User.UserId) {
                        lectCol[i].User.FIO = $scope.editLecturer.User.FIO;
                        lectCol[i].Post = $scope.editLecturer.Post;
                        lectCol[i].Department = $scope.editLecturer.Department;
                        lectCol[i].Specialization = $scope.editLecturer.Spec;
                        break;
                    }
                }
            }
        })
        .error(function (err) {
            responseService.setResponse(data);
            $('#response-modal').modal('show');
        });
        
    }
});
