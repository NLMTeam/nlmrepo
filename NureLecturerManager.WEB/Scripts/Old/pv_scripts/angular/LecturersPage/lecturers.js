﻿$(document).ready(function () {
    //window.heightAlign();

    $('#response-modal').on('shown.bs.modal', function () {
        $('#response-ok').focus();
    });

    $('#add-modal').on('shown.bs.modal', function () {
        $('#add-fio').focus();
    });

    $('#del-modal').on('shown.bs.modal', function () {
        $('#del-cancel-btn').focus();
    });

    $('#edit-modal').on('shown.bs.modal', function () {
        $('#edit-btn').focus();
    });

    $(window).resize(function() {
        $('#second-nav').width($('#lect-div').width());
    });

    $('#second-nav').width($('#lect-div').width());
});