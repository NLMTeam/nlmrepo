﻿/// <reference path="literatureItem.js" />
literatureApp.directive('literature', function (selectedLiterature, selectedDiscipline, literatureModel) {
    return {
        link: function (scope, element, attrs) {
            scope.select = function () {
                // For test for real changing
                var lit = selectedLiterature.getLiterature().body;

                // Find new selected discipline and select it
                var id = scope.literature.LiteratureId;

                if (lit == null || lit.LiteratureId != id) {

                    var model = literatureModel.getLiterature();

                    var selDisc = selectedDiscipline.getDiscipline().body;

                    for (var i = 0; i < model.Literature[selDisc.DisciplineId].length; i++) {


                        if (id == model.Literature[selDisc.DisciplineId][i].LiteratureId) {
                            selectedLiterature.setLiterature(model.Literature[selDisc.DisciplineId][i]);
                        }
                    }
                }
            }; 
        },
        restrict: 'E',
        template:
                '<li ng-click="select($event)" class="cursor-pointer lit-item ng-class: literature.activity" ng-bind="literature.Name"></li>',
        scope: {
            literature: '='
        }
    };
});