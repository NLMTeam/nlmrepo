﻿literatureApp.directive('discipline', function (selectedLiterature, selectedDiscipline, literatureModel) {
    return {
        link: function (scope, element, attrs) {
            scope.select = function (e) {
                e.preventDefault();
                // Find old selected discipline and deselect it
                var disc = selectedDiscipline.getDiscipline();

                // Find new selected discipline and select it
                var id = scope.discipline.DisciplineId;

                if (id != disc.body.DisciplineId) {
                    var model = literatureModel.getLiterature();

                    for (var i = 0; i < model.Disciplines.length; i++) {
                        if (id == model.Disciplines[i].DisciplineId) {
                            selectedDiscipline.setDiscipline(model.Disciplines[i]);
                            selectedLiterature.setLiterature(null);
                            break;
                        }
                    }
                }
            };

        },
        restrict: 'E',
        templateUrl: '/Scripts/pv_scripts/angular/LiteraturePage/Lecturer/directives/discipline.html',
        scope: {
            discipline: '='
        }
    };
});