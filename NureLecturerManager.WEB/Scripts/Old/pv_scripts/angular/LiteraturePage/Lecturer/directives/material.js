﻿literatureApp.directive('material', function (literatureModel, materialId, editMaterial, selectedDiscipline, tracking) {
    return {
        link: function (scope, element, attrs) {
            scope.IsAdmin = selectedDiscipline.getDiscipline().body.IsAdmin;
            scope.tracking = tracking.value;

            scope.delete = function () {
                scope.IsAdmin = selectedDiscipline.getDiscipline().body.IsAdmin;
                scope.tracking = tracking.value;

                if (!scope.IsAdmin || scope.tracking == 'past') {
                    return;
                }

                var id = scope.material.Id;

                materialId.Id = id;
                $('#mat-del-modal').modal('show');
            };

            scope.edit = function () {

                if (!scope.IsAdmin || scope.tracking == 'past') {
                    return;
                }

                var id = scope.material.Id;

                materialId.Id = id;

                // Deleting extension from model
                var splittedName = scope.material.Type.split('.');
                splittedName.splice(splittedName.length - 1, 1);
                splittedName = splittedName.join('.');

                editMaterial.Name = splittedName;
                $('#mat-edit-modal').modal('show');
            };
            $('[data-toggle="tooltip"]').tooltip();
        },
        restrict: 'E',
        templateUrl: '/Scripts/pv_scripts/angular/LiteraturePage/Lecturer/directives/material.html',
        scope: {
            material: '='
        }
    };
});