﻿literatureApp.directive('material', function (literatureModel) {
    return {
        link: function(scope, element, attrs){
            $('[data-toggle="tooltip"]').tooltip();
        },
        restrict: 'E',
        templateUrl: '/Scripts/pv_scripts/angular/LiteraturePage/Student/directives/material.html',
        scope: {
            material: '='
        }
    };
});