﻿literatureApp.factory('currentLiterature', function ($http, responseService) {
    currModel = null;

    return {
        getModel: function () {
            if (currModel == undefined || currModel == null)
                return $http.get('/Literature/GetStudentsLiterature').success(function (data) {
                    currModel = data;
                })
                .error(function (err) {
                    responseService.setResponse("Проблемы с соединением. Исправьте их и перезагрузите страницу.");
                    $('#response-modal').modal('show');
                });

            else {
                return currModel;
            }
        }
    };
});




