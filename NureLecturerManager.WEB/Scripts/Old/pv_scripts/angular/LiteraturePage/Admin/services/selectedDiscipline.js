﻿literatureApp.factory('selectedDiscipline', function () {
    discipline = {body : null};

    return {
        getDiscipline: function () {
            return discipline;                                                                                                                      
        },

        setDiscipline: function (value) {

            // Deselecting old value
            if (discipline.body != null) {
                discipline.body.activity = "";
            }
            
            // Changing current value
            discipline.body = value;
            discipline.body.activity = "active";
        }
    }
});