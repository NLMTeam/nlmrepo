﻿literatureApp.controller('literatureMainController', function ($scope, $http, literatureModel, selectedDiscipline, selectedLiterature, editLiterature, responseService) {

    $scope.ready = false;
    $scope.response = responseService.getResponse();

    var literature = literatureModel.getLiterature();

    if (typeof (literature.success) != "undefined") {
        literature.success(function (data) {
            $scope.model = data;

            selectedDiscipline.setDiscipline($scope.model.Disciplines.length > 0 ? $scope.model.Disciplines[0] : null);
            $scope.currentDiscipline = selectedDiscipline.getDiscipline();

            selectedLiterature.setLiterature(null);
            $scope.selectedLiterature = selectedLiterature.getLiterature();
        });
    }
    else {
        $scope.model = literature;

        selectedDiscipline.setDiscipline($scope.model.Disciplines.length > 0 ? $scope.model.Disciplines[0] : null);
        $scope.currentDiscipline = selectedDiscipline.getDiscipline();

        selectedLiterature.setLiterature(null);
        $scope.selectedLiterature = selectedLiterature.getLiterature();
    }
    
    $scope.ready = true;

    $scope.editClick = function (e) {
        e.preventDefault();
        if (selectedLiterature.getLiterature().body == null)
            return;
        editLiterature.setEditLiterature({
            DisciplineId: $scope.selectedLiterature.body.DisciplineId,
            LiteratureId: $scope.selectedLiterature.body.LiteratureId,
            Name: $scope.selectedLiterature.body.Name,
        });

        $('#edit-modal').modal();
    };

    $scope.getTxt = function (e) {
        e.preventDefault();

        if ($scope.model.Literature[$scope.currentDiscipline.body.DisciplineId] != null &&
            $scope.model.Literature[$scope.currentDiscipline.body.DisciplineId] != undefined &&
            $scope.model.Literature[$scope.currentDiscipline.body.DisciplineId].length > 0) {
            var str = '';
            for (var i = 0; i < $scope.model.Literature[$scope.currentDiscipline.body.DisciplineId].length; i++)
                str += $scope.model.Literature[$scope.currentDiscipline.body.DisciplineId][i].Name.trim() + '*8*--*-_*';

            window.open('/Literature/CheckBooksForEmpty?data=' + str, '_blank');
        } else {
            //responseService.setResponse("Список литературы по даной дисциплине пуст.");
            //$('#response-modal').modal();
            return;
        }
    };

    $scope.addMaterialModal = function () {
        $('#mat-add-modal').modal('show');
    };

    $scope.addLitModal = function (e) {
        e.preventDefault();
        if (selectedDiscipline.getDiscipline().body == null)
            return;
        $('#add-modal').modal();
    };

    $scope.delLitModal = function (e) {
        e.preventDefault();
        if (selectedLiterature.getLiterature().body == null)
            return;
        $('#del-modal').modal();
    };

    //$('#ed-lit-
    $scope.loadLitModal = function (e) {
        e.preventDefault();
        if (selectedDiscipline.getDiscipline().body == null)
            return;
        $('#load-lit-modal').modal();
    };
});

