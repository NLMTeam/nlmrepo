﻿literatureApp.controller('litTxtLoadController', function ($scope, $http, literatureModel, FileUploader, selectedDiscipline, responseService) {

    // All about file create litUploader
    var litUploader = $scope.litUploader = new FileUploader({
        url: '/Literature/LoadLiteratureFromFile'
    });

    // FILTERS

    // Queue size filter
    litUploader.filters.push({
        name: 'queueSizeFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 2;
        }
    });

    // Extension filters
    litUploader.filters.push({
        name: 'extensionFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            var arr = item.name.split('.');
            var extension = arr[arr.length - 1];
            return extension == 'txt';
        }
    });

    // File size filter
    litUploader.filters.push({
        name: 'fileSizeFilter',
        fn: function (item, options) {
            return item.size <= 10240 * 1024;
        }
    });

    // File exists filter
    litUploader.filters.push({
        name: 'fileExists',
        fn: function (item, options) {
            return item.size > 0;
        }
    });

    // CALLBACKS

    litUploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        if (filter.name == 'queueSizeFilter') {
            responseService.setResponse('Множественная загрузка запрещена. Перезагрузите страницу и попробуйте ещё раз.');
            $('#response-modal').modal();
        }

        if (filter.name == 'extensionFilter') {
            responseService.setResponse('Был выбран файл с форматом, который не поддерживается. Повторите попытку, но с учётом допустимых расширений файлов.');
            $('#response-modal').modal();
        }

        if (filter.name == 'fileSizeFilter') {
            responseService.setResponse('Разрешена загрузка файлов, размер которых не превышает 10 Мб. Повторите попытку с учётом этого ограничения.');
            $('#response-modal').modal();
        }

        if (filter.name == 'fileExists') {
            responseService.setResponse('Отправка ярлыков и повреждённых файлов запрещена.');
            $('#response-modal').modal();
        }
    };

    litUploader.onAfterAddingFile = function(fileItem) {
        if (litUploader.queue.length > 1)
            litUploader.queue.splice(0, 1);
    };
    litUploader.onAfterAddingAll = function(addedFileItems) {
    };
    litUploader.onBeforeUploadItem = function (item) {
        item.formData.push({ DisciplineId:  selectedDiscipline.getDiscipline().body.DisciplineId });
    };
    litUploader.onProgressItem = function(fileItem, progress) {
    };
    litUploader.onProgressAll = function(progress) {
    };
    litUploader.onSuccessItem = function(fileItem, response, status, headers) {
        if (response[0] != undefined && response[0].LiteratureId != undefined) {
            responseService.setResponse('Список литературы успешно загружен.');
            $('#response-modal').modal();

            literatureModel.setLiterature(selectedDiscipline.getDiscipline().body.DisciplineId, response);
        }
        else {
            responseService.setResponse(response);
            $('#response-modal').modal();
        }
    };
    litUploader.onErrorItem = function(fileItem, response, status, headers) {
        responseService.setResponse(response);
        $('#response-modal').modal();
    };
    litUploader.onCancelItem = function(fileItem, response, status, headers) {
    };
    litUploader.onCompleteItem = function(fileItem, response, status, headers) {
    };
    litUploader.onCompleteAll = function() {
        $('#load-lit-modal').modal('hide');
        litUploader.queue.splice(0, 1);
    };

    $scope.checkLitTxtFile = function (e) {
        e.preventDefault();
        var currentDisc = selectedDiscipline.getDiscipline().body;
        if (currentDisc == null)
            return;

        var model = literatureModel.getLiterature();

        if (model.Literature[currentDisc.DisciplineId] != null &&
            model.Literature[currentDisc.DisciplineId] != undefined &&
            model.Literature[currentDisc.DisciplineId].length > 0) {

            var arr = [];

            for (var i = 0; i < model.Literature[currentDisc.DisciplineId].length; i++)
                arr.push(model.Literature[currentDisc.DisciplineId][i].Name);
            window.open('/Literature/CheckBooksForEmpty?data=' + arr, '_blank');
        }

        litUploader.uploadAll();
    }
});
