﻿literatureApp.directive('material', function (literatureModel, materialId, editMaterial) {
    return {
        link: function (scope, element, attrs) {
            scope.delete = function () {
                var id = scope.material.Id;

                materialId.Id = id;
                $('#mat-del-modal').modal('show');
            };

            scope.edit = function () {
                var id = scope.material.Id;

                materialId.Id = id;

                // Deleting extension from model
                var splittedName = scope.material.Type.split('.');
                splittedName.splice(splittedName.length - 1, 1);
                splittedName = splittedName.join('.');

                editMaterial.Name = splittedName;
                $('#mat-edit-modal').modal('show');
            };
            $('[data-toggle="tooltip"]').tooltip();
        },
        restrict: 'E',
        templateUrl: '/Scripts/pv_scripts/angular/LiteraturePage/Admin/directives/material.html',
        scope: {
            material: '='
        }
    };
});