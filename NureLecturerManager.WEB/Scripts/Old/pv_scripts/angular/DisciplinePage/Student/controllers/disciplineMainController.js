﻿disciplinesApp.controller('disciplineMainController', function ($scope,
                            selectedDiscipline, selectedDFY,
                            selectedCP, responseService, disciplinePageModel, archievedDisciplines, currentDisciplines) {


    $scope.ready = false;

    var modelInit = currentDisciplines.getModel();

    var selectInit = function () {
        selectedDiscipline.setDiscipline($scope.model.Disciplines[0]);
        $scope.currentDiscipline = selectedDiscipline.getDiscipline();

        if ($scope.currentDiscipline.body != null && $scope.model.DFYs[$scope.currentDiscipline.body.DisciplineId] != undefined &&
                $scope.model.DFYs[$scope.currentDiscipline.body.DisciplineId].length != 0) {

            selectedDFY.setDFY($scope.model.DFYs[$scope.currentDiscipline.body.DisciplineId][0]);
            $scope.currentDFY = selectedDFY.getDFY();

            if ($scope.model.ControlPoints[$scope.currentDFY.body.DFYId] &&
                $scope.model.ControlPoints[$scope.currentDFY.body.DFYId].length != 0) {

                selectedCP.setCP($scope.model.ControlPoints[$scope.currentDFY.body.DFYId][0]);
            }
            else {
                selectedCP.setCP(null);
            }
            $scope.currentCP = selectedCP.getCP();
        }
        else {
            selectedDFY.setDFY(null);
            $scope.currentDFY = selectedDFY.getDFY();
        }
        $scope.currentCP = selectedCP.getCP();
        $scope.currentDFY = selectedDFY.getDFY();
    }

    $scope.current = true;
    if (typeof (modelInit.success) != "undefined") {
        modelInit.success(function (data) {
            $scope.model = data;
            disciplinePageModel.setModel(data);

            // selection services initialization
            if ($scope.model.Disciplines.length > 0) {
                selectInit();
            }
            else {
                selectedDiscipline.setDiscipline(null);
                $scope.currentDiscipline = selectedDiscipline.getDiscipline();
            }

            $scope.ready = true;
        });
    }
    else {
        $scope.model = modelInit;
        disciplinePageModel.setModel(modelInit);

        //selectedDiscipline.setDiscipline($scope.model.Disciplines.length > 0 ? $scope.model.Disciplines[0] : null);
        //$scope.currentDiscipline = selectedDiscipline.getDiscipline();
        selectInit();
    }

    $scope.response = responseService.getResponse();


    $scope.getArchievedModel = function (e) {
        $scope.current = false;

        var model = archievedDisciplines.getModel();
        if (typeof (model.success) != "undefined") {
            model.success(function (data) {
                $scope.model = data;
                disciplinePageModel.setModel(data);

                // selection services initialization
                if ($scope.model.Disciplines.length > 0) {
                    selectInit();
                }

                else {
                    selectedDiscipline.setDiscipline(null);
                    selectInit();
                }

            });
        }
        else {
            $scope.model = model;
            disciplinePageModel.setModel(model);

            //selectedDiscipline.setDiscipline($scope.model.Disciplines.length > 0 ? $scope.model.Disciplines[0] : null);
            //$scope.currentDiscipline = selectedDiscipline.getDiscipline();
            selectInit();
        }
    }

    $scope.getCurrentModel = function () {
        $scope.current = true;

        var model = currentDisciplines.getModel();
        if (typeof (model.success) != "undefined") {
            model.success(function (data) {
                $scope.model = data;
                disciplinePageModel.setModel(data);

                // selection services initialization
                if ($scope.model.Disciplines.length > 0) {
                    selectInit();
                }
                else {
                    selectedDiscipline.setDiscipline(null);
                    $scope.currentDiscipline = selectedDiscipline.getDiscipline();
                }
            });
        }
        else {
            $scope.model = model;
            disciplinePageModel.setModel(model);

            //selectedDiscipline.setDiscipline($scope.model.Disciplines.length > 0 ? $scope.model.Disciplines[0] : null);
            //$scope.currentDiscipline = selectedDiscipline.getDiscipline();
            selectInit();
        }
    }
});