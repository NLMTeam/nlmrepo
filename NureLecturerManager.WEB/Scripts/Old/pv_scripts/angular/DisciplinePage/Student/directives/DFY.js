﻿disciplinesApp.directive('dfy', function (disciplinePageModel, selectedDiscipline, selectedDFY, selectedCP) {
    return {
        link: function (scope, element, attrs) {
            scope.select = function (e) {
                e.preventDefault();
                var DFY = selectedDFY.getDFY();

                // Find new selected DFY and select it
                var id = scope.dfy.DFYId;
                if (DFY.body == null || id != DFY.body.DFYId) {
                    var model = disciplinePageModel.getModel();
                    var disc = selectedDiscipline.getDiscipline().body;

                    for (var i = 0; i < model.DFYs[disc.DisciplineId].length; i++) {
                        if (id == model.DFYs[disc.DisciplineId][i].DFYId) {
                            selectedDFY.setDFY(model.DFYs[disc.DisciplineId][i]);
                            selectedCP.setCP(null);
                            break;
                        }
                    }
                }
            };

        },
        restrict: 'E',
        templateUrl: '/Scripts/pv_scripts/angular/DisciplinePage/Student/directives/dfy.html',
        scope: {
            dfy: '=',
            index: '='
        }
    };
});