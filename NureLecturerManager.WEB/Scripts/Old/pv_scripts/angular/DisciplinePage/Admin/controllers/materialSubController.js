﻿/// <reference path="materialSubController.js" />
disciplinesApp.controller('materialSubController', function ($scope, $http, disciplinePageModel, responseService, workTypeId,
                                                             materialId, FileUploader, editMaterial) {
    // All about file create addUploader
    var addUploader = $scope.addUploader = new FileUploader({
        url: '/Discipline/CreateMaterial'
    });

    // FILTERS

    // Queue size filter
    addUploader.filters.push({
        name: 'queueSizeFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 2;
        }
    });

    // Extension filters
    addUploader.filters.push({
        name: 'extensionFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            var arr = item.name.split('.');
            var extension = arr[arr.length - 1];
            return extension == 'txt' || extension == 'pdf' ||
                    extension == 'xls' || extension == 'xlsx' ||
                    extension == 'ppt' || extension == 'pptx' ||
                    extension == 'pps' || extension == 'ppsx' ||
                    extension == 'doc' || extension == 'docx' ||
                    extension == 'rtf' || extension == 'zip';
        }
    });

    // File size filter
    addUploader.filters.push({
        name: 'fileSizeFilter',
        fn: function (item, options) {
            return item.size <= 10240 * 1024;
        }
    });

    // File exists filter
    addUploader.filters.push({
        name: 'fileExists',
        fn: function (item, options) {
            return item.size > 0;
        }
    });

    // CALLBACKS

    addUploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        if (filter.name == 'queueSizeFilter') {
            responseService.setResponse('Множественная загрузка запрещена. Перезагрузите страницу и попробуйте ещё раз.');
            $('#response-modal').modal();
        }

        if (filter.name == 'extensionFilter') {
            responseService.setResponse('Был выбран файл с форматом, который не поддерживается. Повторите попытку, но с учётом допустимых расширений файлов.');
            $('#response-modal').modal();
        }

        if (filter.name == 'fileSizeFilter') {
            responseService.setResponse('Разрешена загрузка файлов, размер которых не превышает 4 Мб. Повторите попытку с учётом этого ограничения.');
            $('#response-modal').modal();
        }

        if (filter.name == 'fileExists') {
            responseService.setResponse('Отправка ярлыков и повреждённых файлов запрещена.');
            $('#response-modal').modal();
        }
    };
    addUploader.onAfterAddingFile = function(fileItem) {
        if (addUploader.queue.length > 1)
            addUploader.queue.splice(0, 1);
    };
    addUploader.onAfterAddingAll = function(addedFileItems) {
    };
    addUploader.onBeforeUploadItem = function (item) {
        item.formData.push({ workTypeId: workTypeId.Id });
    };
    addUploader.onProgressItem = function(fileItem, progress) {
    };
    addUploader.onProgressAll = function(progress) {
    };
    addUploader.onSuccessItem = function(fileItem, response, status, headers) {
        if (response.MaterialId != undefined) {
            responseService.setResponse('Материал успешно загружен.');
            $('#response-modal').modal();

            disciplinePageModel.addMaterial(response, workTypeId.Id);
        }
        else {
            responseService.setResponse(response);
            $('#response-modal').modal();
        }
    };
    addUploader.onErrorItem = function(fileItem, response, status, headers) {
        responseService.setResponse(response);
        $('#response-modal').modal();
    };
    addUploader.onCancelItem = function(fileItem, response, status, headers) {
    };
    addUploader.onCompleteItem = function(fileItem, response, status, headers) {
    };
    addUploader.onCompleteAll = function() {
        $('#mat-add-modal').modal('hide');
        addUploader.queue.splice(0, 1);
    };

    // Edit model

    $scope.editMaterial = editMaterial;

    // Delete material

    $scope.delete = function () {
        var id = materialId.Id;
        var model = disciplinePageModel.getModel();

        if (id == undefined) {
            responseService.setResponse('Нарушена вёрстка страницы. Перезагрузите её или обратитесь в техподдержку!');
            $('#response-modal').modal();
            return;
        }

        $('#mat-del-modal').modal('hide');
        $http.post('/Discipline/DeleteMaterial?materialId=' + id).success(function (data) {
            responseService.setResponse(data);
            $('#response-modal').modal('show');


            if (data == 'Материал успешно удален.') {
                disciplinePageModel.deleteMaterial(id);
                materialId.Id = undefined;
            }
        })
        .error(function (err) {
            responseService.setResponse(err);
            $('#response-modal').modal('show');
        });
    }

    $scope.edit = function () {
        var id = materialId.Id;

        if (id == undefined || id == -1) {
            responseService.setResponse('Нарушена вёрстка страницы. Перезагрузите её или обратитесь в техподдержку!');
            $('#response-modal').modal();
            return;
        }

        var material = null;
        for (var matColl in model.Materials) {
            for (var i = 0; i < model.Materials[matColl].length; i++) {
                if (model.Materials[matColl][i].MaterialId == id) {
                    material = model.Materials[matColl][i];
                    break;
                }
            }
            if (material != null)
                break;
        }

        $('#mat-edit-modal').modal('hide');

        if (material == null) {
            responseService.setResponse('Не найден редактируемый материал. Попробуйте перезагрузить страницу и обратитесь в техподдержку.');
            $('#response-modal').modal('show');
            return;
        }

        var splittedType = material.Type.split('.');
        var extension = splittedType[splittedType.length - 1];
        var fullName = editMaterial.Name + '.' + extension;

        if (material.Type == fullName) {
            responseService.setResponse('Прежнее имя файла сохранено.');
            $('#response-modal').modal('show');
            return;
        }

        $http.post('/Discipline/EditMaterial?materialId=' + id + '&name=' + fullName).success(function (data) {
            responseService.setResponse(data);
            $('#response-modal').modal('show');

            if (data == 'Материал успешно переименован.') {
                disciplinePageModel.editMaterial(id, fullName);
                materialId.Id = -1;
            }
        })
        .error(function (err) {
            responseService.setResponse(err);
            $('#response-modal').modal('show');
        });
    }
});