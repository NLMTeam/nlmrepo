﻿disciplinesApp.controller('workTypeSubController', function ($scope, $http, selectedCP,
                                                             disciplinePageModel, responseService,
                                                             workTypeId, editWorkType) {
    $scope.editWorkType = editWorkType.getEditWorkType();

    // CUD models
    $scope.newWorkType = {
        Name: '',
        MinMark: '',
        MaxMark: ''
    };

    //Create work type
    $scope.create = function (e) {
        e.preventDefault();
        $('#workType-add-modal').modal('hide');

        $http.post('/Discipline/CreateWorkType', {
            ControlPointId: selectedCP.getCP().body.ControlPointId,
            Name: $scope.newWorkType.Name,
            MinMark: $scope.newWorkType.MinMark,
            MaxMark: $scope.newWorkType.MaxMark

        }).success(function (data) {
            if (data.MaxMark != undefined && data.MinMark != undefined) {
                disciplinePageModel.addWorkType(data);

                responseService.setResponse('Вид работы успешно создан.');
                $('#response-modal').modal('show');

                $scope.newWorkType.Name = '';
                $scope.newWorkType.MinMark = '';
                $scope.newWorkType.MaxMark = '';
            }
            else {
                responseService.setResponse(data);
                $('#response-modal').modal('show');
            }
        })
                .error(function (err) {
                    responseService.setResponse(err);
                    $('#response-modal').modal('show');
                });
    },

    $scope.delete = function () {
        var id = workTypeId.Id;
        var model = disciplinePageModel.getModel();

        if (id == undefined || id == -1) {
            responseService.setResponse('Нарушена вёрстка страницы. Перезагрузите её или обратитесь в техподдержку!');
            $('#response-modal').modal();
            return;
        }

        $('#workType-del-modal').modal('hide');
        $http.post('/Discipline/DeleteWorkType?workTypeId=' + id).success(function (data) {
            responseService.setResponse(data);
            $('#response-modal').modal('show');

            if (data == 'Вид работы успешно удален.') {
                disciplinePageModel.deleteWorkType(id);
                workTypeId.Id = -1;
            }
        })
        .error(function (err) {
            responseService.setResponse(err);
            $('#response-modal').modal('show');
        });
    }

    // Edit work type

    $scope.edit = function (e) {
        e.preventDefault();
        $('#workType-edit-modal').modal('hide');

        var currentCP = selectedCP.getCP().body;
        var id = workTypeId.Id;
        // Finding old work type values
        var oldWorkTypes = disciplinePageModel.getModel().WorkTypes[currentCP.ControlPointId];
        var oldWorkType = null;

        for (var i = 0; i < oldWorkTypes.length; i++)
            if (oldWorkTypes[i].WorkTypeId == id)
                oldWorkType = oldWorkTypes[i];

        if (oldWorkType == null) {
            responseService.setResponse('Ошибка в скрипте. Обратитесь в техподдержку!');
            $('#response-modal').modal();
            return;
        }
        

        if ($scope.editWorkType.Name != oldWorkType.Name ||
            $scope.editWorkType.MinMark != oldWorkType.MinMark ||
            $scope.editWorkType.MaxMark != oldWorkType.MaxMark) {

            $http.post('/Discipline/EditWorkType', {

                WorkTypeId: id,
                ControlPointId: currentCP.ControlPointId,
                Name: $scope.editWorkType.Name,
                MinMark: $scope.editWorkType.MinMark,
                MaxMark: $scope.editWorkType.MaxMark

            }).success(function (data) {
                responseService.setResponse(data);
                $('#response-modal').modal('show');
                if (data == 'Вид работ успешно отредактирован.') {
                    var model = disciplinePageModel.getModel();

                    for (var i = 0; i < model.WorkTypes[currentCP.ControlPointId].length ; i++) {
                        if (model.WorkTypes[currentCP.ControlPointId][i].WorkTypeId == id) {
                            model.WorkTypes[currentCP.ControlPointId][i].Name = $scope.editWorkType.Name;
                            model.WorkTypes[currentCP.ControlPointId][i].MinMark = $scope.editWorkType.MinMark;
                            model.WorkTypes[currentCP.ControlPointId][i].MaxMark = $scope.editWorkType.MaxMark;
                            break;
                        }
                    }

                    // Deleting old editWorkType to clear the modal
                    $scope.editWorkType.Name = '';
                    $scope.editWorkType.MinMark = '';
                    $scope.editWorkType.MaxMark = '';
                }
            })
        .error(function (err) {
            responseService.setResponse(err);
            $('#response-modal').modal('show');
        });
        }
        else {
            responseService.setResponse('Прежние данные сохранены.');
            $('#response-modal').modal('show');
        }
    }

});