﻿disciplinesApp.directive('workType', function (disciplinePageModel, selectedCP, workTypeId, editWorkType) {
    return {
        link: function (scope, element, attrs) {

            scope.upload = function (e) {
                var id = scope.workType.WorkTypeId;
                workTypeId.Id = id;
                e.stopPropagation();
                $('#mat-add-modal').modal('show');
            };

            scope.delete = function (e) {
                e.stopPropagation();

                var id = scope.workType.WorkTypeId;
                
                workTypeId.Id = id;
                $('#workType-del-modal').modal('show');
            };

            scope.edit = function (e) {
                e.stopPropagation();

                var id = scope.workType.WorkTypeId;

                workTypeId.Id = id;

                var cp = selectedCP.getCP().body;
                if (cp == null)
                    return;

                // Finding old work type values
                var oldWorkTypes = disciplinePageModel.getModel().WorkTypes[cp.ControlPointId];
                var oldWorkType = null;

                for (var i = 0; i < oldWorkTypes.length; i++)
                    if (oldWorkTypes[i].WorkTypeId == id)
                        oldWorkType = oldWorkTypes[i];

                if (oldWorkType == null)
                    return;

                console.log(oldWorkType);
                editWorkType.setEditWorkType({
                    Name: oldWorkType.Name,
                    MinMark: oldWorkType.MinMark,
                    MaxMark: oldWorkType.MaxMark
                })
                

                console.log(editWorkType.getEditWorkType());

                $('#workType-edit-modal').modal('show');
            };
        },
        restrict: 'E',
        templateUrl: '/Scripts/pv_scripts/angular/DisciplinePage/Admin/directives/workType.html',
        scope: {
            workType: '=',
            model: '='
        }
    };
});