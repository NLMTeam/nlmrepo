﻿disciplinesApp.factory('editCP', function () {
    editCP = {
        Id: '',
        Number: '',
        Name: '',
        Date: new Date(),
        MinMark: '',
        MaxMark: ''
    };


    return {
        getEditCP: function () {
            return editCP;
        },
        setEditCP: function (value) {
            editCP.Id = value.Id;
            editCP.Number = value.Number;
            editCP.Name = value.Name;

            editCP.Date = value.Date;
            editCP.MinMark = value.MinMark;
            editCP.MaxMark = value.MaxMark;
        }
    }
});