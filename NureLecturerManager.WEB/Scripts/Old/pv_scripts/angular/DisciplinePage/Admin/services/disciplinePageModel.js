﻿disciplinesApp.factory('disciplinePageModel', function ($http, selectedDiscipline, selectedDFY, selectedCP, responseService) {
    model = null;

    return {

        getModel: function () {
            if (model == undefined || model == null)
                return $http.get('/Discipline/GetAdminModel').success(function (data) {
                    model = data;
                    for (var cp in model.ControlPoints) {
                        if (cp.length == undefined) {
                            break;
                        }
                        else {
                            for (var i = 0; i < model.ControlPoints[cp].length; i++) {
                                var dateStr = model.ControlPoints[cp][i].Date.split('/');
                                var dateStr2 = model.ControlPoints[cp][i].Date.split('.');
                                if (dateStr.length == 3)
                                    model.ControlPoints[cp][i].Date = new Date(dateStr[2], dateStr[0] * 1 - 1, dateStr[1]);
                                else
                                    model.ControlPoints[cp][i].Date = new Date(dateStr2[2], dateStr2[1] * 1 - 1, dateStr2[0]);
                            }
                        }
                    }
                })
                .error(function (err) {
                    responseService.setResponse("Проблемы с соединением. Исправьте их и перезагрузите страницу.");
                    $('#response-modal').modal('show');
                });

            else {
                return model;
            }
        },

        addDiscipline: function (data) {
            if (data != null) {
                //var currentDiscipline = selectedDiscipline.getDiscipline().body;
                if (model.Disciplines == null || model.Disciplines == undefined)
                    model.Disciplines = [];

                model.Disciplines.push(data);
            }
        },

        deleteDiscipline: function (id) {
            for (var i = 0; i < model.Disciplines.length; i++) {
                if (model.Disciplines[i].DisciplineId == id) {
                    this.clearDFYs(id);
                    model.Disciplines.splice(i, 1);
                    break;
                }
            }
        },

        addDFY: function (data) {
            if (data != null) {
                var currentDiscipline = selectedDiscipline.getDiscipline().body;

                if (model.DFYs[currentDiscipline.DisciplineId] == null ||
                    model.DFYs[currentDiscipline.DisciplineId] == undefined) {
                    model.DFYs[currentDiscipline.DisciplineId] = [];
                }
                model.DFYs[currentDiscipline.DisciplineId].push(data);
            }
        },

        deleteDFY: function (id) {
            var currentDiscipline = selectedDiscipline.getDiscipline().body;

            for (var i = 0; i < model.DFYs[currentDiscipline.DisciplineId].length; i++) {
                if (model.DFYs[currentDiscipline.DisciplineId][i].DFYId == id) {
                    this.clearCPs(id);
                    model.DFYs[currentDiscipline.DisciplineId].splice(i, 1);
                    break;
                }
            }
        },

        addCP: function (data) {
            if (data != null) {
                var currentDFY = selectedDFY.getDFY().body;

                if (model.ControlPoints[currentDFY.DFYId] == null 
                    || model.ControlPoints[currentDFY.DFYId] == undefined)
                        model.ControlPoints[currentDFY.DFYId] = [];
                    
                model.ControlPoints[currentDFY.DFYId].push(data);
                var newDate = model.ControlPoints[currentDFY.DFYId][model.ControlPoints[currentDFY.DFYId].length - 1].Date.split("/");
                var dateStr2 = model.ControlPoints[currentDFY.DFYId][model.ControlPoints[currentDFY.DFYId].length - 1].Date.split(".");
                if (newDate.length == 3)
                    model.ControlPoints[currentDFY.DFYId][model.ControlPoints[currentDFY.DFYId].length - 1].Date = new Date(newDate[2], newDate[0] * 1 - 1, newDate[1]);
                else
                    model.ControlPoints[currentDFY.DFYId][model.ControlPoints[currentDFY.DFYId].length - 1].Date = new Date(dateStr2[2], dateStr2[1] * 1 - 1, dateStr2[0]);
            }
            },

        deleteCP: function (id) {
            var currentDFY = selectedDFY.getDFY().body;

            for (var i = 0; i < model.ControlPoints[currentDFY.DFYId].length; i++) {
                if (model.ControlPoints[currentDFY.DFYId][i].ControlPointId == id) {
                    this.clearWts(id);
                    model.ControlPoints[currentDFY.DFYId].splice(i, 1);
                    break;
                }
            }
        },

        addWorkType: function (data) {
            if (data == null)
                return;
            
            var currentCP = selectedCP.getCP().body;
            if (model.WorkTypes[currentCP.ControlPointId] == null || model.WorkTypes[currentCP.ControlPointId] == undefined)
                model.WorkTypes[currentCP.ControlPointId] = [];

            model.WorkTypes[currentCP.ControlPointId].push(data);
        },
        
        deleteWorkType: function (id) {
            if (id == undefined || id == -1)
                return;

            var currentCP = selectedCP.getCP().body;
            for (var i = 0; i < model.WorkTypes[currentCP.ControlPointId].length; i++) {
                if (model.WorkTypes[currentCP.ControlPointId][i].WorkTypeId == id) {
                    this.clearMat(id);
                    model.WorkTypes[currentCP.ControlPointId].splice(i, 1);
                    break;
                }
            }
        },

        addMaterial: function (material, workTypeId) {
            if (material == null || workTypeId == undefined)
                return;

            if (model.Materials[workTypeId] == null || model.Materials[workTypeId] == undefined)
                model.Materials[workTypeId] = [];

            model.Materials[workTypeId].push(material);
        },

        deleteMaterial: function (doc) {
            if (doc == undefined || doc == -1)
                return;

            for (var matColl in model.Materials) {
                for (var i = 0; i < model.Materials[matColl].length; i++) {
                    if (model.Materials[matColl][i].Material1 == doc) {
                        model.Materials[matColl].splice(i, 1);
                    }
                }
            }
        },

        editMaterial: function (id, name) {
            if (id == undefined || id == -1)
                return;

            var updated = false;
            for (var matColl in model.Materials) {
                for (var i = 0; i < model.Materials[matColl].length; i++) {
                    if (model.Materials[matColl][i].MaterialId == id) {
                        model.Materials[matColl][i].Type = name;
                        updated = true;
                        break;
                    }
                }
                if (updated)
                    break;
            }
        },
        
        clearDFYs: function (discId) {
            var dfys = model.DFYs[discId];

            // Clear cps

            if (dfys != undefined) {
                for (var i = 0; i < dfys.length; i++) {
                    this.clearCPs(dfys[i].DFYId);
                }
            }

            delete model.DFYs[discId];
        },

        clearCPs: function (dfyId) {
            var cps = model.ControlPoints[dfyId];
            
            // Clear wts

            if (cps != undefined) {
                for (var i = 0; i < cps.length; i++) {
                    this.clearWts(cps[i].ControlPointId);
                }
            }

            // Clear controlPoints
            delete model.ControlPoints[dfyId];
        },

        clearWts: function(cpId){
            var wts = model.WorkTypes[cpId];

            // Clear materials

            if (wts != undefined) {
                for (var i = 0; i < wts.length; i++) {
                    this.clearMat(wts[i].WorkTypeId);
                }
            }

            // Clear wt
            delete model.WorkTypes[cpId];
        },

        clearMat: function (wtId) {
            delete model.Materials[wtId];
        }
    };
});