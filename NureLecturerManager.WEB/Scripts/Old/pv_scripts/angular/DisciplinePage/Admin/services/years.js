﻿disciplinesApp.factory('years', function ($http, responseService) {
    year = null;
    yearsForEdit = [];
    yearsForProlong = [];

    return {
        init: function () {
            if (year == undefined || year == null)
            {
                $http.get('/Discipline/GetYear').success(function (data) {
                    year = data.split('-')[0];
                    var intYear = year * 1;
                    for (var i = 0; i < 3; i++) {
                        yearsForEdit.push(intYear - i - 1 + '-' + (intYear - i));
                    }
                    for (var i=0; i<3; i++){
                        yearsForEdit.push(intYear + i + '-' + (intYear + i + 1));
                    }
                    for (var i = 0; i < 5; i++) {
                        yearsForProlong.push(intYear + i + '-' + (intYear + i + 1));
                    }
                })
                .error(function (err) {
                    responseService.setResponse("Проблемы с соединением. Исправьте их и перезагрузите страницу.");
                    $('#response-modal').modal('show');
                });
            }
        },

        getCurrentYear: function () {
            return year;
        },

        getYearsForEdit: function () {
            return yearsForEdit;
        },

        getYearsForProlong: function () {
            return yearsForProlong;
        }
    };
});