﻿$(document).ready(function () {
    
 // response focuse

    $('#response-modal').on('shown.bs.modal', function () {
        $('#response-ok').focus();
    });

    // dfy focuses

    //$('#dfy-add-modal').on('shown.bs.modal', function () {
    //    $('#dfy-year').focus();
    //});

    $('#dfy-del-modal').on('shown.bs.modal', function () {
        $('#del-dfy-cancel-btn').focus();
    });

    $('#dfy-continue-modal').on('shown.bs.modal', function () {
        $('#con-year').focus();
    });
    // cp focuses

    $('#cp-add-modal').on('shown.bs.modal', function () {
        $('#cp-number').focus();
    });

    $('#cp-del-modal').on('shown.bs.modal', function () {
        $('#cp-del-cancel-btn').focus();
    });

    $('#cp-edit-modal').on('shown.bs.modal', function () {
        $('#edit-cp-number').focus();
    });

    // wt focuses

    $('#workType-add-modal').on('shown.bs.modal', function () {
        $('#worktype-description').focus();
    });

    $('#workType-del-modal').on('shown.bs.modal', function () {
        $('#wt-del-cancel-btn').focus();
    });

    $('#workType-edit-modal').on('shown.bs.modal', function () {
        $('#edit-worktype-description').focus();
    });

    // material focuses

    $('#mat-add-modal').on('shown.bs.modal', function () {
        $('#mat-file-load').focus();
    });

    $('#mat-del-modal').on('shown.bs.modal', function () {
        $('#mat-del-cancel-btn').focus();
    });

    $('#mat-edit-modal').on('shown.bs.modal', function () {
        $('#newMatName').focus();
    });
});