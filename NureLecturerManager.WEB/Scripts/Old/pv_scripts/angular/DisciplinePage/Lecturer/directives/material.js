﻿disciplinesApp.directive('material', function (disciplinePageModel, selectedCP, materialId, editMaterial, tracking, selectedDFY) {
    return {
        link: function (scope, element, attrs) {
            scope.currentDiscipline = selectedDFY.getDFY();
            scope.tracking = tracking;

            scope.delete = function () {
                var id = scope.material.Material1;

                materialId.Id = id;
                $('#mat-del-modal').modal('show');
            };

            scope.edit = function () {
                var id = scope.material.MaterialId;

                materialId.Id = id;

                // Deleting extension from model
                var splittedName = scope.material.Type.split('.');
                splittedName.splice(splittedName.length - 1, 1);
                splittedName = splittedName.join('.');

                editMaterial.Name = splittedName;
                $('#mat-edit-modal').modal('show');
            };

            $('[data-toggle="tooltip"]').tooltip();
        },
        restrict: 'E',
        templateUrl: '/Scripts/pv_scripts/angular/DisciplinePage/Lecturer/directives/material.html',
        scope: {
            material: '='
        }
    };
});