﻿disciplinesApp.controller('cpSubController', function ($scope, $http, selectedCP, selectedDFY,
                                                       disciplinePageModel, editCP, responseService) {

    $scope.editCP = editCP.getEditCP();

    // CUD models
    $scope.newCP = {
        Number: '',
        Name: '',
        Date: new Date(),
        MinMark: '',
        MaxMark: ''
    };

    //Create cp
    $scope.create = function (e) {
        e.preventDefault();
        $('#cp-add-modal').modal('hide');
        $http.post('/Discipline/CreateControlPoint', {
            DFYId: selectedDFY.getDFY().body.DFYId,
            Name: $scope.newCP.Name,
            Number: $scope.newCP.Number,
            Date: $scope.newCP.Date,
            MinMark: $scope.newCP.MinMark,
            MaxMark: $scope.newCP.MaxMark
        }).success(function (data) {
            if (data.Date != undefined && data.MinMark != undefined) {
                disciplinePageModel.addCP(data);

                responseService.setResponse('Контрольная точка успешно создана');
                $('#response-modal').modal('show');

                $scope.newCP.Name = '';
                $scope.newCP.Number = '';
                $scope.newCP.Date = new Date();
                $scope.newCP.MinMark = '';
                $scope.newCP.MaxMark = '';
            }
            else{
                responseService.setResponse(data);
                $('#response-modal').modal('show');
            }
                })
                .error(function (err) {
                    responseService.setResponse(err);
                    $('#response-modal').modal('show');
                });
        }

    //Delete cp
    $scope.delete = function () {
        var currentCP = selectedCP.getCP().body;
        var model = disciplinePageModel.getModel().body;

        if (currentCP == null) {
            responseService.setResponse('Выберите контрольную точку для удаления!');
            $('#response-modal').modal();
            return;
        }

        $('#cp-del-modal').modal('hide');
        $http.post('/Discipline/DeleteControlPoint?cpId=' + currentCP.ControlPointId).success(function (data) {
            responseService.setResponse(data);
            $('#response-modal').modal('show');

            if (data == 'Контрольная точка успешно удалена.') {
                disciplinePageModel.deleteCP(currentCP.ControlPointId);
                selectedCP.setCP(null);
            }
        })
        .error(function (err) {
            responseService.setResponse(err);
            $('#response-modal').modal('show');
        });
    }

    // Edit cp
    $scope.edit = function (e) {
        e.preventDefault();
        $('#cp-edit-modal').modal('hide');

        var tempCP = selectedCP.getCP().body;

        if ($scope.editCP.Name != tempCP.Name || 
            $scope.editCP.Number != tempCP.Number || $scope.editCP.MinMark != tempCP.MinMark ||
            $scope.editCP.MaxMark != tempCP.MaxMark || 
            $scope.editCP.Date - tempCP.Date != 0) {
            $http.post('/Discipline/EditControlPoint', {

                ControlPointId: $scope.editCP.Id,
                DFYId: selectedDFY.getDFY().body.DFYId,
                Name: $scope.editCP.Name,
                Number: $scope.editCP.Number,
                Date: $scope.editCP.Date,
                MinMark: $scope.editCP.MinMark,
                MaxMark: $scope.editCP.MaxMark

            }).success(function (data) {
                responseService.setResponse(data);
                $('#response-modal').modal('show');
                if (data == 'Контрольная точка успешно отредактирована.') {

                    var model = disciplinePageModel.getModel().body;
                    var currentDFY = selectedDFY.getDFY().body;

                    for (var i = 0; i < model.ControlPoints[currentDFY.DFYId].length ; i++) {
                        if (model.ControlPoints[currentDFY.DFYId][i].ControlPointId == $scope.editCP.Id) {
                            model.ControlPoints[currentDFY.DFYId][i].Name = $scope.editCP.Name;
                            model.ControlPoints[currentDFY.DFYId][i].Number = $scope.editCP.Number;
                            model.ControlPoints[currentDFY.DFYId][i].Date = $scope.editCP.Date;
                            model.ControlPoints[currentDFY.DFYId][i].MinMark = $scope.editCP.MinMark;
                            model.ControlPoints[currentDFY.DFYId][i].MaxMark = $scope.editCP.MaxMark;
                            break;
                        }
                    }
                    
                    // Deleting old editCP to clear the modal

                    $scope.editCP.Id = '';
                    $scope.editCP.Name = '';
                    $scope.editCP.Number = '';
                    $scope.editCP.Date = new Date();
                    $scope.editCP.MinMark = '';
                    $scope.editCP.MaxMark = '';
                }
            })
        .error(function (err) {
            responseService.setResponse(err);
            $('#response-modal').modal('show');
        });
        }
        else {
            responseService.setResponse('Прежние данные сохранены.');
            $('#response-modal').modal('show');
        }
    }
});

