﻿disciplinesApp.controller('dfySubController', function ($scope, $http, selectedDFY, selectedCP, disciplinePageModel,
                                                               responseService, tracking, futureDisciplines, years) {

    // CUD models
    $scope.newDFY = {
        lecturers: [],
        year: '',
        semester: ''
    };

    $scope.conDFY = {
        Year: '',
        Semester: ''
    }

    $scope.currentDfy = selectedDFY.getDFY().body;

     //CUD methods

     //Delete discipline
    $scope.delete = function () {
        var currentDFY = selectedDFY.getDFY().body;
        var model = disciplinePageModel.getModel().body;

        if (currentDFY == null) {
            responseService.setResponse('Выберите привязку для удаления!');
            $('#response-modal').modal();
            return;
        }

        $('#dfy-del-modal').modal('hide');
        $http.post('/Discipline/DeleteDFY?dfyId=' + currentDFY.DFYId).success(function (data) {
            responseService.setResponse(data);
            $('#response-modal').modal('show');

            if (data == 'Привязка успешно удалена.') {
                disciplinePageModel.deleteDFY(currentDFY.DFYId);
                selectedCP.setCP(null);
                selectedDFY.setDFY(null);
            }
        })
        .error(function (err) {
            responseService.setResponse(err);
            $('#response-modal').modal('show');
        });
    }

    $scope.continue = function (e) {
        e.preventDefault();
        $scope.prolongYears = years.getYearsForProlong();
        $('#dfy-continue-modal').modal('hide');
        $http.post('/Discipline/ProlongDFY?DFYId=' + selectedDFY.getDFY().body.DFYId + "&year=" +
                                                     $scope.conDFY.Year + "&semester=" + $scope.conDFY.Semester).success(function (data) {
                            if (data.DFY != undefined && data.ControlPoints != undefined
                                && data.WorkTypes != undefined &&
                                data.Materials != undefined) {
                                    tracking.status = 'future';
                                    responseService.setResponse('Привязка успешно продлена');
                                    $('#response-modal').modal('show');
                                    
                                    disciplinePageModel.setModel(futureDisciplines.getModel());
                                    disciplinePageModel.addDFY(data.DFY);
                                    var model = disciplinePageModel.getModel().body;
                                    var id = data.DFY.DFYId;
                                    model.ControlPoints[id] = data.ControlPoints;
                                    
                                    for (var i = 0; i < data.ControlPoints.length; i++) {
                                        model.WorkTypes[data.ControlPoints[i].ControlPointId] = data.WorkTypes[data.ControlPoints[i]
                                                                                                .ControlPointId];

                                        if (data.WorkTypes[data.ControlPoints[i].ControlPointId] != undefined) {
                                            for (var j = 0; j < data.WorkTypes[data.ControlPoints[i].ControlPointId].length; j++) {
                                                model.Materials[data.WorkTypes[data.ControlPoints[i].ControlPointId][j].WorkTypeId] =
                                                                data.Materials[data.WorkTypes[data.ControlPoints[i].ControlPointId][j].WorkTypeId];

                                            }
                                        }
                                    }

                                    $scope.conDFY.Year = '';
                                    $scope.conDFY.Semester = '';
                            }
                            else {
                                responseService.setResponse(data);
                                $('#response-modal').modal('show');
                            }
                        })
        .error(function (err) {
            responseService.setResponse(err);
            $('#response-modal').modal('show');
        });
    }
});