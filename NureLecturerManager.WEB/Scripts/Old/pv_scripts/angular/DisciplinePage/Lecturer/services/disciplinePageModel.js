﻿disciplinesApp.factory('disciplinePageModel', function ($http, selectedDFY, selectedCP, archievedDisciplines, currentDisciplines,
                                                               futureDisciplines) {
    model = {body: null};

    return {

        getModel: function () {
            return model;
        },

        setModel: function (value) {
            model.body = value;
        },

        addDFY: function (data) {
            if (data != null) {
                model.body.Disciplines.push(data);
            }
        },

        deleteDFY: function (id) {
            for (var i = 0; i < model.body.Disciplines.length; i++) {
                if (model.body.Disciplines[i].DFYId == id) {
                    this.clearCPs(id);
                    model.body.Disciplines.splice(i, 1);
                }
            }
        },

        addCP: function (data) {
            if (data != null) {
                var currentDFY = selectedDFY.getDFY().body;

                if (model.body.ControlPoints[currentDFY.DFYId] == null 
                    || model.body.ControlPoints[currentDFY.DFYId] == undefined)
                        model.body.ControlPoints[currentDFY.DFYId] = [];
                    
                model.body.ControlPoints[currentDFY.DFYId].push(data);
                var newDate = model.body.ControlPoints[currentDFY.DFYId][model.body.ControlPoints[currentDFY.DFYId].length-1].Date.split("/");
                var newDate2 = model.body.ControlPoints[currentDFY.DFYId][model.body.ControlPoints[currentDFY.DFYId].length - 1].Date.split(".");
                if (newDate.length == 3)
                    model.body.ControlPoints[currentDFY.DFYId][model.body.ControlPoints[currentDFY.DFYId].length - 1].Date = new Date(newDate[2], newDate[0] * 1 - 1, newDate[1]);
                else
                    model.body.ControlPoints[currentDFY.DFYId][model.body.ControlPoints[currentDFY.DFYId].length - 1].Date = new Date(newDate2[2], newDate2[1] * 1 - 1, newDate2[0]);
                }
            },

        deleteCP: function (id) {
            var currentDFY = selectedDFY.getDFY().body;

            for (var i = 0; i < model.body.ControlPoints[currentDFY.DFYId].length; i++) {
                if (model.body.ControlPoints[currentDFY.DFYId][i].ControlPointId == id) {
                    this.clearWts(id);
                    model.body.ControlPoints[currentDFY.DFYId].splice(i, 1);
                    break;
                }
            }
        },

        addWorkType: function (data) {
            if (data == null)
                return;
            
            var currentCP = selectedCP.getCP().body;
            if (model.body.WorkTypes[currentCP.ControlPointId] == null || model.body.WorkTypes[currentCP.ControlPointId] == undefined)
                model.body.WorkTypes[currentCP.ControlPointId] = [];

            model.body.WorkTypes[currentCP.ControlPointId].push(data);
        },
        
        deleteWorkType: function (id) {
            if (id == undefined || id == -1)
                return;

            var currentCP = selectedCP.getCP().body;
            for (var i = 0; i < model.body.WorkTypes[currentCP.ControlPointId].length; i++) {
                if (model.body.WorkTypes[currentCP.ControlPointId][i].WorkTypeId == id) {
                    this.clearMat(id);
                    model.body.WorkTypes[currentCP.ControlPointId].splice(i, 1);
                    break;
                }
            }
        },

        addMaterial: function (material, workTypeId) {
            if (material == null || workTypeId == undefined)
                return;

            if (model.body.Materials[workTypeId] == null || model.body.Materials[workTypeId] == undefined)
                model.body.Materials[workTypeId] = [];

            model.body.Materials[workTypeId].push(material);
        },

        deleteMaterial: function (doc) {
            if (doc == undefined || doc == -1)
                return;

            // arch
            var tempModel = archievedDisciplines.getModel();
            for (var matColl in tempModel.Materials) {
                for (var i = 0; i < tempModel.Materials[matColl].length; i++) {
                    if (tempModel.Materials[matColl][i].Material1 == doc) {
                        tempModel.Materials[matColl].splice(i, 1);
                    }
                }
            }

            // current
            tempModel = currentDisciplines.getModel();
            for (var matColl in tempModel.Materials) {
                for (var i = 0; i < tempModel.Materials[matColl].length; i++) {
                    if (tempModel.Materials[matColl][i].Material1 == doc) {
                        tempModel.Materials[matColl].splice(i, 1);
                    }
                }
            }

            // future
            tempModel = futureDisciplines.getModel();
            for (var matColl in tempModel.Materials) {
                for (var i = 0; i < tempModel.Materials[matColl].length; i++) {
                    if (tempModel.Materials[matColl][i].Material1 == doc) {
                        tempModel.Materials[matColl].splice(i, 1);
                    }
                }
            }
        },

        editMaterial: function (id, name) {
            if (id == undefined || id == -1)
                return;

            var updated = false;
            for (var matColl in model.body.Materials) {
                for (var i = 0; i < model.body.Materials[matColl].length; i++) {
                    if (model.body.Materials[matColl][i].MaterialId == id) {
                        model.body.Materials[matColl][i].Type = name;
                        updated = true;
                        break;
                    }
                }
                if (updated)
                    break;
            }
        },

        clearDFYs: function (discId) {
            // Clear cps
            this.clearCPs(discId);
        },

        clearCPs: function (dfyId) {
            var cps = model.body.ControlPoints[dfyId];
            
            // Clear wts

            if (cps != undefined) {
                for (var i = 0; i < cps.length; i++) {
                    this.clearWts(cps[i].ControlPointId);
                }
            }

            // Clear controlPoints
            delete model.body.ControlPoints[dfyId];
        },

        clearWts: function(cpId){
            var wts = model.body.WorkTypes[cpId];

            // Clear materials

            if (wts != undefined) {
                for (var i = 0; i < wts.length; i++) {
                    this.clearMat(wts[i].WorkTypeId);
                }
            }

            // Clear wt
            delete model.body.WorkTypes[cpId];
        },

        clearMat: function (wtId) {
            delete model.body.Materials[wtId];
        }

    };
});