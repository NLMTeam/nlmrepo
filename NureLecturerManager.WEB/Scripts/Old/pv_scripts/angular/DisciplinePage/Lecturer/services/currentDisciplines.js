﻿disciplinesApp.factory('currentDisciplines', function ($http, responseService) {
    currModel = null;

    return {
        getModel: function () {
            if (currModel == undefined || currModel == null)
                return $http.get('/Discipline/GetLecturersModel').success(function (data) {
                    currModel = data;
                    for (var cp in currModel.ControlPoints) {
                        if (cp.length == undefined) {
                            break;
                        }
                        else {
                            for (var i = 0; i < currModel.ControlPoints[cp].length; i++) {
                                var dateStr = currModel.ControlPoints[cp][i].Date.split('/');
                                var dateStr2 = currModel.ControlPoints[cp][i].Date.split('.');
                                if (dateStr.length == 3)
                                    currModel.ControlPoints[cp][i].Date = new Date(dateStr[2], dateStr[0] * 1 - 1, dateStr[1]);
                                else
                                    currModel.ControlPoints[cp][i].Date = new Date(dateStr2[2], dateStr2[1] * 1 - 1, dateStr2[0]);
                            }
                        }
                    }
                })
                .error(function (err) {
                    responseService.setResponse("Проблемы с соединением. Исправьте их и перезагрузите страницу.");
                    $('#response-modal').modal('show');
                });

            else {
                return currModel;
            }
        }
    };
});




