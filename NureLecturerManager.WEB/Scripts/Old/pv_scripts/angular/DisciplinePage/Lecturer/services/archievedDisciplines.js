﻿disciplinesApp.factory('archievedDisciplines', function ($http, responseService) {
    archModel = null;

    return {

        getModel: function () {
            if (archModel == undefined || archModel == null) {
                return $http.get('/Discipline/GetLecturersAchievedModel').success(function (data) {
                    archModel = data;
                    for (var cp in archModel.ControlPoints) {
                        if (cp.length == undefined) {
                            break;
                        }
                        else {
                            for (var i = 0; i < archModel.ControlPoints[cp].length; i++) {
                                var dateStr = archModel.ControlPoints[cp][i].Date.split('/');
                                var dateStr2 = archModel.ControlPoints[cp][i].Date.split('.');
                                if (dateStr.length == 3)
                                    archModel.ControlPoints[cp][i].Date = new Date(dateStr[2], dateStr[0] * 1 - 1, dateStr[1]);
                                else
                                    archModel.ControlPoints[cp][i].Date = new Date(dateStr2[2], dateStr2[1] * 1 - 1, dateStr2[0]);
                            }
                        }
                    }
                })
                .error(function (err) {
                    responseService.setResponse("Проблемы с соединением. Исправьте их и перезагрузите страницу.");
                    $('#response-modal').modal('show');
                });
            }
            else {
                return archModel;
            }
        },
    };
});