﻿disciplinesApp.factory('years', function ($http, responseService) {
    year = null;
    yearsForProlong = [];

    return {
        init: function () {
            if (year == undefined || year == null)
            {
                $http.get('/Discipline/GetYear').success(function (data) {
                    year = data.split('-')[0];
                    var intYear = year * 1;
                    for (var i = 0; i < 5; i++) {
                        yearsForProlong.push(intYear + i + '-' + (intYear + i + 1));
                    }
                })
                .error(function (err) {
                    responseService.setResponse("Проблемы с соединением. Исправьте их и перезагрузите страницу.");
                    $('#response-modal').modal('show');
                });
            }
        },

        getCurrentYear: function () {
            return year;
        },

        getYearsForProlong: function () {
            return yearsForProlong;
        }
    };
});