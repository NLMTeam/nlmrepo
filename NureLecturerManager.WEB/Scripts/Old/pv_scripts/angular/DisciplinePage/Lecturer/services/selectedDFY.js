﻿disciplinesApp.factory('selectedDFY', function (selectedCP) {
    DFY = { body: null };

    return {
        getDFY: function () {
            return DFY;
        },

        setDFY: function (value) {
            // Deselecting old value
            if (DFY.body != null) {
                DFY.body.activity = "";
            }

            // Changing current value
            DFY.body = value;
            if (value != null)
                DFY.body.activity = "active";

             selectedCP.setCP(null);
        }
    }
});