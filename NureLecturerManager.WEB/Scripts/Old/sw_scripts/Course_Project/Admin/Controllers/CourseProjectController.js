﻿var app = angular.module('courseProjectApp');

app.controller('courseProjectInit', function ($scope, $rootScope, $http, communicationService, selectService, storeService, dataStorage) {
    $scope.addProjectTheme = function () {
        $("#themeInfo").slideUp();

        var disciplines = dataStorage.getDisciplines();

        if (disciplines == undefined || disciplines.length == 0) {
            return;
        }

        communicationService.execute('create Course Project', []);
    }
    
    $scope.editProjectTheme = function () {
        var courseProject = selectService.getProject();

        var disciplines = dataStorage.getDisciplines();

        if (disciplines == undefined || disciplines.length == 0) {
            return;
        }

        if (typeof (courseProject) == 'undefined') {
            return;
        }
        else {
            $http.get('/CourseProjects/EditCourseProject?courseProjectId=' + courseProject.CourseProjectId).success(function (model) {
                communicationService.execute('Edit Course Project', [model]);
            })
        }
    }

    $scope.deleteProjectTheme = function () {
        $("#themeInfo").slideUp();

        var disciplines = dataStorage.getDisciplines();

        if (disciplines == undefined || disciplines.length == 0) {
            return;
        }

        var courseProject = selectService.getProject();

        if (typeof (courseProject) == 'undefined') {
            return;
        }
        else {
            communicationService.execute('confirm delete open', ["Вы уверены, что хотите удалить тему курсового проекта?", "delete confirmed"]);
        }
    }

    communicationService.register('delete confirmed', function () {

        window.hideInfoQuick();

        $http.post('/CourseProjects/DeleteCourseProject?cpId=' + selectService.getProject().CourseProjectId).success(function (data) {

            var year = selectService.getYear();

            if (year != undefined) {
                dataStorage.storeCourseProjectConsolidations(year, selectService.getProject().CourseProjectId, undefined);
            }

            communicationService.execute('open message', [data]);
            
            dataStorage.deleteProjectConsolidations(selectService.getProject().CourseProjectId);
            dataStorage.storeProjects(selectService.getDiscipline().DisciplineId, undefined);

            communicationService.execute('Course Project Deleted');
        })
    })
});

// 
// Modal Controler
//

app.controller('courseProjectModal', function ($scope, $rootScope, $http, communicationService, storeService, selectService) {
    communicationService.register('create Course Project', function () {
        $scope.createdCourseProject = {};
        $scope.createdCourseProject.Type = "true";

        $('#createCourseProjectDialog').modal('show');
        window.openModalId = "createCourseProjectDialog";
    });

    $scope.cancel = function () {
        $('#' + window.openModalId).modal('hide');
    }

    $scope.createCourseProject = function () {
        var createForm = $("#createProjectForm");

        if (!createForm[0].checkValidity()) {
            createForm.find(":submit").click();
            return;
        }

        var discipline = selectService.getDiscipline();
        var type = $scope.createdCourseProject.Type == "true" ? "SINGLE" : "TEAM";

        $http.post('/CourseProjects/CreateCourseProject', {
            model: {
                Theme: $scope.createdCourseProject.Theme, Description: $scope.createdCourseProject.Description, Type: type,
                Discipline: discipline.Name
            }
        }).success(function (message) {
            $('#' + window.openModalId).modal('hide');

            communicationService.execute('open message', [message]);

            communicationService.execute('Project created');
        });
    }

    communicationService.register('Edit Course Project', function (model) {

        window.hideInfoQuick();

        $scope.editedCourseProject = model;
        $scope.editedCourseProject.Type = model.Type == 'SINGLE' ? "true" : "false";

        $('#editCourseProjectDialog').modal('show');
        window.openModalId = "editCourseProjectDialog";

    })

    $scope.editCourseProject = function () {
        var editForm = $("#editProjectForm");

        if (!editForm[0].checkValidity()) {
            editForm.find(':submit').click();
            return;
        }
        $scope.editedCourseProject.Type = $scope.editedCourseProject.Type == "true" ? "SINGLE" : "TEAM";

        $http.post('/CourseProjects/EditCourseProject', { model: $scope.editedCourseProject }).success(function (data) {
            $("#editCourseProjectDialog").modal('hide');
            communicationService.execute('open message', [data]);
            communicationService.execute('project edited', [$scope.editedCourseProject]);
        })
    }
})

