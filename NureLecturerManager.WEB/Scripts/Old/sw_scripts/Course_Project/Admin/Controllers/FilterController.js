﻿var app = angular.module('courseProjectApp');


app.controller('filter', function callBack($scope, $rootScope, $http, selectService, communicationService, storeService, dataStorage) {
    $scope.CourseProjectMode = 'ThemeList';
    var cpLoad, consLoad;
    cpLoad = consLoad = false;

    $scope.loaded = false;

    $scope.allowProjectEdit = false;
    $scope.allowConsolidationEdit = false;
    $scope.preventDeselect = false;

    $scope.displayYear = "Год";
    $scope.displayGroup = "Группа";

    $scope.currentProject = null;

    $('#themeInfo').slideUp(1, function () { });

    function transformConsolidations (consolidations) {
        if (consolidations != null) {
            for (var i = 0; i < consolidations.length; i++) {
                if (consolidations[i].PassDate != null && consolidations[i].PassDate != "" && typeof(consolidations[i].PassDate) != typeof(new Date())) {
                    var dateArr = consolidations[i].PassDate.split(".");
                    if (dateArr.length == 3) {
                        var date = new Date(+dateArr[2], +dateArr[1] != 0 ? +dateArr[1] - 1 : 12, +dateArr[0]);
                        consolidations[i].PassDate = date;
                    }
                    else {
                        var dateArr = consolidations[i].PassDate.split("/");
                        var date = new Date(+dateArr[2], +dateArr[0] != 0 ? +dateArr[0] - 1 : 12, +dateArr[1]);
                        consolidations[i].PassDate = date;
                    }
                }

                if (consolidations[i].ConsolidationDate != null && consolidations[i].ConsolidationDate != "" && typeof (consolidations[i].ConsolidationDate) != typeof (new Date())) {
                    var dateArr = consolidations[i].ConsolidationDate.split(".");
                    if (dateArr.length == 3) {
                        var date = new Date(+dateArr[2], +dateArr[1] != 0 ? +dateArr[1] - 1 : 12, +dateArr[0]);
                        consolidations[i].ConsolidationDate = date;
                    }
                    else {
                        var dateArr = consolidations[i].ConsolidationDate.split("/");
                        var date = new Date(+dateArr[2], + dateArr[0] != 0 ? +dateArr[0] - 1 : 12, +dateArr[1]);
                        consolidations[i].ConsolidationDate = date;
                    }
                }
            }

            return consolidations;
        }
    }

    storeService.loadModel().success(function (model) {

        // return server CourseProjectMainViewModel

        $scope.Disciplines = model.Disciplines != null ? model.Disciplines : [];
        $scope.CourseProjects = model.CourseProjects != null ? model.CourseProjects : [];
        $scope.Years = model.Years != null ? model.Years : [];
        $scope.Groups = model.Groups != null ? model.Groups : [];
        $scope.Consolidations = model.Consolidations != null ? model.Consolidations : [];

        var consolidations = $scope.Consolidations;

        if (consolidations != null) {
            for (var i = 0; i < consolidations.length; i++) {
                if (consolidations[i].PassDate != null && consolidations[i].PassDate != "") {
                    var dateArr = consolidations[i].PassDate.split(".");
                    if (dateArr.length == 3) {
                        var date = new Date(+dateArr[2], +dateArr[1] != 0 ? +dateArr[1] - 1 : 12, +dateArr[0]);
                        consolidations[i].PassDate = date;
                    }
                    else {
                        dateArr = consolidations[i].PassDate.split("/");
                        var date = new Date(+dateArr[2], + dateArr[0] != 0 ? +dateArr[0] - 1 : 12, +dateArr[1]);
                        consolidations[i].PassDate = date;
                    }
                }

                if (consolidations[i].ConsolidationDate != null) {
                    var dateArr = consolidations[i].ConsolidationDate.split(".");
                    if (dateArr.length == 3) {
                        var date = new Date(+dateArr[2], +dateArr[1] != 0 ? +dateArr[1] - 1 : 12, +dateArr[0]);
                        consolidations[i].ConsolidationDate = date;
                    }
                    else {
                        var dateArr = consolidations[i].ConsolidationDate.split("/");
                        var date = new Date(+dateArr[2], + dateArr[0] != 0 ? +dateArr[0] - 1 : 12, +dateArr[1]);
                        consolidations[i].ConsolidationDate = date;
                    }
                }
            }

            $scope.Consolidations = consolidations;
        }

        // we loaded data
        $scope.loaded = true;

        // mark default values 
        if ($scope.Disciplines.length > 0) {
            $scope.selectedDiscipline = $scope.Disciplines[0];
            selectService.selectDiscipline($scope.Disciplines[0]);
        }

        if ($scope.Years.length > 0) {
            $scope.displayYear = $scope.Years[0];
            selectService.selectYear($scope.Years[0]);
            $scope.selectedYear = $scope.Years[0];
        }

        if ($scope.Groups.length > 0) {
            $scope.displayGroup = $scope.Groups[0];
            selectService.selectGroup($scope.Groups[0]);
            $scope.selectedGroup = $scope.Groups[0];
        }
    });

    $scope.loadDisciplineInfo = function () {
        // select discipline
        selectService.selectDiscipline($scope.selectedDiscipline);

        // must return angular ajax object or data from storage.
        var storeResult = storeService.loadDisciplineInfo($scope.selectedDiscipline.DisciplineId);

        selectService.selectConsolidation(undefined);
        selectService.selectYear(undefined);
        selectService.selectGroup(undefined);
        selectService.selectProject(undefined);
        selectService.selectConsolidation(undefined);

        if (typeof (storeResult) == 'undefined') {
            throw new Error('Store service return undefined');
        }

        // use duck typing to detect what we have got
        if (typeof (storeResult.success) != "undefined") {
            storeResult.success(function (model) {
                $scope.Years = model.Years;
                $scope.CourseProjects = model.CourseProjects;
                $scope.displayYear = "Год";
                $scope.displayGroup = "Группа";

                $scope.currentProject = null;
                delete $scope.selectedConsolidation;
                delete $scope.selectedGroup;
                delete $scope.selectedYear;
                selectService.selectConsolidation(undefined);

                $scope.Consolidations = [];
                $scope.Groups = [];

                if ($scope.CourseProjectMode == "Info") {
                    $("#themeInfo").slideUp('fast', function () { });
                }

                if ($scope.Years.length > 0)
                    $scope.selectYear($scope.Years[0]);
            });
        }
        else {
            $scope.Years = storeResult.Years;
            $scope.CourseProjects = storeResult.Projects;

            $scope.displayYear = "Год";
            $scope.displayGroup = "Группа";

            $scope.currentProject = null;
            delete $scope.selectedConsolidation;
            delete $scope.selectedGroup;
            delete $scope.selectedYear;
            selectService.selectConsolidation(undefined);

            $scope.Consolidations = [];
            $scope.Groups = [];

            if ($scope.CourseProjectMode == "Info") {
                $("#themeInfo").slideUp('fast', function () { });
            }

            if ($scope.Years.length > 0)
                $scope.selectYear($scope.Years[0]);
        }
    }

    $scope.selectYear = function (year) {
        // select year
        $scope.displayYear = year;
        $scope.selectedYear = year;
        selectService.selectYear(year);
        selectService.selectGroup(undefined);
        selectService.selectConsolidation(undefined);
        delete $scope.selectedGroup;

        // load groups
        var discipline = selectService.getDiscipline();
        var storeObject = storeService.loadGroups(year, discipline);

        if (storeObject == undefined) {
            throw new Error('Groups returned by store Service is undefined');
        }

        if (typeof (storeObject.success) != "undefined") {
            storeObject.success(function (groups) {
                $scope.Groups = groups;

                if (groups.length > 0) {
                    $scope.selectGroup(groups[0]);
                }
                else {
                    $scope.displayGroup = "Группа";
                    $scope.Consolidations = [];
                }
            });
        }
        else {
            $scope.Groups = storeObject;

            if (storeObject.length > 0) {
                $scope.selectGroup(storeObject[0]);
            }
            else {
                $scope.displayGroup = "Группа";
                $scope.Consolidations = [];
            }
        }
    }

    $scope.selectProject = function (project) {
        if ($scope.currentProject != null) {
            $("#project" + $scope.currentProject.CourseProjectId).removeClass('active');

            if ($scope.currentProject.CourseProjectId == project.CourseProjectId && !$scope.preventDeselect) {
                var group = selectService.getGroup();

                if (group != undefined) {
                    var discipline = selectService.getDiscipline();
                    var year = selectService.getYear();

                    var stObj = storeService.ConsolidationsForDisciplineAndGroup(year, group, discipline);

                    if (stObj.success == undefined) {
                        $scope.Consolidations = transformConsolidations(stObj);
                    }
                    else {
                        stObj.success(function (consolidations) {
                            $scope.Consolidations = transformConsolidations(consolidations);
                        })
                    }
                }
                selectService.selectProject(undefined);
                $scope.currentProject = null;
                $("#themeInfo").slideUp();
                return;
            }
        }
        $scope.preventDeselect = false;
        // select
        selectService.selectProject(project);
        $scope.currentProject = project;

        // display selction
        $("#project" + project.CourseProjectId).addClass('active');

        // change the description of project if it's viewed
        if ($scope.CourseProjectMode == "Info") {
            $scope.Project = project;
        }

        // load data if it's possible
        var year = selectService.getYear();
        
        if (year != undefined) {
            $scope.loadProjectInfo(project);
        }
    }

    $scope.loadProjectInfo = function (project) {
        var year = selectService.getYear();

        var storeObj = storeService.ConsolidationsForProject(year, project.CourseProjectId);

        if (typeof (storeObj.success) != "undefined") {
            storeObj.success(function (data) {
                var consolidations = transformConsolidations(data);
                $scope.Consolidations = consolidations;
                $scope.selectedConsolidation = undefined;
                selectService.selectConsolidation(undefined);
            });
        }
        else {
            $scope.Consolidations = transformConsolidations(storeObj);
            $scope.selectedConsolidation = undefined;
            selectService.selectConsolidation(undefined);
        }
    }

    $scope.selectGroup = function (group) {
        $scope.displayGroup = group;
        selectService.selectGroup(group);
        $scope.selectedGroup = group;
        // we load consolidations for group and noe deselect project

        if ($scope.currentProject != null) {
            selectService.selectProject(undefined);
            $("#project" + $scope.currentProject.CourseProjectId).removeClass('active');
            $scope.currentProject = null;
        }

        $scope.loadGroupInfo(group);
    }

    $scope.loadGroupInfo = function (group) {
        // load consolidations for discipline + year + group
        var discipline = selectService.getDiscipline();
        var year = selectService.getYear();

        var storeObj = storeService.ConsolidationsForDisciplineAndGroup(year, group, discipline);

        if (storeObj == undefined) {
            throw new Error("Store service returned undefined for projects");
        }

        if (storeObj.success != undefined) {
            storeObj.success(function (consolidations) {
                $scope.Consolidations = transformConsolidations(consolidations);
                $scope.selectedConsolidation = undefined;
                selectService.selectConsolidation(undefined);
            })
        }
        else {
            var consolidations = transformConsolidations(storeObj);
            $scope.Consolidations = consolidations;
            $scope.selectedConsolidation = undefined;
            selectService.selectConsolidation(undefined);
        }
    }

    $scope.selectConsolidation = function (consolidation) {
        var cons = selectService.getConsolidation();
        if (typeof (cons) != 'undefined') {

            var prevElement = document.getElementById(cons.ConsolidationId);

            prevElement.classList.remove('warning');

            if (cons.ConsolidationId == consolidation.ConsolidationId) {
                selectService.selectConsolidation(undefined);
                delete $scope.selectedConsolidation;
                return;
            }
        }

        selectService.selectConsolidation(consolidation);
        $scope.selectedConsolidation = consolidation;

        var consElement = document.getElementById(consolidation.ConsolidationId);

        //consElement.classList.add('active');
        consElement.classList.add('warning');

        // scroll element to top
        var container = $('#projectContainer'),
            scrollTo = $('#project' + consolidation.CourseProjectId);

        container.scrollTop(
            scrollTo.offset().top - container.offset().top + container.scrollTop()
        );

        if ($scope.currentProject != null) {
            $("#project" + $scope.currentProject.CourseProjectId).removeClass('active');
        }

        $("#project" + consolidation.CourseProjectId).addClass('active');
        // find and select project

        for (var i = 0; i < $scope.CourseProjects.length; i++) {
            var project = $scope.CourseProjects[i];

            if (project.CourseProjectId == consolidation.CourseProjectId) {
                selectService.selectProject(project);
                $scope.currentProject = project;
                break;
            }
        }

    }

    communicationService.register('create consolidation complete', function () {
        var discipline = selectService.getDiscipline() != undefined ? selectService.getDiscipline() : $scope.Disciplines[0];
        var group = selectService.getGroup() != undefined ? selectService.getGroup() : $scope.Groups[0];
        var year = selectService.getYear() != undefined ? selectService.getYear() : $scope.Years[0];

        // this made to make store service load data from server
        dataStorage.storeDisciplineConsolidations(discipline.DisciplineId, year, group, undefined);
        var storeObj = storeService.ConsolidationsForDisciplineAndGroup(year, group, discipline);
        
        storeObj.success(function (consolidations) {
            $scope.Consolidations = transformConsolidations(consolidations);
        });

        selectService.selectConsolidation(undefined);
        delete $scope.selectedConsolidation;

        if ($scope.currentProject != undefined) {
            $("#project" + $scope.currentProject.CourseProjectId).removeClass('active');
        }

        delete $scope.currentProject;
        selectService.selectProject(undefined);
        //$scope.selectDiscipline($scope.selectedDiscipline);
    })

    communicationService.register('Delete consolidation success', function () {
        var consolidations = [];
        for (var i = 0; i < $scope.Consolidations.length; i++) {
            if ($scope.Consolidations[i].ConsolidationId != selectService.getConsolidation().ConsolidationId) {
                consolidations.push($scope.Consolidations[i]);
            }
        }
        $scope.Consolidations = consolidations;

        // deselect theme
        if ($scope.currentProject != null) {
            $("#project" + $scope.currentProject.CourseProjectId).removeClass('active');
            $scope.currentProject = null;
            selectService.selectProject(undefined);
        }

        delete $scope.selectedConsolidation;
        selectService.selectConsolidation(undefined);
    })

    $scope.selectCourseProject = function (courseProject) {
        selectService.selectProject(courseProject);
    }

    communicationService.register('Course Project Deleted', function () {
        var project = selectService.getProject();

        var projects = [];

        for (var i = 0; i < $scope.CourseProjects.length; i++) {
            if (project.CourseProjectId != $scope.CourseProjects[i].CourseProjectId) {
                projects.push($scope.CourseProjects[i]);
            }
        }
        var discipline = selectService.getDiscipline();

        $scope.CourseProjects = projects;
        dataStorage.storeProjects(discipline.DisciplineId, projects);

        delete $scope.currentProject;
        selectService.selectProject(undefined);

        var group = selectService.getGroup();
        
        if (group != undefined)
            $scope.selectGroup(group);
        else
            $scope.selectGroup(group);
        //var stObj = storeService.ConsolidationsForDisciplineAndGroup("", group, discipline);

        //if (stObj.success == undefined) {
        //    $scope.Consolidations = stObj;
        //}
        //else {
        //    stObj.success(function (consolidations) {
        //        $scope.Consolidations = consolidations;
        //    });
        //}
    })

    communicationService.register('Project created', function () {
        var discipline = selectService.getDiscipline();
        storeService.loadProjects(discipline).success(function (data) {
            $scope.CourseProjects = data;
        });
        selectService.selectProject(undefined);
    })

    communicationService.register('project edited', function (project) {
        var projects = [];

        var proj = selectService.getProject();

        proj.Theme = project.Theme;
        proj.Description = project.Description;
        proj.Type = project.Type;

        if ($scope.Project != undefined) {
            $scope.Project.Theme = project.Theme;
            $scope.Project.Description = project.Description;
            $scope.Project.Type = project.Type;
        }

        selectService.selectProject(undefined);
    })

    $scope.displayInfo = function (project) {
        $scope.preventDeselect = true;
        $http.get('/CourseProjects/GetProjectInfo?cpId=' + project.CourseProjectId).success(function (data) {
            $scope.Project = data;
            $scope.CourseProjectMode = 'Info';

            $('#themeInfo').slideDown('slow', function () { });
        });
    }

    $scope.displayList = function () {
        $scope.CourseProjectMode = 'ThemeList';
        $('#themeInfo').slideUp('slow', function () { });
    }
})