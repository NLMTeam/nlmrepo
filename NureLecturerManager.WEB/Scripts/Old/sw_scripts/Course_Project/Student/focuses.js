﻿$(document).ready(function () {
    
    // response focuse

    $('#MessageDialog').on('shown.bs.modal', function () {
        $('#response-ok').focus();
    });
    
    // delete focuse
    $('#ConfirmDeleteDialog').on('shown.bs.modal', function () {
        $('#del-cancel').focus();
    });
});