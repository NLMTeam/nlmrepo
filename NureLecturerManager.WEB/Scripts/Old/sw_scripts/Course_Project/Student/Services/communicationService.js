﻿var app = angular.module('courseProjectApp');

app.service('communicationService', function () {
    var callBacks = {};

    var obj = {};

    obj.register = function (name, callback) {
        callBacks[name] = callback;
    }

    obj.execute = function (name, args) {
        callBacks[name].apply(obj, args);
    }

    return obj;
})