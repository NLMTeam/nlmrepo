﻿storeService.loadYears().success(function (data) {
    $scope.Years = data;

    var year = $scope.Years[0];

    if ($scope.Years.length == 0) {
        $scope.Disciplines = [];
        $scope.Groups = [];
        $scope.CourseProjects = [];
        $scope.Consolidations = [];

        $scope.loaded = true;
        return;
    }
    selectService.selectYear(year);
    $scope.displayYear = year;

    storeService.loadGroups(year).success(function (data) {
        $scope.Groups = data;

        if (data.length == 0) {
            $scope.Groups = [];
            $scope.CourseProjects = [];
            $scope.Disciplines = [];
            $scope.Consolidations = [];

            $scope.loaded = true;
        }

        var group = $scope.Groups[0];
        selectService.selectGroup(group);
        $scope.displayGroup = group;

        storeService.loadDisciplines(year, group).success(function (data) {
            $scope.Disciplines = data;

            if ($scope.Disciplines.length == 0) {
                $scope.CourseProjects = [];
                $scope.Consolidations = [];

                $scope.loaded = true;

                return;
            }

            $scope.selectedDiscipline = $scope.Disciplines[0].Name;
            selectService.selectDiscipline($scope.Disciplines[0]);

            var discipline = $scope.Disciplines[0];

            storeService.loadProjects(year, group, discipline.Name).success(function (data) {
                $scope.allowProjectEdit = data.AllowEdit;
                if (data.CourseProjects != null) {
                    $scope.CourseProjects = data.CourseProjects;
                }
                else {
                    $scope.CourseProjects = [];
                }
                cpLoad = true;
                if (consLoad)
                    $scope.loaded = true;
            });

            storeService.ConsolidationsForDisciplineAndGroup(year, group, discipline.Name).success(function (data) {
                consLoad = true;
                if (cpLoad)
                    $scope.loaded = true;
                var consolidations = data.Consolidations;

                if (consolidations != null) {
                    for (var i = 0; i < consolidations.length; i++) {
                        if (consolidations[i].PassDate != null && consolidations[i].PassDate != "") {
                            var dateArr = consolidations[i].PassDate.split(".");
                            var date = new Date(+dateArr[2], +dateArr[1] != 0 ? +dateArr[1] - 1 : 12, +dateArr[0]);
                            consolidations[i].PassDate = date;
                        }

                        if (consolidations[i].ConsolidationDate != null) {
                            var dateArr = consolidations[i].ConsolidationDate.split(".");
                            var date = new Date(+dateArr[2], +dateArr[1] != 0 ? +dateArr[1] - 1 : 12, +dateArr[0]);
                            consolidations[i].ConsolidationDate = date;
                        }
                    }

                    $scope.Consolidations = consolidations;
                }
                else {
                    $scope.Consolidations = [];
                }
                $scope.allowConsolidationEdit = data.AllowEdit;
            });

            $scope.loaded = true;
        });
    });
});

