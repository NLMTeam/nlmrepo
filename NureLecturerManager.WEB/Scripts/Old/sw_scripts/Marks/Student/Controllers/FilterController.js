﻿var marksApp = angular.module('marksApp');

marksApp.controller('filter', function ($scope, $rootScope, $http, storeService, selectService) {
    $scope.loaded = false;

    storeService.getModel().success(function (model) {
        $scope.Years = model.Years;
        $scope.Groups = model.Groups;
        $scope.Disciplines = model.Disciplines;
        $scope.Worktypes = JSON.parse(model.Worktypes);
        $scope.Marks = JSON.parse(model.Marks);

        for (var i = 0; i < $scope.Marks.length; i++) {
            var mark = $scope.Marks[i];

            if (mark.StudentMarks.length < $scope.Worktypes.length) {
                for (var j = mark.StudentMarks.length; j < $scope.Worktypes.length; j++) {
                    mark.StudentMarks.push({ Key: $scope.Worktypes[j], Value: "" });
                }
            }
        }

        selectService.selectDiscipline($scope.Disciplines[0], function (disc) { $scope.currentDiscipline = disc });
        selectService.selectYear($scope.Years[0], function (year) { $scope.currentYear = year });
        selectService.selectGroup($scope.Groups[0], function (group) { $scope.currentGroup = group });
        $scope.currentYear = $scope.Years[0];
        $scope.currentGroup = $scope.Groups[0];
        $scope.currentDiscipline = $scope.Disciplines[0];

        $scope.loaded = true;
    })

    // filter groups and disciplines by year
    $scope.filterByYear = function () {
        window.event.preventDefault();
        $scope.Groups = storeService.getGroups($scope.currentYear, true);

        if ($scope.Groups.length > 0) {
            var group = $scope.Groups[0];
            console.clear();
            console.log(group);

            $("#group" + group).addClass('active');

            // select and display
            selectService.selectGroup(group, function () { $scope.currentGroup = group });
            document.getElementById(group).classList.add('active');
            $scope.Disciplines = storeService.getDisciplines(group, true);

            $scope.currentDiscipline = $scope.Disciplines[0];
        }
        else {
            $scope.Disciplines = [];
        }
    }

    // filter discipline by group
    $scope.filterByGroup = function (group) {
        window.event.preventDefault();

        if (typeof ($scope.currentGroup) != 'undefined') {
            document.getElementById('group' + $scope.currentGroup).classList.remove('active');
        }

        selectService.selectGroup(group, function (group) { $scope.currentGroup = group; console.log('group selected'); });
        document.getElementById('group' + group).classList.add('active');

        $scope.Disciplines = storeService.getDisciplines(group, true);

        $("#disc" + $scope.Disciplines[0].DisciplineId).addClass('active');
    }

    // reload marks for current discipline
    $scope.filterByDiscipline = function (discipline) {
        window.event.preventDefault();

        selectService.selectDiscipline(discipline, function (disc) { $scope.currentDiscipline = disc });


        var marks = JSON.parse(storeService.getWorktypesAndMarks(selectService.getGroup(), discipline.Name, true));

        $scope.Worktypes = marks.Categorys;
        $scope.Marks = marks.Marks;

        for (var i = 0; i < $scope.Marks.length; i++) {
            var mark = $scope.Marks[i];

            if (mark.StudentMarks.length < $scope.Worktypes.length) {
                for (var j = mark.StudentMarks.length; j < $scope.Worktypes.length; j++) {
                    mark.StudentMarks.push({ Key: $scope.Worktypes[j], Value: "" });
                }
            }
        }
    }

    // Select current mark
    $scope.selectMark = function (mark) {
        if (typeof (selectService.getMark()) != 'undefined') {
            document.getElementById(selectService.getMark().UserId).classList.remove('warning');
        }

        if (selectService.getMark() == mark) {
            selectService.selectMark(undefined);
            return;
        }
        selectService.selectMark(mark);

        document.getElementById(mark.UserId).classList.add('warning');
    };
});