﻿var app = angular.module('marksApp');

app.filter('completeMarks', function (storeService) {
    return function (marks) {
        var worktypes = storeService.getCurrent('marks').Key;

        for (var i = 0; i < marks.length; i++) {
            var mark = marks[i];

            if (mark.StudentMarks.length < worktypes.length) {
                for (var j = mark.StudentMarks.length; j < worktypes.length; j++) {
                    mark.StudentMarks.push({ Key: worktypes[j], Value: "" });
                }
            }
        }
    }
})