﻿var app = angular.module('groupApp');

app.directive('studentCreate', function ($http, communicationService, storeService, selectService) {
    return {
        restrict: 'E',
        templateUrl: '/Scripts/sw_scripts/Groups/Admin/Directives/Student/createTemplate.html',
        link: function (scope, element, attrs) {
            communicationService.register('student create open', function () {
                scope.Groups = storeService.getGroups();
                scope.createdStudent = {};
                var group = selectService.getGroup();
                
                scope.createdStudent.GroupNum = group.GroupId;
                $("#StudentCreateDialog").modal('show');
            });

            scope.cancelStudent = function () {
                console.clear();
                $('#StudentCreateDialog').modal('hide');
            }

            scope.createStudent = function () {
                var createForm = $('#StudentCreateForm');
                if (!createForm[0].checkValidity()) {
                    createForm.find(':submit').click();
                    return;
                }

                $http.post('/Groups/CreateStudent', scope.createdStudent).success(function (data) {
                    communicationService.execute('open message', [data]);
                    $('#StudentCreateDialog').modal('hide');
                    communicationService.execute('add student', [scope.createdStudent]);
                    console.log('Создан студент - ');
                    console.log(scope.createdStudent);
                });
            }
        }
    }
});