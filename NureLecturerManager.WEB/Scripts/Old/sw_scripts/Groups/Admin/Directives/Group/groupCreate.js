﻿var app = angular.module('groupApp');

app.directive('groupCreate', function ($http, communicationService) {
    return {
        restrict: "E",
        templateUrl: "/Scripts/sw_scripts/Groups/Admin/Directives/Group/createTemplate.html",
        link: function (scope, element, attrs) {
            communicationService.register('group create open', function () {
                $http.get("/Groups/CreateGroup").success(function (data) {
                    scope.createdGroup = {};
                    scope.createdGroup.Users = data;

                    if (scope.createdGroup.Users != undefined && scope.createdGroup.Users.length > 0) {
                        scope.createdGroup.CaptainName = scope.createdGroup.Users[0].FIO;
                    }

                    console.clear();
                    console.log(scope);

                    $('#GroupCreateDialog').modal('show');
                });
            });

            scope.cancel = function () {
                console.log('Group create dialog hided');
                $('#GroupCreateDialog').modal('hide');
            };

            scope.createGroup = function () {
                console.log("method createGroup");

                var user = { UserId: null };
                for (var i = 0; i < scope.createdGroup.Users.length; i++) {
                    if (scope.createdGroup.Users[i].FIO == scope.createdGroup.CaptainName) {
                        user = scope.createdGroup.Users[i];
                        break;
                    }
                }

                var form = $("#createGroupForm");
                if (!form[0].checkValidity()) {
                    form.find(":submit").click();
                    return;
                }

                $http.post('/Groups/CreateGroup', { GroupNum: scope.createdGroup.Name, CaptainId: user.UserId }).success(function (data) {
                    communicationService.execute('open message', [data]);
                    $('#GroupCreateDialog').modal('hide');

                    communicationService.execute('group create success', [scope.createdGroup.Name]);
                });
            }
        }
    }
})