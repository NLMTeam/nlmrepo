﻿var app = angular.module('groupApp');

app.directive('groupEdit', function ($http, communicationService, selectService) {
    return {
        restrict: 'E',
        templateUrl: "/Scripts/sw_scripts/Groups/Admin/Directives/Group/editTemplate.html",
        link: function (scope, element, attrs) {
            communicationService.register('group edit open', function () {
                var group = selectService.getGroup();

                if (typeof (group) == 'undefined') {
                    var message = { Title: "Инфо", Text: "Выберите, пожалуйста, группу для того чтобы сменить в ней старосту." };
                    communicationService.execute('open message', [message]);
                }

                $http.get('/Groups/ChangeCaptain?GroupNum=' + group.GroupId).success(function (data) {

                    if (typeof (data.Title) !== 'undefined') {
                        communicationService.execute('open message', [data]);
                        return;
                    }

                    scope.editedGroup = {};
                    if (data.Students.length > 0) { 
                        scope.editedGroup.Users = data.Students;
                    }
                    if (typeof (data.Captain) != 'undefined' && data.Captain != null) {
                        scope.editedGroup.CaptainName = data.Captain.FIO;
                    }
                    $('#GroupEditDialog').modal('show');

                });
            });

            scope.hide = function () {
                $('#GroupEditDialog').modal('hide');
            }

            scope.edit = function () {
                var user = { UserId: null };
                console.log(scope);
                for (var i = 0; i < scope.editedGroup.Users.length; i++) {
                    if (scope.editedGroup.Users[i].FIO == scope.editedGroup.CaptainName) {
                        user = scope.editedGroup.Users[i];
                        break;
                    }
                }
                $http.post('/Groups/ChangeCaptain', { captainId: user.UserId }).success(function (data) {
                    communicationService.execute('open message', [data]);
                    $('#GroupEditDialog').modal('hide');

                    communicationService.execute('reload students', []);
                });
            }
        }
    }
});