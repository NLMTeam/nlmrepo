﻿var Modal = function () {

};
Modal.prototype.showModal = function (modalId) {
    var element = document.getElementById(modalId);
    element.classList.add('show');
    element.classList.remove('fade');
};
Modal.prototype.hideModal = function (modalId) {
    var element = document.getElementById(modalId);
    element.classList.add('fade');
    element.classList.remove('show');
};