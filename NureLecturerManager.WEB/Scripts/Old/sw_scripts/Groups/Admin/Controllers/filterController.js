﻿var app = angular.module('groupApp');

app.controller('filter', function ($scope, $rootScope, $http, selectService, storeService, communicationService) {

    var controlLoad, studentLoad;
    $scope.loaded = controlLoad = studentLoad = false;

    storeService.loadYears().success(function (data) {
        $scope.Years = data;
        if ($scope.Years.length == 0) {
            $scope.Groups = [];
            $scope.GroupStudents = [];
            $scope.GroupControls = [];

            $scope.mode = {};
            $scope.mode.Value = "StudentControls";
            selectService.setMode('StudentControls');

            $scope.loaded = true;
            return;
        }
        storeService.loadGroups($scope.Years[0]).success(function (groups) {
            $scope.Groups = groups;

            if ($scope.Groups.length == 0) {
                $scope.GroupStudents = [];
                $scope.GroupControls = [];

                $scope.loaded = true;
                return;
            }

            storeService.loadControls(groups[0]).success(function (controls) {
                $scope.GroupControls = controls;
                selectService.setControl($scope.GroupControls[0]);
                controlLoad = true;
                if (studentLoad)
                    $scope.loaded = true;
            });

            storeService.loadStudents(groups[0]).success(function (students) {
                $scope.GroupStudents = students;
                selectService.setStudent(students[0]);
                studentLoad = true;
                if (controlLoad)
                    $scope.loaded = true;
            });
            selectService.setGroup($scope.Groups[0]);
            $scope.CurrentGroup = $scope.Groups[0];
            setTimeout(function () {
                $('#group' + $scope.CurrentGroup.GroupId).addClass('active');
            }, 1, {});
        });
        selectService.setYear($scope.Years[0]);
        $scope.mode = {};
        $scope.mode.Value = "StudentControls";
        selectService.setMode('StudentControls');
        selectService.setControlMode('my');
        $scope.mode.CurrentYear = $scope.Years[0];
    });

    $scope.switchToControls = function (mode) {
        mode.Value = "StudentControls";
        selectService.setMode("StudentControls");

    };
    $scope.switchToInfos = function (mode) {
        mode.Value = "StudentInfos";
        selectService.setMode("StudentInfos");
    };

    $scope.loadGroup = function (group) {
        storeService.loadStudents(group).success(function (data) {
            $scope.GroupStudents = data;
            if (typeof ($scope.chosedStudent) != 'undefined')
                delete $scope.chosedStudent;
        });
        storeService.loadControls(group).success(function (data) {
            $scope.GroupControls = data;
            if (typeof ($scope.chosedControl) != 'undefined')
                delete $scope.chosedControl;
        });

        if (typeof ($scope.CurrentGroup) != 'undefined') {
            var groupElement = document.getElementById('group' + $scope.CurrentGroup.GroupId);
            groupElement.classList.remove('active');
        }

        $scope.CurrentGroup = group;
        selectService.setGroup(group);

        console.log(selectService.getGroup());
    };

    $scope.filterGroups = function () {

        selectService.setYear($scope.mode.CurrentYear);
        delete $scope.CurrentGroup;
        selectService.setGroup(undefined);
        $scope.chosedStudent = undefined;
        $scope.chosedControl = undefined;

        storeService.loadGroups($scope.mode.CurrentYear).success(function (data) {
            $scope.Groups = data;
            delete $scope.CurrentGroup;

            if ($scope.Groups.length > 0) {
                if (selectService.getControlMode() == 'all') {
                    storeService.loadAllControls($scope.mode.CurrentYear).success(function (data) {
                        $scope.GroupControls = data;
                    });
                }
                else {
                    storeService.loadControls($scope.Groups[0]).success(function (data) {
                        $scope.GroupControls = data;
                    })
                }

                storeService.loadStudents($scope.Groups[0]).success(function (data) {
                    $scope.GroupStudents = data;
                })
            }

            if ($scope.Groups.length > 0) {
                selectService.setGroup($scope.Groups[0]);

                $scope.CurrentGroup = $scope.Groups[0];

                setTimeout(function () {
                    $('#group' + $scope.CurrentGroup.GroupId).addClass('active');
                }, 1, {});
            }
        });

    };

    $scope.choseGroupControl = function (control) {
        if (typeof ($scope.chosedControl) != 'undefined' && $scope.chosedControl != null) {
            var element = document.getElementById('control' + $scope.chosedControl.GroupControlId);
            element.classList.remove('warning');
        }

        $scope.chosedControl = control;
        selectService.setControl(control);

        var element = document.getElementById('control' + control.GroupControlId);
        element.classList.add('warning');
    }

    $scope.choseStudent = function (student) {
        if (typeof ($scope.chosedStudent) !== 'undefined' && $scope.chosedStudent != null) {
            var element = document.getElementById('student' + $scope.chosedStudent.UserId);
            element.classList.remove('warning');
        }
        $scope.chosedStudent = student;
        selectService.setStudent(student);

        var element = document.getElementById('student' + student.UserId);

        element.classList.add('warning');
    }

    $scope.loadGroupControls = function () {
        selectService.setControlMode("my");
        if (typeof (storeService.getControls()) == 'undefined') {
            storeService.loadControls(selectService.getGroup()).success(function (data) {
                $scope.GroupControls = data;
            });
        }
        else {
            $scope.GroupControls = storeService.getControls();
        }
    }

    $scope.loadAllControls = function () {
        selectService.setControlMode('all');

        storeService.loadAllControls(selectService.getYear()).success(function (data) {
            $scope.GroupControls = data;
        });
    }

    communicationService.register('reload controls', function (group) {
        if (selectService.getGroup().GroupId == group) {
            storeService.loadControls(selectService.getGroup()).success(function (data) {
                $scope.GroupControls = data;
            });
        }
    });

    communicationService.register('update edited control', function (control) {
        var controls = [];

        for (var i = 0; i < $scope.GroupControls.length; i++) {
            var cont = $scope.GroupControls[i];

            if (cont.GroupControlId != control.GroupControlId) {
                controls.push(cont);
            }
            else {
                console.log(control);
                if (typeof (control.toLocaleDateString) != 'undefined') {
                    controls.push({
                        WorkType: control.WorkTypeName, GroupControlId: control.GroupControlId, GroupNum: control.GroupNum,
                        Place: control.Place, Type: control.Type, Date: control.Date, FormatDate: control.Date.toLocaleDateString()
                    });
                }
                else {
                    controls.push({
                        WorkType: control.WorkTypeName, GroupControlId: control.GroupControlId, GroupNum: control.GroupNum,
                        Place: control.Place, Type: control.Type, Date: control.Date, FormatDate: control.Date.toLocaleDateString()
                    });
                }
            }
        }

        $("#control" + control.GroupControlId).removeClass('bg-selected-warning');
        $scope.GroupControls = controls;

        delete $scope.chosedControl;
        selectService.setControl(undefined);
    });

    communicationService.register('delete success', function (control) {
        var controls = [];

        for (var i = 0; i < $scope.GroupControls.length; i++) {
            var cont = $scope.GroupControls[i];

            if (cont.GroupControlId != control.GroupControlId) {
                controls.push(cont);
            }
        }

        $scope.GroupControls = controls;

        delete $scope.chosedControl;
        selectService.setControl(undefined);
    })

    communicationService.register('add student', function (student) {
        if (selectService.getGroup().GroupId == student.GroupNum) {
            storeService.loadStudents(selectService.getGroup()).success(function (students) {
                $scope.GroupStudents = students;
            });
        }
        delete $scope.chosedStudent;
        selectService.setStudent(undefined);
    });

    communicationService.register('edit student success', function (student) {
        var students = [];

        for (var i = 0; i < $scope.GroupStudents.length; i++) {
            var stud = $scope.GroupStudents[i];

            if (stud.UserId != student.userId) {
                students.push(stud);
            }
            else {
                student.UserId = student.userId;
                students.push(student);
            }
        }
        console.log(students);
        $scope.GroupStudents = students;
    })

    communicationService.register('delete student success', function (student) {
        var students = [];

        for (var i = 0; i < $scope.GroupStudents.length; i++) {
            var stud = $scope.GroupStudents[i];

            if (stud.UserId != student.UserId) {
                students.push(stud);
            }
        }

        $scope.GroupStudents = students;
    })

    communicationService.register('group delete success', function (group) {
        var groups = [];

        console.clear();
        console.log(group);

        for (var i = 0; i < $scope.Groups.length; i++) {
            if ($scope.Groups[i].GroupId != group.GroupId) {
                groups.push($scope.Groups[i]);
            }
        }

        console.log(groups);
        console.log($scope.Groups);
        $scope.Groups = groups;
        delete $scope.CurrentGroup;
        selectService.setGroup(undefined);
    })

    communicationService.register('group create success', function (groupId) {
        var groupSplit = groupId.split('-');

        var year = "20" + groupSplit[1];

        $http.get('/Groups/GetGroupAndYears?year=' + year).success(function (data) {

            $scope.Years = data.Key;
            $scope.Groups = data.Value;

            $scope.mode.CurrentYear = year;
            if ($scope.Groups.length > 0) {

                $scope.CurrentGroup = $scope.Groups[0];

                setTimeout(function () {
                    $('#group' + $scope.CurrentGroup.GroupId).addClass('active');
                }, 1, {});
            }
        })
    })

    communicationService.register('reload students', function () {
        storeService.loadStudents(selectService.getGroup()).success(function (students) {
            $scope.GroupStudents = students;
        });
        delete $scope.chosedStudent;
        selectService.setStudent(undefined);
    });
});