﻿var app = angular.module('groupApp');

app.controller('groupInit', function ($scope, communicationService) {
    $scope.create = function () {
        communicationService.execute('group create', []);
    };

    $scope.delete = function () {
        communicationService.execute('group delete', []);
    };

    $scope.changeCaptain = function () {
        communicationService.execute('group edit', []);
    }
});