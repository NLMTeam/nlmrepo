﻿var groupApp = angular.module("groupApp", []);

groupApp.controller("groupController", function groupController($scope, $http, selectService) {

    $scope.Years = window.Years;
    $scope.Groups = window.Groups;
    $scope.GroupControls = window.GroupControls;
    $scope.GroupStudents = window.GroupStudents;

    $scope.mode = {};
    $scope.mode.Value = "StudentControls";
    $scope.mode.CurrentYear = $scope.Years[0];
    $scope.createdGroup = {};
    $scope.createdGroup.Users = [];

    $scope.switchToControls = function (mode) {
        mode.Value = "StudentControls";
        selectService.setMode("StudentControls");
    };
    $scope.switchToInfos = function (mode) {
        mode.Value = "StudentInfos";
        selectService.setMode("StudentInfos");
    };

    $scope.loadGroup = function (group) {
        window.event.preventDefault();
        event.stopPropagation();
        $http.get("/Groups/GetGroupStudents?GroupId=" + group.GroupId + "&CaptainId=" + group.CaptainId).success(function (data) {
            $scope.GroupStudents = data;
            if (typeof ($scope.chosedStudent) != 'undefined')
                delete $scope.chosedStudent;
        });
        $http.get("/Groups/GetGroupControls?GroupId=" + group.GroupId + "&CaptainId=" + group.CaptainId).success(function (data) {
            $scope.GroupControls = data;
            if (typeof ($scope.chosedControl) != 'undefined')
                delete $scope.chosedControl;
        });

        if (typeof ($scope.CurrentGroup) != 'undefined') {
            var groupElement = document.getElementById('group' + $scope.CurrentGroup.GroupId);
            groupElement.classList.remove('active');
        }

        $scope.CurrentGroup = group;
        selectService.setGroup(group);

        var groupElement = document.getElementById('group' + group.GroupId);
        groupElement.classList.add('active');
    };

    $scope.filterGroups = function () {
        selectService.setYear($scope.mode.CurrentYear);
        $http.get("/Groups/GetYearGroups?year=" + $scope.mode.CurrentYear).success(function (data) {
            $scope.Groups = data;
        });
    };

    $scope.showGroupCreationModal = function () {
        $http.get("/Groups/CreateGroup").success(function (data) {
            (new Modal()).showModal('GroupCreateDialog');
            $scope.createdGroup.Users = data;
        });
    };
    $scope.cancelGroupCreation = function () {
        (new Modal()).showModal('GroupCreateDialog');
    };

    $scope.createGroup = function () {
        var user = { UserId: null };
        for (var i = 0; i < $scope.createdGroup.Users.length; i++) {
            if ($scope.createdGroup.Users[i].FIO == $scope.createdGroup.CaptainName) {
                user = $scope.createdGroup.Users[i];
                break;
            }
        }
        $http.post('/Groups/CreateGroup', { GroupNum: $scope.createdGroup.Name, CaptainId: user.UserId }).success(function (data) {
            console.log(data);
            $scope.Message = data;
            (new Modal()).showModal('GroupCreateDialog');
            (new Modal()).showModal('MessageDialog');
        });
    };

    $scope.deleteGroup = function () {
        $http.post('/Groups/DeleteGroup', { groupNum: $scope.CurrentGroup.GroupId }).success(function (data) {
            $scope.Message = data;
            (new Modal()).showModal('MessageDialog');
        });
        delete $scope.CurrentGroup;
    }

    $scope.changeCaptain = function () {
        if (typeof ($scope.CurrentGroup) == 'undefined') {
            $scope.Message = {};
            $scope.Message.Title = "Инфо";
            $scope.Message.Text = "Выберите, пожалуйста, группу для того чтобы сменить в ней старосту.";
            (new Modal()).showModal('MessageDialog');
        }
        $http.get('/Groups/ChangeCaptain?GroupNum=' + $scope.CurrentGroup.GroupId).success(function (data) {
            if (typeof (data.Title) !== 'undefined') {
                $scope.Message = data;
                (new Modal()).showModal('MessageDialog');
                return;
            }
            $scope.editedGroup = {};
            $scope.editedGroup.Users = data;
            (new Modal()).showModal('GroupEditDialog');

            $scope.$apply();

            var select = document.getElementById('captainSelect');
            select.firstChild.remove();
        });
    }

    $scope.editGroup = function () {
        console.log($scope);
        var user = { UserId: null };
        for (var i = 0; i < $scope.editedGroup.Users.length; i++) {
            if ($scope.editedGroup.Users[i].FIO == $scope.editedGroup.CaptainName) {
                user = $scope.editedGroup.Users[i];
                break;
            }
        }
        $http.post('/Groups/ChangeCaptain', { captainId: user.UserId }).success(function (data) {
            $scope.Message = data;
            var modal = new Modal();
            modal.hideModal('GroupEditDialog');
            modal.showModal('MessageDialog');
        });
    }

    $scope.hideMessageDialog = function () {
        var modal = new Modal();
        modal.hideModal('MessageDialog');
    }
    $scope.hideGroupEditDialog = function () {
        var modal = new Modal();
        modal.hideModal('GroupEditDialog');
    }
    $scope.cancel = function () {
        (new Modal()).hideModal($scope.openModalId);
    }

    // Navbar buttons

    // Group controls create, edit delete

    $scope.openGroupControlCreate = function () {
        $http.get("/Groups/CreateGroupControl").success(function (data) {
            $scope.createControl = {};
            $scope.createControl.WorkTypes = data;
            (new Modal()).showModal("GroupControlCreateDialog");
        });
    };
    $scope.cancelGroupControlCreation = function () {
        (new Modal()).hideModal('GroupControlCreateDialog');
    };

    $scope.createGroupControl = function () {
        $http.post("/Groups/CreateGroupControl", {
            WorkType: $scope.createControl.WorkTypeName, GroupNum: $scope.createControl.GroupNum, Type: $scope.createControl.Type,
            Date: $scope.createControl.Date, Place: $scope.createControl.Place
        }).success(function (data) {
            $scope.Message = data;

            var modal = new Modal();
            modal.hideModal('GroupControlCreateDialog');
            modal.showModal("MessageDialog");

            if (typeof ($scope.CurrentGroup) == 'undefined') {
                if ($scope.Groups[0].GroupId == $scope.createControl.GroupNum) {
                    $http.get("/Groups/GetGroupControls?GroupId=" + $scope.Groups[0].GroupId + "&CaptainId=" + $scope.Groups[0].CaptainId).success(function (data) {
                        $scope.GroupControls = data;
                    });
                }
            }
            else if ($scope.CurrentGroup.GroupId == $scope.createControl.GroupNum) {
                $http.get("/Groups/GetGroupControls?GroupId=" + $scope.CurrentGroup.GroupId + "&CaptainId=" + $scope.CurrentGroup.CaptainId).success(function (data) {
                    $scope.GroupControls = data;
                });
            }
        });
        delete $scope.chosedControl;
    }
    $scope.deleteStudentControl = function () {
        var modal = new Modal();
        if (typeof ($scope.chosedControl) == 'undefined') {
            $scope.Message = { Title: 'Инфо', Text: 'Выберите контроль в таблице для того, чтобы его удалить.' };
            modal.showModal('MessageDialog');
        }

        $http.post('/Groups/DeleteGroupControl', { id: $scope.chosedControl.GroupControlId }).success(function (data) {
            $scope.Message = data;
            modal.showModal('MessageDialog');
            if (data.Title != 'Ошибка') {
                if (typeof ($scope.CurrentGroup) == 'undefined') {
                    if ($scope.Groups[0].GroupId == $scope.createControl.GroupNum) {
                        $http.get("/Groups/GetGroupControls?GroupId=" + $scope.Groups[0].GroupId + "&CaptainId=" + $scope.Groups[0].CaptainId).success(function (data) {
                            $scope.GroupControls = data;
                        });
                    }
                }
                else if ($scope.CurrentGroup.GroupId == $scope.createControl.GroupNum) {
                    $http.get("/Groups/GetGroupControls?GroupId=" + $scope.CurrentGroup.GroupId + "&CaptainId=" + $scope.CurrentGroup.CaptainId).success(function (data) {
                        $scope.GroupControls = data;
                    });
                }
            }
        });
        delete $scope.chosedControl;
    };
    $scope.openGroupControlEdit = function () {
        if (typeof ($scope.chosedControl) == 'undefined') {
            $scope.Message = { Title: "Инфо", Text: "Выберите контроль для того чтобы его отредактировать." };
        }
        $http.get('/Groups/EditGroupControl?groupControlId=' + $scope.chosedControl.GroupControlId).success(function (data) {
            $scope.editControl = {
                WorkTypes: data, Type: $scope.chosedControl.Type, Date: $scope.chosedControl.FormatDate,
                Place: $scope.chosedControl.Place, GroupControlId: $scope.chosedControl.GroupControlId
            };

            $scope.$apply("");

            var element = document.getElementById('groupControlEditSelect');
            element.firstChild.remove();

            element.value = $scope.chosedControl.WorkType;

            (new Modal()).showModal('GroupControlEditDialog');
            $scope.openModalId = 'GroupControlEditDialog';
        })
    }

    $scope.editGroupControl = function () {
        var workType = { WorkTypeId: -1 };
        for (var i = 0; i < $scope.editControl.WorkTypes.Length; i++) {
            if ($scope.editControl.WorkTypes[i].Name == $scope.editControl.WorkTypeName) {
                workType = $scope.editControl.WorkTypes[i];
            }
        }

        $http.post('/Groups/EditGroupControl', {
            WorkType: $scope.editControl.WorkTypeName, Type: $scope.editControl.Type,
            PassDate: $scope.editControl.Date, Place: $scope.editControl.Place
        }).success(function (data) {

            var modal = new Modal();
            $scope.Message = data;
            modal.showModal('MessageDialog');
            modal.hideModal('GroupControlEditDialog');

            console.log($scope);

            if (typeof ($scope.CurrentGroup) == 'undefined') {
                if ($scope.Groups[0].GroupId == $scope.editControl.GroupNum) {
                    $http.get("/Groups/GetGroupControls?GroupId=" + $scope.Groups[0].GroupId + "&CaptainId=" + $scope.Groups[0].CaptainId).success(function (data) {
                        $scope.GroupControls = data;
                    });
                }
            }
            else if ($scope.CurrentGroup.GroupId == $scope.editControl.GroupNum) {
                $http.get("/Groups/GetGroupControls?GroupId=" + $scope.CurrentGroup.GroupId + "&CaptainId=" + $scope.CurrentGroup.CaptainId).success(function (data) {
                    $scope.GroupControls = data;
                });
            }

        });
    }

    // Student Create, Edit Delete

    $scope.createStudent = function () {
        $http.post('/Groups/CreateStudent', $scope.createdStudent).success(function (data) {
            $scope.Message = data;
            var modal = new Modal();
            modal.hideModal('StudentCreateDialog');
            modal.showModal('MessageDialog');
            if (typeof ($scope.CurrentGroup) == 'undefined') {
                if ($scope.Groups[0].GroupId == $scope.createdStudent.GroupNum) {
                    $http.get("/Groups/GetGroupStudents?GroupId=" + $scope.Groups[0].GroupId + "&CaptainId=" + $scope.Groups[0].CaptainId).success(function (data) {
                        $scope.GroupStudents = data;
                    });
                }
            }
            else {
                if ($scope.CurrentGroup.GroupId == $scope.createdStudent.GroupNum) {
                    $http.get("/Groups/GetGroupStudents?GroupId=" + $scope.CurrentGroup.GroupId + "&CaptainId=" + $scope.CurrentGroup.CaptainId).success(function (data) {
                        $scope.GroupStudents = data;
                    });
                }
            }
        });
        delete $scope.chosedStudent;
    };

    $scope.deleteStudentInfo = function () {
        var modal = new Modal();
        if (typeof ($scope.chosedStudent) == 'undefined') {
            $scope.Message = { Title: 'Инфо', Text: 'Выберите студента в таблице для того, чтобы его удалить.' };
            modal.showModal('MessageDialog');
            return;
        }

        $http.post('/Groups/DeleteStudent', { GroupNum: $scope.chosedStudent.GroupId, userId: $scope.chosedStudent.UserId }).success(function (data) {
            $scope.Message = data;
            modal.showModal('MessageDialog');

            if (typeof ($scope.CurrentGroup) == 'undefined') {
                $http.get("/Groups/GetGroupStudents?GroupId=" + $scope.Groups[0].GroupId + "&CaptainId=" + $scope.Groups[0].CaptainId).success(function (data) {
                    $scope.GroupStudents = data;
                });
            } else {
                $http.get("/Groups/GetGroupStudents?GroupId=" + $scope.CurrentGroup.GroupId + "&CaptainId=" + $scope.CurrentGroup.CaptainId).success(function (data) {
                    $scope.GroupStudents = data;
                });
            }
        });

        delete $scope.chosedStudent;

    };

    $scope.openStudentEdit = function () {
        if (typeof ($scope.editedStudent) == 'undefined') {
            $scope.Message = { Title: "Инфо", Text: 'Выберите студента для того, чтобы отредактировать.' };
            (new Modal()).showModal('MessageDialog');
        }
        $scope.editedStudent = { userId: $scope.chosedStudent.UserId, FIO: $scope.chosedStudent.FIO, Mail: $scope.chosedStudent.Mail };

        (new Modal()).showModal("StudentEditDialog");
        $scope.openModalId = "StudentEditDialog";
        var element = document.getElementById('studentEditSelect');
        if (typeof ($scope.CurrentGroup) != 'undefined') {
            element.value = $scope.CurrentGroup.GroupId;
        }
        else {
            element.value = $scope.Groups[0].GroupId;
        }
    }

    $scope.editStudent = function () {
        $http.post('/Groups/EditStudent', $scope.editedStudent).success(function (data) {
            $scope.Message = data;

            var modal = new Modal();
            modal.showModal('MessageDialog');
            modal.hideModal('StudentEditDialog');

            if (typeof ($scope.CurrentGroup) != 'undefined') {
                $http.get("/Groups/GetGroupStudents?GroupId=" + $scope.CurrentGroup.GroupId + "&CaptainId=" + $scope.CurrentGroup.CaptainId).success(function (data) {
                    $scope.GroupStudents = data;
                });
            }
            else {
                $http.get("/Groups/GetGroupStudents?GroupId=" + $scope.Groups[0].GroupId + "&CaptainId=" + $scope.Groups[0].CaptainId).success(function (data) {
                    $scope.GroupStudents = data;
                });
            }
        });
    }

});

