﻿var app = angular.module('groupApp');

app.controller('navbar', function ($scope, $http, selectService, communicationService, studentCUDservice, controlCUDservice, groupCUDservice, FileUploader, storeService) {

    $scope.fileLoading = "no-load";

    $scope.showCreateModal = function () {
        if (storeService.getGroups().length == 0) {
            return;
        }
        var mode = selectService.getMode();
        if (mode == "StudentControls") {
            communicationService.execute('control create');
        }
        else {
            communicationService.execute('student create');
        }
    };

    $scope.confirmStudentDelete = function () {
        storeService.printData();
        var mode = selectService.getMode();
        if (mode == 'StudentInfos') {
            var students = storeService.getStudents();
            var student = selectService.getStudent();

            if (students == undefined || students.length == 0 || student == undefined) {
                return;
            }
            
            communicationService.execute('student delete');
        }
        else {
            if (selectService.getControlMode() == "my") {
                var controls = storeService.getControls();
            }
            else {
                var controls = storeService.getAllControls();
            }

            var control = selectService.getControl();

            if (controls == undefined || controls.length == 0 || control == undefined) {
                return;
            }
            communicationService.execute('control delete')
        }
    }

    $scope.deleteStudent = function () {
        delete $scope.delete;

        var modal = new Modal();
        modal.hideModal('confirmDelete');
    }

    $scope.showEditModal = function () {
        var mode = selectService.getMode();
        if (mode == "StudentControls") {
            if (selectService.getControlMode() == "my") {
                var controls = storeService.getControls();
            }
            else {
                var controls = storeService.getAllControls();
            }
            var control = selectService.getControl();
            if (controls == undefined || controls.length == 0 || control == undefined) {
                return;
            }
            communicationService.execute('control edit');
        }
        else {
            var students = storeService.getStudents();
            var student = selectService.getStudent();
            if (students == undefined || students.length == 0 || student == undefined) {
                return;
            }
            communicationService.execute('student edit');
        }
    }

    $scope.generateFile = function () {
        var group = selectService.getGroup();
        var mode = selectService.getMode();
        if (mode == "StudentInfos") {
            $http.get("/Groups/GenerateGroupExcelFile?groupId=" + group.GroupId).success(function (url) {
                window.open(url);
            })
        }
        else {
            $http.get('/Groups/GenerateControlExcelFile?groupId=' + group.GroupId).success(function (url) {
                window.open(url);
            })
        }
    }

    $scope.uploader = new FileUploader({
        url: ""
    });

    $scope.uploadFile = function () {

        $("#uploadFile").click();

        var group = selectService.getGroup();
        var mode = selectService.getMode();

        if (mode == "StudentInfos") {
            var url1 = "/Groups/UploadGroupExcelFile?groupId=" + group.GroupId;
        }
        else {
            var url1 = "/Groups/UploadControlExcelFile?groupId=" + group.GroupId;
        }

        $("#uploadFile").on('change', function (e) {

            $scope.fileLoading = "loading";
            $scope.uploader.url = url1;

            $scope.item = $scope.uploader.queue[0];
            $scope.item.url = url1;

            $scope.item.remove = function () {
                $scope.uploader.queue[0].remove();
                $scope.fileLoading = "no-load";
            }

            $scope.item.cancel = function () {
                $scope.uploader.queue[0].cancel();
            }

            $scope.uploader.onProgressItem = function (fileName, progress) {
                if (progress == 100) {
                    $scope.fileLoading = "server-processing";
                    try {
                        $scope.$apply();
                    }
                    catch (e) {
                        console.warn(e.message);
                    }
                }
            }

            $scope.uploader.onSuccessItem = function (file, response, status, headers) {
                $scope.fileLoading = "no-load";

                communicationService.execute('open message', [response]);

                if (mode == "StudentInfos") {
                    communicationService.execute('reload students', []);
                }
                else {
                    communicationService.execute('reload controls', [selectService.getGroup().GroupId]);
                }
            }

            $scope.uploader.onErrorItem = function (file, response, status, headers) {
                communicationService.execute('open message', [{ Title : "Ошибка", Text : e }]);
            }

            //$scope.item.upload = function () {
            //    $scope.uploader.queue[0].upload();
            //}

            console.log($scope);

            try {
                $scope.$apply();
            }
            catch (e) {
                console.warn(e.message);
            }
            //var files = e.target.files;
            //if (files.length > 0) {
            //    var data = new FormData();
            //    for (var x = 0; x < files.length; x++) {
            //        data.append("file" + x, files[x]);
            //    }
            //    $scope.fileLoading = true;
            //    $scope.$apply();
            //    $.ajax({
            //        url: url1,
            //        data: data,
            //        cache: false,
            //        contentType: false,
            //        processData: false,
            //        type: "POST",
            //        success: function (data) {
            //            $scope.fileLoading = false;
            //            communicationService.execute('open message', [data]);

            //            if (mode == "StudentInfos") {
            //                communicationService.execute('reload students', []);
            //            }
            //            else {
            //                communicationService.execute('reload controls', [selectService.getGroup().GroupId]);
            //            }
            //        },
            //    });
            //}
        })
    }
})