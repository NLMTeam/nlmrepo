﻿var app = angular.module('groupApp');

app.service('controlCUDservice', function (communicationService) {
    var obj = {};
    communicationService.register('control create', function () {
        console.log('control create');
        communicationService.execute('control create open');
    });

    communicationService.register('control edit', function () {
        console.log('control edit');
        communicationService.execute('control edit open');
    });

    communicationService.register('control delete', function () {
        console.log('control delete');
        communicationService.execute('control delete open');
    });

    return obj;
});