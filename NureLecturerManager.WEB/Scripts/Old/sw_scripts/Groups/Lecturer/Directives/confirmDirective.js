﻿var app = angular.module('groupApp');

app.directive('confirmDelete', function (communicationService) {
    return {
        restrict: 'E',
        templateUrl: '/Scripts/sw_scripts/Groups/Lecturer/Directives/confirmTemplate.html',
        link: function (scope, element, attrs) {
            var _callback = function () { };
            communicationService.register('confirm delete', function (message, callback) {
                scope.Message = message;
                $('#confirmDelete').modal('show');
                _callback = callback;
            });

            scope.cancelConfirm = function () {
                $('#confirmDelete').modal('hide');
            };

            scope.delete = function () {
                $('#confirmDelete').modal('hide');
                _callback.apply(this, []);
            };
        }
    }
})