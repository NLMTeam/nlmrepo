﻿var app = angular.module('groupApp');

app.directive('controlCreate', function ($http, communicationService, storeService) {
    return {
        restrict: 'E',
        scope: {},
        link: function (scope, element, attrs) {
            communicationService.register('control create open', function () {
                $http.get("/Groups/CreateGroupControl").success(function (data) {
                    if (data.length == 0) {
                        communicationService.execute('open message', [{ Title: "Ошибка", Text: "Не найдены виды работ." }]);
                        return;
                    }
                    scope.createControl = {};
                    scope.createControl.WorkTypes = data;
                    scope.Groups = storeService.getGroups();
                    scope.createControl.WorkTypeName = scope.createControl.WorkTypes[0].Name;
                    scope.createControl.GroupNum = scope.Groups[0].GroupId;
                    $('#GroupControlCreateDialog').modal('show');
                });
            });
            scope.cancelCreateControl = function () {
                $('#GroupControlCreateDialog').modal('hide');
            };
            scope.createCont = function () {
                console.log($('#createControlForm')[0].checkValidity());
                if (!$('#createControlForm')[0].checkValidity()) {
                    $('#createControlForm').find(':submit').click();
                    return;
                }
                $http.post("/Groups/CreateGroupControl", {
                    WorkType: scope.createControl.WorkTypeName, GroupNum: scope.createControl.GroupNum, Type: scope.createControl.Type,
                    Date: scope.createControl.Date, Place: scope.createControl.Place
                }).success(function (data) {
                    scope.Message = data;

                    console.log('closing modal');
                    $("#GroupControlCreateDialog").modal('hide');

                    communicationService.execute('open message', [data]);

                    communicationService.execute('reload controls', [scope.createControl.GroupNum]);
                });
            }
        },
        templateUrl: "/Scripts/sw_scripts/Groups/Lecturer/Directives/Control/createTemplate.html"
    };
});