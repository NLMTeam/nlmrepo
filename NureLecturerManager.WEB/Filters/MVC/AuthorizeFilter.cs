﻿using NLM.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Http.Controllers;
using NLM.BLL.DTO;
using System.Web;

namespace NLM.WEB.Filters.MVC
{
    public class AuthorizeFilter : System.Web.Mvc.AuthorizeAttribute
    {
        private string[] allowedRoles = null;

        public AuthorizeFilter(params string[] roles)
        {
            allowedRoles = roles;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            UserDTO user = (UserDTO)filterContext.HttpContext.Session["User"];

            if (user == null)
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }

            if (!allowedRoles.Contains(user.Type))
            {
                filterContext.Result = new HttpStatusCodeResult(403);
            }
        }
    }
}
