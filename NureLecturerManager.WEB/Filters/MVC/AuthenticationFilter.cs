﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLM.BLL.DTO;
using System.IO;
using NLM.BLL.Interfaces;
using NLM.BLL.Exceptions;
using NLM.WEB.Controllers;
using System.Text;
using System.Web.Mvc.Filters;

namespace NLM.WEB.Filters.MVC
{
    public class AuthenticationFilter : FilterAttribute, IAuthenticationFilter
    {
        private IAccountService userService = DependencyResolver.Current.GetService<IAccountService>();

        public void OnAuthentication(AuthenticationContext filterContext)
        {
            UserDTO user = (UserDTO)filterContext.HttpContext.Session["User"];
            String year = null;
            String semester = null;
            byte[] token = null;

            if (user == null)
            {
                if (!tryRestoreFromCookies(filterContext, ref user, ref year, ref semester, ref token))
                {
                    filterContext.HttpContext.Session["returnAction"] = filterContext.ActionDescriptor.ActionName;
                    filterContext.HttpContext.Session["returnController"] = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;

                    filterContext.Result = new HttpUnauthorizedResult();
                }
                else
                {
                    filterContext.HttpContext.Session["User"] = user;
                    filterContext.HttpContext.Session["Type"] = user.Type;

                    lock (ConfigController.threadLock)
                    {
                        using (StreamReader stR = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("/Files/Config.txt")))
                        {
                            filterContext.HttpContext.Session["Year"] = stR.ReadLine();
                            filterContext.HttpContext.Session["Semester"] = stR.ReadLine();
                        }
                    }

                    filterContext.HttpContext.Session["Token"] = token;
                }
            }
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
        }

        private bool tryRestoreFromCookies(AuthenticationContext context, ref UserDTO user, ref String year, ref String semester, ref Byte[] token)
        {
            user = new UserDTO();
            Int32 userId = -1;

            if (context.HttpContext.Request.Cookies.Get("pass3") != null)
            {
                // Не работает декодирование!!!
                String hex = context.HttpContext.Request.Cookies.Get("pass3").Value;

                int NumberChars = hex.Length;
                token = new byte[NumberChars / 2];
                for (int i = 0; i < NumberChars; i += 2)
                    token[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
                userId = userService.GetUserIdByToken(token);
            }
            else
            {
                return false;
            }


            if (userId == -1)
            {
                if (context.HttpContext.Request.Cookies["pass3"] != null)
                {
                    var cookie = new HttpCookie("pass3")
                    {
                        Expires = DateTime.Now.AddDays(-1d)
                    };
                    context.HttpContext.Request.Cookies.Add(cookie);
                }
                return false;
            }

            var temp = userService.SearchUser(userId).SingleOrDefault();

            if (temp == null)
            {
                return false;
            }
            else
            {
                user.Mail = temp.Mail;
                user.FIO = temp.FIO;
                user.Image = temp.Image;
                user.Type = temp.Type;
                user.Login = temp.Login;
                user.UserId = temp.UserId;
            }
            return true;
        }

        //private void UpdateCookies(AuthorizationContext context, UserDTO user)
        //{
        //    if (context.HttpContext.Request.Cookies.Get("Login") == null)
        //    {
        //        context.HttpContext.Response.Cookies.Add(new HttpCookie("Login", user.Login));
        //    }

        //    if (context.HttpContext.Request.Cookies.Get("FIO") == null)
        //        context.HttpContext.Request.Cookies.Add(new HttpCookie("FIO", user.FIO));

        //    if (context.HttpContext.Request.Cookies.Get("Image") == null)
        //        context.HttpContext.Request.Cookies.Add(new HttpCookie("Image", user.Image));

        //    if (context.HttpContext.Request.Cookies.Get("Mail") == null)
        //        context.HttpContext.Request.Cookies.Add(new HttpCookie("Mail", user.Mail));

        //    if (context.HttpContext.Request.Cookies.Get("Type") == null)
        //        context.HttpContext.Request.Cookies.Add(new HttpCookie("Type", user.Type));
        //}
    }
}