﻿using NLM.BLL.DTO;
using NLM.BLL.Exceptions;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NLM.WEB.Filters.MVC
{
    public class OnExceptionFilter: FilterAttribute, IExceptionFilter
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled)
            {
                UserDTO user = (UserDTO)filterContext.HttpContext.Session["User"];
                logger.Error(String.Format("Controller: {0} || Message: {1} || UserName: {2} || StackTrace: {3}", filterContext.Controller, filterContext.Exception.Message, user == null ? null : user.Login, filterContext.Exception.StackTrace));

                if (filterContext.Exception.GetType() == typeof(NotExpectedUserTypeException))
                {
                    filterContext.Result =
                         new RedirectResult("/Account/LogOff");
                }
                else
                {
                    filterContext.Result =
                        new RedirectResult("~/Content/ErrorPage.html");
                }
                filterContext.ExceptionHandled = true;
            }
        }
    }
}