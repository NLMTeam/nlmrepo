﻿using NLM.BLL.DTO;
using NLM.BLL.Exceptions;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace NLM.WEB.Filters.API
{
    public class APIExceptionFilter : Attribute, IExceptionFilter
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public bool AllowMultiple
        {
            get { return true; }
        }

        public Task ExecuteExceptionFilterAsync(HttpActionExecutedContext context, CancellationToken cancellationToken)
        {
            logger.Error(String.Format("Controller: {0} || Message: {1} || Exception Type: {2} || StackTrace: {3}", context.ActionContext.ControllerContext.ControllerDescriptor.ControllerName, 
                context.Exception.Message, context.Exception.GetType(), context.Exception.StackTrace));

            if (context.Exception.GetType() == typeof(NotExpectedUserTypeException))
            {
                context.Response = context.Request.CreateResponse(HttpStatusCode.Redirect);
                context.Response.Headers.Location = new Uri("/Account/LogOff"); // not sure about uri
                //context.Response = new RedirectResult(new Uri("/Account/LogOff"), context.Request);
            }
            else
            {
                context.Response = context.Request.CreateResponse(HttpStatusCode.Redirect);
                context.Response.Headers.Location = new Uri("~/Content/ErrorPage.html"); // not sure about uri
                    //filterContext.Result =
                     //new RedirectResult("~/Content/ErrorPage.html");
            }

            return Task.FromResult<object>(null);
        }

        //public void OnException(ExceptionContext filterContext)
        //{
        //    if (!filterContext.ExceptionHandled)
        //    {
        //        UserDTO user = (UserDTO)filterContext.HttpContext.Session["User"];
        //        logger.Error(String.Format("Controller: {0} || Message: {1} || UserName: {2} || StackTrace: {3}", filterContext.Controller, filterContext.Exception.Message, user == null ? null : user.Login, filterContext.Exception.StackTrace));

        //        if (filterContext.Exception.GetType() == typeof(NotExpectedUserTypeException))
        //        {
        //            filterContext.Result =
        //                 new RedirectResult("/Account/LogOff");
        //        }
        //        else
        //        {
        //            filterContext.Result =
        //                new RedirectResult("~/Content/ErrorPage.html");
        //        }
        //        filterContext.ExceptionHandled = true;
        //    }
        //}
    }
}