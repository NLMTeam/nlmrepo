﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLM.BLL.DTObjects;

namespace NLM.WEB.Util.Comparators
{
    public class DisciplineForYearComparator : IEqualityComparer<DisciplineForYearDTO>
    {
        public bool Equals(DisciplineForYearDTO x, DisciplineForYearDTO y)
        {
            if (x == null && y == null)
            {
                return true;
            }
            if (x == null || y == null)
            {
                return false;
            }
            return x.DisciplineForYearId == y.DisciplineForYearId;
        }

        public int GetHashCode(DisciplineForYearDTO obj)
        {
            return obj.DisciplineForYearId.GetHashCode();
        }
    }
}