﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLM.BLL.DTO;

namespace NLM.WEB.Util.Comparators
{
    public class GroupControlComparer : IEqualityComparer<GroupControlDTO>
    {
        public bool Equals(GroupControlDTO x, GroupControlDTO y)
        {
            if (x == null && y == null)
            {
                return true;
            }
            else if (x == null || y == null)
            {
                return false;
            }
            else
            {
                return x.GroupControlId == y.GroupControlId;
            }
        }

        public int GetHashCode(GroupControlDTO obj)
        {
            return obj.GroupControlId.GetHashCode();
        }
    }
}