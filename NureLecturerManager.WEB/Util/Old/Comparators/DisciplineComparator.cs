﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NLM.BLL.DTO;

namespace NLM.WEB.Util.Comparators
{
    public class DisciplineComparator : IEqualityComparer<DisciplineDTO>
    {
        public bool Equals(DisciplineDTO x, DisciplineDTO y)
        {
            if (x == null && y == null)
            {
                return true;
            }
            else if (x == null || y == null)
            {
                return false;
            }
            return x.DisciplineId == y.DisciplineId;
        }

        public int GetHashCode(DisciplineDTO obj)
        {
            return obj.Name.GetHashCode();
        }
    }
}