﻿using System;
using System.Collections.Generic;
using NLM.BLL.DTO;

namespace NLM.WEB.Util.Comparators
{
    public class GroupComparer : IEqualityComparer<GroupDTO>
    {
        public bool Equals(GroupDTO x, GroupDTO y)
        {
            if (x == null && y == null)
            {
                return true;
            }
            else if (x == null || y == null)
            {
                return false;
            }
            return x.GroupId == y.GroupId;
        }

        public int GetHashCode(GroupDTO obj)
        {
            return obj.GroupId.GetHashCode();
        }
    }
}