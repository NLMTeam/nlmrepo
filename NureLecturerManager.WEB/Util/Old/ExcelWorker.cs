﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using NLM.WEB.Models.Lecturer;
using NLM.BLL.Interfaces;
using System.Web.Mvc;
using NLM.BLL.DocWorkers;
using NLM.BLL.DTO;
using NLM.BLL.Exceptions;
using NLM.WEB.Util.Exceptions;

namespace NLM.WEB.Util
{
    public static class ExcelWorker
    {
        private static IControlPointService controlPointService = DependencyResolver.Current.GetService<IControlPointService>();
        private static IControlService controlService = DependencyResolver.Current.GetService<IControlService>();
        private static IAccountService accountService = DependencyResolver.Current.GetService<IAccountService>();

        public static void GenerateMarkExcelFile(string fileName, IEnumerable<Category> categorys, IEnumerable<StudentMarkViewModel> marks)
        {
            ExcelHelper helper = new ExcelHelper();
            //helper.GenerateEmptyFile(fileName);


            Dictionary<int, List<string>> result = new Dictionary<int, List<string>>();

            result.Add(0, new List<string>());
            var categoryList = new List<string>(categorys.Select(cat => cat.Name));
            categoryList.Insert(0, "ФИО студента");
            result.Add(1, categoryList);
            int i = 2;

            foreach(var mark in marks)
            {
                List<string> rowValues = new List<string>();
                rowValues.Add(mark.FIO);
                if (mark.Marks != null)
                {
                    rowValues.AddRange(mark.Marks.Select(m => m.Value));
                }
                result.Add(i, rowValues);
                i++;
            }

            helper.FillFileContent(result, fileName);

        }

        public static KeyValuePair<IEnumerable<Category>, IEnumerable<StudentMarkViewModel>> ReadMarksFromFile(string filePath)
        {
            ExcelHelper helper = new ExcelHelper();
            var data = helper.ReadFileContent(filePath);

            List<Category> categorys = new List<Category>();
            List<StudentMarkViewModel> marks = new List<StudentMarkViewModel>();

            if (data[1].Count < 1) {
                throw new ExcelFormatException("Во второй строке файла должны быть указаны все контрольные точки и виды работ по которым выставляются оценки.");
            }

            for(int i=1; i < data[1].Count; i++)
            {
                string catName = data[1][i];
                Category category = null;
                bool categoryCP = false;
                if (catName.ToUpper().StartsWith("КТ"))
                {
                    category = new Category(controlPointService.SearchControlPoints(name: catName).SingleOrDefault());
                    categoryCP = true;
                }
                else
                {
                    category = new Category(controlService.SearchWorkType(Name: catName).SingleOrDefault());
                    categoryCP = false;
                }
                if (categoryCP && category.ControlPoint == null || !categoryCP && category.WorkType == null)
                { 
                    throw new ExcelDataException("Имя " + catName + " не является корректным именем контрольной точки или вида работы.");
                }

                categorys.Add(category); 
            }

            ICollection<UserDTO> users = null;
            for (int i = 2; i < data.Count; i++)
            {
                StudentMarkViewModel mark = new StudentMarkViewModel();
                List<string> markRow = data[i];

                UserDTO user = null;

                users = accountService.SearchUser(FIO: markRow[0]);

                if (users.Count == 0)
                {
                    throw new ExcelDataException("Студент с именем " + markRow[0] + " не найден в базе данных.");
                }

                if (users.Count > 1)
                {
                    user = users.Where(u => u.Type == "Студент").Single();
                }
                else
                {
                    user = users.Single();
                }

                mark.FIO = user.FIO;
                mark.UserId = user.UserId;
                mark.Marks = new Dictionary<Category, string>();
                for (int j = 1; j < markRow.Count; j++)
                {
                    mark.Marks.Add(categorys[j - 1], markRow[j]);
                }

                marks.Add(mark);
            }

            return new KeyValuePair<IEnumerable<Category>, IEnumerable<StudentMarkViewModel>>(categorys, marks);
        }

        public static void GenerateGroupStudentFile(string fileName, IEnumerable<KeyValuePair<GroupDTO, IEnumerable<UserDTO>>> model)
        {
            var helper = new ExcelHelper();

            Dictionary<int, List<string>> values = new Dictionary<int, List<string>>();

            values.Add(0, new List<string>());
            int i = 1;

            foreach (var groupRecord in model)
            {
                values.Add(i++, new List<string>() { "Группа -", groupRecord.Key.GroupId, });
                values.Add(i++, new List<string>() { "ФИО", "Email" });

                foreach(var student in groupRecord.Value)
                {
                    List<string> studentRecord = new List<string>() {
                        student.FIO, student.Mail
                    };

                    values.Add(i++, studentRecord);
                }
            }

            helper.FillFileContent(values, fileName);
        }

        public static IEnumerable<KeyValuePair<GroupDTO, IEnumerable<UserDTO>>> ParseStudentFileContent(string filePath)
        {
            List<KeyValuePair<GroupDTO, IEnumerable<UserDTO>>> list = new List<KeyValuePair<GroupDTO, IEnumerable<UserDTO>>>();

            var helper = new ExcelHelper();
            Dictionary<int, List<string>> data = helper.ReadFileContent(filePath);

            int i = 1;
            while( i < data.Count )
            {
                list.Add(readGroup(data, ref i));
            }

            return list;
        }

        private static KeyValuePair<GroupDTO, IEnumerable<UserDTO>> readGroup(Dictionary<int, List<string>> values, ref int i)
        {
            var row = values[i];

            if (row.Count < 2)
            {
                throw new ExcelFormatException("Во втором столбце строки №" + i + " должен содержаться идентификатор группы");
            }

            var groupId = row[1];
            GroupDTO group = accountService.SearchGroup(GroupNum: groupId).LastOrDefault();
            if (group == null)
            {
                throw new ExcelFormatException("Группа с идентификатором " + groupId + " ( строка № " + i + " ) не найдена в базе.");
            }

            List<UserDTO> students = new List<UserDTO>();

            if (values.Count < i + 2)
            {
                return new KeyValuePair<GroupDTO, IEnumerable<UserDTO>>(group, students);
            }

            i += 2;
            row = values[i];

            while (row[0] != "Группа -" && values.ContainsKey(i))
            {
                row = values[i++];

                if (row.Count < 2)
                {
                    throw new ExcelFormatException("Строка №" + i + " должна содержать ФИО и email пользователя.");
                }

                string FIO = row[0];
                string Mail = row[1];

                UserDTO user = new UserDTO()
                {
                    FIO = FIO,
                    Mail = Mail
                };

                students.Add(user);
            }

            return new KeyValuePair<GroupDTO, IEnumerable<UserDTO>>(group, students);
        }

        public static void GenerateGroupControlFile(string fileName, GroupDTO group, IEnumerable<GroupControlDTO> controls)
        {
            Dictionary<int, List<string>> values = new Dictionary<int, List<string>>();

            values.Add(0, new List<string>());

            values.Add(1, new List<string>() { "Группа - ", group.GroupId });

            values.Add(2, new List<string>() { "Вид работы", "Тип сдачи", "Дата сдачи", "Место сдачи" });

            int i = 3;

            foreach (var control in controls)
            {
                var controlValues = new List<string>();

                var workType = controlService.SearchWorkType(control.WorkTypeId).SingleOrDefault();

                values.Add(i++, new List<string>() { workType.Name, control.Type, control.PassDate.Value.ToLongDateString(), control.PassPlace });
            }

            ExcelHelper helper = new ExcelHelper();

            helper.FillFileContent(values, fileName);
        }

        public static KeyValuePair<GroupDTO, IEnumerable<GroupControlDTO>> ParseGroupControlFile(string fileName)
        {
            var helper = new ExcelHelper();
            var content = helper.ReadFileContent(fileName);

            if (content.Count < 3)
            {
                throw new ExcelFormatException("Таблица должна содержать более 3 строк.");
            }
            if (content[1].Count < 2)
            {
                throw new ExcelFormatException("В столбце 2 строки 2 должен идентификатор группы.");
            }
            string GroupId = content[1][1];
            GroupDTO group = accountService.SearchGroup(GroupId).SingleOrDefault();
            if (group == null)
            { 
                throw new ExcelDataException(GroupId + " не является правильным идентификатором группы.");
            }
            List<GroupControlDTO> controls = new List<GroupControlDTO>();

            for (int i = 3; content.ContainsKey(i); i++)
            {
                var controlValue = content[i];

                if (controlValue.Count < 4)
                {
                    throw new ExcelFormatException("Строка " + i + " должна содержать значение всех необходимых полей. ( Наименование вида работы, тип контроля, дата сдачи контроля и место сдачи.");
                }

                string workTypeName = controlValue[0];
                string type = controlValue[1];
                DateTime passDate = DateTime.Parse(controlValue[2]);
                string place = controlValue[3];

                WorkTypeDTO workType = controlService.SearchWorkType(Name: workTypeName).LastOrDefault();
                if (workType == null)
                {
                    throw new ExcelDataException("В базе данных не найден вид работы с названием " + workTypeName);
                }

                GroupControlDTO gc = controlService.SearchGroupControls(workTypeId: workType.WorkTypeId, GroupNum: group.GroupId, Type: type, Place: place, pass: passDate).SingleOrDefault();
                if (gc != null)
                { 
                    controls.Add(gc);
                }
                else
                {
                    controls.Add(new GroupControlDTO() { WorkTypeId = workType.WorkTypeId, GroupNumber = group.GroupId, PassDate = passDate, PassPlace = place, Type = type });
                }
            }

            return new KeyValuePair<GroupDTO, IEnumerable<GroupControlDTO>>(group, controls);

        }

    }
}