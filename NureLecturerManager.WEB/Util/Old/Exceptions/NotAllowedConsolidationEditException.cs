﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLM.WEB.Util.Exceptions
{
    public class NotAllowedConsolidationEditException : Exception
    {
        public NotAllowedConsolidationEditException() : base()
        {
        }

        public NotAllowedConsolidationEditException(string message) : base(message)
        {

        }
    }
}