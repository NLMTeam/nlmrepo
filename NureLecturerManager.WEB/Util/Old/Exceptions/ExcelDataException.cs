﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NLM.WEB.Util.Exceptions
{
    public class ExcelDataException : Exception
    {
        public ExcelDataException() : base() { }

        public ExcelDataException(string message) : base(message) { }
    }
}