﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.BLL.DTO;

namespace NLM.WEB.Util.Helpers.Permissions
{
    public interface IPermissionHelper
    {
        // Course Project
        bool AllowConsolidationEdit(string year, string disciplineName, UserDTO user);
        bool AllowConsolidationEdit(string year, int courseProjectId, string groupId, UserDTO user);
        bool AllowConsolidationDelete(int consolidationId, UserDTO user);
        bool AllowProjectEdit(string year, string disciplineName, UserDTO user);
        bool AllowProjectDelete(int courseProjectId, UserDTO user);
        // Groups
        bool AllowGroupControlEdit(int groupControlId, UserDTO user);

        // marks

        bool AllowMarksEdit(string year, string disciplineName, UserDTO user);
    }
}
