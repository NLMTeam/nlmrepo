﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web.Mvc;
//using NLM.BLL.DTO;
//using NLM.BLL.Interfaces;
//using NLM.BLL.DTObjects;
//using NLM.BLL.Exceptions;
//using NLM.WEB.Util.Comparators;

//namespace NLM.WEB.Util.Helpers
//{
//    public class GroupHelper : IGroupHelper
//    {
//        private IAccountService accountService;
//        private IDisciplineService discService;
//        public GroupHelper(IDependencyResolver dependecyResolver)
//        {
//            accountService = dependecyResolver.GetService<IAccountService>();
//            discService = dependecyResolver.GetService<IDisciplineService>();
//        }

//        public bool CheckUserGroup(int userId, string groupId)
//        {
//            var student = accountService.SearchStudent(userId).First();

//            if (student == null)
//            {
//                // i think here must be something like log because suggested that it's student
//                return false;
//            }

//            return student.GroupId == groupId;
//        }

//        public ICollection<string> GetYearGroups(int year)
//        {
//            // check every group in database using special method
//            var groups = accountService.SearchGroup().Where(group => CheckGroupForYear(group.GroupId, year)).Select(group => group.GroupId).Distinct();

//            return groups.ToList();
//        }

//        public bool CheckGroupForYear(string groupId, int year)
//        {
//            // act according to group format, which allows us to make decisions
//            var splittedGroup = groupId.Split('-');
//            if (splittedGroup.Length != 3)
//            {
//                // must be some log, because of incorrect group format
//                return false;
//            }

//            return ("20" + splittedGroup[1]) == year.ToString();
//        }

//        public ICollection<string> GetYears()
//        {
//            // using discussed groupNum format to detect all groups in our database
//            var years = accountService.SearchGroup().Select(group =>
//            {
//                var splittedGroup = group.GroupId.Split('-');
//                if (splittedGroup.Length != 3)
//                    return null;

//                return "20" + splittedGroup[1];

//            }).Where(s => s != null).Distinct();

//            return years.ToList();
//        }

//        public ICollection<DisciplineDTO> GetGroupDisciplines(string year, string groupId)
//        {
//            // take parts and up to discipline

//            // discipline parts
//            var disciplineParts = discService.SearchPartOfDiscipline(GroupNum: groupId);

//            // discipline for years
//            var disciplineForYears = disciplineParts.Select(part => {
//                DisciplineForYearDTO yearDisc = null;

//                yearDisc = discService.SearchDisciplineForYear(part.DisciplineForYearId, year: year).SingleOrDefault();

//                return yearDisc;
//                }).Where(disc => disc != null);

//            // disciplines
//            var disciplines = disciplineForYears.Select(discYear => discService.SearchDiscipline(discYear.DisciplineId).Single());

//            return disciplines.Distinct(new DisciplineComparator()).ToList();
//        }

//        public ICollection<UserDTO> GetGroupStudents(string groupId)
//        {
//            return accountService.SearchStudent(GroupNum: groupId).Select(s => accountService.SearchUser(s.UserId).Single()).ToList();
//        }

//        public string GetYearOfGroup(string groupId)
//        {
//            var splitGroup = groupId.Split('-');
//            if (splitGroup.Length != 3)
//            {
//                // some log
//                return null;
//            }
//            return "20" + splitGroup[1];
//        }
//    }
//}