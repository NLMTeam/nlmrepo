﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web.Mvc;
//using NLM.BLL.DTO;
//using NLM.BLL.Interfaces;
//using NLM.WEB.Models.Lecturer;
//using NLM.BLL.DTObjects;
//using NLM.WEB.Models;
//using NLM.BLL.Exceptions;
//using NLM.WEB.Util.Comparators;
//using System.Web;
//using NLM.WEB.Models.CourseProjects;
//using NLM.WEB.Util.Exceptions;

//namespace NLM.WEB.Util.Helpers.CourseProject
//{
//    public class CourseProjectHelper : ICourseProjectHelper
//    {
//        // services
//        private IAccountService accountService;
//        private IDisciplineService discService;
//        private ICourseProjectService cpService;

//        // helpers
//        private IGroupHelper groupHelper;

//        // configers
//        private FileConfiger configer;

//        private const string ADMIN_TYPE = "Админ";
//        private const string LECTURER_TYPE = "Лектор";
//        private const string STUDENT_TYPE = "Студент";

//        public CourseProjectHelper(IDependencyResolver dependencyResolver)
//        {
//            if (dependencyResolver == null)
//                dependencyResolver = DependencyResolver.Current;

//            accountService = dependencyResolver.GetService<IAccountService>();
//            discService = dependencyResolver.GetService<IDisciplineService>();
//            cpService = dependencyResolver.GetService<ICourseProjectService>();

//            groupHelper = dependencyResolver.GetService<IGroupHelper>();

//            configer = new FileConfiger(HttpContext.Current.Server.MapPath("/Files/Config.txt"));
//        }

//        #region private methods

//        private ICollection<ConsolidationViewModel> GetConsolidationForDisciplineAndGroup(DisciplineForYearDTO discYear, string groupId)
//        {

//            var consolidations = cpService.SearchConsolidation(disciplineForYearId: discYear.DisciplineForYearId).AsEnumerable();

//            consolidations = consolidations.Where(cons => groupHelper.CheckUserGroup(cons.UserId, groupId));

//            List<ConsolidationViewModel> models = new List<ConsolidationViewModel>();

//            foreach(var cons in consolidations)
//            {
//                ConsolidationViewModel model = new ConsolidationViewModel()
//                {
//                    ConsolidationId = cons.ConsolidationId,
//                    ConsolidationDate = cons.ConsolidationDate.ToShortDateString(),
//                    Mark = cons.Mark,
//                    GroupNum = groupId,
//                    PassDate = cons.PassDate != null && cons.PassDate.HasValue ? cons.PassDate.Value.ToShortDateString() : "",
//                    FIO = accountService.SearchUser(cons.UserId).First().FIO,
//                    CourseProjectId = cons.CourseProjectId
//                };

//                models.Add(model);
//            }

//            return models.OrderBy(model => model.FIO).ToList();
//        }

//        #endregion

//        public CourseProjectMainViewModel GetModel(UserDTO user)
//        {
//            if (user.Type == ADMIN_TYPE)
//            {
//                return this.GetAdminModel();
//            }
//            if (user.Type == LECTURER_TYPE)
//            {
//                return this.GetLecturerModel(user);
//            }
//            return null;
//        }
 
//        public CourseProjectMainViewModel GetAdminModel()
//        {
//            CourseProjectMainViewModel model = new CourseProjectMainViewModel();

//            // exception safe 

//            // get all disciplines
//            model.Disciplines = discService.SearchDiscipline(isCourseProject: true);

//            if (model.Disciplines.Count == 0)
//            {
//                return model;
//            }

//            // select first discipline
//            var discipline = model.Disciplines.First();

//            // exception safe
//            ICollection<DisciplineForYearDTO> discYears = null;
//            DisciplineForYearDTO discYear = null;

//            // get years for discipline
//            discYears = discService.SearchDisciplineForYear(discId: discipline.DisciplineId);
                
//            // select as first current year
//            var year = configer.Year;
//            if (discYears.Count(dYear => dYear.Year == year) > 0)
//            {
//                // place config year as first
//                var years = new List<string>();
//                years.Add(year);

//                years.AddRange(discYears.Where(dy => dy.Year != year).Select(dy => dy.Year));

//                model.Years = years;

//                // get default discipline for year
//                discYear = discYears.First(dfy => dfy.Year == year);
//            }
//            else
//            {
//                // select years to model and distinct them
//                model.Years = discYears.Select(yearDisc => yearDisc.Year).Distinct().ToList();

//                var courseProjects = cpService.SearchCourseProjects(discId: discipline.DisciplineId);
//                model.CourseProjects = courseProjects;

//                return model;
//            }

//            // select default year
//            discYear = discYear != null ? discYear : discYears.First();

//            // exception safe
//            // get groups for this year
//            model.Groups = this.GetGroupsOfYear(discYear.Year, discipline.DisciplineId);
//            if (model.Groups.Count == 0)
//            {
//                // exception safe 

//                // get Course Projects for discipline
//                var courseProjects = cpService.SearchCourseProjects(discId: discipline.DisciplineId);
//                model.CourseProjects = courseProjects;

//                return model;
//            }

//            // exception safe 
//            // get Course Projects for discipline
//            model.CourseProjects = cpService.SearchCourseProjects(discId: discipline.DisciplineId);

//            if (model.CourseProjects.Count == 0)
//            {
//                return model;
//            }

//            // select default group
//            var group = model.Groups.First();

//            // exception safe

//            // get consolidations for discipline + year + group
//            model.Consolidations = GetConsolidationForDisciplineAndGroup(discYear, group);

//            return model;
//        }

//        public CourseProjectMainViewModel GetLecturerModel(UserDTO lecturer)
//        {
//            CourseProjectMainViewModel model = new CourseProjectMainViewModel();

//            // read configs
//            string year = configer.Year;
//            string semestr = configer.Semestr;

//            // exception safe loading of disciplines
//            ICollection<DisciplineDTO> disciplines = null;

//            model.Disciplines = discService.GetDisciplinesForLecturer(lecturer.UserId, year, semestr != null ? Convert.ToInt32(semestr) : -228)
//                                        .Where(disc => disc.IsCourseProject).ToList();


//            if (model.Disciplines.Count() == 0)
//            {
//                return model;
//            }
//            // select first discipline
//            DisciplineDTO discipline = disciplines.First();

//            // exception safe method for loading years
//            ICollection<DisciplineForYearDTO> yearDiscs = null;

//            yearDiscs = discService.SearchDisciplineForYear(discId: discipline.DisciplineId);
//            yearDiscs = yearDiscs.Where(dfy =>
//            {
//                // check dfy for this lecturer
//                if (dfy.LecturerIds.Contains(lecturer.UserId))
//                {
//                    return true;
//                }

//                // check discipline part for this lecturer
//                var part = discService.SearchPartOfDiscipline(dfy.DisciplineForYearId)
//                                        .Where(discPart => discPart.LecturerIds.Contains(lecturer.UserId));

//                if (part.Count() > 0)
//                {
//                    return true;
//                }
//                return false;

//            }).ToArray();

//            model.Years = yearDiscs.Select(dfy => dfy.Year).ToList(); 
          
//            if (model.Years.Count == 0)
//            { 
//                // some warn log about not found years
//                model.CourseProjects = cpService.SearchCourseProjects(discId: discipline.DisciplineId);

//                return model;
//            }

//            // select first year
//            year = model.Years.First();
//            var discForYear = yearDiscs.First();

//            // exception safe method for loading groups
//            IEnumerable<string> groups = null;

//            // just groups where he participates as lecturer or assistant
//            if (discForYear.LecturerIds.Contains(lecturer.UserId))
//            {
//                groups = this.GetGroupsOfYear(year, discipline.DisciplineId);
//            }
//            else
//            {
//                groups = this.GetGroupsOfYear(year, discipline.DisciplineId).Where(s => 
//                {
//                    var part = discService.SearchPartOfDiscipline(discForYear.DisciplineForYearId, s).FirstOrDefault();

//                    return part != null && part.LecturerIds.Contains(lecturer.UserId);
//                });
//            }
//            model.Groups = groups.ToList();


//            if (model.Groups.Count == 0)
//            { 
//                model.CourseProjects = cpService.SearchCourseProjects(discId: discipline.DisciplineId);

//                return model;
//            }
//            // select group
//            var group = model.Groups.First();

//            // load projects 

//            model.CourseProjects = cpService.SearchCourseProjects(discId: discipline.DisciplineId);

//            if (model.CourseProjects.Count == 0)
//            {
//                return model;
//            }

//            // exception safe method for consolidations

//            var consolidations = this.GetConsolidationForDisciplineAndGroup(discForYear, group);
//            model.Consolidations = consolidations;

//            return model;
//        }

//        public CourseProjectMainViewModel GetArchievedModel(UserDTO lecturer)
//        {
//            CourseProjectMainViewModel model = new CourseProjectMainViewModel();

//            // read configs
//            string year = configer.Year;
//            string semestr = configer.Semestr;

//            // exception safe loading of disciplines
//            ICollection<DisciplineDTO> disciplines = null;

//            model.Disciplines = discService.GetDisciplinesForLecturerArchieved(lecturer.UserId, year, Convert.ToInt32(semestr));

//            if (model.Disciplines.Count() == 0)
//            {
//                return model;
//            }
//            // select first discipline
//            DisciplineDTO discipline = disciplines.First();

//            // exception safe method for loading years
//            ICollection<DisciplineForYearDTO> yearDiscs = null;

//            yearDiscs = discService.SearchDisciplineForYear(discId: discipline.DisciplineId);
//            yearDiscs = yearDiscs.Where(dfy =>
//            {
//                // check dfy for this lecturer
//                if (dfy.LecturerIds.Contains(lecturer.UserId))
//                {
//                    return true;
//                }


//                // check discipline part for this lecturer
//                var part = discService.SearchPartOfDiscipline(dfy.DisciplineForYearId).Where(discPart => discPart.LecturerIds.Contains(lecturer.UserId));

//                if (part.Count() > 0)
//                {
//                    return true;
//                }
//                return false;

//            }).ToArray();

//            model.Years = yearDiscs.Select(dfy => dfy.Year).ToList();
           
//            if (model.Years.Count == 0)
//            { 
//            // some warn log about not found years
//                model.CourseProjects = cpService.SearchCourseProjects(discId: discipline.DisciplineId);

//                return model;
//            }

//            // select first year
//            year = model.Years.First();
//            var discForYear = yearDiscs.First();

//            // exception safe method for loading groups
//            IEnumerable<string> groups = null;
//            // just groups where he participates as lecturer or assistant
//            if (discForYear.LecturerIds.Contains(lecturer.UserId))
//            {
//                groups = this.GetGroupsOfYear(year, discipline.DisciplineId);
//            }
//            else
//            {
//                groups = this.GetGroupsOfYear(year, discipline.DisciplineId).Where(s =>
//                {
//                    var part = discService.SearchPartOfDiscipline(discForYear.DisciplineForYearId, s).FirstOrDefault();

//                    return part != null && part.LecturerIds.Contains(lecturer.UserId);
//                });
//            }
//            if (groups.Count() > 0)
//            {
//                model.Groups = groups.ToList();
//            }
//            else
//            {

//                model.CourseProjects = cpService.SearchCourseProjects(discId: discipline.DisciplineId);

//                return model;
//            }


//            // select group
//            var group = model.Groups.First();

//            // load projects 

//            model.CourseProjects = cpService.SearchCourseProjects(discId: discipline.DisciplineId);

//            if (model.CourseProjects.Count == 0)
//            {
//                return model;
//            }

//            // exception safe method for consolidations
//            model.Consolidations = this.GetConsolidationForDisciplineAndGroup(discForYear, group).ToList();
//            return model;
//        }
  
//        public CourseProjectMainStudentModel GetStudentModel(StudentDTO student)
//        {
//            CourseProjectMainStudentModel model = new CourseProjectMainStudentModel();

//            // get student group
//            string studentGroup = student.GroupId;

//            // get discipline parts;
//            var discParts = discService.SearchPartOfDiscipline(GroupNum: studentGroup).AsEnumerable();

//            // get disciplines for year
//            var discYears = discParts.Select(part => discService.SearchDisciplineForYear(part.DisciplineForYearId).Single());

//            ICollection<DisciplineDTO> disciplines = null;

//            // select and distinct disciplines using dfy dto objects
//            disciplines = discYears.Select(discYear => discService.SearchDiscipline(discYear.DisciplineId).First())
//                                                                    .Distinct(new DisciplineComparator())
//                                                                    .Where(disc => disc.IsCourseProject).ToList();

//            // place to model
//            model.Disciplines = disciplines;

//            if (model.Disciplines.Count() == 0)
//                return model; 

//            // gte discipline for current config
//            string year = configer.Year;
//            int semestr = Convert.ToInt32(configer.Semestr);
//            DisciplineDTO discipline = disciplines.FirstOrDefault(disc => discService.SearchDisciplineForYear(discId: disc.DisciplineId).Where(dfy => dfy.Year == year && dfy.Semester == semestr).Count() > 0);
//            DisciplineForYearDTO disciplineForYear = null;

//            if (discipline == null)
//            {
//                discipline = disciplines.FirstOrDefault(disc => discService.SearchDisciplineForYear(discId: disc.DisciplineId).Where(dfy => dfy.Year == year).Count() > 0);
//                disciplineForYear = discService.SearchDisciplineForYear(discId: discipline.DisciplineId, year: year).First();

//                if (discipline.DisciplineId != model.Disciplines.First().DisciplineId)
//                {
//                    // replace chosed discipline to first place

//                    List<DisciplineDTO> discs = new List<DisciplineDTO>();
//                    discs.Add(discipline);
//                    discs.AddRange(disciplines.Where(disc => disc.DisciplineId != discipline.DisciplineId));

//                    model.Disciplines = discs;

//                }

//            }
//            else
//            {
//                disciplineForYear = discService.SearchDisciplineForYear(discId: discipline.DisciplineId, year: year, semester: semestr).First();

//                if (discipline.DisciplineId != model.Disciplines.First().DisciplineId)
//                {
//                    // replace chosed discipline to first place

//                    List<DisciplineDTO> discs = new List<DisciplineDTO>();
//                    discs.Add(discipline);
//                    discs.AddRange(disciplines.Where(disc => disc.DisciplineId != discipline.DisciplineId));

//                    model.Disciplines = discs;

//                }

//            }

//            // get parts for chosed discipline
//            discParts = discService.SearchPartOfDiscipline(disciplineYId: disciplineForYear.DisciplineForYearId);

//            // get groups that learning this
//            var groups = discParts.Select(part => part.GroupId).Distinct();

//            if (groups.First() != studentGroup)
//            {

//                // place student group as first (default)
//                List<string> groupList = new List<string>();

//                groupList.Add(studentGroup);
//                groupList.AddRange(groups.Where(s => s != studentGroup));

//                // place this groups to model
//                model.Groups = groupList;
//            }
//            else
//            {
//                model.Groups = groups.ToList();
//            }

//            // exception safe
//                // get course projects for first (chosed) discipline. place them to model
//            model.CourseProjects = cpService.SearchCourseProjects(discId: discipline.DisciplineId);

//            // get consolidations for first (default) discipline and first (default) group.
//            model.Consolidations = GetConsolidationForDisciplineAndGroup(disciplineForYear, studentGroup);


//            model.Consolidation = model.Consolidations.FirstOrDefault(cons => cpService.SearchConsolidation(cons.ConsolidationId).First().UserId == student.UserId);

//            model.IsConsolidated = model.Consolidation != null;

//            return model;
//        }

//        public DisciplineDataModel GetDisciplineInfo(int disciplineId)
//        {
//            DisciplineDataModel model = new DisciplineDataModel();

//            // get projects for discipline
//            model.CourseProjects = cpService.SearchCourseProjects(discId: disciplineId);
//            // place to main model

//            // take year's from discipline for year
//            model.Years = discService.SearchDisciplineForYear(discId: disciplineId).Select(discYear => discYear.Year).ToList();

//            return model;
//        }

//        public ICollection<string> GetYears()
//        {
//            // get all disciplines for years
//            var disciplineForYears = discService.SearchDisciplineForYear();

//            var year = configer.Year;
//            var semestr = configer.Semestr;

//            // place first default year

//            var disciplineForYear = disciplineForYears.Where(dfy => dfy.Year == year).First();

//            List<DisciplineForYearDTO> discs = new List<DisciplineForYearDTO>();

//            discs.Add(disciplineForYear);
//            discs.AddRange(disciplineForYears.Where(dfy => dfy.DisciplineForYearId != disciplineForYear.DisciplineForYearId));

//            // return selected year from disciplines
//            return discs.Select(disc => disc.Year).Distinct().ToList();
//        }

//        public ICollection<string> GetGroupsOfYear(string year, int disciplineId)
//        {
//            // disciplines for year
//            var discYears = discService.SearchDisciplineForYear(discId: disciplineId, year: year);

//            // parts of this disciplines

//            var discParts = discService.SearchPartOfDiscipline().Where(part => discYears.Count(disc => disc.DisciplineForYearId == part.DisciplineForYearId) > 0);

//            // select groups
//            var groups = discParts.Select(part => part.GroupId);
//            return groups.Distinct().ToList();
//        }

//        public ICollection<DisciplineDTO> GetGroupDisciplines(string year, string groupNum)
//        {
//            return groupHelper.GetGroupDisciplines(year, groupNum).Where(disc => disc.IsCourseProject).ToList();
//        }

//        public ICollection<CourseProjectDTO> GetCourseProjectsForDiscipline(int disciplineId)
//        {
//            // take courseProjects from dataBase
//            var courseProjects = cpService.SearchCourseProjects(discId: disciplineId);
//            return courseProjects;

//        }

//        public CourseProjectViewModel GetProjectViewModel(int courseProjectId)
//        {
//            // take course project from data base
//            var courseProject = cpService.SearchCourseProjects(courseProjectId: courseProjectId).Single();

//            // fill model with data
//            CourseProjectViewModel cpModel = new CourseProjectViewModel();
//            cpModel.Description = courseProject.Description;
//            cpModel.Theme = courseProject.Theme;
//            cpModel.Type = courseProject.Type;

//            // now take discipline dto for it's name and fill Discipline field
//            var discipline = discService.SearchDiscipline(courseProject.DisciplineId).Single();
//            cpModel.Discipline = discipline.Name;

//            return cpModel;
//        }

//        public ICollection<ConsolidationViewModel> GetMarksForDisciplineAndGroup(string year, string disciplineName, string groupNum)
//        {
//            var discipline = discService.SearchDiscipline(Name: disciplineName).First();
//            var disciplineForYear = discService.SearchDisciplineForYear(discId: discipline.DisciplineId, year: year).Single();
//            var consolidations = this.GetConsolidationForDisciplineAndGroup(disciplineForYear, groupNum);

//            return consolidations;
//        }

//        public ICollection<ConsolidationViewModel> GetMarksOfCourseProject(int courseProjectId, string year)
//        {
//            var consolidations = cpService.SearchConsolidation(courseProjectId: courseProjectId).Where(cons => discService.SearchDisciplineForYear(cons.DisciplineForYearId).First().Year == year);

//            List<ConsolidationViewModel> models = new List<ConsolidationViewModel>();
//            foreach (var consolidation in consolidations)
//            {
//                ConsolidationViewModel item = new ConsolidationViewModel()
//                {
//                    ConsolidationDate = consolidation.ConsolidationDate.ToShortDateString(),
//                    PassDate = consolidation.PassDate.HasValue ? consolidation.PassDate.Value.ToShortDateString() : "",
//                    Mark = consolidation.Mark,
//                    ConsolidationId = consolidation.ConsolidationId,
//                    FIO = accountService.SearchUser(consolidation.UserId).Single().FIO,
//                    GroupNum = accountService.SearchStudent(consolidation.UserId).Single().GroupId,
//                    CourseProjectId = courseProjectId
//                };

//                models.Add(item);
//            }

//            return models;
//        }

//        public InitialFillConsolidationViewModel GetConsolidationFillModel(string groupId)
//        {
//            InitialFillConsolidationViewModel model = new InitialFillConsolidationViewModel();

//            model.Students = groupHelper.GetGroupStudents(groupId);

//            return model;
//        }

//        public InitialFillConsolidationViewModel GetYearInfo(string year)
//        {
//            InitialFillConsolidationViewModel model = new InitialFillConsolidationViewModel();

//            // needs to get groups -> students and disciplines -> projects

//            // group students
//            model.Students = this.GetStudentsOfGroup(model.Groups.First());

//            return model;
//        }
  
//        public CreateConsolidationViewModel GetConsolidationDataModel(int consolidationId)
//        {
//            CreateConsolidationViewModel model = new CreateConsolidationViewModel();

//            // get consolidation
//            var consolidation = cpService.SearchConsolidation(consolidationId).Single();

//            // fill model
//            model.UserId = consolidation.UserId;
//            model.CourseProjectId = consolidation.CourseProjectId;
//            model.PassDate = consolidation.PassDate.HasValue ? consolidation.PassDate.Value.ToShortDateString() : "";
//            model.Mark = consolidation.Mark.HasValue ?consolidation.Mark.Value : 0;
//            model.GroupId = accountService.SearchStudent(consolidation.UserId).Single().GroupId;

//            // get discipline for year dto to place disciplineId and year
//            var yearDisc = discService.SearchDisciplineForYear(consolidation.DisciplineForYearId).First();

//            // fill model with disciplineId and year
//            model.DisciplineId = yearDisc.DisciplineId;

//            return model;
//        }

//        public InitialCourseProjectFill GetCourseProjectFillModel()
//        {
//            InitialCourseProjectFill data = new InitialCourseProjectFill();

//            // exception safe method

//            data.Disciplines = discService.SearchDiscipline().Where(disc => disc.IsCourseProject).ToList();

//            return data;
//        }

//        public CreateCourseProjectViewModel GetCourseProjectDataModel(int courseProjectId)
//        {
//            CreateCourseProjectViewModel model = new CreateCourseProjectViewModel();

//            // get courseProject and discipline dto from dataBase
//            var courseProject = cpService.SearchCourseProjects(courseProjectId: courseProjectId).Single();

//            if (courseProject == null)
//            {
//                return null;
//            }

//            // fill model

//            model.Description = courseProject.Description;
//            model.Theme = courseProject.Theme;
//            model.Type = courseProject.Type;

//            return model;
//        }

//        public ICollection<UserDTO> GetStudentsOfGroup(string groupId)
//        {
//            return groupHelper.GetGroupStudents(groupId).ToList();
//        }

//        public string GetStudentYear(int disciplineId, UserDTO user)
//        {
//            // get student
//            StudentDTO student = accountService.SearchStudent(user.UserId).First();

//            // take discipline parts where participates student and up to discipline for years
//            ICollection<PartOfDisciplineDTO> disciplineParts = discService.SearchPartOfDiscipline(GroupNum: student.GroupId);

//            // take discipline for years
//            ICollection<DisciplineForYearDTO> yearDiscs = disciplineParts.Select(part => discService.SearchDisciplineForYear(part.DisciplineForYearId).First()).ToList();

//            // filter by discipline
//            yearDiscs = yearDiscs.Where(dfy => dfy.DisciplineId == disciplineId).Distinct(new DisciplineForYearComparator()).ToList();

//            // check about existing year and semestr from config
//            string year = configer.Year;
//            int semestr = Convert.ToInt32(configer.Semestr);

//            ICollection<DisciplineForYearDTO> currentYear = yearDiscs.Where(disc => disc.Year == year && disc.Semester == semestr).ToList();
//            if (currentYear.Count() > 0)
//            {
//                // we find year so we can return it
//                return year;
//            }
//            else
//            {
//                // we can't find config year
//                return yearDiscs.First().Year;
//            }
//        }

//        public ConsolidationDTO GetStudentConsolidation(int disciplineId, UserDTO user)
//        {
//            // get student
//            StudentDTO student = accountService.SearchStudent(user.UserId).First();

//            // take discipline parts where participates student and up to discipline for years
//            ICollection<PartOfDisciplineDTO> disciplineParts = discService.SearchPartOfDiscipline(GroupNum: student.GroupId);

//            // take discipline for years
//            ICollection<DisciplineForYearDTO> yearDiscs = disciplineParts.Select(part => discService.SearchDisciplineForYear(part.DisciplineForYearId).First()).ToList();

//            // filter by discipline
//            yearDiscs = yearDiscs.Where(dfy => dfy.DisciplineId == disciplineId).Distinct(new DisciplineForYearComparator()).ToList();

//            // check about existing year and semestr from config
//            string year = configer.Year;
//            int semestr = Convert.ToInt32(configer.Semestr);

//            ICollection<DisciplineForYearDTO> currentYear = yearDiscs.Where(disc => disc.Year == year && (semestr == 0 || disc.Semester == semestr)).ToList();

//            if (currentYear.Count() > 0)
//            {
//                ConsolidationDTO consolidation = cpService.SearchConsolidation(userId: user.UserId, disciplineForYearId: currentYear.First().DisciplineForYearId).First();

//                return consolidation;
//            }
//            else
//            {
//                throw new NotAllowedConsolidationEditException();
//            }
//        }
//    }
//}