﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.BLL.DTO;

namespace NLM.WEB.Util.Helpers
{
    public interface IGroupHelper
    {
        bool CheckUserGroup(int userId, string groupId);
        bool CheckGroupForYear(string groupId, int year);
        string GetYearOfGroup(string groupId);
        ICollection<string> GetYears();
        ICollection<string> GetYearGroups(int year);
        ICollection<DisciplineDTO> GetGroupDisciplines(string year, string groupId);
        ICollection<UserDTO> GetGroupStudents(string groupId);
    }
}
