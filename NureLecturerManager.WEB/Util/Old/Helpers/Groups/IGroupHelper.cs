﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.WEB.Models.Lecturer;
using NLM.BLL.DTO;

namespace NLM.WEB.Util.Helpers.Groups
{
    public interface IGroupHelper
    {
        GroupMainViewModel GetStudentModel(StudentDTO student);
        GroupMainViewModel GetModel();
        ICollection<string> GetYears();
        ICollection<GroupDTO> GetYearGroups(int year);
        ICollection<StudentViewModel> GetGroupStudents(GroupDTO group);
        ICollection<GroupControlViewModel> GetGroupControls(GroupDTO group);
        ICollection<GroupControlViewModel> GetAllControls(int year);
        ICollection<UserDTO> GetGroupCreateFillModel();
        GroupEditViewModel GetGroupEditFillModel(string groupId);
        void ExecuteGroupChanges(ICollection<KeyValuePair<GroupDTO, ICollection<UserDTO>>> groupStudents);
        void ExecuteControlChanges(KeyValuePair<GroupDTO, ICollection<GroupControlDTO>> data);
    }
}
