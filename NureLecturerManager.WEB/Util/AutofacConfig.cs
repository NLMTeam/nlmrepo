﻿using System.Configuration;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using NLM.BLL.Infrastructure;

namespace NLM.WEB.Util
{
    public static class AutofacConfig
    {
        public static void ConfigContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new BLLModule(ConfigurationManager.ConnectionStrings["DataContext"].ConnectionString));

            //builder.Register(c => new GroupHelper(DependencyResolver.Current)).As<IGroupHelper>();
            //builder.Register(a => new CourseProjectHelper(DependencyResolver.Current)).As<ICourseProjectHelper>();
            ////builder.Register(a => new MarksHelper(DependencyResolver.Current)).As<IMarkHelper>();
            ////builder.Register(a => new Helpers.Groups.GroupHelper(DependencyResolver.Current)).As<Helpers.Groups.IGroupHelper>();
            //builder.Register(z => new PermissionHelper(DependencyResolver.Current)).As<IPermissionHelper>();

            builder.RegisterControllers(typeof (MvcApplication).Assembly);
            
            DependencyResolver.SetResolver(new AutofacDependencyResolver(builder.Build()));
        }
    }
}
