﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLM.BLL.DTO;
using NLM.BLL.Infrastructure;
using NLM.BLL.Interfaces;
using NLM.DAL.Interfaces;
using AutoMapper;
using NLM.DAL.Models;

namespace NLM.BLL.Services
{
    internal class GroupService : IGroupService
    {
        private IUnitOfWork _unit;
        private IMapper _mapper;

        public GroupService(IUnitOfWork u)
        {
            _unit = u;

            MapperConfiguration config = new MapperConfiguration(cfg => 
            {
                cfg.CreateMap<GroupDTO, Group>();
                cfg.CreateMap<Group, GroupDTO>();

                cfg.CreateMap<User, UserDTO>();
            });

            _mapper = config.CreateMapper();
        }

        public CUDStatus CreateGroup(GroupDTO group)
        {
            if (_unit.Groups.Find(g => g.GroupId == group.GroupId).Count > 0)
            {
                return new CUDStatus(ActionStatus.AlreadyCreated);
            }

            try
            {
                _unit.Groups.Create(_mapper.Map<Group>(group));
                _unit.Save();
            }
            catch (Exception e)
            {
                return new CUDStatus(ActionStatus.DALException, e.Message);
            }

            return new CUDStatus(ActionStatus.Ok);
        }

        public CUDStatus UpdateGroup(GroupDTO group)
        {
            if (_unit.Groups.Find(g => g.GroupId == group.GroupId).Count == 0)
            {
                return new CUDStatus(ActionStatus.NotFound);
            }

            try
            {
                _unit.Groups.Update(_mapper.Map<Group>(group));
                _unit.Save();
            }
            catch (Exception e)
            {
                return new CUDStatus(ActionStatus.DALException, e.Message);
            }

            return new CUDStatus(ActionStatus.Ok);
        }

        public CUDStatus DeleteGroup(string name)
        {
            if (_unit.Groups.Find(g => g.GroupId == name).Count == 0)
            {
                return new CUDStatus(ActionStatus.NotFound);
            }

            try
            {
                _unit.Groups.Delete(name);
                _unit.Save();
            }
            catch (Exception e)
            {
                return new CUDStatus(ActionStatus.DALException, e.Message);
            }

            return new CUDStatus(ActionStatus.Ok);
        }

        public IEnumerable<GroupDTO> FindGroups(Func<GroupDTO, bool> condition)
        {
            return _unit.Groups.Find(g => condition(_mapper.Map<GroupDTO>(g))).Select(_mapper.Map<GroupDTO>);
        }

        public GroupDTO GetGroup(string groupName)
        {
            Group group = _unit.Groups.Get(groupName);
            return _mapper.Map<GroupDTO>(group);
        }

        public IEnumerable<GroupDTO> GetGroups()
        {
            return _unit.Groups.GetAll().Select(_mapper.Map<GroupDTO>);
        }

        public IEnumerable<UserDTO> GetGroupStudents(string name)
        {
            IEnumerable<User> students = _unit.Students.Find(s => s.GroupNumber == name).Select(s => s.User);
            return students.Select(_mapper.Map<UserDTO>);
        }
    }
}
