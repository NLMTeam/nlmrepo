﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Mazurova.BLL.Services
//{
//    class OldGroupService
//    {
//        // Методы группы
//        public ICollection<StudentDTO> GetStudentsOfGroup(string groupName)
//        {
//            if (unit.Groups.Get(groupName) == null)
//            {
//                return new List<StudentDTO>();
//            }

//            List<StudentDTO> students = new List<StudentDTO>();

//            foreach (var stud in unit.Students.Find(s => s.GroupNumber == groupName))
//            {
//                students.Add(new StudentDTO(stud));
//            }
//            return students;
//        }

//        public ICollection<GroupDTO> SearchGroup(string GroupNum = null, int captainId = -1)
//        {
//            List<GroupDTO> list = new List<GroupDTO>();
//            foreach (var group in unit.Groups
//                                      .Find(g => (GroupNum == null || g.GroupId == GroupNum) &&
//                                                 (captainId == -1 || g.CaptainId == captainId)))
//            {
//                list.Add(new GroupDTO(group));
//            }

//            return list;
//        }

//        public bool CreateGroup(string GroupNum, int captainId = -1)
//        {
//            Group g = new Group()
//            {
//                GroupId = GroupNum,
//                CaptainId = captainId
//            };

//            Group test = unit.Groups.Get(g.GroupId);
//            if (test == null)
//            {
//                unit.Groups.Create(g);
//                unit.Save();
//                if (captainId != -1)
//                {
//                    if (unit.Students.Get(captainId) == null)
//                    {
//                        unit.Students.Create(new Student() { UserId = captainId, GroupNumber = GroupNum });
//                        unit.Save();
//                    }
//                }
//                return true;
//            }
//            else
//            {
//                return false;
//            }
//        }

//        public bool ChangeCaptain(string GroupNum, int captainId)
//        {
//            Group g = unit.Groups.Get(GroupNum);
//            Student stud = unit.Students.Get(captainId);
//            if (g == null || stud == null)
//            {
//                return false;
//            }

//            g.CaptainId = captainId;
//            unit.Groups.Update(g);
//            unit.Save();
//            return true;
//        }

//        public UserDTO GetCaptain(string groupNum)
//        {
//            Group u1 = unit.Groups.Get(groupNum);
//            if (u1 != null)
//            {
//                int userId = u1.CaptainId.Value;
//                User u = unit.Users.Get(userId);
//                return new UserDTO() { UserId = userId, FIO = u.FIO, Login = u.Login, Mail = u.Mail, Type = u.Type.ToString(), Parol = null };
//            }
//            else
//                throw new GroupNotFoundException();
//        }

//        public bool DeleteGroup(string GroupNum)
//        {
//            if (unit.Groups.Get(GroupNum) != null)
//            {
//                unit.Groups.Delete(GroupNum);
//                unit.Save();
//                return true;
//            }
//            else
//            {
//                return false;
//            }
//        }
//    }
//}
