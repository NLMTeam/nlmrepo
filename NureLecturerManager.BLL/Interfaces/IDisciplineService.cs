﻿using NLM.BLL.DTO.DisciplinePart;
using NLM.BLL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.BLL.Interfaces
{
    public interface IDisciplineService
    {
        IEnumerable<DisciplineDTO> GetDisciplines();
        CUDStatus CreateDiscipline(DisciplineDTO Discipline);
        CUDStatus DeleteDiscipline(Int32 id);
        ICollection<DisciplineDTO> FindDiscipline(String name);


        IEnumerable<DisciplineForYearDTO> GetDFYs();
        IEnumerable<DisciplineForYearDTO> GetDFYsForDiscpline(Int32 disciplineId);
        CUDStatus CreateDFY(DisciplineForYearDTO dfy);
        CUDStatus DeleteDFY(Int32 dfyId);
        IEnumerable<DisciplineForYearDTO> FindDisciplineForYear(Int32 disciplineId, String year, Int32 semester);


        IEnumerable<DisciplineForYearDTO> SearchDFYsForDiscipline(String discName);
        IEnumerable<DisciplineForYearLightDTO> SearchDFYsForYearAndLecturer(String year, Int32 semester, Int32 lecturerId);
        IEnumerable<DisciplineForYearLightDTO> SearchDFYsForYearAndGroup(String year, Int32 semester, String groupName);
    }
}
