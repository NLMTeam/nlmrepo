﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.BLL.DTO;
using NLM.BLL.Infrastructure;

namespace NLM.BLL.Interfaces
{
    public interface IStudentService
    {
        IEnumerable<StudentDTO> GetStudents();
        IEnumerable<StudentDTO> FindStudents(Func<UserDTO, bool> condition);

        StudentDTO GetStudent(int id);

        CUDStatus CreateStudent(UserDTO user, string groupName);
        CUDStatus UpdateStudent(UserDTO user, string groupName);
        CUDStatus DeleteStudent(int userId);
    }
}
