﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;

namespace NLM.BLL.Email
{
    public static class Sender
    {
        private static SmtpClient client = new SmtpClient("smtp.gmail.com", 587);

        private static string from = "discipline.manager@gmail.com";
        private static string pass = "PhilKirkorovDiscKoKo11Ko";
        public static string SenderName
        {
            get
            {
                return from;
            }
            set 
            {
                from = value;
            }
        }

        public static string Password 
        {
            get 
            {
                return pass;
            }
            set 
            {
                pass = value;
            }
        }

        public static string SmtpAddress
        {
            get
            {
                return client.Host;
            }
            set 
            {
                client.Host = value;
            }
        }

        public static int SmtpPort
        {
            get 
            {
                return client.Port;
            }
            set 
            {
                client.Port = value;
            }
        }

        public static bool UseDefaultCredentials
        {
            get 
            {
                return client.UseDefaultCredentials;
            }
            set 
            {
                client.UseDefaultCredentials = value;
            }
        }

        public static bool EnableSsl
        {
            get 
            {
                return client.EnableSsl;
            }
            set 
            {
                client.EnableSsl = value;
            }
        }

        public static void Send(MailMessage message)
        {
            try
            {
                if (!client.UseDefaultCredentials)
                {
                    client.Credentials = new System.Net.NetworkCredential(from, pass);
                }
                Task.Run(() => client.Send(message));
            }
            catch
            {
                // Тут надо как-то уведомить пользователя о том, что SMTP-сервер накрылся к херам. Можно исключением, но только после прохождения всех юнит-тестов.
            }
        }
    }
}
