﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.BLL.DTO;

namespace NLM.BLL.DTObjects
{
    public class CategoryDTO
    {
        public ControlPointDTO ControlPoint { get; set; }
        public WorkTypeDTO WorkType { get; set; }
    }
}
