﻿using System;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;

namespace NLM.BLL.DTO
{
    public class ControlPointMarkDTO
    {
        internal ControlPointMarkDTO(ControlPointMark cpm)
        {
            this.ControlPointId = cpm.ControlPointId;
            this.FinalMark = cpm.FinalMark;
            this.Mark = cpm.Mark;
            this.PassDate = cpm.PassDate;
            this.UserId = cpm.UserId;
        }
        public int ControlPointId { get; set; }
        public int UserId { get; set; }
        public int Mark { get; set; }
        public System.DateTime PassDate { get; set; }
        public string FinalMark { get; set; }
    }
}
