﻿using System;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;

namespace NLM.BLL.DTO
{
    public class GroupControlDTO
    {
        public GroupControlDTO()
        {
        }

        internal GroupControlDTO(GroupControl gc)
        {
            this.GroupControlId = gc.GroupControlId;
            this.GroupNumber = gc.GroupNumber;
            this.PassDate = gc.PassDate;
            this.PassPlace = gc.PassPlace;

            this.Type = gc.Type;
            this.WorkTypeId = gc.WorkTypeId;
        }
    
        public int WorkTypeId { get; set; }
        public string GroupNumber { get; set; }
        public Nullable<System.DateTime> PassDate { get; set; }
        public string Type { get; set; }
        public string PassPlace { get; set; }
        public int GroupControlId { get; set; }
    }
}
