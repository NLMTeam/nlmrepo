﻿using System;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;

namespace NLM.BLL.DTO
{
    public class CourseProjectDTO
    {
        public CourseProjectDTO()
        {
        }

        internal CourseProjectDTO(CourseProject cp)
        {
            this.CourseProjectId = cp.CourseProjectId;
            this.Description = cp.Description;
            this.Document = cp.Document;
            this.DisciplineId = cp.DisciplineId.Value;
            this.Theme = cp.Theme;
            this.Type = cp.Type.ToString();
            this.UserId = cp.UserId.Value;
            this.IsArchieved = cp.IsArchieved;
        }
    
        public int CourseProjectId { get; set; }
        public int UserId { get; set; }
        public string Theme { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Document { get; set; }
        public int DisciplineId { get; set; }
        public bool IsArchieved { get; set; }
    }
}
