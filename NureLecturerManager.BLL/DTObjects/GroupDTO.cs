﻿using System;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;
using System.Collections.Generic;

namespace NLM.BLL.DTO
{
    public class GroupDTO
    {
        public GroupDTO()
        {
        }

        internal GroupDTO(Group g)
        {
            this.CaptainId = g.CaptainId;
            this.GroupId = g.GroupId;
        }
        public string GroupId { get; set; }
        public Nullable<int> CaptainId { get; set; }
    }

    public class GroupEqualityComparer : IEqualityComparer<GroupDTO>
    {
        public bool Equals(GroupDTO x, GroupDTO y)
        {
            if (x == null && y == null)
            {
                return true;
            }
            else if (x == null || y == null)
            {
                return false;
            }
            else
            {
                return x.GroupId == y.GroupId;
            }
        }

        public int GetHashCode(GroupDTO obj)
        {
            return obj.GroupId.GetHashCode();
        }
    }
}
