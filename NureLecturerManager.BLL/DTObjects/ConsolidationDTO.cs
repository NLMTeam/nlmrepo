﻿using System;
using NLM.DAL.Models;

namespace NLM.BLL.DTO
{
    public class ConsolidationDTO
    {
        internal ConsolidationDTO(Consolidation c)
        {
            this.ConsolidationDate = c.ConsolidationDate;
            this.ConsolidationId = c.ConsolidationId;
            this.CourseProjectId = c.CourseProjectId;
            this.Mark = c.Mark;
            this.PassDate = c.PassDate;
            this.UserId = c.UserId;
            this.DisciplineForYearId = c.DisciplineForYearId;
        }
        public int ConsolidationId { get; set; }
        public int UserId { get; set; }
        public int CourseProjectId { get; set; }
        public System.DateTime ConsolidationDate { get; set; }
        public Nullable<System.DateTime> PassDate { get; set; }
        public Nullable<int> Mark { get; set; }
        public int DisciplineForYearId { get; set; }
    }
}
