﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.DAL.Models;

namespace NLM.BLL.DTObjects
{
    public class AnswerDTO
    {
        public AnswerDTO()
        {
            
        }

        internal AnswerDTO(Answer a)
        {
            Id = a.Id;
            Text = a.Text;
            QuestionId = a.QuestionId;
            AuthorId = a.QuestionId;
        }

        public int Id { get; set; }
        public string Text { get; set; }
        public int QuestionId { get; set; }
        public int AuthorId { get; set; }
    }
}
