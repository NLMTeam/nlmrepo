﻿using NLM.BLL.DTObjects;
using NLM.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.BLL.DTO.DisciplinePart
{
    public class DisciplineForYearDTO
    {
        public DisciplineForYearDTO()
        {

        }

        public DisciplineForYearDTO(DisciplineForYear dy)
        {
            this.DisciplineForYearId = dy.DisciplineForYearId;
            this.DisciplineId = dy.DisciplineId;

            Lecturers = new List<LecturerForDisciplineDTO>();
            Assistants = new List<LecturerForDisciplineDTO>();

            foreach (var l in dy.Lecturers)
            {
                Lecturers.Add(new LecturerForDisciplineDTO()
                {
                    Id = l.LecturerId,
                    Name = l.Lecturer.User.FIO
                });
            }

            foreach (var l in dy.Assistants)
            {
                Assistants.Add(new LecturerForDisciplineDTO()
                {
                    Id = l.LecturerId,
                    Name = l.Lecturer.User.FIO
                });
            }

            this.Semester = dy.Semester;
            this.Year = dy.Year;
        }

        public int DisciplineForYearId { get; set; }
        public int DisciplineId { get; set; }
        public ICollection<LecturerForDisciplineDTO> Lecturers{ get; set; }
        public ICollection<LecturerForDisciplineDTO> Assistants { get; set; }
        public int Semester { get; set; }
        public string Year { get; set; }
    }
}
