﻿using System;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;

namespace NLM.BLL.DTO.DisciplinePart
{
    public class DisciplineDTO
    {
        public DisciplineDTO()
        {
        }

        internal DisciplineDTO(Discipline d)
        {
            this.DisciplineId = d.DisciplineId;

            this.Name = d.Name;
            this.IsCourseProject = d.IsCourseProject;
        }

        public int DisciplineId { get; set; }
        public string Name { get; set; }
        public bool IsCourseProject { get; set; }
    }
}
