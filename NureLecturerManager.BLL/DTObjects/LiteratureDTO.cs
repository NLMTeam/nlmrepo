﻿using System;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;

namespace NLM.BLL.DTO
{
    public class LiteratureDTO
    {
        public LiteratureDTO()
        {
        }

        internal LiteratureDTO(Literature l)
        {
            this.DisciplineId = l.DisciplineId;
            this.LiteratureId = l.LiteratureId;
            this.Name = l.Name;
        }
        public int LiteratureId { get; set; }
        public string Name { get; set; }
        public int DisciplineId { get; set; }
    }
}
