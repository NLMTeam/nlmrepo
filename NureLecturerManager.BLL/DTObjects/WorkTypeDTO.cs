﻿using System;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;

namespace NLM.BLL.DTO
{
    public class WorkTypeDTO
    {
        public WorkTypeDTO()
        {
        }

        internal WorkTypeDTO(WorkType wt)
        {
            this.DisciplineForYearId = wt.DisciplineForYearId;
            this.MaxMark = wt.MaxMark;
            this.MinMark = wt.MinMark;
            this.Name = wt.Name;
            this.WorkTypeId = wt.WorkTypeId;
        }
    
        public int WorkTypeId { get; set; }
        public string Name { get; set; }
        public int DisciplineForYearId { get; set; }
        public Nullable<int> MaxMark { get; set; }
        public Nullable<int> MinMark { get; set; }
    }
}
