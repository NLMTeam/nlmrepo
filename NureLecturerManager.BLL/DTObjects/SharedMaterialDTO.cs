﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLM.DAL.Models;

namespace NLM.BLL.DTObjects
{
    public class SharedMaterialDTO
    {
        public SharedMaterialDTO()
        {
            
        }

        internal SharedMaterialDTO(SharedMaterials s)
        {
            Id = s.Id;
            Type = s.Type;
            MaterialDoc = s.MaterialDoc;
            DisciplinesId = s.DisciplinesId;
        }

        public int DisciplinesId { get; set; }

        public string MaterialDoc { get; set; }

        public string Type { get; set; }

        public int Id { get; set; }
    }
}
