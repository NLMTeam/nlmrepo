﻿using System;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;

namespace NLM.BLL.DTO
{
    public class ControlPointDTO
    {
        public ControlPointDTO()
        {
        }

        internal ControlPointDTO(ControlPoint cp)
        {
            this.ControlPointId = cp.ControlPointId;
            if (cp.Date != null)
                this.Date = cp.Date.Value.Date.ToShortDateString();
            else
            {
                this.Date = null;
            }
            this.MaxMark = cp.MaxMark;
            this.MinMark = cp.MinMark;
            this.Name = cp.Name;
            this.Number = cp.Number;
            this.DisciplineForYearId = cp.DisciplineForYearId;
        }
    
        public int ControlPointId { get; set; }
        public int DisciplineForYearId { get; set; }
        public int Number { get; set; }
        public string Name { get; set; }
        public string Date { get; set; }
        public Nullable<int> MaxMark { get; set; }
        public Nullable<int> MinMark { get; set; }
    }
}
