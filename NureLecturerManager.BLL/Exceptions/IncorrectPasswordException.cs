﻿using System;

namespace NLM.BLL.Exceptions
{
    public class IncorrectPasswordException: Exception
    {
        public IncorrectPasswordException(): base()
        {
                
        }

        public IncorrectPasswordException(string message): base(message)
        {
            
        }
    }
}