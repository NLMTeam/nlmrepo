﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLM.BLL.DTO;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;
using NLM.BLL.Exceptions;
using NLM.BLL.DTObjects;
using NLM.BLL.Infrastructure;

namespace NLM.BLL.Services
{
    public class DisciplineService : Interfaces.IDisciplineService
    {
        private IUnitOfWork unit;
        public DisciplineService(IUnitOfWork u)
        {
            unit = u;
        }
        public ICollection<DisciplineDTO> GetAllDisciplines()
        {
            List<DisciplineDTO> discs = new List<DisciplineDTO>();
            foreach (var discipline in unit.Disciplines.GetAll())
            {
                discs.Add(new DisciplineDTO(discipline));
            }

            return discs;
        }

        public bool CreateDiscipline(string name, bool isCourseProject = false)
        {
            if (unit.Disciplines.Find(d => d.Name == name && d.IsCourseProject == isCourseProject).FirstOrDefault() == null)
            {
                Discipline d = new Discipline()
                {
                    Name = name,
                    IsCourseProject = isCourseProject
                };
                unit.Disciplines.Create(d);
                unit.Save();
                return true;
            }

            return false;
        }

        public bool DeleteDiscipline(string name)
        {
            Discipline di = unit.Disciplines.Find(d => d.Name == name).Single();
            if (di != null)
            {
                unit.Disciplines.Delete(di.DisciplineId);
                unit.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteDiscipline(int disciplineId)
        {
            Discipline di = unit.Disciplines.Get(disciplineId);
            if (di != null)
            {
                unit.Disciplines.Delete(disciplineId);
                unit.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        public ICollection<DisciplineDTO> SearchDiscipline(int disciplineId = -1, string Name = null, bool? isCourseProject = null)
        {
            List<DisciplineDTO> list = new List<DisciplineDTO>();
            ICollection<Discipline> discCol = unit.Disciplines.Find(p => (disciplineId == -1 || p.DisciplineId == disciplineId)
                                                               && (Name == null || p.Name == Name) && (isCourseProject == null ||
                                                                   p.IsCourseProject == isCourseProject));
            foreach (var disc in discCol)
            {
                list.Add(new DisciplineDTO(disc));
            }

            return list;
        }

        public bool UpdateDiscipline(int discId, string name)
        {

            Discipline disc = unit.Disciplines.Get(discId);
            if (disc != null)
            {
                disc.Name = name;

                unit.Disciplines.Update(disc);
                unit.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        // Filtering for lecturer deepper
        public ICollection<DisciplineForYearDeepDTO> GetDisciplinesForLecturerDeep(int lecturerId)
        {
            ICollection<Assistant> assistants = unit.Assistants.Find(assis => assis.LecturerId == lecturerId);
            ICollection<DisciplineForYear> dfys = unit.DisciplinesForYear.Find(dy => dy.Lecturers.Select(l => l.LecturerId).Contains(lecturerId));
            List<int> discIds = assistants.Select(p => p.DisciplineForYearId).Distinct().ToList();
            discIds.AddRange(dfys.Select(t => t.DisciplineForYearId));
            discIds = discIds.Distinct().ToList();

            IEnumerable<DisciplineForYearDeepDTO> discCol = unit.DisciplinesForYear.Find(d => discIds.Contains(d.DisciplineForYearId)).Select(t =>
                                                                                           new DisciplineForYearDeepDTO()
                                                                                           {
                                                                                               DisciplineForYearId = t.DisciplineForYearId,
                                                                                               DisciplineName = t.Discipline.Name,
                                                                                               IsCourseProject = t.Discipline.IsCourseProject,
                                                                                               LecturerIds = t.Lecturers.Select(l => l.LecturerId).ToList(),
                                                                                               AssistantsIds = t.Assistants.Select(a => a.LecturerId).ToList(),
                                                                                               Year = t.Year,
                                                                                               Semester = t.Semester
                                                                                           });


            return discCol.ToList();
        }


        // Filtering for lecturer 
        public ICollection<DisciplineDTO> GetDisciplinesForLecturer(int lecturerId, String year = null, Int32 semester = -228)
        {
            ICollection<Assistant> assistants = unit.Assistants.Find(assis => assis.LecturerId == lecturerId)
                                                                .Where(assis => (year == null || assis.DisciplineForYear.Year == year) && 
                                                                                    (semester == -228 || assis.DisciplineForYear.Semester == semester))
                                                                .ToList();

            ICollection<DisciplineForYear> dfys = unit.DisciplinesForYear.Find(dy => dy.Lecturers.Select(l => l.LecturerId).Contains(lecturerId)
                                                                               && (year == null || dy.Year == year)
                                                                               && (semester == -228 || semester == dy.Semester));

            ICollection<int> dfyIds = assistants.Select(p => p.DisciplineForYearId).Distinct().ToList();
            List<int> discIds = unit.DisciplinesForYear.Find(df => dfyIds.Contains(df.DisciplineForYearId)).Select(d => d.DisciplineId).ToList();
            discIds.AddRange(dfys.Select(t => t.DisciplineId));
            discIds = discIds.Distinct().ToList();

            ICollection<Discipline> discCol = unit.Disciplines.Find(d => discIds.Contains(d.DisciplineId));

            return discCol.Select(disc => new DisciplineDTO(disc)).ToList();
        }

        public ICollection<DisciplineDTO> GetDisciplinesForLecturerArchieved(int lecturerId, String year, Int32 semester)
        {
            ICollection<Assistant> parts = unit.Assistants
                                                      .Find(p => p.LecturerId == lecturerId &&
                                                                    (Convert.ToInt32(p.DisciplineForYear.Year.Split('-')[0]) < 
                                                                    Convert.ToInt32(year.Split('-')[0]) || 
                                                                    year == p.DisciplineForYear.Year && 
                                                                    p.DisciplineForYear.Semester < semester));

            ICollection<DisciplineForYear> dfys = unit.DisciplinesForYear.Find(dy => dy.Lecturers
                                                                            .Select(l => l.LecturerId)
                                                                            .Contains(lecturerId) &&
                                                                            (Convert.ToInt32(dy.Year.Split('-')[0]) <
                                                                            Convert.ToInt32(year.Split('-')[0]) ||
                                                                            year == dy.Year &&
                                                                            dy.Semester < semester));

            ICollection<int> dfyIds = parts.Select(p => p.DisciplineForYearId).Distinct().ToList();
            List<int> discIds = unit.DisciplinesForYear.Find(df => dfyIds.Contains(df.DisciplineForYearId)).Select(d => d.DisciplineId).ToList();
            discIds.AddRange(dfys.Select(t => t.DisciplineId));
            discIds = discIds.Distinct().ToList();

            ICollection<Discipline> discCol = unit.Disciplines.Find(d => discIds.Contains(d.DisciplineId));

            // Filter from current
            ICollection<int> curCol = this.GetDisciplinesForLecturer(lecturerId, year, semester).Select(d1 => d1.DisciplineId).ToList();

            return discCol.Where(disc => !curCol.Contains(disc.DisciplineId)).Select(disc => new DisciplineDTO(disc)).ToList();
        }

        // Filtering for student

        public ICollection<DisciplineDTO> GetDisciplinesForStudent(string groupId, string year, int semester)
        {
            List<DisciplineDTO> list = new List<DisciplineDTO>();
            ICollection<GroupControl> gcs = unit.GroupControls.Find(p => p.GroupNumber == groupId);
            IEnumerable<int> discYIds = gcs.Select(p => p.WorkType.DisciplineForYearId).Distinct();

            IEnumerable<int> discIds = unit.DisciplinesForYear.Find(d => discYIds.Contains(d.DisciplineForYearId) &&
                                                                                         d.Year == year && d.Semester == semester)
                                                                                         .Select(d => d.DisciplineId);

            ICollection<Discipline> discCol = unit.Disciplines.Find(d => discIds.Contains(d.DisciplineId));

            return discCol.Select(disc => new DisciplineDTO(disc)).ToList();
        }

        public ICollection<DisciplineDTO> GetArchievedDisciplinesForStudent(string groupId, string year, int semester)
        {
            List<DisciplineDTO> list = new List<DisciplineDTO>();
            ICollection<GroupControl> gcs = unit.GroupControls.Find(p => p.GroupNumber == groupId);
            IEnumerable<int> discYIds = gcs.Select(p => p.WorkType.DisciplineForYearId).Distinct();

            IEnumerable<int> discIds = unit.DisciplinesForYear.Find(d => discYIds.Contains(d.DisciplineForYearId) &&
                                                                                         Convert.ToInt32(d.Year.Split('-')[0]) <
                                                                                         Convert.ToInt32(year.Split('-')[0]) ||
                                                                                         d.Year == year &&
                                                                                         d.Semester < semester)
                                                                                         .Select(d => d.DisciplineId);

            ICollection<Discipline> discCol = unit.Disciplines.Find(d => discIds.Contains(d.DisciplineId));

            // Filtering from current

            ICollection<int> curCol = this.GetDisciplinesForStudent(groupId, year, semester).Select(d1 => d1.DisciplineId).ToList();

            return discCol.Where(disc => !curCol.Contains(disc.DisciplineId)).Select(disc => new DisciplineDTO(disc)).ToList();
        }

        // Discipline for year

        //public string Year { get; set; }
        //public int Semester { get; set; }
        //public int? LecturerId { get; set; }
        //public int DisciplineId { get; set; }

        public bool CreateDisciplineForYear(int discId, ICollection<int> lecturerIds, ICollection<Int32> assistantsIds, string year, int semester)
        {
            if (unit.DisciplinesForYear.Find(d => d.DisciplineId == discId
                                                  && d.Year == year &&
                                                  d.Semester == semester).FirstOrDefault() == null)
            {
                DisciplineForYear d = new DisciplineForYear()
                {
                    DisciplineId = discId,
                    Year = year,
                    Semester = semester
                };
                unit.DisciplinesForYear.Create(d);
                unit.Save();

                DisciplineForYear newDiscY = unit.DisciplinesForYear.Find(dy => dy.DisciplineId == discId && dy.Year == year && dy.Semester == semester)
                                                .FirstOrDefault();

                if (lecturerIds != null)
                {
                    foreach (int id in lecturerIds)
                    {
                        LecturerForYear lfy = new LecturerForYear()
                        {
                            LecturerId = id,
                            DisciplineForYearId = newDiscY.DisciplineForYearId
                        };

                        unit.LecturerForYears.Create(lfy);
                        unit.Save();
                    }
                }

                if (assistantsIds != null)
                {
                    foreach (int id in assistantsIds)
                    {
                        Assistant assist = new Assistant()
                        {
                            LecturerId = id,
                            DisciplineForYearId = newDiscY.DisciplineForYearId
                        };

                        unit.Assistants.Create(assist);
                        unit.Save();
                    }
                }

                return true;
            }

            return false;
        }

        public bool DeleteDisciplineForYear(int id)
        {
            DisciplineForYear dy = unit.DisciplinesForYear.Find(d => d.DisciplineForYearId == id).Single();
            if (dy != null)
            {
                unit.DisciplinesForYear.Delete(dy.DisciplineForYearId);
                unit.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        public ICollection<DisciplineForYearDTO> SearchDisciplineForYear(int dyId = -1, int discId = -1, ICollection<int> lecturersIds = null, ICollection<Int32> assistants = null, string year = null, int semester = -1)
        {
            List<DisciplineForYearDTO> list = new List<DisciplineForYearDTO>();
            IEnumerable<int> lectColl = lecturersIds != null ? lecturersIds.OrderBy(l => l) : null;
            IEnumerable<int> lectCol2 = assistants != null ? assistants.OrderBy(l => l) : null;

            ICollection<DisciplineForYear> discCol = unit.DisciplinesForYear.Find(p => (dyId == -1 || p.DisciplineForYearId == dyId) &&
                                                                                       (discId == -1 || p.DisciplineId == discId) &&
                                                                                       (lecturersIds == null || p.Lecturers.Select(p1 => p1.LecturerId)
                                                                                            .OrderBy(i1 => i1)
                                                                                            .SequenceEqual(lectColl)) &&
                                                                                            (assistants == null || p.Assistants.Select(p1 => p1.LecturerId)
                                                                                            .OrderBy(i1 => i1)
                                                                                            .SequenceEqual(lectCol2)) &&
                                                                                       (year == null || p.Year == year) &&
                                                                                       (semester == -1 || p.Semester == semester));
            foreach (var disc in discCol)
            {
                list.Add(new DisciplineForYearDTO(disc));
            }

            return list;
        }

        public ICollection<DisciplineForYearDTO> SearchDisciplineForYearXor(int dyId = -1, int discId = -1, int lecturerId = -1, string year = null, int semester = -1)
        {
            List<DisciplineForYearDTO> list = new List<DisciplineForYearDTO>();
            ICollection<DisciplineForYear> discCol = unit.DisciplinesForYear.Find(p => (dyId == -1 || p.DisciplineForYearId == dyId) &&
                                                                                       (discId == -1 || p.DisciplineId == discId) &&
                                                                                       (lecturerId == -1 || p.Lecturers.Select(p1 => p1.LecturerId)
                                                                                            .Contains(lecturerId)) &&
                                                                                       (year == null || p.Year == year) &&
                                                                                       (semester == -1 || p.Semester == semester));
            foreach (var disc in discCol)
            {
                list.Add(new DisciplineForYearDTO(disc));
            }

            return list;
        }


        public bool UpdateDisciplineForYear(int dyId, ICollection<int> lecturerIds, ICollection<Int32> assistants, int discId = -1, string year = null, int semester = -1)
        {

            DisciplineForYear dy = unit.DisciplinesForYear.Get(dyId);

            if (dy != null)
            {
                DisciplineForYear dy2 = unit.DisciplinesForYear.Find(dy1 => dy1.DisciplineId == dy.DisciplineId && (year == null || dy1.Year == year) &&
                                (semester == -1 || dy1.Semester == semester) && dy1.DisciplineForYearId != dy.DisciplineForYearId).FirstOrDefault();
                if (dy2 != null)
                {
                    return false;
                }

                dy.DisciplineId = discId == -1 ? dy.DisciplineId : discId;
                dy.Year = year == null ? dy.Year : year;
                dy.Semester = semester == -1 ? dy.Semester : semester;

                unit.DisciplinesForYear.Update(dy);
                unit.Save();

                var tempCol = unit.LecturerForYears.Find(lfy => lfy.DisciplineForYearId == dyId);
                var tempCol2 = unit.Assistants.Find(lfy => lfy.DisciplineForYearId == dyId);

                if (lecturerIds == null)
                {
                    foreach (var temp in tempCol)
                    {
                        unit.LecturerForYears.Delete(temp.LecturerForYearId);
                    }
                    unit.Save();
                }
                else
                {
                    foreach (var temp in tempCol)
                    {
                        if (!lecturerIds.Contains(temp.LecturerId))
                        {
                            unit.LecturerForYears.Delete(temp.LecturerForYearId);
                        }
                    }
                    unit.Save();

                    if (lecturerIds != null)
                    {
                        foreach (var id in lecturerIds)
                        {
                            if (unit.LecturerForYears.Find(lfp => lfp.DisciplineForYearId == dyId && lfp.LecturerId == id).Count == 0)
                            {
                                unit.LecturerForYears.Create(new LecturerForYear
                                {
                                    LecturerId = id,
                                    DisciplineForYearId = dyId
                                });
                            }
                        }
                        unit.Save();
                    }
                }

                if (assistants == null)
                {
                    foreach (var temp in tempCol2)
                    {
                        unit.Assistants.Delete(temp.DisciplineForYearId, temp.LecturerId);

                        ICollection<GroupControl> gcsCol = unit.GroupControls.Find(gc => gc.WorkType.DisciplineForYearId == temp.DisciplineForYearId);
                        foreach (var gc in gcsCol)
                        {
                            unit.AssistantsForGC.Delete(gc.GroupControlId, temp.LecturerId);
                        }
                        unit.Save();
                    }
                    unit.Save();
                }
                else
                {
                    foreach (var temp in tempCol2)
                    {
                        if (!assistants.Contains(temp.LecturerId))
                        {
                            unit.Assistants.Delete(temp.DisciplineForYearId, temp.LecturerId);
                            ICollection<GroupControl> gcsCol = unit.GroupControls.Find(gc => gc.WorkType.DisciplineForYearId == temp.DisciplineForYearId);
                            foreach (var gc in gcsCol)
                            {
                                unit.AssistantsForGC.Delete(gc.GroupControlId, temp.LecturerId);
                            }
                            unit.Save();
                        }
                    }
                    unit.Save();

                    if (assistants != null)
                    {
                        foreach (var id in assistants)
                        {
                            if (unit.Assistants.Find(lfp => lfp.DisciplineForYearId == dyId && lfp.LecturerId == id).Count == 0)
                            {
                                unit.Assistants.Create(new Assistant
                                {
                                    LecturerId = id,
                                    DisciplineForYearId = dyId
                                });
                            }
                        }
                        unit.Save();
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        //public ICollection<PartOfDisciplineDTO> SearchArchievedPartForStudent(String GroupNum, String year, int term)
        //{
        //    List<PartOfDisciplineDTO> list = new List<PartOfDisciplineDTO>();
        //    ICollection<PartOfDiscipline> parts = unit.PartOfDisciplines.Find(p => (p.GroupNumber == GroupNum && (p.DisciplineForYear.Year == year &&
        //                                                                                             p.DisciplineForYear.Semester < term ||
        //                                                                                             Convert.ToInt32(p.DisciplineForYear.Year.Split('-')[0])
        //                                                                                             < Convert.ToInt32(year.Split('-')[0]))));

        //    int i = -1;
        //    foreach (var part in parts)
        //    {
        //        i++;
        //        list.Add(new PartOfDisciplineDTO(part));
        //    }

        //    return list;
        //}

        // Returns DFYId

        public int ProlongDFY(int dfyId, string year, int semester)
        {
            // Find old DFY

            DisciplineForYearDTO oldDFY = this.SearchDisciplineForYear(dfyId).Single();

            // Search for new DFY
            DisciplineForYear newNullDFY = unit.DisciplinesForYear.Find(dfy => dfy.DisciplineId == oldDFY.DisciplineId &&
                                                                           dfy.Year == year && dfy.Semester == semester).FirstOrDefault();

            if (newNullDFY != null)
            {
                return -1; // AlreadyRegistered
            }

            // Create new DFY 
            this.CreateDisciplineForYear(oldDFY.DisciplineId, oldDFY.LecturerIds, oldDFY.AssistantsIds, year, semester);

            // Search for new DFY
            DisciplineForYear newDFY = unit.DisciplinesForYear.Find(dfy => dfy.DisciplineId == oldDFY.DisciplineId &&
                                                                           dfy.Year == year && dfy.Semester == semester).Single();
            if (newDFY == null)
            {
                return -2; // NotFound
            }

            // bind old cps to new dfy
            foreach (var point in unit.ControlPoints
                                      .Find(cp => (cp.DisciplineForYearId == oldDFY.DisciplineForYearId)))
            {
                unit.ControlPoints.Create(new ControlPoint()
                {
                    Date = point.Date,
                    DisciplineForYearId = newDFY.DisciplineForYearId,
                    MaxMark = point.MaxMark,
                    MinMark = point.MinMark,
                    Name = point.Name,
                    Number = point.Number
                });
                unit.Save();

                var newCP = unit.ControlPoints.Find(cp => cp.Date == point.Date &&
                                                          cp.DisciplineForYearId == newDFY.DisciplineForYearId &&
                                                          cp.MaxMark == point.MaxMark && cp.MinMark == point.MinMark &&
                                                          cp.Name == point.Name && cp.Number == point.Number).Single();
            }

            // bind old workTypes to new dfy
            var wts = unit.WorkTypes.Find(w => w.DisciplineForYearId == oldDFY.DisciplineForYearId);

            foreach (var wt in wts)
            {
                unit.WorkTypes.Create(new WorkType()
                {
                    DisciplineForYearId = newDFY.DisciplineForYearId,
                    MaxMark = wt.MaxMark,
                    MinMark = wt.MinMark,
                    Name = wt.Name,
                });
                unit.Save();

                var newWT = unit.WorkTypes.Find(wtt => wtt.DisciplineForYearId == newDFY.DisciplineForYearId &&
                                                   wtt.MaxMark == wt.MaxMark &&
                                                   wtt.MinMark == wt.MinMark &&
                                                   wtt.Name == wt.Name).Single();
            }

            return newDFY.DisciplineForYearId;
        }
        public bool IsAdmin(int dfyId, int lectId)
        {
            if (unit.LecturerForYears.Find(lfy => lfy.DisciplineForYearId == dfyId && lfy.LecturerId == lectId).Count == 0)
                return false;
            else
                return true;
        }

        public ICollection<DisciplineForYearDeepDTO> SearchDisciplinesForYearDeep(int dfyId)
        {
            List<DisciplineForYearDeepDTO> list = new List<DisciplineForYearDeepDTO>();
            ICollection<DisciplineForYear> discCol = unit.DisciplinesForYear.Find(d => d.DisciplineForYearId == dfyId);
            foreach (var disc in discCol)
            {
                list.Add(new DisciplineForYearDeepDTO(disc));
            }

            return list;
        }

        public ICollection<DisciplineDTO> SearchDisciplinesForGroup(String groupNum, String year, Int32 semester)
        {
            ICollection<DisciplineDTO> list =
                unit.GroupControls.Find(gc => gc.GroupNumber == groupNum && gc.WorkType.DisciplineForYear.Year == year &&
                                                gc.WorkType.DisciplineForYear.Semester == semester)
                                    .Select(gc => new DisciplineDTO(gc.WorkType.DisciplineForYear.Discipline))
                                    .Distinct(new DisciplineDTOComparator())
                                    .ToList();

            return list;
        }

        public ICollection<DisciplineDTO> SearchArchievedDisciplinesForGroup(String groupNum, String year, Int32 semester)
        {
            Int32 firstYear = Convert.ToInt32(year.Split('-')[0]);

            ICollection <DisciplineDTO> list =
                unit.GroupControls.Find(gc => gc.GroupNumber == groupNum && 
                                                Convert.ToInt32(gc.WorkType.DisciplineForYear.Year.Split('-')[0]) < firstYear ||
                                                (Convert.ToInt32(gc.WorkType.DisciplineForYear.Year.Split('-')[0]) == firstYear && 
                                                gc.WorkType.DisciplineForYear.Semester < semester))
                                    .Select(gc => new DisciplineDTO(gc.WorkType.DisciplineForYear.Discipline))
                                    .Distinct(new DisciplineDTOComparator())
                                    .ToList();

            return list;
        }

        public bool IsAdminForDiscipline(int discId, int lectId, string year, Int32 semester)
        {
            if (unit.DisciplinesForYear.Find(dfy => dfy.DisciplineId == discId &&
                                                dfy.Lecturers.Select(l => l.LecturerId).Contains(lectId) && dfy.Year == year && 
                                                dfy.Semester == semester).Count == 0)
                return false;
            else
                return true;
        }
    }
}
