﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;
using NLM.BLL.DTO;
using NLM.BLL.Interfaces;
using NLM.BLL.Exceptions;
using NLM.BLL.DTObjects;

namespace NLM.BLL.Services
{
    public class HelpService : IHelpService
    {
        private IUnitOfWork unit;

        public HelpService(IUnitOfWork u)
        {
            unit = u;
        }
        public ICollection<LiteratureDTO> GetLiteratureForDiscipline(int disciplineId)
        {
            List<LiteratureDTO> list = new List<LiteratureDTO>();

            ICollection<Literature> lit = unit.Literature.Find(l => l.DisciplineId == disciplineId);
            foreach (var book in lit)
            {
                list.Add(new LiteratureDTO()
                {
                    LiteratureId = book.LiteratureId,
                    Name = book.Name,
                    DisciplineId = book.DisciplineId
                });
            }

            return list;
        }

        public ICollection<LiteratureDTO> GetLiteratureForDiscipline(string disciplineName)
        {
            List<LiteratureDTO> list = new List<LiteratureDTO>();

            ICollection<Literature> lit = unit.Literature.Find(l => l.Discipline.Name == disciplineName);
            foreach (var book in lit)
            {
                list.Add(new LiteratureDTO()
                {
                    LiteratureId = book.LiteratureId,
                    Name = book.Name,
                    DisciplineId = book.DisciplineId
                });
            }

            return list;
        }

        public ICollection<LiteratureDTO> SearchLiterature(int literatureId = -1, String Name = null, int disciplineId = -1)
        {
            List<LiteratureDTO> list = new List<LiteratureDTO>();

            ICollection<Literature> lit = unit.Literature.Find(l => (literatureId == -1 || l.LiteratureId == literatureId)
                                                                && (Name == null || l.Name == Name)
                                                                &&
                                                                (disciplineId == -1 || l.DisciplineId == disciplineId));
            foreach (var book in lit)
            {
                list.Add(new LiteratureDTO()
                {
                    LiteratureId = book.LiteratureId,
                    Name = book.Name,
                    DisciplineId = book.DisciplineId
                });
            }

            return list;
        }

        public bool CreateLiterature(string name, int disciplineId)
        {
            if (unit.Literature.Find(l => l.Name == name).FirstOrDefault() != null)
            {
                return false;
            }

            Literature liter = new Literature()
            {
                Name = name,
                DisciplineId = disciplineId
            };

            unit.Literature.Create(liter);
            unit.Save();
            return true;
        }

        /// <summary>
        /// Исключение здесь: AlreadyRegistered генерится, а NotFound заменён на булеву
        /// </summary>
        public bool CreateLiterature(string name, string disciplineName)
        {
            if (unit.Literature.Find(l => l.Name == name).FirstOrDefault() != null)
                throw new AlreadyRegisteredItemException();

            Discipline discipline = unit.Disciplines.Find(d => d.Name == disciplineName)
                                                    .SingleOrDefault();
            if (discipline != null)
            {
                CreateLiterature(name, discipline.DisciplineId);
                unit.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ChangeBookDiscipline(int bookId, int disciplineId)
        {
            Literature lit = unit.Literature.Get(bookId);
            if (lit == null)
            {
                return false;
            }

            lit.DisciplineId = disciplineId;
            unit.Literature.Update(lit);
            unit.Save();
            return true;
        }

        public bool DeleteBook(int bookId)
        {
            if (unit.Literature.Get(bookId) == null)
            {
                return false;
            }
            unit.Literature.Delete(bookId);
            unit.Save();
            return true;
        }

        public bool UpdateLiterature(int literatureId, string name = null, int disciplineId = -1)
        {
            Literature literature = unit.Literature.Get(literatureId);

            if (literature == null)
            {
                return false;
            }

            if (unit.Literature.Find(l => l.Name == name && l.DisciplineId == literature.DisciplineId).FirstOrDefault() != null)
                throw new AlreadyRegisteredItemException();

            literature.Name = name != null ? name : literature.Name;
            literature.DisciplineId = disciplineId != -1 ? disciplineId : literature.DisciplineId;

            unit.Literature.Update(literature);
            unit.Save();
            return true;
        }

        // Shared materials section
        public ICollection<SharedMaterialDTO> SearchSharedMaterials(int id = 0, string type = null, string materialDoc = null,
            int disciplinesId = 0)
        {
            var list = new List<SharedMaterialDTO>(
                unit.SharedMaterials.Find(
                    q =>
                        (id == 0 || q.Id == id) && (type == null || q.Type == type) &&
                        (materialDoc == null || q.MaterialDoc == materialDoc) &&
                        (disciplinesId == 0 || q.DisciplinesId == disciplinesId)).Select(s => new SharedMaterialDTO(s)));

            return list;
        }

        public bool CreateSharedMaterial(string type, string materialDoc, int disciplinesId)
        {

                ICollection<SharedMaterialDTO> oldMat = SearchSharedMaterials(type: type, materialDoc: materialDoc, disciplinesId: disciplinesId);

            if (oldMat.Count == 0)
            {
                SharedMaterials sharedMaterials = new SharedMaterials()
                {
                    MaterialDoc = materialDoc,
                    Type = type,
                    DisciplinesId = disciplinesId,
                };
                unit.SharedMaterials.Create(sharedMaterials);
                unit.Save();
                return true;
            }

            return false;
        }

        public bool DeleteSharedMaterials(int sharedMaterialsId)
        {
            if (SearchSharedMaterials(sharedMaterialsId).Count == 0)
            {
                return false;
            }

            unit.SharedMaterials.Delete(sharedMaterialsId);
            unit.Save();
            return true;
        }

        public bool UpdateSharedMaterial(int sharedMaterialsId, string type = null, string materialDoc = null)
        {
            var sharedMaterial = unit.SharedMaterials.Get(sharedMaterialsId);

            if (sharedMaterial == null)
            {
                return false;
            }

            sharedMaterial.Type = type ?? sharedMaterial.Type;
            sharedMaterial.MaterialDoc = materialDoc ?? sharedMaterial.MaterialDoc;

            unit.SharedMaterials.Update(sharedMaterial);
            unit.Save();
            return true;
        }

        public ICollection<SharedMaterialDTO> GetSharedMaterialsForDiscipline(int disciplineId)
        {
            List<SharedMaterialDTO> list = new List<SharedMaterialDTO>();
            ICollection<SharedMaterials> mat = unit.SharedMaterials.Find(m => m.DisciplinesId == disciplineId);
            foreach (var material in mat)
            {
                list.Add(new SharedMaterialDTO(material));
            }

            return list;
        }
    }
}
