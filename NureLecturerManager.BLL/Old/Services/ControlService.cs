﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLM.DAL.Interfaces;
using NLM.DAL.Models;
using NLM.BLL.DTO;
using NLM.BLL.Exceptions;

namespace NLM.BLL.Services
{
    public class ControlService : Interfaces.IControlService
    {
        private IUnitOfWork unit;

        public ControlService(IUnitOfWork u)
        {
            unit = u;
        }
        public ICollection<WorkTypeDTO> GetWorkTypesOfDFY(Int32 DFYId)
        {
            List<WorkTypeDTO> list = new List<WorkTypeDTO>();

            var workTypes = unit.WorkTypes.Find(wt => wt.DisciplineForYearId == DFYId);

            foreach (var workType in workTypes)
            {
                list.Add(new WorkTypeDTO(workType));
            }

            return list;
        }

        public ICollection<WorkTypeDTO> SearchWorkType(int WorkTypeId = -1, 
                                                        string Name = null, int DFYId = -1, 
                                                        int minScore = -1, int maxScore = -1)
        {
            List<WorkTypeDTO> list = new List<WorkTypeDTO>();
            ICollection<WorkType> wts = unit.WorkTypes
                .Find(wt => (WorkTypeId == -1 || wt.WorkTypeId == WorkTypeId)
                            && (Name == null || wt.Name == Name)
                            && (DFYId == -1 || wt.DisciplineForYearId == DFYId)
                            && (minScore == -1 || minScore == wt.MinMark)
                            && (maxScore == -1 || maxScore == wt.MaxMark));
            foreach (var workType in wts)
            {
                list.Add(new WorkTypeDTO(workType));
            }

            return list;
        }

        public bool CreateWorkType(string name, int DFYId, 
                                    int minScore, int maxScore)
        {
            if (unit.WorkTypes.Find(w => w.Name == name &&
                                         w.DisciplineForYearId == DFYId &&
                                         w.MinMark == minScore &&
                                         w.MaxMark == maxScore)
                              .FirstOrDefault() != null)
            {
                return false;
            }

            WorkType wt = new WorkType()
            {
                Name = name,
                DisciplineForYearId = DFYId,
                MinMark = minScore,
                MaxMark = maxScore,
            };

            unit.WorkTypes.Create(wt);
            unit.Save();
            return true;
        }

        public bool UpdateWorkType(int worktypeId, string name = null, int DFYId = -1 , 
                                   int minScore = -1, int maxScore = -1)
        {
            var work = unit.WorkTypes.Get(worktypeId);
            if (work != null)
            {
                work.Name = name != null ? name : work.Name;
                work.DisciplineForYearId = DFYId != -1 ? DFYId : work.DisciplineForYearId;
                work.MinMark = minScore != -1 ? minScore : work.MinMark;
                work.MaxMark = maxScore != -1 ? maxScore : work.MaxMark;

                unit.WorkTypes.Update(work);
                unit.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteWorkType(int worktypeId)
        {
            if (unit.WorkTypes.Get(worktypeId) != null)
            {
                unit.WorkTypes.Delete(worktypeId);
                unit.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        public ICollection<GroupControlDTO> GetGroupControls(string GroupNum)
        {
            List<GroupControlDTO> list = new List<GroupControlDTO>();
            ICollection<GroupControl> gcs = unit.GroupControls
                .Find(gc => gc.GroupNumber == GroupNum);
            foreach (var control in gcs)
            {
                list.Add(new GroupControlDTO(control));
            }

            return list;
        }

        public ICollection<GroupControlDTO> SearchGroupControls(int groupControlId = -1, int workTypeId = -1, 
                                                                string GroupNum = null, string Type = null, string Place = null,
                                                                DateTime? pass = null)
        {
            List<GroupControlDTO> list = new List<GroupControlDTO>();
            ICollection<GroupControl> gcs =
                unit.GroupControls.Find(gc => (groupControlId == -1 || gc.GroupControlId == groupControlId)
                                              && (workTypeId == -1 || gc.WorkTypeId == workTypeId)
                                              && (GroupNum == null || gc.GroupNumber == GroupNum)
                                              && (Type == null || gc.Type == Type)
                                              && (Place == null || gc.PassPlace == Place)
                                              && (pass == null || gc.PassDate == pass.Value));
            foreach (var control in gcs)
            {
                list.Add(new GroupControlDTO(control));
            }

            return list;
        }

        public bool CreateGroupControl(int worktypeId, string GroupNumber, string type, string place, DateTime? passDate)
        {
            if (unit.GroupControls.Find(gc => gc.WorkTypeId == worktypeId && gc.GroupNumber == GroupNumber &&
                                              gc.PassDate == passDate && gc.Type == type && gc.PassPlace == place)
                                              .FirstOrDefault() != null)
            {
                return false;
            }

            GroupControl gc1 = new GroupControl()
            {
                WorkTypeId = worktypeId,
                GroupNumber = GroupNumber,
                PassPlace = place,
                PassDate = passDate,
                Type = type
            };

            unit.GroupControls.Create(gc1);
            unit.Save();
            return true;
        }

        public bool UpdateGroupControl(int groupControlId, int worktypeId = -1, string GroupNumber = null, 
                        DateTime? passDate = null, string type = null, string place = null)
        {
            var gc = unit.GroupControls.Get(groupControlId);
            if (gc != null)
            {
                gc.WorkTypeId = worktypeId != -1 ? worktypeId : gc.GroupControlId;
                gc.GroupNumber = GroupNumber != null ? GroupNumber : gc.GroupNumber;
                gc.PassDate = passDate;
                gc.Type = type != null ? type : gc.Type;
                gc.PassPlace = place != null ? place : gc.PassPlace;

                unit.GroupControls.Update(gc);
                unit.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteGroupControl(int groupcontrolId)
        {
            if (unit.GroupControls.Get(groupcontrolId) != null)
            {
                unit.GroupControls.Delete(groupcontrolId);
                unit.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SetMark(int groupControlId, int userId, int? mark, DateTime date = default(DateTime))
        {
            StudentControl sc = new StudentControl()
            {
                GroupControlId = groupControlId,
                UserId = userId,
                Mark = mark,
                PassDate = date
            };
            StudentControl sC = unit.StudentControls.Find(g => g.GroupControlId == sc.GroupControlId && g.UserId == sc.UserId)
                                                    .FirstOrDefault();
            if (sC!=null)
            {
                sC.UserId = sc.UserId;
                sC.Mark = sc.Mark;
                sC.PassDate = sc.PassDate;
                sC.GroupControlId = sc.GroupControlId;

                unit.StudentControls.Update(sC);
            }
            else
                unit.StudentControls.Create(sc);
            unit.Save();
        }

        public ICollection<KeyValuePair<UserDTO, int>> GetMarksOfGroup(int groupControlId)
        {
            List<KeyValuePair<UserDTO, int>> list = new List<KeyValuePair<UserDTO, int>>();
            ICollection<StudentControl> stcs = unit.StudentControls
                .Find(sc => sc.GroupControlId == groupControlId);
            foreach (var control in stcs)
            {
                UserDTO user = new UserDTO(unit.Users.Get(control.UserId));
                int? mark = control.Mark;
                mark = mark == null ? -1 : mark;
                list.Add(new KeyValuePair<UserDTO, int>(user, mark.Value));
            }

            return list;
        }

        public ICollection<StudentControlDTO> SearchStudentcontrols(int userId = -1, 
                                                                    int Mark = -1, int GroupControlId = -1, 
                                                                    DateTime passDate = default(DateTime))
        {
            List<StudentControlDTO> list = new List<StudentControlDTO>();
            foreach (var control in unit.StudentControls
                                        .Find(sc => (userId == -1 || sc.UserId == userId)
                                                    && (Mark == -1 || sc.Mark.Value == Mark)
                                                    && (GroupControlId == -1 || sc.GroupControlId == GroupControlId)
                                                    && (passDate == default(DateTime) || sc.PassDate == passDate)))
            {
                list.Add(new StudentControlDTO(control));
            }

            return list;
        }

        public void UpdateMark(int groupControlId, int userId, int? mark, 
                                DateTime date = default(DateTime))
        {
            StudentControl cont = unit.StudentControls.Get(groupControlId, userId);
            if (cont == null)
            {
                this.SetMark(groupControlId, userId, mark, date);
            }
            else
            {
                cont.GroupControlId = groupControlId != -1 ? groupControlId : cont.GroupControlId;
                cont.UserId = userId != -1 ? userId : cont.UserId;
                cont.Mark = mark != -1 ? mark : cont.Mark;
                cont.PassDate = date != default(DateTime) ? date : cont.PassDate;

                unit.StudentControls.Update(cont);
            }

        }

        public bool DeleteMark(int groupControlId, int userId)
        {
            if (unit.StudentControls.Get(groupControlId, userId) != null)
            {
                unit.StudentControls.Delete(groupControlId, userId);
                unit.Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        // False = Access denied
        public Int32 UpdateMarksList(ICollection<StudentControlDTO> marks, Int32 dfyId, Int32 userId)
        {
            Int32 denieded = 0;
            foreach (var mark in marks)
            {
                if (IsLecturerOrCurrentAssistant(dfyId, mark.GroupControlId, userId))
                {
                    this.UpdateMark(groupControlId: mark.GroupControlId, userId: mark.UserId, mark: mark.Mark, date: mark.PassDate);
                }
                else
                {
                    denieded++;
                    continue;
                }
            }

            return denieded;
        }

        private bool IsLecturerOrCurrentAssistant(Int32 dfyId, Int32 groupControlId, Int32 userId)
        {
            if (unit.LecturerForYears.Find(ldfy => ldfy.DisciplineForYearId == dfyId && ldfy.LecturerId == userId).FirstOrDefault() != null)
            {
                return true;
            }
            else
            {
                if (unit.AssistantsForGC.Get(groupControlId, userId) != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
