﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLM.BLL.Exceptions
{
    public class SomethingWithDBException: Exception
    {
        public SomethingWithDBException():base()
        {

        }

        public SomethingWithDBException(String message) : base(message)
        {

        }
    }
}
