﻿using System;
using System.Collections.Generic;
using NLM.BLL.DTO;
using NLM.BLL.DTObjects;

namespace NLM.BLL.Interfaces
{
    public interface IDisciplineService
    {
        ICollection<DisciplineDTO> GetAllDisciplines();
        ICollection<DisciplineDTO> SearchDiscipline(int disciplineId = -1, String Name = null, bool? isCourseProject = null);
        bool CreateDiscipline(string name, bool isCourseProject = false);
        bool UpdateDiscipline(int discId, string name);
        bool DeleteDiscipline(int disciplineId);
        bool DeleteDiscipline(string name);

        //ICollection<PartOfDisciplineDTO> GetDisciplineParts(string disciplineName);
        //ICollection<PartOfDisciplineDTO> SearchPartOfDiscipline(int disciplineYId = -1, String GroupNum = null, ICollection<int> LecturersIds = null,
        //                                                        String Year = null, int Term = -1,
        //                                                        int partId = -1, String ControlForm = null);
        //bool CreatePartOfDiscipline(string GroupNum, ICollection<int> lecturerIds, int discId, string controlForm);
        //bool UpdatePartOfDiscipline(int partId, String groupNum = null, ICollection<int> lecturerIds = null, int discId = -1,String ControlForm = null);
        //bool DeletePartOfDiscipline(int partId);
        ICollection<DisciplineDTO> GetDisciplinesForLecturer(int lecturerId, String year = null, Int32 semester = -228);
        ICollection<DisciplineDTO> GetDisciplinesForLecturerArchieved(int lecturerId, String year, Int32 semester);
        ICollection<DisciplineDTO> GetDisciplinesForStudent(string groupId, string year, int semester);
        ICollection<DisciplineDTO> GetArchievedDisciplinesForStudent(string groupId, string year, int semester);


        bool CreateDisciplineForYear(int discId, ICollection<int> lecturerIds, ICollection<Int32> assistantsIds, string year, int semester);

        bool DeleteDisciplineForYear(int id);

        ICollection<DisciplineForYearDTO> SearchDisciplineForYear(int dyId = -1, int discId = -1, ICollection<int> lecturersIds = null,
                                                                        ICollection<Int32> assistants = null, 
                                                                         string year = null, int semester = -1);

        bool UpdateDisciplineForYear(int dyId, ICollection<int> lecturerIds, ICollection<int> assistants, int discId = -1, 
                                                                                        string year = null, int semester = -1);

        //ICollection<PartOfDisciplineDTO> SearchArchievedPartForStudent(String GroupNum, String year, int term);
        //ICollection<PartOfDisciplineDTO> SearchPartOfDisciplineXor(int disciplineYId = -1, string GroupNum = null,
        //                                                        int LecturerId = -1, string Year = null,
        //                                                        int Term = -1, int partId = -1,
        //                                                        String ControlForm = null);
        ICollection<DisciplineForYearDTO> SearchDisciplineForYearXor(int dyId = -1, int discId = -1, int lecturerId = -1, string year = null, int semester = -1);
        int ProlongDFY(int dfyId, string year, int semester);
        ICollection<DisciplineForYearDeepDTO> GetDisciplinesForLecturerDeep(int lecturerId);
        bool IsAdmin(int dfyId, int lectId);
        ICollection<DisciplineForYearDeepDTO> SearchDisciplinesForYearDeep(int dfyId);
        ICollection<DisciplineDTO> SearchDisciplinesForGroup(String groupNum, String year, Int32 semester);
        ICollection<DisciplineDTO> SearchArchievedDisciplinesForGroup(String groupNum, String year, Int32 semester);
        bool IsAdminForDiscipline(int discId, int lectId, string year, Int32 semester);
    }
}
