﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLM.BLL.DTO;

namespace NLM.BLL.Interfaces
{
    public interface IControlService
    {
        ICollection<WorkTypeDTO> GetWorkTypesOfDFY(Int32 DFYId);
        ICollection<WorkTypeDTO> SearchWorkType(int WorkTypeId = -1, String Name = null,
                                                int DFYId = -1, int minScore = -1,
                                                int maxScore = -1);
        bool CreateWorkType(string name, int DFYId, int minScore, int maxScore);
        bool UpdateWorkType(int worktypeId, string name, int DFYId = -1,
                            int minScore = -1, int maxScore = -1);
        bool DeleteWorkType(int worktypeId);

        ICollection<GroupControlDTO> GetGroupControls(string GroupNum);
        ICollection<GroupControlDTO> SearchGroupControls(int GroupcontrolId = -1, int workTypeId = -1, String GroupNum = null, String Type = null,
                                                         String Place = null, DateTime? pass = null);
        bool CreateGroupControl(int worktypeId, string GroupNumber, string type, string place, DateTime? pass = null);
        bool UpdateGroupControl(int groupControlId, int worktypeId = -1, 
                                String GroupNumber = null, DateTime? pass = null, String type = null, 
                                String place = null);
        bool DeleteGroupControl(int groupControlId);

        void SetMark(int groupControlId, int userId, int? mark, DateTime date = default(DateTime));
        ICollection<KeyValuePair<UserDTO, int>> GetMarksOfGroup(int groupControlId);
        ICollection<StudentControlDTO> SearchStudentcontrols(int userId = -1, int Mark = -1,
                                                            int GroupControlId = -1, DateTime passDate = default(DateTime));
        void UpdateMark(int groupControlId, int userId, int? mark, DateTime date = default(DateTime));
        bool DeleteMark(int groupControlId, int userId);
        Int32 UpdateMarksList(ICollection<StudentControlDTO> marks, Int32 dfyId, Int32 userId);
    }
}
