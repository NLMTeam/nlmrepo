/**
 * Created by Paul on 05.02.2017.
 */

/**
 * Created by Paul on 31.01.2017.
 */

import { Component, OnInit } from "@angular/core";
import { NgForm} from '@angular/forms';

// services
import {DisciplineService} from "./services/Discipline.service";

// models
import {Discipline} from "./models/Discipline";

// lib
import { StatusCode } from "./library/StatusCodes";

@Component({
    selector: "discipline",
    templateUrl: "app/views/discipline.html",
    styleUrls: [ "app/styles/discipline.css", "app/styles/modal.css", "app/styles/form.css",
                    "app/styles/adminForm.css", "app/styles/dfyPlusDiscipline.css" ],
    providers: [DisciplineService]
})
export class DisciplineComponent implements OnInit {

    private editedDisciplineName: string;
    private editedDiscipline: Discipline = null;

    private deletingDiscipline: Discipline = null;
    private deleteNotificationShow: boolean = false;

    private disciplineCollection: Discipline[] = [];

    private isFormVisible: boolean = false;

    private editError: string = "";

    public constructor(private DisciplineService: DisciplineService) { }

    ngOnInit() {
        this.DisciplineService.GetDisciplines().then(data => {
            console.log(data);
            this.disciplineCollection = data;
        });
    }

    clearEditError(){
        this.editError = "";
    }

    saveEdit(form: NgForm) {
        this.editError = "";
        if (this.editedDisciplineName == null){
            return;
        }

        this.editedDisciplineName = this.editedDisciplineName.trim();

        if (this.editedDisciplineName.length == 0) {
            return;
        }

        if (this.editedDiscipline == null) {
            let discipline = new Discipline(this.editedDisciplineName, false);
            this.DisciplineService.CreateDiscipline(discipline).then(resp => {

                if (resp.status == StatusCode.Created) {
                    let disc = resp.json();
                    discipline.DisciplineId = disc.DisciplineId;

                    this.disciplineCollection.push(discipline);
                    console.log(discipline);
                    this.editedDisciplineName = null;
                    form.resetForm();
                }
            }, error => {
                if (error.status == StatusCode.BadRequest) {
                    console.log("Bad request");
                    this.editError = "Введите название дисциплины";
                }

                if (error.status == StatusCode.Conflict){
                    console.log("Conflict");
                    this.editError = "Дисциплина с таким названием уже существует в БД";
                }
            });
        }
    }

    showDeleteNotification(Discipline: Discipline) {
        this.deletingDiscipline = Discipline;
        this.deleteNotificationShow = true;
        let elem: HTMLBodyElement = <HTMLBodyElement>document.getElementsByTagName("body")[0];
        elem.setAttribute('style', 'overflow: hidden');
    }

    hideDeleteNotification() {
        this.deleteNotificationShow = false;
        this.deletingDiscipline = null;
        let elem: HTMLBodyElement = <HTMLBodyElement>document.getElementsByTagName("body")[0];
        elem.setAttribute('style', 'overflow: auto');
    }

    submitDelete() {

        if (this.deletingDiscipline == null) {
            return;
        }

        this.DisciplineService.DeleteDiscipline(this.deletingDiscipline).subscribe(resp => {

            if (resp.status == StatusCode.NoContent) {
                this.disciplineCollection = this.disciplineCollection.filter(g => g.DisciplineId != this.deletingDiscipline.DisciplineId);
            }

            if (resp.status == StatusCode.BadRequest) {
                // handle error
            }

            this.hideDeleteNotification();

        });
    }

    toggleForm() {
        this.isFormVisible = this.isFormVisible ? false : true;
    }
}

