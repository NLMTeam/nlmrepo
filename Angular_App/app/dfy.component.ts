/**
 * Created by Paul on 05.02.2017.
 */

/**
 * Created by Paul on 05.02.2017.
 */

/**
 * Created by Paul on 31.01.2017.
 */

import { Component, OnInit } from "@angular/core";

// services
import {DFYService} from "./services/dfy.service";

// models
import {DFY} from "./models/dfy";

// lib
import { StatusCode } from "./library/StatusCodes";

// routing
import { ActivatedRoute } from '@angular/router';
import {Subscription} from "rxjs/Subscription";

@Component({
    selector: "dfy",
    templateUrl: "app/views/dfy.html",
    styleUrls: [ "app/styles/dfy.css", "app/styles/modal.css", "app/styles/form.css",
        "app/styles/adminForm.css", "app/styles/dfyPlusDiscipline.css", "app/styles/adminForm.css" ],
    providers: [DFYService]
})
export class DFYComponent implements OnInit {
    
    private newDFYYear: string = "2010-2011";
    private newDFYSemester: number = 1;

    private deletingdfy: DFY = null;
    private deleteNotificationShow: boolean = false;

    private disciplineId: number;
    private disciplineName: string;

    private dfyCollection: DFY[] = [];

    private isFormVisible: boolean = false;
    private serverError: string = "";

    private routeSubscription: Subscription;
    private querySubscription: Subscription;

    public constructor(private route: ActivatedRoute, private dfyService: DFYService) {
        this.routeSubscription = route.params.subscribe(params=>this.disciplineId=params['id']);
        this.querySubscription = route.queryParams.subscribe(
            (queryParam: any) => {
                this.disciplineName = queryParam['name'];
            }
        );
    }

    ngOnInit() {
        this.dfyService.GetDFYs(this.disciplineId).then(data => {
            console.log(data);
            this.dfyCollection = data;
        });
    }

    addDFY() {
        let newDFY = new DFY(-1, this.disciplineId, this.newDFYSemester, this.newDFYYear);

        this.dfyService.CreateDFY(newDFY).then(resp => {

            if (resp.status == StatusCode.Created) {
                let dfy = resp.json();
                newDFY.DFYId = dfy.DisciplineForYearId;

                this.dfyCollection.push(newDFY);
                this.serverError = "";
                console.log(dfy);
            }
        }, error => {
            if (error.status == StatusCode.BadRequest) {
                console.log("Bad request");
                this.serverError = "Не указан год или семестр...";
            }

            if (error.status == StatusCode.Conflict) {
                console.log("Conflict");
                this.serverError = "Привязка по этой дисциплине с указанными параметрами уже присутствует в БД";
            }
        });
    }

    showDeleteNotification(dfy: DFY) {
        this.deletingdfy = dfy;
        this.deleteNotificationShow = true;
        let elem: HTMLBodyElement = <HTMLBodyElement>document.getElementsByTagName("body")[0];
        elem.setAttribute('style', 'overflow: hidden');
        console.log(dfy);
    }

    hideDeleteNotification() {
        this.deleteNotificationShow = false;
        this.deletingdfy = null;
        let elem: HTMLBodyElement = <HTMLBodyElement>document.getElementsByTagName("body")[0];
        elem.setAttribute('style', 'overflow: auto');
    }

    submitDelete() {

        if (this.deletingdfy == null) {
            return;
        }

        this.dfyService.DeleteDFY(this.deletingdfy).subscribe(resp => {

            if (resp.status == StatusCode.NoContent) {
                this.dfyCollection = this.dfyCollection.filter(d => d.DFYId != this.deletingdfy.DFYId);
            }

            if (resp.status == StatusCode.BadRequest) {
                // handle error
            }

            this.hideDeleteNotification();

        });
    }

    toggleForm() {
        this.isFormVisible = this.isFormVisible ? false : true;
    }
}

