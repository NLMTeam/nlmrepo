import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";

// services
import { GroupService } from "./services/group.service";

// models 
import { Group } from "./models/group";

// lib
import { StatusCode } from "./library/StatusCodes";
import { Session } from "./library/Session";

@Component({
    selector: "groups",
    templateUrl: "app/views/groups.html",
    styleUrls: [ "app/styles/group.css", 'app/styles/adminForm.css', 'app/styles/form.css', "app/styles/modal.css" ]
})
export class GroupComponent implements OnInit { 

    private groups: Group[] = [];

    private editedGroupName: string;
    private editedGroup: Group = null;

    private deletingGroup: Group = null;
    private deleteNotificationShow: boolean = false; 

    private isFormVisible: boolean = false;

    private role: string = null;

    public constructor(private groupService: GroupService) { }

    ngOnInit() {
        this.role = Session.Role;

        this.groupService.GetGroups().then(groups => {
            this.groups = groups;
        });
    }   

    saveEdit(groupForm : NgForm) {

        this.editedGroupName = this.editedGroupName.trim();

        if (this.editedGroupName.length == 0) {
            return;
        }

        if (this.editedGroup == null) {
            let group = new Group(this.editedGroupName, 0, null);

            this.groupService.CreateGroup(group).subscribe(resp => {

                if (resp.status == StatusCode.Created) {
                    this.groups.push(group);
                    this.editedGroupName = null;
                    groupForm.resetForm();
                }

                if (resp.status == StatusCode.BadRequest) {
                    console.log(resp);
                }
            });
        }
    }

    showDeleteNotification(group: Group) {
        this.deletingGroup = group;
        this.deleteNotificationShow = true;
    }

    hideDeleteNotification() {
        this.deleteNotificationShow = false;
        this.deletingGroup = null;
    }

    submitDelete() {
        
        if (this.deletingGroup == null) {
            return;
        }

        this.groupService.DeleteGroup(this.deletingGroup).subscribe(resp => {
            
            if (resp.status == StatusCode.NoContent) {
                this.groups = this.groups.filter(g => g.GroupId != this.deletingGroup.GroupId);
            }

            if (resp.status == StatusCode.BadRequest) {
                // handle error
            }

            this.hideDeleteNotification();

        });
    }

    toggleForm() {
        this.isFormVisible = this.isFormVisible ? false : true;
    }
}