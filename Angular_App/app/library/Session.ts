export class Session {
    private static  _token: string = null;
    private static  _role: string = null;

    private static _tokenCallbacks: ((newToken:string) => void)[] = [];
    private static _roleCallbacks: ((newRole:string) => void)[] = [];

    public static get Token() {
        if (Session._token == null) {
            Session._token = localStorage.getItem('NLM_Token');
        }

        return Session._token;
    }
    public static set Token(value: string) {
        Session._token = value;

        Session._tokenCallbacks.forEach(func => {
            func.apply(null, [value]);
        });

        if (value != null) {
            localStorage.setItem('NLM_Token', value);
        }
        else {
            localStorage.removeItem('NLM_Token');
        }
    }

    public static get Role() {
        if (Session._role == null) {
            Session._role = localStorage.getItem('NLM_Role');
        }

        return Session._role;
    }
    public static set Role(value:string) {
        Session._role = value;

        Session._roleCallbacks.forEach(func => {
            func.apply(null, [value]);
        });

        localStorage.setItem('NLM_Role', value);
    }

    public static  RegisterTokenCallback(func : (newToken:string) => void) {
        Session._tokenCallbacks.push(func);
    }

    public static  RegisterRoleCallback(func : (newRole: string) => void) {
        Session._roleCallbacks.push(func);
    }

}