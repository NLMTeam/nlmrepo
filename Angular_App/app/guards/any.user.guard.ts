import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import {Observable} from "rxjs/Rx";

// lib
import { Session } from "./../library/Session";
import {Injectable} from "@angular/core";

@Injectable()
export class AnyUserGuard implements  CanActivate {

    constructor(private router : Router) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Observable<boolean> | boolean {

        if (Session.Token == null) {
            this.router.navigate(['login']);
        }

        return true;
    }
}