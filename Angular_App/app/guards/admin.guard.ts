import { Injectable } from "@angular/core";
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import {Observable} from "rxjs/Rx";

// lib
import { Session } from "./../library/Session";

@Injectable()
export class AdminGuard implements  CanActivate {

    constructor(private router : Router) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Observable<boolean> | boolean{

        if (Session.Role != "ADMIN") {
            // should be something better based on actual route and actual role
            this.router.navigate(['login']);
        }

        return true;
    }
}
