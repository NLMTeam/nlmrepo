/**
 * Created by Paul on 31.01.2017.
 */

import { Component, OnInit } from "@angular/core";

// services
import {DFYPlusDisciplineService} from "./services/dFYPlusDiscipline.service";
import {ServerConfigurationService} from "./services/serverConfiguration.service"

// models 
import {DFYPlusDiscipline} from "./models/DFYPlusDiscipline";

// lib
import { StatusCode } from "./library/StatusCodes";
import {DFY} from "./models/DFY";

@Component({
    selector: "dfyPlusDiscipline",
    templateUrl: "app/views/dfyPlusDiscipline.html",
    styleUrls: [ "app/styles/dfyPlusDiscipline.css" ],
    providers: [DFYPlusDisciplineService, ServerConfigurationService]
})
export class DFYPlusDisciplineComponent implements OnInit {

    private editedDFYPlusDisciplineName: string;
    private editedDFYPlusDiscipline: DFYPlusDiscipline = null;

    private deletingDFYPlusDiscipline: DFYPlusDiscipline = null;
    private deleteNotificationShow: boolean = false;

    private dfyCollection: { [key: string]: DFYPlusDiscipline[] } = {};
    private currentDFYCollection: DFYPlusDiscipline[] = [];
    private currentYear: string;
    private currentSemester: number;
    private isSecondSemester: boolean;

    public constructor(private DFYPlusDisciplineService: DFYPlusDisciplineService, private ServerConfiguration: ServerConfigurationService) { }

    ngOnInit() {
        this.ServerConfiguration.GetServerConfigurations().then(config => {
            this.currentYear =  config[0];
            this.currentSemester = config[1];

            if (config[1] == 2){
                this.isSecondSemester = true;
            }
            else{
                this.isSecondSemester = false;
            }

            this.DFYPlusDisciplineService.GetDFYPlusDisciplines(this.currentYear, this.currentSemester).then(data => {
                this.dfyCollection = {};
                this.dfyCollection[this.currentYear + this.currentSemester] = data;
                this.currentDFYCollection = this.dfyCollection[this.currentYear + this.currentSemester];
            });
        });
    }

    incrementYear(){
        let years: string[] = this.currentYear.split('-');
        let firstYear: number = years[0]*1+1;
        let secondYear: number = years[1]*1+1;
        this.currentYear = firstYear + "-" + secondYear;
        this.chooseDateInterval();
    }

    decrementYear(){
        let years: string[] = this.currentYear.split('-');
        let firstYear: number = years[0]*1-1;
        let secondYear: number = years[1]*1-1;
        this.currentYear = firstYear + "-" + secondYear;
        this.chooseDateInterval();
    }

    changeSemester(){
        if (this.isSecondSemester){
            this.currentSemester = 1;
        }
        else{
            this.currentSemester = 2;
        }

        this.isSecondSemester = !this.isSecondSemester;
        this.chooseDateInterval();
    }

    chooseDateInterval(){
        if (this.dfyCollection[this.currentYear + this.currentSemester] == null) {
            this.DFYPlusDisciplineService.GetDFYPlusDisciplines(this.currentYear, this.currentSemester).then(data => {
                this.dfyCollection[this.currentYear + this.currentSemester] = data;
                this.currentDFYCollection = this.dfyCollection[this.currentYear + this.currentSemester];
            });
        }
        else {
            this.currentDFYCollection = this.dfyCollection[this.currentYear + this.currentSemester];
        }
    }



    // saveEdit() {
    //
    //     this.editedDFYPlusDisciplineName = this.editedDFYPlusDisciplineName.trim();
    //
    //     if (this.editedDFYPlusDisciplineName.length == 0) {
    //         return;
    //     }
    //
    //     if (this.editedDFYPlusDiscipline == null) {
    //         let DFYPlusDiscipline = new DFYPlusDiscipline(this.editedDFYPlusDisciplineName, 0, null);
    //
    //         this.DFYPlusDisciplineService.CreateDFYPlusDiscipline(DFYPlusDiscipline).subscribe(resp => {
    //
    //             console.log(resp);
    //
    //             if (resp.status == StatusCode.Created) {
    //                 this.DFYPlusDisciplines.push(DFYPlusDiscipline);
    //             }
    //
    //             if (resp.status == StatusCode.BadRequest) {
    //                 // handle error
    //             }
    //         });
    //     }
    // }
    //
    // showDeleteNotification(DFYPlusDiscipline: DFYPlusDiscipline) {
    //     this.deletingDFYPlusDiscipline = DFYPlusDiscipline;
    //     this.deleteNotificationShow = true;
    // }
    //
    // hideDeleteNotification() {
    //     this.deleteNotificationShow = false;
    //     this.deletingDFYPlusDiscipline = null;
    // }
    //
    // submitDelete() {
    //
    //     if (this.deletingDFYPlusDiscipline == null) {
    //         return;
    //     }
    //
    //     this.DFYPlusDisciplineService.DeleteDFYPlusDiscipline(this.deletingDFYPlusDiscipline).subscribe(resp => {
    //
    //         if (resp.status == StatusCode.NoContent) {
    //             this.DFYPlusDisciplines = this.DFYPlusDisciplines.filter(g => g.DFYPlusDisciplineId != this.deletingDFYPlusDiscipline.DFYPlusDisciplineId);
    //         }
    //
    //         if (resp.status == StatusCode.BadRequest) {
    //             // handle error
    //         }
    //
    //         this.hideDeleteNotification();
    //
    //     });
    // }
}
