import { Component } from '@angular/core';
import { Router } from "@angular/router";

import { Session } from "./library/Session";

@Component({
  selector: 'nlm-app',
  templateUrl: "app/views/layout.html",
  styleUrls: [ "app/styles/bootstrap.min.css", "app/styles/font-awesome.min.css", "app/styles/layout.css" ]
})
export class AppComponent  {
    private isUserAuthenticated: boolean;

    private STORAGE_KEY = 'NLM_Token';

    constructor (private router : Router) {
        console.log('app component constructor');

        Session.RegisterTokenCallback(token => {
            console.log('token callback');
            this.isUserAuthenticated = token != null;
        });
        this.isUserAuthenticated = Session.Token != null;
    }

    logOut() {
        console.log('Logging out');
        Session.Token = null;
        this.router.navigate(['login']);
    }
}
