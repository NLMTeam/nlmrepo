import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule }    from '@angular/http';
import { FormsModule }   from '@angular/forms';

import { Routes, RouterModule } from '@angular/router';

// components
import { AppComponent }  from './app.component';
import { DFYPlusDisciplineComponent } from "./dfyPlusDiscipline.component";
import { GroupComponent } from "./group.component";
import { StudentComponent } from "./student.component";
import { LoginComponent } from "./login.component";
import { DisciplineComponent } from "./discipline.component";
import { DFYComponent } from "./dfy.component";

// services
import { DFYPlusDisciplineService} from "./services/dFYPlusDiscipline.service";
import { ServerConfigurationService} from "./services/serverConfiguration.service";
import { GroupService } from "./services/group.service";
import { StudentService } from "./services/student.service";
import { AccountService } from "./services/account.service";

// guards
import { AdminGuard } from "./guards/admin.guard";
import { AnyUserGuard } from "./guards/any.user.guard";
import { LecturerGuard } from "./guards/lecturer.guard";
import { StudentGuard } from "./guards/student.guard";

const appRoutes: Routes = [
    { path: '', component: LoginComponent },
    { path: 'disciplines', component: DFYPlusDisciplineComponent },
    { path: 'disciplinesA', component: DisciplineComponent },
    { path: 'dfyA/:id', component: DFYComponent},
    { path: 'group', component: GroupComponent, pathMatch : 'full', canActivate: [ AnyUserGuard ] },
    { path: 'group/:id', component: StudentComponent, canActivate: [ AnyUserGuard ] },
    { path: 'login', component: LoginComponent }
];

@NgModule({
  imports:      [ BrowserModule, RouterModule.forRoot(appRoutes), HttpModule, FormsModule ],
  declarations: [ AppComponent, DFYPlusDisciplineComponent, DFYComponent, DisciplineComponent, GroupComponent, StudentComponent, LoginComponent ],
  bootstrap:    [ AppComponent ],
  providers:    [ DFYPlusDisciplineService, ServerConfigurationService, GroupService, StudentService,
      AccountService, AdminGuard, LecturerGuard, StudentGuard, AnyUserGuard ]
})
export class AppModule { }
