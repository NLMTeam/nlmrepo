import { Component, OnInit, OnDestroy } from "@angular/core";
import { NgForm } from "@angular/forms";

// routing
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

// services
import { StudentService } from "./services/student.service";

// models
import { StudentViewModel } from "./models/StudentViewModel";

// lib
import { StatusCode } from "./library/StatusCodes";
import { Session } from "./library/Session";

@Component({
    selector: "student",
    templateUrl: "app/views/students.html",
    styleUrls: [ "app/styles/students.css", "app/styles/adminForm.css", 'app/styles/modal.css', 'app/styles/form.css' ]
})
export class StudentComponent implements OnInit, OnDestroy {
    
    private groupId: string;
    private subscription: Subscription; 
    private students: StudentViewModel[] = [];

    private editingStudent: StudentViewModel = new StudentViewModel();
    private isEditingExistingStudent: boolean = false;

    private deletingStudent: StudentViewModel = null;
    private deleteNotificationShow: boolean = false; 

    private isFormVisible: boolean = false;

    private role: string = null;

    public constructor(activatedRoute: ActivatedRoute, private studentService: StudentService) {
        this.subscription = activatedRoute.params.subscribe(params => this.groupId = params['id']);
    }

    ngOnInit() {
        this.role = Session.Role;
        this.studentService.GetGroupStudents(this.groupId).then(res => {
            this.students = res;
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    saveEdit(studentForm : NgForm) {

        if (!this.isEditingExistingStudent) {

            this.studentService.CreateStudent(this.editingStudent, this.groupId).subscribe(res => {
                if (res.status == StatusCode.Created) {
                    let student = res.json();
                    this.editingStudent.UserId = student.UserId;

                    this.students.push(this.editingStudent);
                    this.editingStudent = new StudentViewModel();
                    studentForm.resetForm();
                } 
                else if (res.status == StatusCode.BadRequest) {
                    // handle server validation error
                }
            });

        }
    }

    submitDelete() {
        this.studentService.DeleteStudent(this.deletingStudent.UserId).subscribe(res => {
        
            if (res.status == StatusCode.NoContent) {
                this.students = this.students.filter(s => s.UserId != this.deletingStudent.UserId);
            }
            if (res.status == StatusCode.BadRequest) {
                // handle server validation error
            }

            this.hideDeleteNotification();        
        });
    }

    showDeleteNotification(student: StudentViewModel) {
        this.deletingStudent = student;
        this.deleteNotificationShow = true;
    }

    hideDeleteNotification() {
        this.deleteNotificationShow = false;
        this.deletingStudent = null;
    }

    toggleForm() {
        this.isFormVisible = this.isFormVisible ? false : true;
    }

}