import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { Response } from "@angular/http";

//services
import { AccountService } from "./services/account.service";

//library
import { StatusCode } from "./library/StatusCodes";
import { Session } from "./library/Session";

@Component({
    selector: 'login',
    templateUrl: 'app/views/login.html',
    styleUrls: [ 'app/styles/form.css', 'app/styles/authorizationForm.css', 'app/styles/authorizationTitle.css' ]
})
export class LoginComponent {
    private login: string;
    private password: string;

    private enterError: boolean = false;
    private enterSuccess: boolean = false;

    private readonly STORAGE_KEY = 'NLM_Token';

    constructor(private accountService : AccountService, private router: Router) { }

    public SubmitForm() {
        this.accountService.LoginUser(this.login, this.password).subscribe(res => {

            if (res.status == StatusCode.OK) {
                this.enterSuccess = true;

                let token: string = res.json();
                Session.Token = token;

                this.accountService.GetUserRoleByToken(token).subscribe(res => {
                    let role: string = res.json();
                    Session.Role = role;
                    this.router.navigate(['group']);
                });
            }

        }, error => {
            console.log(error);
            if (error instanceof Response) {
                let errResp = <Response>error;
                
                if (errResp.status == StatusCode.BadRequest) {
                    this.enterError = true;
                }
            }
        });
    }
}