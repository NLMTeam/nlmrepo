/**
 * Created by Paul on 31.01.2017.
 */

/**
 * Created by Paul on 31.01.2017.
 */

/**
 * Created by Paul on 31.01.2017.
 */

import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

import "rxjs/add/operator/toPromise";

import { DFYPlusDiscipline } from "./../models/DFYPlusDiscipline";

@Injectable()
export class DFYPlusDisciplineService {

    private coreUrl: string = "http://localhost:52115/";

    public constructor (private http: Http) { }

    private convertToDFYPlusDisciplineArray(res: any) : DFYPlusDiscipline[] {
        return res.map((g: any) => this.convertToDFYPlusDiscipline(g));
    }

    private convertToDFYPlusDiscipline(obj: any) : DFYPlusDiscipline {
        return new DFYPlusDiscipline(obj.DisciplineForYearId, obj.DisciplineId,
            obj.DisciplineName, obj.CanActivate, obj.Lecturers, obj.Assistants);
    }

    public GetDFYPlusDisciplines(year: string, semester: number) : Promise<DFYPlusDiscipline[]> {
        return this.http.get(this.coreUrl + "api/YearSemesterDisciplines?year=" + year + "&semester=" + semester).toPromise().then(res => {
            return this.convertToDFYPlusDisciplineArray(res.json());
        }).catch((ex) => {
            console.error('Error fetching disciplines', ex);
        });;
    }

    public CreateDFYPlusDiscipline(DFYPlusDiscipline: DFYPlusDiscipline) {
        return this.http.post(this.coreUrl + "api/YearSemesterDisciplines", DFYPlusDiscipline);
    }

    public DeleteDFYPlusDiscipline(DFYPlusDiscipline: DFYPlusDiscipline) {
        return this.http.delete(this.coreUrl + "api/YearSemesterDisciplines/" + DFYPlusDiscipline.DFYId);
    }
}