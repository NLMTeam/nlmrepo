/**
 * Created by Paul on 01.02.2017.
 */

/**
 * Created by Paul on 31.01.2017.
 */

/**
 * Created by Paul on 31.01.2017.
 */

/**
 * Created by Paul on 31.01.2017.
 */

import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

import "rxjs/add/operator/toPromise";

@Injectable()
export class ServerConfigurationService {

    private coreUrl: string = "http://localhost:52115/";

    public constructor (private http: Http) { }

    private convertToServerConfiguration(obj: any) : [string, number] {
        console.log(obj.m_Item1);
        return [obj.m_Item1, obj.m_Item2];
    }

    public GetServerConfigurations() : Promise<[string, number]> {
        return this.http.get(this.coreUrl + "api/ServerConfiguration").toPromise().then(res => {
            return this.convertToServerConfiguration(res.json());
        });
    }

    // public CreateServerConfiguration(ServerConfiguration: ServerConfiguration) {
    //     return this.http.post(this.coreUrl + "api/YearSemesterDisciplines", ServerConfiguration);
    // }
    //
    // public DeleteServerConfiguration(ServerConfiguration: ServerConfiguration) {
    //     return this.http.delete(this.coreUrl + "api/YearSemesterDisciplines/" + ServerConfiguration.DFYId);
    // }
}