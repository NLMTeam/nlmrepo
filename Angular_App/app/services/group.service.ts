import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

import "rxjs/add/operator/toPromise";

import { Group } from "./../models/Group";

@Injectable()
export class GroupService {

    private coreUrl: string = "http://localhost:52115/";

    public constructor (private http: Http) { }

    private convertToGroupArray(res: any) : Group[] {
        return res.map( ( g : any ) => this.convertToGroup(g));
    }

    private convertToGroup(obj: any) : Group {
        return new Group(obj.GroupId, obj.CaptainId, obj.CaptainName);
    }

    public GetGroups() {
        return this.http.get(this.coreUrl + "api/groups").toPromise().then(res => {
            return this.convertToGroupArray(res.json());
        });
    }

    public CreateGroup(group: Group) {
        return this.http.post(this.coreUrl + "api/groups", group);
    }

    public DeleteGroup(group: Group) {
        return this.http.delete(this.coreUrl + "api/groups?groupId=" + group.GroupId);
    }
}