/**
 * Created by Paul on 31.01.2017.
 */

import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

import "rxjs/add/operator/toPromise";

import { Discipline } from "./../models/Discipline";

@Injectable()
export class DisciplineService {

    private coreUrl: string = "http://localhost:52115/";

    public constructor (private http: Http) { }

    private convertToDisciplineArray(res: any) : Discipline[] {
        return res.map(g => this.convertToDiscipline(g));
    }

    private convertToDiscipline(obj: any) : Discipline {
        return new Discipline(obj.Name, obj.IsCourseProject, obj.DisciplineId);
    }

    public GetDisciplines() {
        return this.http.get(this.coreUrl + "api/Disciplines").toPromise().then(res => {
            return this.convertToDisciplineArray(res.json());
        }).catch((ex) => {
            console.error('Error fetching disciplines', ex);
        });
    }

    public CreateDiscipline(Discipline: Discipline) {
        return this.http.post(this.coreUrl + "api/Disciplines", Discipline).toPromise();
    }

    public DeleteDiscipline(Discipline: Discipline) {
        return this.http.delete(this.coreUrl + "api/Disciplines/" + Discipline.DisciplineId);
    }
}