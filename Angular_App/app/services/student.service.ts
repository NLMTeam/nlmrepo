import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

import { StudentViewModel } from "./../models/StudentViewModel";

@Injectable()
export class StudentService {

    private coreUrl: string = "http://localhost:52115/";

    public constructor(private http: Http) { }

    private convertToStudentModelArray(res: any) {
        return res.map( (s : any)  => new StudentViewModel(s.UserId, s.Mail, s.FIO, s.Login));
    }

    public GetGroupStudents(groupName : string) {
        return this.http.get(this.coreUrl + "api/groups/studentsOfGroup?name="+groupName).toPromise().then(res => {
            console.log(res.json());
            return this.convertToStudentModelArray(res.json());
        });
    }

    public CreateStudent(student: StudentViewModel, groupName: string) {
        return this.http.post(this.coreUrl + "api/students?groupName=" + groupName, { 
            UserId: student.UserId,
            Login: student.Login,
            Mail: student.Email,
            FIO: student.FIO
        });
    }

    public DeleteStudent(studentId: number) {
        return this.http.delete(this.coreUrl + "api/students?studentId=" + studentId);
    }

}