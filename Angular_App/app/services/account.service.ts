import { Injectable } from "@angular/core";
import { Http } from '@angular/http';

@Injectable()
export class AccountService {

    private coreUrl: string = 'http://localhost:52115/';
    
    constructor(private http: Http) { }

    public LoginUser(login: string, password: string) {
        return this.http.post(this.coreUrl + 'api/user/login', { Login: login, Password: password });
    }

    public GetUserRoleByToken(token : string) {
        return this.http.get(this.coreUrl+ "api/user/getrole?tokenView=" + token);
    }
    
}