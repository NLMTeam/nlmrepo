import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

import "rxjs/add/operator/toPromise";

import { DFY } from "./../models/DFY";

@Injectable()
export class DFYService {

    private coreUrl: string = "http://localhost:52115/";

    public constructor (private http: Http) { }

    private convertToDFYArray(res: any) : DFY[] {
        return res.map(g => this.convertToDFY(g));
    }

    private convertToDFY(obj: any) : DFY {
        return new DFY(obj.DisciplineForYearId, obj.DisciplineId, obj.Semester, obj.Year,
                                            obj.Lecturers, obj.Assistants);
    }

// <<<<<<< HEAD
//     public GetDFYs() {
//         return this.http.get(this.coreUrl + "api/DisciplinesForYear").toPromise().then(res => {
// =======
    public GetDFYs(disciplineId: number) {
        return this.http.get(this.coreUrl + "api/DisciplinesForYear/" + disciplineId).toPromise().then(res => {
            return this.convertToDFYArray(res.json());
        }).catch((ex) => {
            console.error('Error fetching dfys', ex);
        });
    }

    public CreateDFY(DFY: DFY) {
        return this.http.post(this.coreUrl + "api/DisciplinesForYear", DFY).toPromise();
    }

    public DeleteDFY(DFY: DFY) {
        return this.http.delete(this.coreUrl + "api/DisciplinesForYear/" + DFY.DFYId);
    }
}