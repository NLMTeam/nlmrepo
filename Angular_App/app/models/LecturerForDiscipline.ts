/**
 * Created by Paul on 31.01.2017.
 */

export class LecturerForDiscipline {
    private id:number;
    private name:string;

    public constructor(id: number, name: string) {
        this.name = name;
        this.id = id;
    }

    public get Id() { return this.id; }
    public set Id(value: number) { this.id = value; }

    public get Name() { return this.name; }
    public set Name(value: string) { this.name = value; }
}

