export class StudentViewModel {
    
    private userId: number;
    private email: string;
    private fio: string;
    private login: string;

    public constructor(userId:number = 0, email: string = null, fio: string = null, login: string = null) { 
        this.userId = userId;
        this.email = email;
        this.fio = fio;
        this.login = login;
    }

    public get UserId() { return this.userId; }
    public set UserId(value: number) { this.userId = value; }

    public get Email() { return this.email; }
    public set Email(value: string) { this.email = value; }

    public get FIO() { return this.fio; }
    public set FIO(value: string) { this.fio = value; }

    public get Login() { return this.login; }
    public set Login(value: string) { this.login = value; }
     
}