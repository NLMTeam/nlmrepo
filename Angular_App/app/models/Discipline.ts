/**
 * Created by Paul on 31.01.2017.
 */

export class Discipline {
    private disciplineId: number;
    private  name: string;
    private isCourseProject: boolean;

    public constructor(name: string, isCourseProject: boolean = false, disciplineId: number = -1) {
        this.name = name;
        this.isCourseProject = isCourseProject;

        this.disciplineId = disciplineId;
    }

    public get DisciplineId() { return this.disciplineId; }
    public set DisciplineId(value: number) { this.disciplineId = value; }

    public get Name() { return this.name; }
    public set Name(value: string) { this.name = value; }

    public get IsCourseProject() { return this.isCourseProject; }
    public set IsCourseProject(value:boolean) { this.isCourseProject = value; }
}
