export class Group {
    
    private groupId: string;
    private captainId: number; 
    private captainName: string;

    public constructor(groupId:string = null, captainId: number = -1, captainName: string = null) {
        this.captainId = captainId;
        this.captainName = captainName;

        this.groupId = groupId;
    }

    public get GroupId() { return this.groupId; }
    public set GroupId(value: string) { this.groupId = value; }

    public get CaptainId() { return this.captainId; }
    public set CaptainId(value: number) { this.captainId = value; }

    public get CaptainName() { return this.captainName; }
    public set CaptainName(value:string) { this.captainName = value; }
}
