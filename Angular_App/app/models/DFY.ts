import {LecturerForDiscipline} from "./LecturerForDiscipline";
/**
 * Created by Paul on 31.01.2017.
 */

export class DFY {
    private disciplineForYearId: number;
    private  disciplineId: number;
    private lecturers: LecturerForDiscipline[];
    private assistants: LecturerForDiscipline[];
    private  semester: number;
    private year: string;

    public constructor(dFYId: number, disciplineId: number, semester: number, year: string, lecturers: LecturerForDiscipline[] = [],
        assistants: LecturerForDiscipline[] = []) {

        this.disciplineForYearId = dFYId;
        this.disciplineId = disciplineId;
        this.year = year;
        this.semester = semester;
        this.lecturers = lecturers;
        this.assistants = assistants;
    }
    
    public get DFYId() { return this.disciplineForYearId; }
    public set DFYId(value: number) { this.disciplineForYearId = value; }

    // public get Name() { return this.name; }
    // public set Name(value: string) { this.name = value; }

    public get Lecturers() { return this.lecturers; }
    public set Lecturers(value: LecturerForDiscipline[]) { this.lecturers = value; }

    public get Assistants() { return this.assistants; }
    public set Assistants(value: LecturerForDiscipline[]) { this.assistants = value; }

    public get Year() { return this.year; }
    public set Year(value: string) { this.year = value; }

    public get Semester() { return this.semester; }
    public set Semester(value: number) { this.semester = value; }
}
