import {LecturerForDiscipline} from "./LecturerForDiscipline";
/**
 * Created by Paul on 31.01.2017.
 */

export class DFYPlusDiscipline {
    private disciplineForYearId: number;
    private  disciplineId: number;
    private lecturers: LecturerForDiscipline[];
    private assistants: LecturerForDiscipline[];
    private disciplineName: string;
    private canActivate: boolean;

    public constructor(dFYId: number, disciplineId: number, disciplineName: string,
                       canActivate: boolean=false, lecturers: LecturerForDiscipline[] = [], assistants: LecturerForDiscipline[] = []) {

        this.disciplineForYearId = dFYId;
        this.disciplineId = disciplineId;
        this.canActivate = canActivate;
        this.lecturers = lecturers;
        this.assistants = assistants;
        this.disciplineName = disciplineName;
    }

    public get DFYId() { return this.DFYId; }
    public set DFYId(value: number) { this.DFYId = value; }

    public get DisciplineName() { return this.disciplineName; }
    public set DisciplineName(value: string) { this.disciplineName = value; }

    public get Lecturers() { return this.lecturers; }
    public set Lecturers(value: LecturerForDiscipline[]) { this.lecturers = value; }

    public get Assistants() { return this.assistants; }
    public set Assistants(value: LecturerForDiscipline[]) { this.assistants = value; }

    public get CanActivate() { return this.canActivate; }
    public set CanActivate(value: boolean) { this.canActivate = value; }
}
/**
 * Created by Paul on 31.01.2017.
 */
